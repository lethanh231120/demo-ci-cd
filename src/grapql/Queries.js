import { gql } from '@apollo/client'

export const LOAD_TOKENS = gql`
  query
    {
      tokens(first:1000, orderBy:txCount, orderDirection:desc) 
      {
        id
        symbol
        name
        decimals
      }
    }    
`
