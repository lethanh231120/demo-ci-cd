import React, { useEffect, useState, useContext } from 'react'
import { Menu, Modal, Typography, Popover } from 'antd'
import { NavLink, useNavigate } from 'react-router-dom'
import { Layout } from 'antd'
import { ExportOutlined } from '@ant-design/icons'
import 'antd/dist/antd.min.css'
import Signup from '../../components/auth/register/index'
import UpdatePassword from '../../components/auth/update-password'
import { useSelector, useDispatch } from 'react-redux'
import { removeCookie, getCookie, STORAGEKEY } from '../../utils/storage'
import { resetUserInfo } from '../../redux/useInfo'
import { setCookie } from '../../utils/storage'
import _ from 'lodash'
import jwt_decode from 'jwt-decode'
import { AccountMetamask } from '../index'
import ModalSignIn from '../../components/modal/signin/ModalSignIn'

const { Header } = Layout

const items = [
  {
    label: (<NavLink className='header__link' to='/'>Home</NavLink>),
    key: '/'
  },
  // {
  //   label: (<NavLink className='header__link' to='portfolio'>Portfolio Tracker</NavLink>),
  //   key: '/portfolio'
  // },
  {
    label: (<NavLink className='header__link' to='portfolio-manager'>Portfolio Manager</NavLink>),
    key: '/portfolio-manager'
  },
  {
    label: (<NavLink className='header__link' to='swap'>Swap</NavLink>),
    key: '/swap'
  },
  {
    label: (<NavLink className='header__link' to='cryptocurrencies'>Cryptocurrencies</NavLink>),
    key: '/cryptocurrencies'
  },
  // {
  //   label: (<NavLink className='header__link' to='portfolio-manager/stock'>Stock</NavLink>),
  //   key: '/stock'
  // },
  {
    label: (
      <NavLink className='header__link' to='price'>Pricing</NavLink>
    ),
    key: '/pricing'
  },
  {
    label: (
      <NavLink className='header__link' to='earn'>Earn</NavLink>
    ),
    key: '/earn'
  }
]

const Navbar = () => {
  const [isModalSignin, setIsModalSignin] = useState(false)
  const [isModalSignup, setIsModalSignup] = useState(false)
  const [isModalPasswordUpdate, setIsModalPasswordUpdate] = useState(false)
  const [addressSigner, setAddressSigner] = useState()
  const { user } = useSelector(state => state.userInfo)
  const dispatch = useDispatch()
  const navigate = useNavigate()
  const token = getCookie(STORAGEKEY.ACCESS_TOKEN)
  const userInfo = getCookie(STORAGEKEY.USER_INFO)

  const accounts = useContext(AccountMetamask)

  useEffect(() => {
    setAddressSigner(accounts?.accounts && accounts?.accounts[0])
  }, [accounts])

  // logout
  // remove token cookie
  const logout = async() => {
    await removeCookie(STORAGEKEY.ACCESS_TOKEN)
    await removeCookie(STORAGEKEY.USER_INFO)
    dispatch(resetUserInfo())
    navigate('/')
  }

  // check token expired
  if (token) {
    const exp = jwt_decode(token).exp
    const expirationTime = (exp * 1000) - 60000
    if (Date.now() >= expirationTime) {
      removeCookie(STORAGEKEY.ACCESS_TOKEN)
      removeCookie(STORAGEKEY.USER_INFO)
    }
  }

  // set cookie user info
  useEffect(() => {
    const setUserInfo = async() => {
      await setCookie(STORAGEKEY.USER_INFO, user)
    }
    !_.isEmpty(user) && setUserInfo()
  }, [user])

  const handleCancel = () => {
    setIsModalSignin(false)
  }

  const handleDisConnectWallet = () => {
    accounts.handleSetAccounts({})
    // window.location.reload()
  }

  return (
    <>
      <Layout className='layout'>
        <Header className='layout-header'>
          <div className='layout-header-logo'>
            <div className='layout-header-logo-name'>NIKA.guru</div>
            <Menu
              mode='horizontal'
              items={items}
            />
          </div>
          <div style={{ display: 'flex', alignItems: 'center' }}>
            {addressSigner ? (
              <div className='layout-header-wallet'>
                <div style={{ background: '#404040', cursor: 'pointer', alignItems: 'center', padding: '5px 8px', borderRadius: '4px', height: '30%', lineHeight: '30px' }}>
                  <Popover
                    placement='bottomRight'
                    content={(<div>
                      <button onClick={handleDisConnectWallet}><ExportOutlined/> Disconnect Wallet</button>
                    </div>)}
                    trigger='click'
                  >
                    <img style={{ width: '30px', height: '30px', borderRadius: '25px', objectFit: 'cover', marginRight: '5px' }} src='/coins/addresswallet.png'/>
                    <span>{addressSigner.slice(0, 4)}...{addressSigner.slice(addressSigner.length - 5, addressSigner.length - 1)}</span>
                  </Popover>
                </div>
              </div>
            ) : '' }
            {token ? <div className='layout-header-user-info'>
              <Typography
                component='span'
                variant='subtitle1'
                fontWeight='bold'
              >
                <NavLink className='header__link' to='profile'>
                  {({ isActive }) => (
                    <div
                      className={isActive ? 'activeClassName' : ''}
                      style={{ color: '#fff' }}
                    >
                      {userInfo && userInfo.name}
                    </div>
                  )}
                </NavLink>
              </Typography>
              <Typography
                variant='subtitle1'
                className='header__link'
                onClick={() => setIsModalPasswordUpdate(true)}
              >
                Change Password
              </Typography>
              <Typography
                variant='subtitle1'
                onClick={logout}
                className='header__link'
              >
                Logout
              </Typography>
            </div>
              : <div className='layout-header-auth'>
                <Typography onClick={() => setIsModalSignin(true)}>
                  Signin
                </Typography>
                <Typography onClick={() => setIsModalSignup(true)}>
                  Signup
                </Typography>
              </div>}
          </div>
        </Header>
      </Layout>
      <ModalSignIn isModalSignin={isModalSignin} handleCancel={handleCancel}/>
      <Modal
        visible={isModalSignup}
        footer={null}
        onOk={() => setIsModalSignup(false)}
        onCancel={() => setIsModalSignup(false)}
        className='modal-signup'
      >
        <Signup setIsModalSignup={setIsModalSignup}/>
      </Modal>
      <Modal
        visible={isModalPasswordUpdate}
        onOk={() => setIsModalPasswordUpdate(false)}
        onCancel={() => setIsModalPasswordUpdate(false)}
        footer={null}
      >
        <UpdatePassword setIsModalPasswordUpdate={setIsModalPasswordUpdate}/>
      </Modal>
    </>
  )
}
export default Navbar
