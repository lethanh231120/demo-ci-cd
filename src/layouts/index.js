import React, { useEffect, createContext, useState } from 'react'
import Header from './header'
import { Route, Routes } from 'react-router-dom'
import HomePage from '../pages/home'
import PortfolioPage from '../pages/portfolio'
import PortfolioManagePage from '../pages/portfolio-manager'
import SwapPage from '../pages/swap'
import Cryptocurrencies from '../pages/cryptocurrencies'
import PricingPage from '../pages/price'
import ProfilePage from '../pages/profile'
import EditProfilePage from '../pages/profile/edit-profile'
import Earn from '../pages/earn'
import PageNotFound from '../pages/404'
import ConnectWalletPage from '../pages/connect-wallet'
import ConnectPortfolioPage from '../pages/connect-portfolio'
import ResetPassword from '../components/auth/reset-password'
import StockInfo from '../pages/stock/StockInfo'
import DetailNFT from '../components/nft/DetailNFT'
import DetailOnchainPage from '../pages/portfolio-manager/detail-onchain'
import DetailConnection from '../pages/portfolio-manager/detail-onchain/DetailConnection'
import DetailOffchainPage from '../pages/portfolio-manager/detail-offchain'
import { ConfirmEmail } from '../components/auth/confirm-email'
import { CoinDetail } from '../components/coins/CoinDetail'
import { Layout, Modal } from 'antd'
import { Stock } from '../pages/stock'
import DetailStock from '../pages/portfolio-manager/detail-stock/DetailStock'
import ListCoinIndirectAccount from '../pages/portfolio-manager/detail-offchain/indirect/ListCoinInDirect'
import './header/index.scss'
import DetailAccount from '../pages/portfolio-manager/detail-offchain/DetailAccount'
import ListCoinDirectAccount from '../pages/portfolio-manager/detail-offchain/direct/ListCoinDirect'
import ListCoinDirectConnection from '../pages/portfolio-manager/detail-onchain/direct-onchain/ListCoinDirectConnection'
import ListCoinIndirectConnection from '../pages/portfolio-manager/detail-onchain/indirect/ListCoinIndirectConnection'
// import ILKRegistryAbi from '../abi/ILKRegistryAbi.json'
// import ERC20_ABI from '../abi/ERC20.json'
// import { ethers } from 'ethers'
// import _ from 'lodash'

const { Content } = Layout
// const INFU_URL = process.env.REACT_APP_INFURA_URL_KOVAN
// const ILK_REGISTRY = process.env.REACT_APP_ILK_REGISTRY
// const ILK_REGISTRY_GOERLI = '0x525FaC4CEc48a4eF2FBb0A72355B6255f8D5f79e'

export const PlatFormContext = createContext()
export const AccountMetamask = createContext()
export const StockContext = createContext()
export const FavouriteAddress = createContext()
const Router = () => {
  const [propsImport, setPropsImport] = useState({
    isLoading: false,
    code: '',
    isSuccess: false,
    error: '',
    status: '',
    address: '',
    chainId: '',
    addressDelete: '',
    initial: true,
    listNft: [],
    listHolding: [],
    type: '',
    exchange: '',
    form: {}
  })
  // const infuprovider = new ethers.providers.JsonRpcProvider('https://goerli.infura.io/v3/f946e1a9058841b0a21b80df53cb249a')
  // const infuprovider = new ethers.providers.JsonRpcProvider(INFU_URL)
  const [accounts, setAccounts] = useState([])
  const [chainId, setChainId] = useState()
  const [stock, setStock] = useState({ isChange: false })
  // const [ilks, setIlks] = useState([])

  const contextStock = {
    stock: stock,
    handleChangeStock: ({ isChange, stocks }) => {
      setStock({
        ...stock,
        isChange: isChange,
        stocks: stocks
      })
    }
  }
  const error = () => {
    Modal.error({
      title: 'Oops, Something Went Wrong',
      content: (
        <div className='modal'>
          <p>MetaMask Extension Not Found</p>
        </div>
      ),
      onOk() {}
    })
  }

  const accountsMetamask = {
    accounts: accounts,
    chainId: chainId,
    // ilks: ilks,
    handleSetAccounts: (data) => {
      const { newAccounts } = data
      setAccounts(newAccounts && newAccounts)
    }
  }

  const state = {
    propsImport: propsImport,
    handleSetPropsImport: (data) => {
      const { loading, success, statusCode, messageError, statusImport, addressImport, chainId } = data
      setPropsImport({
        ...propsImport,
        isLoading: loading,
        code: statusCode && statusCode,
        isSuccess: success,
        error: messageError && messageError,
        status: statusImport,
        address: addressImport,
        chainId: chainId
      })
    },
    handleSetData: (data) => {
      const { loading, success, addressDelete, initial, listNft, listHolding, type, exchange, form } = data
      setPropsImport({
        ...propsImport,
        isLoading: loading,
        isSuccess: success,
        addressDelete: addressDelete,
        listHolding: listHolding?.flat(1),
        initial: initial,
        listNft: listNft?.flat(1),
        type: type,
        exchange: exchange,
        form: form
      })
    }
  }

  const getData = async() => {
    if (window.ethereum) {
      window.ethereum.request({ method: 'eth_accounts' }).then(res => setAccounts(res))
      window.ethereum.request({ method: 'eth_chainId' }).then(res => setChainId(res))
    } else {
      return error()
    }
  }

  // const getInfoILK = async(ilkRegistryToken, i, listIlk, infuprovider) => {
  //   const ilk = await ilkRegistryToken.get(i)
  //   const data = await ilkRegistryToken.info(ilk)
  //   const abiJoin = new ethers.Contract(data.gem, ERC20_ABI, infuprovider)
  //   const balance = await abiJoin.balanceOf(accounts[0])
  //   listIlk.push({
  //     'ilk': ilk,
  //     'info': data,
  //     'balance': Number.parseFloat(balance.toBigInt().toString()) * (1 / Math.pow(10, 18))
  //   })
  // }

  useEffect(() => {
    getData()
  }, [])

  useEffect(() => {
    // if (!_.isEmpty(accounts)) {
    //   // tao smart contarct cua ILK registry
    //   const ilkRegistryToken = new ethers.Contract(ILK_REGISTRY_GOERLI, ILKRegistryAbi, infuprovider)
    //   // lay so luong dong maker ho tro
    //   const listIlk = []
    //   ilkRegistryToken.count().then(res => {
    //     // loop de lay dia chi ilk cua item thu i
    //     for (let i = 0; i < Number.parseInt(res.toBigInt().toString()); i++) {
    //       // lay ilk cua 1 vi tri
    //       getInfoILK(ilkRegistryToken, i, listIlk, infuprovider)
    //     }
    //   })

    //   setTimeout(() => {
    //     setIlks(listIlk)
    //   }, 5000)
    // }
  }, [accounts])

  function handleAccountsChanged(accounts) {
    if (accounts.length === 0) {
      getData()
    } else if (accounts?.accounts && accounts[0] !== accounts?.accounts[0]) {
      accounts = accounts[0]
    }
  }

  window.ethereum ? window.ethereum.on('accountsChanged', handleAccountsChanged) : error()

  return (
    <AccountMetamask.Provider value={accountsMetamask}>
      <Header/>
      <Content style={{ margin: '0 6%' }}>
        <PlatFormContext.Provider value={state}>
          <StockContext.Provider value={contextStock}>
            <Routes>
              <Route path='/' element={<HomePage />} />
              <Route path='portfolio'>
                <Route path='' element={<PortfolioPage />} />
              </Route>
              <Route path='portfolio-manager'>
                <Route path='' element={<PortfolioManagePage />} />
                <Route path='onchain'>
                  <Route path='' element={<DetailOnchainPage />} />
                  <Route path=':connectionId'>
                    <Route path='' element={<DetailConnection />} />
                    <Route path='indirect' element={<ListCoinIndirectConnection />} />
                    <Route path='direct' element={<ListCoinDirectConnection />} />
                  </Route>
                </Route>
                <Route path='offchain'>
                  <Route path='' element={<DetailOffchainPage />} />
                  <Route path=':accountId'>
                    <Route path='' element={<DetailAccount />} />
                    <Route path='indirect' element={<ListCoinIndirectAccount />}/>
                    <Route path='direct' element={<ListCoinDirectAccount />}/>
                  </Route>
                </Route>
                <Route path='detail-stock' element={<DetailStock />} />
                <Route path='stock' element={<Stock />} />
              </Route>
              <Route path='nft/:nftId' element={<DetailNFT />} />
              <Route path='coins/:coinId' element={<CoinDetail />} />
              <Route path='swap' element={<SwapPage />} />
              <Route path='cryptocurrencies' element={<Cryptocurrencies />} />
              <Route path='price' element={<PricingPage />} />
              <Route path='profile' element={<ProfilePage />} />
              <Route path='edit-profile' element={<EditProfilePage />} />
              <Route path='forgot-password' element={<ResetPassword />} />
              <Route path='confirm-email' element={<ConfirmEmail />} />
              <Route path='stock-info' element={<StockInfo />}/>
              <Route path='connect/:platformId' element={<ConnectWalletPage />} />
              <Route path='connect-portfolio' element={<ConnectPortfolioPage />} />
              <Route path='earn' element={<Earn />} />
              <Route path='*' element={<PageNotFound />} />
            </Routes>
          </StockContext.Provider>
        </PlatFormContext.Provider>
      </Content>
    </AccountMetamask.Provider>
  )
}

export default Router
