import { createSlice, createAsyncThunk } from '@reduxjs/toolkit'
import { get } from '../api/accountService'

export const getProfile = createAsyncThunk(
  'profile/getProfile',
  async() => {
    return await get('accounts/profile/current-profile')
  }
)

const profileSlice = createSlice({
  name: 'profile',
  initialState: {
    profile: {},
    status: null
  },
  extraReducers: {
    // get profile
    [getProfile.pending]: (state, action) => {
      state.status = 'loading'
    },
    [getProfile.fulfilled]: (state, action) => {
      state.profile = action.payload
      state.status = 'success'
    },
    [getProfile.rejected]: (state, action) => {
      state.status = 'failed'
    }
  }
})
export default profileSlice.reducer

