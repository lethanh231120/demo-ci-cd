import { combineReducers } from '@reduxjs/toolkit'

import userInfo from './useInfo'
import profileSlice from './profileSlice'
import coinsPriceSlice from './coinsPriceSlice'
import stockAddressSlice from './stockAddressSlice'

export default combineReducers({
  userInfo: userInfo,
  profile: profileSlice,
  coinPrice: coinsPriceSlice,
  stockAddress: stockAddressSlice
})
