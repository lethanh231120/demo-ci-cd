import { createSlice, createAsyncThunk } from '@reduxjs/toolkit'
import { getCookie, STORAGEKEY } from '../utils/storage'
import { get, patch, post } from '../api/stockService'

const token = getCookie(STORAGEKEY.ACCESS_TOKEN)
const config = {
  headers: {
    'Authorization': `Bearer ${token}`
  }
}

export const importStockAddress = createAsyncThunk(
  'stock/addStockAddress',
  async(data) => {
    return await post('stocks/add-connection', data, config)
  }
)

export const getAllConnection = createAsyncThunk(
  'stock/getAllConnection',
  async() => {
    return await get('stocks/all-connection', config)
  }
)

export const changeAmount = createAsyncThunk(
  'stock/changeAmount',
  async(data, id) => {
    return await patch(`stocks/change-amount-symbol/connectionId=${id}`, data, config)
  }
)

const stockSlice = createSlice({
  name: 'import stock connection',
  initialState: {
    status: false,
    loading: false,
    message: null,
    status_code: null,
    list_connection: []
  },
  extraReducers: {
    [importStockAddress.pending]: (state, action) => {
      state.loading = true
    },
    [importStockAddress.fulfilled]: (state, action) => {
      state.loading = false
      state.status = true
      state.status_code = 'B.STO.201.C1'
      state.message = 'Import New Stock Connection'
    },
    [importStockAddress.rejected]: (state, action) => {
      state.loading = false
      state.message = 'Import failed. Please check again'
    },

    // get all connections
    [getAllConnection.pending]: (state, action) => {
      state.loading = true
    },
    [getAllConnection.fulfilled]: (state, action) => {
      state.status = true
      state.status_code = 'B.STO.201'
      state.message = 'Get all connection'
      state.list_connection = action.payload
    },
    [getAllConnection.rejected]: (state, action) => {
      state.message = 'Get failed'
    },

    // change amount
    [changeAmount.pending]: (state, action) => {
      state.loading = true
    },
    [changeAmount.fulfilled]: (state, action) => {
      state.loading = false
      state.status = true
      state.status_code = 'B.STO.200.C4'
      state.message = 'Successfully'
    },
    [changeAmount.fulfilled]: (state, action) => {
      state.loading = false
      state.message = 'Change failed'
    }
  }
})

export default stockSlice.reducer
