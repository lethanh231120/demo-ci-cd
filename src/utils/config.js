
import { getCookie, STORAGEKEY } from '../utils/storage'

const token = getCookie(STORAGEKEY.ACCESS_TOKEN)
export const config = {
  headers: {
    'Authorization': `Bearer ${token}`
  }
}
