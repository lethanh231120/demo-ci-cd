import { ETHEREUM_CHAINID, BINANCE_SMART_CHAIN_CHAINID, SOLANA_CHAINID } from '../constants/ChainId'
export const totalMoney = (listData, chainId) => {
  return listData?.find((item) => {
    let data
    switch (chainId) {
      case ETHEREUM_CHAINID:
        data = item?.coinId === 'ethereum'
        break
      case BINANCE_SMART_CHAIN_CHAINID:
        data = item?.coinid === 'bnb-chain'
        break
      case SOLANA_CHAINID:
        data = item?.coinId === 'solana'
        break
      default:
        break
    }
    return data
  })
}
