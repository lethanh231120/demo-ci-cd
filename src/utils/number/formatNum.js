export const numFixed2 = (number) => {
  const newNum = number?.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')
  return newNum
}

export const numFixed5 = (number) => {
  const newNum = number?.toFixed(5).replace(/\d(?=(\d{3})+\.)/g, '$&,')
  return newNum
}
export const numFixed27 = (number) => {
  const newNum = number?.toFixed(27).replace(/\d(?=(\d{3})+\.)/g, '$&,')
  return newNum
}
export const smallNumber = (number) => {
  return ('' + +number).replace(/(-?)(\d*)\.?(\d*)e([+-]\d+)/,
    function(a, b, c, d, e) {
      return e < 0
        ? b + '0.' + Array(1 - e - c.length).join(0) + c + d
        : b + c + d + Array(e - d.length + 1).join(0)
    })
}
export const bigNumber = (number) => {
  return Number(number).toLocaleString('fullwide', { useGrouping: false })
}
