import { Binance } from '../../components/offchain-wallet/Binance'
import Bitfinex from '../../components/offchain-wallet/Bitfinex'
import Gateio from '../../components/offchain-wallet/Gateio'
import HuobiGlobal from '../../components/offchain-wallet/HuobiGlobal'
import Kraken from '../../components/offchain-wallet/Kraken'
import Poloniex from '../../components/offchain-wallet/Poloniex'
import Kucoin from '../../components/offchain-wallet/Kucoin'

export const exchanges = [
  {
    id: 'binance',
    icon: '/exchange/binance.png',
    platform: <Binance/>,
    name: 'Binance'
  },
  {
    id: 'gateio',
    icon: '/exchange/gateio.png',
    platform: <Gateio/>,
    name: 'Gate.io'
  },
  {
    id: 'bitfinex',
    icon: '/exchange/bitfinex.png',
    platform: <Bitfinex/>,
    name: 'Bitfinex'
  },
  {
    id: 'kucoin',
    icon: '/exchange/kucoin.png',
    platform: <Kucoin/>,
    name: 'Kucoin'
  },
  {
    id: 'huobiglobal',
    icon: '/exchange/huobiglobal.png',
    platform: <HuobiGlobal/>,
    name: 'HuobiGlobal'
  },
  {
    id: 'kraken',
    icon: '/exchange/kraken.png',
    platform: <Kraken/>,
    name: 'Kraken'
  },
  {
    id: 'poloniex',
    icon: '/exchange/poloniex.png',
    platform: <Poloniex/>,
    name: 'Poloniex'
  }
]
