
export const connectType = [
  {
    id: 'onchain',
    icon: '/coins/onchain.jpeg',
    name: 'Onchain Wallet'
  },
  {
    id: 'offchain',
    icon: '/coins/offchange.jpg',
    name: 'Offchain Wallet'
  },
  {
    id: 'stock',
    icon: '/coins/stock.png',
    name: 'Stock'
  }
]
