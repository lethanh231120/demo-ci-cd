import { Binance } from '../../components/offchain-wallet/Binance'
import { Bitcoin } from '../../components/onchain-wallet/Bitcoin'
import Coinbase from '../../components/onchain-wallet/Coinbase'
import { Metamask } from '../../components/onchain-wallet/Metamask'
import { BinanceSmartChain } from '../../components/onchain-wallet/BinanceSmartChain'
import { EthereumWallet } from '../../components/onchain-wallet/EthereumWallet'
import { Solana } from '../../components/onchain-wallet/Solana'
import { TrustWallet } from '../../components/onchain-wallet/TrustWallet'
import { Ripple } from '../../components/onchain-wallet/Ripple'
export const wallets = [
  {
    id: 'binance',
    icon: '/coins/binance.png',
    platform: <Binance/>,
    name: 'Binance'
  },
  {
    id: 'metamask',
    icon: '/coins/metamask.png',
    platform: <Metamask/>,
    name: 'Metamask'
  },
  {
    id: 'coinbase',
    icon: '/coins/coinbase.png',
    platform: <Coinbase/>,
    name: 'Coinbase'
  },
  {
    chainId: '1',
    id: 'ethereum-wallet',
    icon: '/coins/ethereum_wallet.png',
    platform: <EthereumWallet/>,
    name: 'Ethereum Wallet'
  },
  {
    chainId: '0',
    id: 'bitcoin',
    icon: '/coins/bitcoin.png',
    platform: <Bitcoin/>,
    name: 'Bitcoin'
  },
  {
    chainId: '56',
    id: 'binance-mart-chain',
    icon: '/coins/binance_smart_chain.png',
    platform: <BinanceSmartChain/>,
    name: 'Binance Smart Chain'
  },
  {
    chainId: '-1',
    id: 'ripple',
    icon: '/coins/xrp.png',
    platform: <Ripple/>,
    name: 'Ripple Wallet'
  },
  {
    chainId: '1399811149',
    id: 'solana-wallet',
    icon: '/coins/solana_wallet.png',
    platform: <Solana/>,
    name: 'Solana Wallet'
  },
  {
    id: 'trust-wallet',
    icon: '/coins/trust_wallet.png',
    platform: <TrustWallet/>,
    name: 'Trust Wallet'
  }
]
