import { Stock } from '../../components/stock-platform/Stock'

export const stockPlatforms = [
  {
    id: 'stock',
    icon: '/coins/stock.png',
    platform: <Stock/>,
    name: 'Stock'
  }
]
