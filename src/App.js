import React from 'react'
import './App.css'
import HomePage from './layouts'
import Footer from './layouts/footer'

function App() {
  return (
    <div className='App'>
      <HomePage/>
      <Footer />
    </div>
  )
}

export default App
