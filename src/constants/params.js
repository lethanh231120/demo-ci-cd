export const SKIP = 0
export const DEFAULT_PAGE = 1
export const LIMIT = 20
export const CURRENCY = 'EUR'
export const FIVE_MINUTE = 300000
export const ONE_HOUR = 3600000
export const ONE_DAY = 86400000
export const ONE_WEEK = 604800000
export const ONE_MONTH = 2592000000
export const TWO_MONTHS = 5184000000
export const THREE_MONTHS = 7776000000
export const ONE_YEAR = 31104000000

