import React, { useContext } from 'react'
import { Form, Button } from 'antd'
import PlatformHeader from './form-input/PlatformHeader'
import ConnectionName from './form-input/ConnectionName'
import AddressWallet from './form-input/AddressWallet'
import { SOLANA_CHAINID } from '../../constants/ChainId'
import { PlatFormContext } from '../../layouts'
import { postConnection } from '../../api/connectService'
import { config } from '../../utils/config'
export const Solana = () => {
  const state = useContext(PlatFormContext)
  const [form] = Form.useForm()
  const onFinish = async(values) => {
    try {
      const data = {
        ...values,
        'chainId': SOLANA_CHAINID
      }
      state.handleSetPropsImport({ loading: true })
      const res = await postConnection('connect/import-connection', data, config)
      res && state.handleSetPropsImport({
        loading: false,
        success: true,
        statusCode: res?.code
      })
    } catch (error) {
      error?.response?.data && state.handleSetPropsImport({
        messageError: error.response.data.error,
        loading: true,
        statusCode: error.response.data.code
      })
    }
  }
  return (
    <div className='bitcoin'>
      <PlatformHeader src='/coins/solana_wallet.png' text='Solana Wallet'/>
      <Form
        onFinish={onFinish}
        autoComplete='off'
        layout='vertical'
        form={form}
      >
        <ConnectionName/>
        <AddressWallet/>
        <Form.Item shouldUpdate >
          {() => (
            <Button
              type='primary'
              htmlType='submit'
              disabled={
                !form.isFieldsTouched(true) ||
                form.getFieldsError().filter(({ errors }) => errors.length)
                  .length > 0
              }
            >
              Submit
            </Button>
          )}
        </Form.Item>
      </Form>
    </div>
  )
}
