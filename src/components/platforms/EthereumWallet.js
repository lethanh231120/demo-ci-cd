import React, { useContext } from 'react'
import { Typography, Form, Button, Tabs, Select } from 'antd'
import PlatformHeader from './form-input/PlatformHeader'
import ConnectionName from './form-input/ConnectionName'
import AddressWallet from './form-input/AddressWallet'
import ButtonSubmit from './form-input/ButtonSubmit'
import { ETHEREUM_CHAINID } from '../../constants/ChainId'
import { postConnection } from '../../api/connectService'
import { config } from '../../utils/config'
import { PlatFormContext } from '../../layouts'
const { TabPane } = Tabs
const { Text } = Typography

const { Option } = Select
const children = []
for (let i = 10; i < 36; i++) {
  children.push(<Option key={i.toString(36) + i}>{i.toString(36) + i}</Option>)
}

export const EthereumWallet = () => {
  const state = useContext(PlatFormContext)
  const [form] = Form.useForm()
  const onFinish = async(values) => {
    try {
      state.handleSetPropsImport({ loading: true })
      const data = {
        ...values,
        'chainId': ETHEREUM_CHAINID
      }
      const res = await postConnection('connect/import-connection', data, config)
      res && state.handleSetPropsImport({
        loading: true,
        success: false,
        statusCode: res?.code,
        statusImport: res?.status,
        addressImport: values?.address,
        chainId: ETHEREUM_CHAINID
      })
    } catch (error) {
      state.handleSetPropsImport({
        messageError: 'Gateway time-out',
        loading: true,
        statusCode: error.response.data.code
      })
    }
  }
  return (
    <div className='ethereum-wallet'>
      <PlatformHeader src='/coins/ethereum_wallet.png' text='Ethereum Wallet'/>
      <Tabs defaultActiveKey='1'>
        <TabPane tab='Manual' key='1'>
          <Form
            onFinish={onFinish}
            autoComplete='off'
            layout='vertical'
            form={form}
          >
            <ConnectionName/>
            <AddressWallet/>
            <Typography className='ethereum-wallet-button'>
              <Text>Add your Ethereum wallet or ENS address here</Text>
            </Typography>
            <ButtonSubmit text='submit'/>
          </Form>
        </TabPane>
        <TabPane tab='Metamask' key='2'>
          <Form
            onFinish={onFinish}
            autoComplete='off'
            layout='vertical'
            form={form}
          >
            <ConnectionName/>
            <Typography className='ethereum-wallet-text-button'>
              <Text>Automatically connect to ethereum-wallet</Text>
            </Typography>
            <Form.Item shouldUpdate >
              {() => (
                <Button
                  type='primary'
                  htmlType='submit'
                >
                    Connect
                </Button>
              )}
            </Form.Item>
          </Form>
        </TabPane>
        <TabPane tab='WalletConnect' key='3'>
          <Form
            onFinish={onFinish}
            autoComplete='off'
            layout='vertical'
            form={form}
          >
            <ConnectionName/>
            <Typography className='ethereum-wallet-text-button'>
              <Text>Connect your Binance Smart Chain account with WalletConnect</Text>
            </Typography>
            <Form.Item shouldUpdate >
              {() => (
                <Button
                  type='primary'
                  htmlType='submit'
                >
                    CONTINUE WITH WALLETCONNECT
                </Button>
              )}
            </Form.Item>
          </Form>
        </TabPane>
      </Tabs>
    </div>
  )
}
