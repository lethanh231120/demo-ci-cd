import React, { useEffect, useContext } from 'react'
import { Typography, Form, Button, Tabs, Select, Modal } from 'antd'
import { SearchOutlined } from '@ant-design/icons'
import './platform.scss'
import { FormListItem } from '../form/FormList'
import ConnectionName from './form-input/ConnectionName'
import PlatformHeader from './form-input/PlatformHeader'
import AddressWallet from './form-input/AddressWallet'
import { postConnection } from '../../api/connectService'
import { config } from '../../utils/config'
import { ethers } from 'ethers'
import { useWeb3React } from '@web3-react/core'
import { walletconnect } from './wallet-connect/Connectors'
import { PlatFormContext } from '../../layouts'
const { TabPane } = Tabs
const { Text } = Typography
const { Option } = Select
const children = []
const { ethereum } = window
const provider = ((window.ethereum != null) ? new ethers.providers.Web3Provider(window.ethereum) : ethers.getDefaultProvider())

for (let i = 10; i < 36; i++) {
  children.push(<Option key={i.toString(36) + i}>{i.toString(36) + i}</Option>)
}

export const Metamask = () => {
  const state = useContext(PlatFormContext)
  const [form] = Form.useForm()
  const { activate, chainId, account } = useWeb3React()
  const error = () => {
    Modal.error({
      title: 'Oops, Something Went Wrong',
      content: (
        <div className='modal'>
          <p>MetaMask Extension Not Found</p>
        </div>
      ),
      onOk() {}
    })
  }

  const onFinish = async(values) => {
    if (ethereum) {
      try {
        const accounts = await ethereum.request({ method: 'eth_requestAccounts' })
        const chain = await provider.getNetwork()
        const data = {
          connectionName: values.connectionName !== undefined ? values.connectionName : 'Metamask',
          address: accounts[0],
          'chainId': `${chain.chainId}`
        }
        state.handleSetPropsImport({ loading: true })
        const res = await postConnection('connect/import-connection', data, config)
        res && state.handleSetPropsImport({
          loading: true,
          success: false,
          statusCode: res?.code,
          statusImport: res?.status,
          addressImport: accounts[0],
          chainId: `${chain.chainId}`
        })
      } catch (error) {
        error?.response?.data && state.handleSetPropsImport({
          messageError: error.response.data.error,
          loading: true,
          statusCode: error.response.data.code
        })
      }
    } else {
      return error()
    }
  }

  const onFinishManual = () => {}
  const onFinishWallet = () => {
    activate(walletconnect)
  }

  useEffect(() => {
    const connectWallet = async() => {
      try {
        const data = {
          connectionName: 'Metamask',
          address: account,
          'chainId': `${chainId}`
        }
        state.handleSetPropsImport({ loading: true })
        const res = await postConnection('connect/import-connection', data, config)
        res && state.handleSetPropsImport({
          loading: true,
          success: false,
          statusCode: res?.code,
          statusImport: res?.status,
          addressImport: account,
          chainId: `${chainId}`
        })
      } catch (error) {
        error?.response?.data && state.handleSetPropsImport({
          messageError: error.response.data.error,
          loading: true,
          statusCode: error.response.data.code
        })
      }
    }
    chainId !== undefined && account !== undefined && connectWallet()
  }, [chainId, account])

  return (
    <div className='metamask'>
      <PlatformHeader src='/coins/metamask.png' text='Metamask'/>
      <Tabs defaultActiveKey='1'>
        <TabPane tab='MetaMask' key='1'>
          <Form
            onFinish={onFinish}
            autoComplete='off'
            layout='vertical'
            form={form}
          >
            <ConnectionName/>
            <Typography className='metamask-text-button'>
              <Text>Automatically connect to Metamask</Text>
            </Typography>
            <Form.Item shouldUpdate >
              {() => (
                <Button
                  type='primary'
                  htmlType='submit'
                >
                  Connect
                </Button>
              )}
            </Form.Item>
          </Form>
        </TabPane>
        <TabPane tab='Manual' key='2'>
          <Form
            onFinish={onFinishManual}
            autoComplete='off'
            layout='vertical'
            form={form}
          >
            <div className='metamask-form-item'>
              <Form.Item label='Blockchain/Crypto' name={['form', 'connectionName']}>
                <Select
                  showSearch
                  suffixIcon={<SearchOutlined />}
                  size='middle'
                  placeholder='Search to Select'
                  optionFilterProp='children'
                  filterOption={(input, option) => option.children.includes(input)}
                  filterSort={(optionA, optionB) =>
                    optionA.children
                      .toLowerCase()
                      .localeCompare(optionB.children.toLowerCase())
                  }
                >
                  {children}
                </Select>
              </Form.Item>
              <AddressWallet/>
            </div>
            <FormListItem/>
            <Form.Item shouldUpdate >
              {() => (
                <Button
                  type='primary'
                  htmlType='submit'
                  // disabled={
                  //   !form.isFieldsTouched(true) ||
                  //   form.getFieldsError().filter(({ errors }) => errors.length)
                  //     .length > 0
                  // }
                >
                  Connect
                </Button>
              )}
            </Form.Item>
          </Form>
        </TabPane>
        <TabPane tab='WalletConnect' key='3'>
          <Form
            onFinish={onFinishWallet}
            autoComplete='off'
            layout='vertical'
            form={form}
          >
            <ConnectionName/>
            <Typography className='metamask-text-button'>
              <Text>Connect your MetaMask account with WalletConnect</Text>
            </Typography>
            <Form.Item shouldUpdate >
              {() => (
                <Button
                  type='primary'
                  htmlType='submit'
                >
                  CONTINUE WITH WALLETCONNECT
                </Button>
              )}
            </Form.Item>
          </Form>
        </TabPane>
      </Tabs>
    </div>
  )
}
