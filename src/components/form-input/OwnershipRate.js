import React from 'react'
import { Form, Input } from 'antd'

const OwnershipRate = () => {
  return (
    <Form.Item
      label='Ownership rate'
      name='ownPersentage'
    >
      <Input type='number' placeholder='Ownership rate'/>
    </Form.Item>
  )
}

export default OwnershipRate
