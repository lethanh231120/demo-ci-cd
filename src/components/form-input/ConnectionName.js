import React, { memo } from 'react'
import { Form, Input } from 'antd'

const ConnectionName = () => {
  return (
    <Form.Item
      label='Connection Name (optional)'
      name='connectionName'
      rules={[
        {
          max: 50,
          message: 'Connection Name cannot exceed 50 characters.'
        }
      ]}
    >
      <Input placeholder='Connection Name'/>
    </Form.Item>
  )
}
export default memo(ConnectionName)

