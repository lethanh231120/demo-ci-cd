import React, { useState, useEffect } from 'react'
import { LazyLoadImage } from 'react-lazy-load-image-component'
import { IPFS } from '../../constants/CheckIPFS.js'
import { IpfsImage } from 'react-ipfs-image'
import { Typography, Image } from 'antd'
import './style.scss'
import { useNavigate } from 'react-router-dom'

const { Text, Title } = Typography

const NFT = (props) => {
  const { connectionName, name, num, total, data, urlImage, type, setIsGroupNFT, setCurrenConnection, price } = props
  const [indexImg, setIndexImg] = useState(0)
  const [image, setImage] = useState()

  const navigate = useNavigate()

  const handleClickNft = () => {
    if (type === 'connections') {
      setIsGroupNFT(true)
      setCurrenConnection({ connectionName, data })
    }
    if (type === 'item') {
      navigate(`../nft/${data?.nftInfo?.tokenAddress.slice(0, 5)}`, { state: { data: data, price: price }})
    }
  }

  useEffect(() => {
    if (urlImage) {
      if (Array.isArray(urlImage)) {
        console.log(1111)
        setTimeout(function() {
          if (indexImg < urlImage?.length - 1) {
            let image
            if (urlImage[indexImg] && urlImage[indexImg].slice(0, 7) === IPFS) {
              image = <div className='ant-image'><IpfsImage hash={urlImage[indexImg]}/></div>
            }
            if (urlImage[indexImg] && urlImage[indexImg].slice(0, 7) !== IPFS) {
              image = <LazyLoadImage alt='image-nft' src={urlImage[indexImg]} width= '100%' height= '70%' delayTime='500' effect='blur'/>
            }
            if (!urlImage[indexImg]) {
              image = <LazyLoadImage alt='image-nft' src='/coins/nft.png' width= '100%' height= '70%' delayTime='500' effect='blur' />
            }
            setImage(image)
            setIndexImg(indexImg + 1)
          }
          if (indexImg === urlImage?.length - 1) {
            setIndexImg(0)
          }
        }, 2000)
      } else {
        console.log(2222)
        let image
        if (urlImage && urlImage.slice(0, 7) === IPFS) {
          image = <div className='ant-image'><IpfsImage hash={urlImage}/></div>
        }
        if (urlImage && urlImage.slice(0, 7) !== IPFS) {
          image = <LazyLoadImage alt='image-nft' src={urlImage} delayTime='500' effect='blur'/>
        }
        if (!urlImage) {
          image = <LazyLoadImage alt='image-nft' src='/coins/nft.png' delayTime='500' effect='blur' />
        }
        setImage(image)
      }
    }
  }, [image])

  return (
    <div
      className='nft-item'
      onClick={handleClickNft}
    >
      <div className='nft-item-image'>
        {image}
      </div>
      <div className='nft-item-content'>
        {connectionName && (
          <Title className='nft-item-name' level={5}>{connectionName}</Title>
        )}
        <Title className='nft-item-name' level={5}>{name}</Title>
        <Text className='nft-item-price'>
          $ {total?.toFixed(4).replace(/\d(?=(\d{3})+\.)/g, '$&,')}
        </Text>
      </div>
      {num && (
        <div className='nft-item-count'>
          {num}<Image src='/layer.png' preview={false}/>
        </div>
      )}
    </div>
  )
}

export default NFT
