import React, { useEffect, useState } from 'react'
import { Typography, Row, Col, Space, Spin } from 'antd'
import './style.scss'
import { GroupItemNft } from './GroupItemNft'
import ListNFT from './ListNFT'
import { BINANCE_SMART_CHAIN_CHAINID, ETHEREUM_CHAINID, SOLANA_CHAINID } from '../../constants/ChainId'
import { post } from '../../api/coinPriceService'
import _ from 'lodash'

const { Text, Title } = Typography

export const ListGroupNFT = ({ dataConnection, isGroupNFT, setIsGroupNFT, isLoading }) => {
  const [listGroup, setListGroup] = useState()
  const [listNft, setListNft] = useState([])
  const [sumTotalNft, setSumTotalNft] = useState()
  const [listCoinInfo, setListCoinInfo] = useState()
  const handleClickGroupNFT = (item) => {
    setListGroup(item)
    setIsGroupNFT(true)
  }

  useEffect(() => {
    const arr = []
    dataConnection?.nft && dataConnection?.nft.forEach((item) => {
      if (item) {
        item && Object.keys(item).forEach(function(key) {
          if (key && item[key]) {
            arr.push({ 'id': key, 'value': item[key] })
          }
        })
      }
    })
    setListNft(arr)
  }, [dataConnection])

  const hancleBack = () => {
    setIsGroupNFT(false)
  }

  useEffect(() => {
    const calculateAllNft = async() => {
      const allNft = []
      const listCoinId = []
      // lay tat cac chainId gan vao mang allNft
      listNft.forEach((item) => {
        item?.value?.forEach((itm) => allNft.push(itm?.nftInfo?.chainId))
      })
      console.log(allNft)
      // lay danh sach cac chainId
      const uniqueSet = new Set(allNft)
      const listChain = [...uniqueSet]
      console.log(listChain)
      // gan coin id cua tung chain vao listCoinId
      listChain.forEach((item) => {
        switch (item) {
          case ETHEREUM_CHAINID:
            listCoinId.push('ethereum')
            break
          case BINANCE_SMART_CHAIN_CHAINID:
            listCoinId.push('bnb-chain')
            break
          case SOLANA_CHAINID:
            listCoinId.push('solana')
            break
          default:
            break
        }
      })

      if (!_.isEmpty(listCoinId)) {
        const listCoinInfo = await post('price/info/list', { 'listCoinId': listCoinId })
        setListCoinInfo(listCoinInfo?.data?.coins)
        let totalAll = 0
        listNft.forEach((item) => {
          const totalOfGroup = item?.value?.reduce(myFunc, 0)
          // tinh tong tien
          // tại mỗi currenValue lấy chainId check => thông tin coin info của coin chó chainId đó
          function myFunc(total, currenValue) {
            const infoNativeCoin = listCoinInfo?.data?.coins?.find((item) => {
              let data
              switch (currenValue?.nftInfo?.chainId) {
                case ETHEREUM_CHAINID:
                  data = item?.coinId === 'ethereum'
                  break
                case BINANCE_SMART_CHAIN_CHAINID:
                  data = item?.coinId === 'bnb-chain'
                  break
                case SOLANA_CHAINID:
                  data = item?.coinId === 'solana'
                  break
                default:
                  break
              }
              return data
            })
            const price = parseFloat(currenValue?.nftInfo?.price ? currenValue?.nftInfo?.price : 0)
            const amount = (currenValue?.nftInfo?.amount ? currenValue?.nftInfo?.amount : 0)
            const money = price * amount * infoNativeCoin?.price
            const value = total + money
            return value
          }
          totalAll += totalOfGroup
        })
        setSumTotalNft(totalAll?.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,'))
      }
    }
    calculateAllNft()
  }, [listNft])

  return (
    <>
      {isLoading ? (
        <Space size='middle'>
          <Spin size='large' />
        </Space>
      )
        : isGroupNFT ? (<ListNFT listGroup={listGroup} hancleBack={hancleBack} listCoinInfo={listCoinInfo}/>)
          : (<div className='nft'>
            <div className='nft-header'>
              <div className='nft-header-total'>
                <Text>Total Worth</Text>
                <Title level={2}>$ {sumTotalNft}</Title>
              </div>
              <div className='nft-header-count'>
                <Text>NFT Count</Text>
                <Title level={2}>{listNft?.length}</Title>
              </div>
            </div>
            <div className='nft-header-list'>
              <Row gutter={[16, 16]}>
                {listNft ? listNft.map((item, index) => (
                  <Col span={6} key={index}>
                    <div onClick={() => handleClickGroupNFT(item)}>
                      <GroupItemNft item={item} listCoinInfo={listCoinInfo}/>
                    </div>
                  </Col>
                ))
                  : '' }
              </Row>
            </div>
          </div>
          )}
    </>
  )
}
