import React, { useEffect, useState } from 'react'
import { Image, Typography } from 'antd'
import { IPFS } from '../../constants/CheckIPFS.js'
import { IpfsImage } from 'react-ipfs-image'
import { LazyLoadImage } from 'react-lazy-load-image-component'
import { ETHEREUM_CHAINID, BINANCE_SMART_CHAIN_CHAINID, SOLANA_CHAINID } from '../../constants/ChainId.js'
const { Text, Title } = Typography

export const GroupItemNft = ({ item, listCoinInfo }) => {
  const [itemGroup, setItemGroup] = useState()
  useEffect(() => {
    let money = 0
    const image = []
    let name = ''
    item?.value && item?.value?.forEach((itemNFT) => {
      const infoNativeCoin = listCoinInfo?.find((item) => {
        let data
        switch (itemNFT?.nftInfo?.chainId) {
          case ETHEREUM_CHAINID:
            data = item?.coinId === 'ethereum'
            break
          case BINANCE_SMART_CHAIN_CHAINID:
            data = item?.coinId === 'bnb-chain'
            break
          case SOLANA_CHAINID:
            data = item?.coinId === 'solana'
            break
          default:
            break
        }
        return data
      })
      image.push((itemNFT.nftInfo.metadata !== '' && itemNFT.nftInfo.metadata !== undefined)
        ? JSON.parse(itemNFT?.nftInfo?.metadata)?.image ? JSON.parse(itemNFT?.nftInfo?.metadata)?.image
          : JSON.parse(itemNFT?.nftInfo?.metadata)?.image_url
        : '')
      name = itemNFT?.nftInfo?.name ? itemNFT?.nftInfo?.name : ''
      money += parseFloat(itemNFT?.nftInfo?.price ? itemNFT?.nftInfo?.price : 0) * itemNFT?.nftInfo?.amount * infoNativeCoin?.price
    })
    setItemGroup({
      totalMoney: money,
      image,
      name,
      numOfItem: item?.value.length
    })
  }, [item, listCoinInfo])

  const renderImage = (url) => {
    let urlImage
    if (url && url.slice(0, 7) === IPFS) {
      urlImage = <div className='ant-image'><IpfsImage hash={url}/></div>
    }
    if (url && url.slice(0, 7) !== IPFS) {
      urlImage = <LazyLoadImage alt='image-nft' src={url} width= '100%' height= '70%' delayTime='500' effect='blur'/>
    }
    if (!url) {
      urlImage = <LazyLoadImage alt='image-nft' src='/coins/nft.png' width= '100%' height= '70%' delayTime='500' effect='blur'/>
    }
    return urlImage
  }

  console.log('itemGroup?.numOfItem', itemGroup?.numOfItem)
  return (
    <div className='nft-header-item'>
      {renderImage(itemGroup?.image[0])}
      <div className='nft-header-item-content'>
        <Title className='nft-header-item-name' level={5}>{itemGroup && itemGroup?.name}</Title>
        <Text className='nft-header-item-price'>
          ${itemGroup?.totalMoney ? itemGroup?.totalMoney?.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,') : 0}
        </Text>
      </div>
      <div className='nft-header-item-count'>
        {itemGroup && itemGroup?.numOfItem} <Image src='/layer.png' preview={false}/>
      </div>
    </div>
  )
}
