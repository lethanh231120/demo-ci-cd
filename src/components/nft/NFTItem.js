import React, { useEffect, useState } from 'react'
import { Typography } from 'antd'
import { useNavigate } from 'react-router-dom'
import { IPFS } from '../../constants/CheckIPFS'
import { IpfsImage } from 'react-ipfs-image'
import { LazyLoadImage } from 'react-lazy-load-image-component'
import { ETHEREUM_CHAINID, SOLANA_CHAINID, BINANCE_SMART_CHAIN_CHAINID } from '../../constants/ChainId'

const { Text, Title } = Typography

export const NFTItem = ({ item, listCoinInfo }) => {
  const [dataNft, setDataNft] = useState()
  const navigate = useNavigate()
  const [infoNativeCoin, setInfoNativeCoin] = useState()

  const handleClickDetailNFT = (item) => {
    navigate(`../../nft/${item?.nftInfo?.tokenAddress.slice(0, 5)}`, { state: { data: item, infoNativeCoin: infoNativeCoin }})
  }

  useEffect(() => {
    const infoNativeCoin = listCoinInfo?.find((coin) => {
      let data
      switch (item?.nftInfo?.chainId) {
        case ETHEREUM_CHAINID:
          data = coin?.coinId === 'ethereum'
          break
        case BINANCE_SMART_CHAIN_CHAINID:
          data = coin?.coinId === 'bnb-chain'
          break
        case SOLANA_CHAINID:
          data = coin?.coinId === 'solana'
          break
        default:
          break
      }
      return data
    })
    setInfoNativeCoin(infoNativeCoin)
    setDataNft({
      name: item?.nftInfo?.name,
      image: (item.nftInfo.metadata !== '' && item.nftInfo.metadata !== undefined)
        ? (JSON.parse(item?.nftInfo?.metadata)?.image ? JSON.parse(item?.nftInfo?.metadata)?.image : JSON.parse(item?.nftInfo?.metadata)?.image_url) : '',
      totalMoney: (parseFloat(item?.nftInfo?.price
        ? item?.nftInfo?.price : 0) * (item?.nftInfo?.amount
        ? item?.nftInfo?.amount : 0) * infoNativeCoin?.price)?.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')
    })
  }, [item, listCoinInfo])

  const renderImage = (url) => {
    let urlImage
    if (url && url.slice(0, 7) === IPFS) {
      urlImage = <div className='ant-image'><IpfsImage hash={url}/></div>
    }
    if (url && url.slice(0, 7) !== IPFS) {
      urlImage = <LazyLoadImage alt='image-nft' src={url} width= '100%' height= '70%' delayTime='500' effect='blur'/>
    }
    if (!url) {
      urlImage = <LazyLoadImage alt='image-nft' src='/coins/nft.png' width= '100%' height= '70%' delayTime='500' effect='blur' />
    }
    return urlImage
  }

  return (
    <div className='nft-header-item' onClick={() => handleClickDetailNFT(item)}>
      {renderImage(dataNft?.image)}
      <div className='nft-header-item-content'>
        <Title className='nft-header-item-name' level={5}>
          {(item.nftInfo.metadata !== '' && item.nftInfo.metadata !== undefined)
            ? (JSON.parse(item?.nftInfo?.metadata)?.name ? JSON.parse(item?.nftInfo?.metadata)?.name : '_') : '_'}
        </Title>
        <Text className='nft-header-item-price'>
          $ {dataNft ? dataNft?.totalMoney : 0}
        </Text>
      </div>
    </div>
  )
}
