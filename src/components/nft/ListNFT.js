import React, { useEffect, useState } from 'react'
import { Typography, Row, Col } from 'antd'
import './style.scss'
import { NFTItem } from './NFTItem'
import { ArrowLeftOutlined } from '@ant-design/icons'
import { ETHEREUM_CHAINID, SOLANA_CHAINID, BINANCE_SMART_CHAIN_CHAINID } from '../../constants/ChainId'

const { Text, Title } = Typography

const ListNFT = ({ listGroup, hancleBack, listCoinInfo }) => {
  const [total, setTotal] = useState(0)
  useEffect(() => {
    const renderTotal = () => {
      const totalOfGroup = listGroup?.value?.reduce(myFunc, 0)
      function myFunc(total, currenValue) {
        const infoNativeCoin = listCoinInfo?.find((item) => {
          let data
          switch (currenValue?.nftInfo?.chainId) {
            case ETHEREUM_CHAINID:
              data = item?.coinId === 'ethereum'
              break
            case BINANCE_SMART_CHAIN_CHAINID:
              data = item?.coinId === 'bnb-chain'
              break
            case SOLANA_CHAINID:
              data = item?.coinId === 'solana'
              break
            default:
              break
          }
          return data
        })
        const price = parseFloat((currenValue?.nftInfo?.price !== '') ? currenValue?.nftInfo?.price : 0)
        const amount = (currenValue?.nftInfo?.amount ? parseInt(currenValue?.nftInfo?.amount) : 0)
        const money = price * amount * infoNativeCoin?.price
        const value = total + money
        return value
      }
      setTotal(totalOfGroup)
    }
    listCoinInfo && renderTotal()
  }, [listGroup, listCoinInfo])

  return (
    <div className='nft'>
      <div className='nft-title'>
        <div className='nft-title-back' onClick={hancleBack}>
          <ArrowLeftOutlined/>
        </div>
        {listGroup?.value[0]?.nftInfo?.name}
      </div>
      <div className='nft-header'>
        <div className='nft-header-total'>
          <Text>Total Worth</Text>
          <Title level={2}>$ {total ? total?.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,') : 0}</Title>
        </div>
        <div className='nft-header-count'>
          <Text>NFT Count</Text>
          <Title level={2}>{listGroup && listGroup?.value?.length}</Title>
        </div>
      </div>
      <div className='nft-header-list'>
        <Row gutter={[16, 16]}>
          {listGroup?.value && listGroup?.value?.map((item, index) => (
            <Col span={6} key={index}>
              <NFTItem item={item} listCoinInfo={listCoinInfo}/>
            </Col>
          ))}
        </Row>
      </div>
    </div>
  )
}

export default ListNFT
