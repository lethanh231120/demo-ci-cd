import React, { useEffect, useState } from 'react'
import { Row, Col, Typography, Table, Tooltip, message } from 'antd'
import { useLocation } from 'react-router-dom'
// import { useQuery } from '@tanstack/react-query'
// import { getNFT } from '../../api/nftService'
import { CopyOutlined } from '@ant-design/icons'
import moment from 'moment'
import { LazyLoadImage } from 'react-lazy-load-image-component'
import { IPFS } from '../../constants/CheckIPFS'
import { IpfsImage } from 'react-ipfs-image'

const { Text, Title } = Typography

const DetailNFT = () => {
  const [transactions, setTransactions] = useState()
  const { state } = useLocation()
  const { data, price } = state

  useEffect(() => {
    setTransactions(data?.nftTrades)
  }, [data])

  console.log(data, price)
  const copyAddress = (text) => {
    navigator.clipboard.writeText(text)
    message.success({
      content: 'Copy address successfully',
      duration: 3
    })
  }

  const columns = [
    {
      title: '#'
      // render: (_, record) => (<span style={{ color: '#fff' }}>{record?.prices?.rank}</span>)
    },
    {
      title: 'Price',
      render: (_, record) => (<div style={{ color: '#fff', textAlign: 'left' }}>
        $ {record?.value > 0 ? (record.value * (1 / Math.pow(10, 18))).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,') : record?.value}
      </div>)
    },
    {
      title: 'From',
      render: (_, record) => (<div style={{ display: 'flex', alignItems: 'center' }}>
        <span className='address'>
          {record?.fromAddress}
        </span>
        <span className='icon-copy'>
          <CopyOutlined onClick={() => copyAddress(record?.fromAddress)}/>
        </span>
      </div>)
    },
    {
      title: 'To',
      render: (_, record) => (<div style={{ display: 'flex', alignItems: 'center' }}>
        <span className='address'>
          {record?.toAddress}
        </span>
        <span className='icon-copy'>
          <CopyOutlined onClick={() => copyAddress(record?.toAddress)}/>
        </span>
      </div>)
    },
    {
      title: 'Date',
      render: (_, record) => (<Tooltip color='#fff' placement='topLeft' title={`${moment(record?.blockTimestamp).format('MMMM Do YYYY, h:mm:ss a')}`}>
        <div className='date'>{moment(record?.blockTimestamp).fromNow()}</div>
      </Tooltip>)
    }
  ]

  const renderImage = (url) => {
    let urlImage
    if (url && url.slice(0, 7) === IPFS) {
      urlImage = <div className='ant-image'><IpfsImage hash={url}/></div>
    }
    if (url && url.slice(0, 7) !== IPFS) {
      urlImage = <LazyLoadImage alt='image-nft' src={url} width= '100%' height= '70%' delayTime='500' effect='blur'/>
    }
    if (!url) {
      urlImage = <LazyLoadImage alt='image-nft' src='/coins/nft.png' width= '100%' height= '70%' delayTime='500' effect='blur' />
    }
    return urlImage
  }

  const renderMetadata = (dataMeta) => {
    if (dataMeta !== undefined) {
      if (dataMeta !== 'undefined') {
        const dataConvert = JSON.parse(dataMeta)
        console.log('dataConvert', dataConvert)
        const listAttri = dataConvert?.attributes
        console.log('listAttri', listAttri)
        return (<>
          <div className='nft-detail-content-item'>
            {dataConvert ? (<Title level={1} style={{ fontWeight: 700, color: '#fff' }}>
              {dataConvert?.name}
            </Title>) : ''}
            <Text style={{ fontSize: '20px', fontWeight: 500, color: '#808080' }}>
              Price: <span style={{ color: '#fff' }}>
                $ {(parseFloat(data?.nftInfo?.price
                  ? data?.nftInfo?.price : 0) * (data?.nftInfo?.amount
                  ? data?.nftInfo?.amount : 0) * price)?.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')}
              </span>
            </Text>
            <Title style={{ padding: '15px 0', color: '#fff' }} level={2}>Description</Title>
            {dataConvert ? (<Text style={{ color: '#808080' }}>
              {dataConvert?.description}
            </Text>) : ''}
          </div>
          <div className='nft-detail-content-item'>
            <Title level={4} style={{ color: '#fff' }}>Attributes</Title>
            <div className='nft-detail-content-list-attr'>
              {listAttri?.map((item, index) => (
                <div className='nft-detail-content-item-attr' key={index}>
                  <Text style={{ color: '#808080', fontWeight: '700' }}>
                    {item?.type ? item?.type : (item?.trait_type ? item?.trait_type : (Object.keys(item) ? Object.keys(item) : ''))}
                  </Text>
                  <Title level={4} style={{ fontWeight: '500' }}>
                    {item?.description ? item?.description : (item?.value ? item?.value : (Object.keys(item) ? item[Object.keys(item)] : ''))}
                  </Title>
                </div>
              ))}
            </div>
          </div>
        </>)
      }
    }
  }
  return (
    <>
      <div className='nft-detail'>
        <Row gutter={16}>
          <Col span={12}>
            <div className='nft-detail-image'>
              {renderImage((data?.nftInfo?.metadata !== '' && data?.nftInfo?.metadata !== undefined)
                ? JSON.parse(data?.nftInfo?.metadata)?.image ? JSON.parse(data?.nftInfo?.metadata)?.image
                  : JSON.parse(data?.nftInfo?.metadata)?.image_url : '')}
            </div>
          </Col>
          <Col span={12}>
            <div className='nft-detail-content'>
              {data?.nftInfo?.metadata ? renderMetadata(data?.nftInfo?.metadata)
                : (<Text style={{ fontSize: '20px', fontWeight: 500, color: '#808080' }}>
                Price: <span style={{ color: '#fff' }}>
                  $ {data?.nftInfo?.price !== '' ? (parseInt(data?.nftInfo?.price) * (1 / Math.pow(10, 18))).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,') : 0}
                  </span>
                </Text>)}
              <div>
                <Title style={{ color: '#808080' }} level={4}>About Collection</Title>
                <div className='nft-detail-content-list-link'>
                  <a href={`https://etherscan.io/token/${data?.nftInfo?.tokenAddress}?a=${data?.nftInfo?.tokenId}`} target='_blank' rel='noopener noreferrer'>Etherscan</a>
                  <a href={`https://opensea.io/assets/ethereum/${data?.nftInfo?.tokenAddress}/${data?.nftInfo?.tokenId}`} target='_blank' rel='noopener noreferrer'>OpenSea</a>
                  <a href={`https://rarible.com/token/${data?.nftInfo?.tokenAddress}:${data?.nftInfo?.tokenId}?tab=overview`} target='_blank' rel='noopener noreferrer'>Rarible</a>
                  <a href={`https://looksrare.org/collections/${data?.nftInfo?.tokenAddress}/${data?.nftInfo?.tokenId}`} target='_blank' rel='noopener noreferrer'>LooksRare</a>
                  <a href='#' target='_blank'>Website</a>
                  <a href='#' target='_blank'>Discord</a>
                  <a href='#' target='_blank'>Twitter</a>
                </div>
              </div>
            </div>
          </Col>
        </Row>
      </div>
      <div className='nft-detail-transaction-title'>Transactions</div>
      <Table
        style={{ padding: '40px 100px' }}
        columns={columns}
        dataSource={transactions && transactions}
        scroll={{ x: 'max-content' }}
        showSorterTooltip={false}
        pagination={false}
      />
    </>
  )
}
export default DetailNFT
