import { Button, Form, Input, notification } from 'antd'
import React from 'react'
// import ERC20abi from '../../../abi/ERC20.json'
import { ethers } from 'ethers'
import aaveLendingPoolAbi from '../../../abi/aaveLendingPool.json'
// const web3Provider = new ethers.providers.JsonRpcProvider(process.env.REACT_APP_INFURA_URL_KOVAN)

const BorrowModal = (props) => {
  const { dataForBorrowModal, lendingPoolProviderAddress, onBehalfOf, setIsBorrowModal } = props
  const { name, borrowableAmount, decimal, address } = dataForBorrowModal
  const [form] = Form.useForm()
  const interestRateMode = '1'
  const referralCode = '0'
  const provider = new ethers.providers.Web3Provider(window.ethereum)
  const signer = provider.getSigner()
  const lendingPoolContract = new ethers.Contract(lendingPoolProviderAddress && lendingPoolProviderAddress, aaveLendingPoolAbi, signer)

  const handleBorrow = async(value) => {
    const amount = parseFloat(value.borrow)
    if (amount > borrowableAmount) {
      notification.error({
        message: 'Error',
        description: 'You cannot borrow token amount higher than your borrowable'
      })
    } else {
      const amountInWei = (amount * Math.pow(10, decimal)).toString()
      lendingPoolContract.borrow(address, amountInWei, interestRateMode, referralCode, onBehalfOf).then(
        () => {
          form.resetFields()
          setIsBorrowModal(false)
          notification.success({
            message: 'success',
            description: `You borrowed ${amount} ${name} from AAVE successfull`
          })
        }).catch((err) => {
        notification.error({
          message: 'Error',
          description: err.message
        })
      })
    }
  }
  return (
    <div className='borrow__modal--container'>
      <div className='borrow__modal--title'>Borrow Modal</div>
      <div className='borrow__modal--max-amount'>Available {name} Amount you can borrow: {borrowableAmount}</div>
      <Form className='borrow__modal--form' onFinish={handleBorrow}>
        <Form.Item name='borrow' label='Borrow Amount'>
          <Input type='number' placeholder='Fill your amount you want borrow'/>
        </Form.Item>
        <Form.Item>
          <Button type='default' htmlType='submit'>Borrow</Button>
        </Form.Item>
      </Form>
    </div>
  )
}

export default BorrowModal
