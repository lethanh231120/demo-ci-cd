import { Button, Form, Input, notification } from 'antd'
import React from 'react'
import ERC20abi from '../../../abi/ERC20.json'
import { ethers } from 'ethers'
import aaveLendingPoolAbi from '../../../abi/aaveLendingPool.json'
const web3Provider = new ethers.providers.JsonRpcProvider(process.env.REACT_APP_INFURA_URL_KOVAN)

const DepositModal = (props) => {
  const { dataForDepositModal, lendingPoolProviderAddress, onBehalfOf, setIsDepositModal } = props
  const { name, balance, address, decimal } = dataForDepositModal
  const [form] = Form.useForm()
  // const aTokenAddress = '0xFf795577d9AC8bD7D90Ee22b6C1703490b6512FD' // DAI address
  const tokenContract = new ethers.Contract(address, ERC20abi, web3Provider)
  const aDecimals = decimal
  const provider = new ethers.providers.Web3Provider(window.ethereum)
  const signer = provider.getSigner()
  const maxApprove = 1000
  const referralCode = '0'
  const maxApproveInUnit256 = ethers.utils.parseUnits(maxApprove.toString(), aDecimals)
  const lendingPoolContract = new ethers.Contract(lendingPoolProviderAddress && lendingPoolProviderAddress, aaveLendingPoolAbi, signer)

  // await lendingPoolContract.getUserAccountData(onBehalfOf).then(
  //   res => {
  //     console.log('GET USER ACCOUNT DATA', res)
  //     const totalCollateralETHInHex = res[0]._hex.toString()
  //     const totalCollateralETHInDec = totalCollateralETHInHex.toString() / ele
  //     setTotalCollateralETH(totalCollateralETHInDec)
  //     const availableBorrowETHInHex = res.availableBorrowsETH._hex.toString()
  //     const availableBorrowETHInDec = availableBorrowETHInHex.toString() / ele
  //     setAvailableBorrowETH(availableBorrowETHInDec)
  //   }
  // )
  const handleDeposit = async(value) => {
    const amount = parseFloat(value.deposit)
    if (amount > balance) {
      notification.error({
        message: 'Error',
        description: 'You cannot deposit token amount higher than your balance'
      })
    } else {
      const amountInUint256 = ethers.utils.parseUnits(amount.toString(), aDecimals)
      await tokenContract.connect(signer).approve(
        lendingPoolProviderAddress && lendingPoolProviderAddress,
        maxApproveInUnit256
      )
      await lendingPoolContract.deposit(address.toString(), amountInUint256._hex, onBehalfOf, referralCode)
        .then(() => {
          form.resetFields()
          setIsDepositModal(false)
          notification.success({
            message: 'Success',
            description: `You deposited ${amount} ${name} to AAVE successfull`
          })
        })
        .catch((err) => {
          notification.error({
            message: 'Error',
            description: err.message
          })
        })
    }
  }

  // const handleDepositAll = () => {
  //   const amount = balance
  //   console.log('ALL BALANCE', amount)
  // }

  return (
    <div className='deposit__modal--container'>
      <div className='deposit__modal--title'>Deposit Modal</div>
      { balance > 0
        ? <div className='deposit__modal--max-amount'>Your Available {name} Amount you can deposit: {balance}</div>
        : <div>You have no {name} token to deposit</div>
      }
      { balance > 0
        ? <Form className='deposit__modal--form' onFinish={handleDeposit}>
          <Form.Item name='deposit' label='Deposit Amount'>
            <Input type='number' placeholder='Fill your amount you want'/>
          </Form.Item>
          <Form.Item>
            <Button type='default' htmlType='submit'>Deposit</Button>
            {/* <Button type='primary' onClick={handleDepositAll}>Deposit All</Button> */}
          </Form.Item>
        </Form>
        : ''
      }
    </div>
  )
}

export default DepositModal
