import React from 'react'
import { Card, Typography, Image } from 'antd'
import { ArrowRightOutlined } from '@ant-design/icons'
import './style.scss'

const { Text } = Typography

const MarketCoin = ({ title }) => {
  return (
    <>
      <Card
        className='market-card'
        style={{
          width: '100%',
          textAlign: 'left'
        }}
      >
        <div className='market-card-title'>
          <Text level={3}>{title && title}</Text>
          <ArrowRightOutlined/>
        </div>
        <div className='market-list'>
          <div className='market-item'>
            <div className='market-item-image'>
              <Image src='/coins/binance.png' preview={false} width='30'/>
            </div>
            <Text>BNB</Text>
            <Text>$20.000</Text>
            <Text>+1.5%</Text>
          </div>
          <div className='market-item'>
            <div className='market-item-image'>
              <Image src='/coins/binance.png' preview={false} width='30'/>
            </div>
            <Text>BNB</Text>
            <Text>$20.000</Text>
            <Text>+1.5%</Text>
          </div>
          <div className='market-item'>
            <div className='market-item-image'>
              <Image src='/coins/binance.png' preview={false} width='30'/>
            </div>
            <Text>BNB</Text>
            <Text>$20.000</Text>
            <Text>+1.5%</Text>
          </div>
        </div>
      </Card>
    </>
  )
}

export default MarketCoin
