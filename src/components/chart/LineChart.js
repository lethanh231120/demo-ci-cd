import React, { useEffect, useState } from 'react'
import { LineChart, Line, YAxis, XAxis } from 'recharts'
const Chart = ({ record }) => {
  const [dataPrice, setDataPrice] = useState([])
  useEffect(() => {
    const newData = []
    record && record?.map((item) => {
      newData.push(
        {
          price: item?.price ? item?.price : (item?.totalValue ? item?.totalValue : item)
        }
      )
    })
    setDataPrice(newData)
  }, [record])

  const dataSort = [dataPrice && dataPrice].flat(1)
  dataSort?.sort((a, b) => a.price - b.price)

  return (
    <LineChart width={120} height={40} data={dataPrice && dataPrice}>
      <YAxis domain={[dataSort && parseFloat(dataSort[0]?.price), dataSort && parseFloat(dataSort[dataSort?.length - 1]?.price)]} hide={true}/>
      <XAxis dataKey='price' hide={true} reversed={true} />
      <Line
        type='linear'
        dot={false}
        dataKey='price'
        stroke={dataPrice[0]?.price > dataPrice[dataPrice?.length - 1]?.price ? '#6ccf59' : '#ff4d4d'}
        // stroke={(change7d !== undefined) && ((change7d > 0 || change7d === 0) ? '#6ccf59' : '#ff4d4d')}
        strokeWidth={0.7}
      />
    </LineChart>
  )
}
export default Chart
