import React, { memo } from 'react'
import { LineChart, Line, YAxis, XAxis, CartesianGrid, Tooltip } from 'recharts'
import CustomTooltip from './CustomTooltip'
const LineChartAsset = ({ dataChart, width, height, percentInfo }) => {
  const dataSort = [dataChart && dataChart].flat(1)
  dataSort?.sort((a, b) => a.totalValue - b.totalValue)
  return (
    <LineChart width={width} height={height} data={dataChart}>
      <YAxis domain={[dataSort && parseFloat(dataSort[0]?.totalValue), dataSort && parseFloat(dataSort[dataSort?.length - 1]?.totalValue)]} hide={true}/>
      <XAxis dataKey='timestamp' hide={true}/>
      <CartesianGrid vertical={false} horizontal={false}/>
      <Line
        type='linear'
        dot={false}
        dataKey='totalValue'
        stroke={ percentInfo?.balanceFluctuations < 0 ? '#ff4d4d' : '#6ccf59'}
        strokeWidth={1}
      />
      <Tooltip
        content={<CustomTooltip payload={dataChart} />}
      />
    </LineChart>
  )
}
export default memo(LineChartAsset)
