import React from 'react'
import { PieChart, Pie, Cell, Tooltip } from 'recharts'

const COLORS = ['#0088FE', '#00C49F', '#FFBB28', '#FF8042']

const RADIAN = Math.PI / 180
const renderCustomizedLabel = ({
  cx,
  cy,
  midAngle,
  innerRadius,
  outerRadius,
  percent,
  index
}) => {
  const radius = innerRadius + (outerRadius - innerRadius) * 0.5
  const x = cx + radius * Math.cos(-midAngle * RADIAN)
  const y = cy + radius * Math.sin(-midAngle * RADIAN)

  return (
    <text
      x={x}
      y={y}
      fill='white'
      textAnchor={x > cx ? 'start' : 'end'}
      dominantBaseline='central'
    >
      {(percent * 100).toFixed(0) > 5 ? `${(percent * 100).toFixed(0)}%` : ''}
    </text>
  )
}

const CustomTooltip = ({ active, payload }) => {
  if (active && payload && payload.length) {
    return (
      <div className='custom-tooltip'>
        <p className='label'>{`Total value of ${payload[0].name}: ${payload[0].value}$`}</p>
      </div>
    )
  }

  return null
}
export default function PieChartAsset(props) {
  const { data, sizeForPieChart } = props

  return (
    <PieChart width={400} height={400}>
      <Pie
        data={data && data}
        cx={200}
        cy={200}
        labelLine={false}
        label={renderCustomizedLabel}
        outerRadius={sizeForPieChart}
        fill='#8884d8'
        dataKey='totalValue'
      >
        {data?.map((entry, index) => (
          <Cell key={`cell-${index}`} fill={COLORS[index % COLORS.length]} />
        ))}
      </Pie>
      <Tooltip content={<CustomTooltip />}/>
    </PieChart>
  )
}

