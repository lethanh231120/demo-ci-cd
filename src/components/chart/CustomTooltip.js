import React from 'react'
import moment from 'moment'
import './styles.scss'

const CustomTooltip = ({ active, payload }) => {
  if (active && payload && payload.length) {
    return (
      <div className='line-custom-tooltip'>
        <div className='line-tooltip-details'>
          <div>{moment(payload[0].payload.timestamp).format('LLL')}</div>
          {payload[0].payload.totalValue && (
            <div className='line-tooltip-item-usd'>USD: ${parseFloat(payload[0].payload.totalValue).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')}</div>
          )}
        </div>
      </div>
    )
  }

  return null
}

export default CustomTooltip
