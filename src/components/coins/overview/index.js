import React, { useState } from 'react'
import { Area, ResponsiveContainer, AreaChart, XAxis, YAxis, CartesianGrid, Tooltip } from 'recharts'
import moment from 'moment'
import { Checkbox } from 'antd'
import { EllipsisOutlined, CaretUpOutlined, CaretDownOutlined } from '@ant-design/icons'
import '../style.scss'
import CustomTooltip from '../CustomTooltip'
const CheckboxGroup = Checkbox.Group
const plainOptions = ['USD', 'BTC', 'ETH']
const defaultCheckedList = ['USD']

const index = ({ list, setParams, params, data }) => {
  const [checkedList, setCheckedList] = useState(defaultCheckedList)
  const time = [
    {
      name: '24H',
      value: '1d'
    },
    {
      name: '1W',
      value: '1w'
    },
    {
      name: '1M',
      value: '1m'
    },
    {
      name: '3M',
      value: '3m'
    },
    {
      name: '6M',
      value: '6m'
    },
    {
      name: '1Y',
      value: '1y'
    },
    {
      name: 'All',
      value: 'all'
    }
  ]

  const handleClickType = (value) => {
    setParams({
      ...params,
      time: value
    })
  }

  const onChange = (list) => {
    setCheckedList(list)
  }

  console.log('list', list)

  return (
    <>
      <div className='coin-detail-title'>
        <div className='coin-detail-name'>
          {params.coinId} Coin Price Chart (USD)
        </div>
        <div className='coin-detail-percent'>
          {params.time.toUpperCase()} Change
          <span className='coin-detail-percent-num'>
            <CaretUpOutlined/>
            <CaretDownOutlined/>
            0.19%
          </span>
          <EllipsisOutlined/>
        </div>
      </div>
      <div className='coin-detail-chart'>
        <ResponsiveContainer width='100%' height={550}>
          <AreaChart
            data={list}
            margin={{ top: 20, right: 30, left: 0, bottom: 0 }}
          >
            <defs>
              <linearGradient id='colorValue' x1='0' y1='0' x2='0' y2='1'>
                <stop offset='5%' stopColor='#ff9332' stopOpacity={0.4} />
                <stop offset='95%' stopColor='#ff9332' stopOpacity={0.0} />
              </linearGradient>
            </defs>
            <XAxis
              dataKey='timestamp'
              tickFormatter={function(value) {
                let date
                switch (params.time) {
                  case '1d':
                    date = moment(value).format('HH:mm')
                    break
                  case '1w':
                    date = moment(value).format('MMM D')
                    break
                  case '1m':
                    date = moment(value).format('MMM DD')
                    break
                  case '3m':
                    date = moment(value).format('MMM DD')
                    break
                  case '6m':
                    date = moment(value).format('MMM DD')
                    break
                  case '1y':
                    date = moment(value).format('MMM DD')
                    break
                  case 'all':
                    date = moment(value).format('MMM YYYY')
                    break
                  default:
                    break
                }
                return date
              }}
              interval={
                params.time === '1d' ? 2
                  : params.time === '1w' ? 24
                    : params.time === '1m' ? 48
                      : params.time === '3m' ? 270
                        : params.time === '6m' ? 480
                          : params.time === '1y' ? 720 : 8640
              }
              axisLine={false}
              tickLine={false}
              // tickCount={10}
              padding={{ left: 30 }}
              reversed={true}
            />
            {
              (list && list[0]?.price > 10000)
                ? <YAxis
                  tickCount={9}
                  axisLine={false}
                  tickLine={false}
                  tickFormatter={function(value) {
                    const price = new Intl.NumberFormat('en-US', {
                      style: 'currency',
                      currency: 'USD',
                      maximumFractionDigits: 0
                    }).format(value)
                    return price
                  }}
                  domain={['dataMin - 100', 'dataMax + 100']}
                />
                : (list && list[0].price < 100)
                  ? <YAxis
                    tickCount={9}
                    axisLine={false}
                    tickLine={false}
                    tickFormatter={function(value) {
                      const price = new Intl.NumberFormat('en-US', {
                        style: 'currency',
                        currency: 'USD',
                        maximumFractionDigits: 0
                      }).format(value)
                      return price
                    }}
                    domain={['dataMin - 0.000001', 'dataMax + 0.000001']}
                  />
                  : <YAxis
                    tickCount={9}
                    axisLine={false}
                    tickLine={false}
                    tickFormatter={function(value) {
                      const price = new Intl.NumberFormat('en-US', {
                        style: 'currency',
                        currency: 'USD',
                        maximumFractionDigits: 0
                      }).format(value)
                      return price
                    }}
                    domain={['dataMin - 10', 'dataMax + 10']}
                  />
            }
            <CartesianGrid vertical={false} horizontal={false}/>
            <Tooltip content={<CustomTooltip payload={list} />} />
            <Area
              type='linear'
              dataKey='price'
              stroke='#ff9332'
              fillOpacity={0.5}
              fill='url(#colorValue)'
            />
          </AreaChart>
        </ResponsiveContainer>
      </div>
      <div className='coin-detail-option'>
        <div className='coin-detail-types'>
          <div className='coin-detail-changer coin-detail-types-item'>change</div>
          {
            time.map((item, index) => (
              <div
                onClick={() => handleClickType(item.value)}
                key={index}
                className={`${params.time === item.value ? 'coin-detail-types-active' : ''} coin-detail-types-item`}
              >{item.name}</div>
            ))
          }
        </div>
        <div className='coin-detail-checkboxs'>
          <CheckboxGroup options={plainOptions} value={checkedList} onChange={onChange} />
        </div>
      </div>
      <div className='coin-detail-price-change'>
        <div className='coin-detail-price-change-item'>
          5M
          <span
            className='coin-detail-price-change-percent'
            style={{
              // backgroundColor: data.percentChange5m >= 0 ? 'rgba(52, 199, 89, 0.1)' : 'rgba(255,53,53,0.1)',
              // color: data.percentChange5m >= 0 ? '#34b349' : '#ff4d4d'
            }}
          >
            {/* {data?.percentChange5m >= 0 ? <CaretUpOutlined/> : <CaretDownOutlined/>} */}
            {/* {(parseFloat(data.percentChange5m >= 0 ? data.percentChange5m : data.percentChange5m.toString().slice(1)).toFixed(2))} % */}
          </span>
        </div>
        <div className='coin-detail-price-change-item'>
          1H
          <span
            className='coin-detail-price-change-percent'
            style={{
              // backgroundColor: data.percentChange1h >= 0 ? 'rgba(52, 199, 89, 0.1)' : 'rgba(255,53,53,0.1)',
              // color: data.percentChange1h >= 0 ? '#34b349' : '#ff4d4d'
            }}
          >
            {/* {data?.percentChange1h >= 0 ? <CaretUpOutlined/> : <CaretDownOutlined/>} */}
            {/* {(parseFloat(data.percentChange1h >= 0 ? data.percentChange1h : data.percentChange1h.toString().slice(1)).toFixed(2))} % */}
          </span>
        </div>
        <div className='coin-detail-price-change-item'>
          2H
          <span
            className='coin-detail-price-change-percent'
            style={{
              // backgroundColor: data.percentChange2h >= 0 ? 'rgba(52, 199, 89, 0.1)' : 'rgba(255,53,53,0.1)',
              // color: data.percentChange2h >= 0 ? '#34b349' : '#ff4d4d'
            }}
          >
            {/* {data.percentChange2h >= 0 ? <CaretUpOutlined/> : <CaretDownOutlined/>} */}
            {/* {parseFloat(data.percentChange2h >= 0 ? data.percentChange2h : data.percentChange2h.toString().slice(1)).toFixed(2)} % */}
          </span>
        </div>
        <div className='coin-detail-price-change-item'>
          24H
          <span
            className='coin-detail-price-change-percent'
            style={{
              // backgroundColor: data.percentChange24h >= 0 ? 'rgba(52, 199, 89, 0.1)' : 'rgba(255,53,53,0.1)',
              // color: data.percentChange24h >= 0 ? '#34b349' : '#ff4d4d'
            }}
          >
            {/* {data.percentChange24h >= 0 ? <CaretUpOutlined/> : <CaretDownOutlined/>} */}
            {/* {parseFloat(data.percentChange24h >= 0 ? data.percentChange24h : data.percentChange24h.toString().slice(1)).toFixed(2)} % */}
          </span>
        </div>
        <div className='coin-detail-price-change-item'>
          7D
          <span
            className='coin-detail-price-change-percent'
            style={{
              // backgroundColor: data.percentChange7d >= 0 ? 'rgba(52, 199, 89, 0.1)' : 'rgba(255,53,53,0.1)',
              // color: data.percentChange7d >= 0 ? '#34b349' : '#ff4d4d'
            }}
          >
            {/* {data.percentChange7d >= 0 ? <CaretUpOutlined/> : <CaretDownOutlined/>} */}
            {/* {parseFloat(data.percentChange7d >= 0 ? data.percentChange7d : data.percentChange7d.toString().slice(1)).toFixed(2)} % */}
          </span>
        </div>
        <div className='coin-detail-price-change-item'>
          1Y
          <span
            className='coin-detail-price-change-percent'
            style={{
              // backgroundColor: data.percentChange7d >= 0 ? 'rgba(52, 199, 89, 0.1)' : 'rgba(255,53,53,0.1)',
              // color: data.percentChange7d >= 0 ? '#34b349' : '#ff4d4d'
            }}
          >
            <CaretDownOutlined/>
            1.09%
          </span>
        </div>
      </div>
    </>
  )
}
export default index
