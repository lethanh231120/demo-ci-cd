import React from 'react'
import moment from 'moment'
const CustomTooltip = ({ active, payload }) => {
  if (active && payload && payload.length) {
    return (
      <div className='custom-tooltip'>
        <div className='tooltip-details'>
          <p>{moment(payload[0].payload.timestamp).format('LLL')}</p>
          {payload[0].payload.price && (
            <p className='tooltip-item-usd'>USD: ${payload[0].payload.price.toFixed(6).replace(/\d(?=(\d{3})+\.)/g, '$&,')}</p>
          )}
        </div>
      </div>
    )
  }

  return null
}

export default CustomTooltip
