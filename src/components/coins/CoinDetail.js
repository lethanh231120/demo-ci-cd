import React, { useState } from 'react'
import { Tabs } from 'antd'
import { useQuery } from '@tanstack/react-query'
import { getCoin } from '../../api/coinService'
import { get } from '../../api/coinPriceService'
import { Row, Col, Image, message, Popover } from 'antd'
import { StarOutlined, CopyOutlined, CaretDownOutlined, EllipsisOutlined, CaretUpOutlined, InfoCircleOutlined } from '@ant-design/icons'
import './style.scss'
import OverView from './overview'
import { useLocation } from 'react-router-dom'
import { BILLION } from '../../constants/TypeConstants'
import { useParams } from 'react-router-dom'
// import Swap from '../../pages/swap'
import _ from 'lodash'

const { TabPane } = Tabs

export const CoinDetail = () => {
  const { coinId } = useParams()
  const { state } = useLocation()
  const { data } = state
  const [dataInfo, setDataInfo] = useState()
  const [dataChart, setDataChart] = useState()
  const [params, setParams] = useState({
    coinId: coinId,
    time: '1d'
  })

  useQuery(
    ['coinDetail'],
    async() => {
      const coinInfo = await getCoin(`info/${coinId}/detail`)
      const coinLink = await getCoin(`info/${coinId}/links`)
      const contracts = await getCoin(`info/${coinId}/contracts`)
      const tags = await getCoin(`info/${coinId}/tags`)
      setDataInfo({
        'coinInfo': coinInfo && coinInfo.data,
        'coinLink': coinLink && coinLink.data,
        'contracts': contracts && contracts?.data,
        'tags': tags && tags?.data
      })
    }
  )

  useQuery(
    ['coinChart', params],
    async() => {
      const dataCoin = await get(`price/${params.coinId}/period?time=${params.time}`)
      setDataChart(dataCoin?.data?.price_chart)
    }
  )

  const copyAddress = (text) => {
    navigator.clipboard.writeText(text)
    message.success({
      content: 'Copy address successfully',
      duration: 3
    })
  }

  const handleClickTag = (item) => {
    console.log(item)
  }

  return (
    <>
      <div>
        <div className='coin-detail-header'>
          <div className='coin-detail-header-overview'>
            <div className='coin-detail-header-info'>
              <div className='coin-detail-header-info-image'>
                <Image src={dataInfo?.coinInfo?.CoinImage ? dataInfo.coinInfo?.CoinImage : '/coins/bitcoin.png'} preview={false}/>
              </div>
              <div className='coin-detail-header-info-name'>
                {dataInfo?.coinInfo?.CoinName}
                <span className='coin-detail-header-info-symple'>
                  ({dataInfo?.coinInfo?.CoinSymbol.toUpperCase()})
                </span>
              </div>
              <div className='coin-detail-header-info-icon'>
                <StarOutlined/>
              </div>
              <div className='coin-detail-header-info-icon'>
                <StarOutlined/>
              </div>
            </div>
            <div className='coin-detail-header-price'>
              <div className='coin-detail-header-price-num'>
                $ {data?.price.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')}
              </div>
              <div
                className='coin-detail-header-price-percent'
                style={{
                  backgroundColor: data.marketCapChangePercentage24h >= 0 ? 'rgba(52, 199, 89, 0.1)' : 'rgba(255,53,53,0.1)',
                  color: data.marketCapChangePercentage24h >= 0 ? '#34b349' : '#ff4d4d'
                }}
              >
                {data.marketCapChangePercentage24h >= 0 ? <CaretUpOutlined/> : <CaretDownOutlined/>}
                {parseFloat(data.marketCapChangePercentage24h >= 0 ? data.marketCapChangePercentage24h : data.marketCapChangePercentage24h.toString().slice(1)).toFixed(2)} %
              </div>
            </div>
            <div className='coin-detail-header-market'>
              <div className='coin-detail-header-market-num'>
                B 1
              </div>
              <div className='coin-detail-header-market-percent'>
                <CaretUpOutlined/>+0.3%
              </div>
            </div>
          </div>
          <Row gutter={32}>
            <Col span={14}>
              <Row gutter={14}>
                <Col span={6}>
                  <span className='coin-detail-header-content-item-title'>
                    Market Cap
                    <div className='coin-detail-header-content-item-icon'>
                      <InfoCircleOutlined />
                    </div>
                  </span>
                </Col>
                <Col span={18}>
                  <div className='coin-detail-header-content-list-item-network'>
                    <div className='coin-detail-header-content-item-box'>
                      $ {(data?.marketCap * BILLION)?.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')} B
                    </div>
                  </div>
                </Col>
              </Row>
              <Row gutter={14}>
                <Col span={10}>
                  <span className='coin-detail-header-content-item-title'>
                    Volumn 24h
                    <div className='coin-detail-header-content-item-icon'>
                      <InfoCircleOutlined />
                    </div>
                  </span>
                </Col>
                <Col span={14}>
                  <div className='coin-detail-header-content-list-item-network'>
                    <div className='coin-detail-header-content-item-box'>
                      {/* $ {(data?.marketCap * BILLION)?.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')} B */}
                    </div>
                  </div>
                </Col>
              </Row>
              <Row gutter={14}>
                <Col span={6}>
                  <div className='coin-detail-header-content-item-title'>
                    Circulating Supply
                    <div className='coin-detail-header-content-item-icon'>
                      <InfoCircleOutlined />
                    </div>
                  </div>
                </Col>
                <Col span={18}>
                  <div className='coin-detail-header-content-list-item-network'>
                    <div className='coin-detail-header-content-item-box'>
                      {(data?.circulatingSupply * BILLION)?.toFixed(2).replace(/(.)(?=(\d{3})+$)/g, '$1,')} B
                    </div>
                  </div>
                </Col>
              </Row>
              <Row gutter={14}>
                <Col span={6}>
                  <span className='coin-detail-header-content-item-title'>
                    Total Supply
                    <div className='coin-detail-header-content-item-icon'>
                      <InfoCircleOutlined />
                    </div>
                  </span>
                </Col>
                <Col span={18}>
                  <div className='coin-detail-header-content-list-item-network'>
                    <div className='coin-detail-header-content-item-box'>
                      {(data?.totalSupply * BILLION)?.toFixed(2).replace(/(.)(?=(\d{3})+$)/g, '$1,')} B
                    </div>
                  </div>
                </Col>
              </Row>
              <Row gutter={14}>
                <Col span={6}>
                  <span className='coin-detail-header-content-item-title'>
                    Fully Diluted Valuation
                    <div className='coin-detail-header-content-item-icon'>
                      <InfoCircleOutlined />
                    </div>
                  </span>
                </Col>
                <Col span={18}>
                  <div className='coin-detail-header-content-list-item-network'>
                    <div className='coin-detail-header-content-item-box'>
                      {(data?.fullyDilutedValuation * BILLION)?.toFixed(2).replace(/(.)(?=(\d{3})+$)/g, '$1,')}
                    </div>
                  </div>
                </Col>
              </Row>
            </Col>
            <Col span={10}>
              {!_.isEmpty(dataInfo?.contracts) ? (
                <Row gutter={12}>
                  <Col span={4}>
                    <span className='coin-detail-header-content-item-title'>
                      Contracts
                    </span>
                  </Col>
                  <Col span={20}>
                    <div className='coin-detail-header-content-list-item-network'>
                      <div className='coin-detail-header-content-item-box'>
                        {dataInfo?.contracts[0]?.TokenAddress.slice(0, 4)}...{dataInfo?.contracts[0]?.TokenAddress.slice(dataInfo?.contracts[0]?.TokenAddress.length - 5, dataInfo?.contracts[0]?.TokenAddress.length - 1)}
                        <CopyOutlined onClick={() => copyAddress(dataInfo?.contracts[0]?.TokenAddress)}/>
                        <Popover
                          placement='bottomRight'
                          content={(
                            <div style={{ width: '200px' }}>
                              {dataInfo?.contracts?.map((item, index) => (
                                <div
                                  key={index}
                                  style={{
                                    display: 'flex',
                                    alignItems: 'center',
                                    borderBottom: '1px solid #000',
                                    height: '40px'
                                  }}>
                                  <div style={{ width: '30px', height: '30px' }}>
                                    <img style={{ width: '100%', height: '100%', objectFit: 'cover' }} src={item?.ChainImage}/>
                                  </div>
                                  <div style={{ margin: '0 10px' }}>
                                    <div>
                                      {item?.ChainName}
                                    </div>
                                    <div>
                                      {item?.TokenAddress.slice(0, 6)}...{item?.TokenAddress.slice(item?.TokenAddress.length - 7, item?.TokenAddress.length - 1)}
                                    </div>
                                  </div>
                                  <CopyOutlined onClick={() => copyAddress(item?.TokenAddress)}/>
                                </div>))}
                            </div>
                          )}
                          trigger='click'
                        >
                          <EllipsisOutlined style={{ marginLeft: '10px', background: '#000', cursor: 'pointer' }}/>
                        </Popover>
                      </div>
                    </div>
                  </Col>
                </Row>
              ) : ('')}
              {!_.isEmpty(dataInfo?.coinLink?.Website) ? (
                <Row gutter={12}>
                  <Col span={4}>
                    <span className='coin-detail-header-content-item-title'>
                      Website
                    </span>
                  </Col>
                  <Col span={20}>
                    <div className='coin-detail-header-content-list-item-network'>
                      {dataInfo?.coinLink?.Website?.map((item, index) => (
                        <div key={index} className='coin-detail-header-content-item-box'>
                          <a href={item?.link_url} target='_blank' rel='noopener noreferrer'>
                            {item?.link_title ? item?.link_title : item?.link_url}
                          </a>
                        </div>
                      ))}
                    </div>
                  </Col>
                </Row>
              ) : ('') }
              {!_.isEmpty(dataInfo?.coinLink?.Explorers) ? (
                <Row gutter={12}>
                  <Col span={4}>
                    <span className='coin-detail-header-content-item-title'>
                      Explorers
                    </span>
                  </Col>
                  <Col span={20}>
                    <div className='coin-detail-header-content-list-item-network'>
                      <div className='coin-detail-header-content-item-box'>
                        {dataInfo?.coinLink?.Explorers[0]?.link_title ? dataInfo?.coinLink?.Explorers[0]?.link_title : dataInfo?.coinLink?.Explorers[0]?.link_url}
                        <Popover
                          placement='bottomRight'
                          content={(
                            <div>
                              {dataInfo?.coinLink?.Explorers?.map((item, index) => (
                                <div
                                  key={index}
                                  style={{
                                    display: 'flex',
                                    alignItems: 'center',
                                    height: '40px'
                                  }}>
                                  <a href={item?.link_url} target='_blank' rel='noopener noreferrer'>
                                    {item?.link_title ? item?.link_title : item?.link_url}
                                  </a>
                                </div>
                              ))}
                            </div>
                          )}
                          trigger='click'
                        >
                          <EllipsisOutlined style={{ marginLeft: '10px', background: '#000', cursor: 'pointer' }}/>
                        </Popover>
                      </div>
                    </div>
                  </Col>
                </Row>
              ) : ('') }
              {!_.isEmpty(dataInfo?.coinLink?.Community) ? (
                <Row gutter={12}>
                  <Col span={4}>
                    <span className='coin-detail-header-content-item-title'>
                    Community
                    </span>
                  </Col>
                  <Col span={20}>
                    <div className='coin-detail-header-content-list-item-network'>
                      {dataInfo?.coinLink?.Community?.map((item, index) => (
                        <div key={index} className='coin-detail-header-content-item-box'>
                          <a href={item?.link_url} target='_blank' rel='noopener noreferrer'>
                            <i className={`${item?.link_icon}`} style={{ marginRight: '5px' }}/>
                            {item?.link_title ? item?.link_title : item?.link_url}
                          </a>
                        </div>
                      ))}
                    </div>
                  </Col>
                </Row>
              ) : ('') }
              {!_.isEmpty(dataInfo?.coinLink?.['Search on']) ? (
                <Row gutter={12}>
                  <Col span={4}>
                    <span className='coin-detail-header-content-item-title'>
                    Search on
                    </span>
                  </Col>
                  <Col span={20}>
                    <div className='coin-detail-header-content-list-item-network'>
                      {dataInfo?.coinLink?.['Search on']?.map((item, index) => (
                        <div key={index} className='coin-detail-header-content-item-box'>
                          <a href={item?.link_url} target='_blank' rel='noopener noreferrer'>
                            {item?.link_title ? item?.link_title : item?.link_url}
                          </a>
                        </div>
                      ))}
                    </div>
                  </Col>
                </Row>
              ) : ('') }
              {!_.isEmpty(dataInfo?.coinLink?.['Source Code']) ? (
                <Row gutter={12}>
                  <Col span={4}>
                    <span className='coin-detail-header-content-item-title'>
                    Source Code
                    </span>
                  </Col>
                  <Col span={20}>
                    <div className='coin-detail-header-content-list-item-network'>
                      {dataInfo?.coinLink?.['Source Code']?.map((item, index) => (
                        <div key={index} className='coin-detail-header-content-item-box'>
                          <a href={item?.link_url} target='_blank' rel='noopener noreferrer'>
                            {item?.link_title ? item?.link_title : item?.link_url}
                          </a>
                        </div>
                      ))}
                    </div>
                  </Col>
                </Row>
              ) : ('') }
              {!_.isEmpty(dataInfo?.tags) ? (
                <Row gutter={12}>
                  <Col span={4}>
                    <span className='coin-detail-header-content-item-title'>
                      Tags
                    </span>
                  </Col>
                  <Col span={20}>
                    <div className='coin-detail-header-content-list-item-network'>
                      {dataInfo?.tags?.map((item, index) => (
                        <div key={index} className='coin-detail-header-content-item-box' onClick={() => handleClickTag(item)}>
                          {item}
                        </div>
                      ))}
                    </div>
                  </Col>
                </Row>
              ) : ('')}
            </Col>
          </Row>
        </div>
        <div className='coin-detail-content'>
          <Row gutter={60}>
            <Col span={18}>
              <Tabs defaultActiveKey='1'>
                <TabPane tab='OVERVIEW' key='1'>
                  <OverView
                    list={dataChart && dataChart}
                    // data={data}
                    setParams={setParams}
                    params={params}
                  />
                </TabPane>
                <TabPane tab='NEWS' key='2'>
                  dkjdhskjahdkjs
                </TabPane>
                <TabPane tab='MARKETS' key='3'>
                  Content of Tab Pane 3
                </TabPane>
                <TabPane tab='HOLDINGS' key='4'>
                  Content of Tab Pane 4
                </TabPane>
                <TabPane tab='TEAM UPDATES' key='5'>
                  Content of Tab Pane 5
                </TabPane>
                <TabPane tab='ON-CHAIN DATA' key='6'>
                  Content of Tab Pane 6
                </TabPane>
              </Tabs>
            </Col>
            <Col span={6}>
              <div className='coin-detail-swap'>
                {/* <Swap/> */}
              </div>
            </Col>
          </Row>
        </div>
      </div>
    </>
  )
}
