import React, { useEffect, useState } from 'react'
import { Input, Table } from 'antd'
import { Token as TokenUni } from '@uniswap/sdk-core'
import { Token as TokenPk } from '@pancakeswap/sdk'
import { ethers } from 'ethers'
import ERC20ABI from '../../abi/abi.json'
import BEP20ABI from '../../abi/bep20abi.json'
// import BeatLoader from 'react-spinners/BeatLoader'
import { useQuery } from '@tanstack/react-query'
import { get } from '../../api/supportDexService'
import BeatLoader from 'react-spinners/BeatLoader'

// const getContract = (tokenAddress) => new ethers.Contract(tokenAddress, ERC20ABI, web3Provider)

const REACT_APP_INFURA_URL_MAINNET = process.env.REACT_APP_INFURA_URL_MAINNET
const web3Provider = new ethers.providers.JsonRpcProvider(REACT_APP_INFURA_URL_MAINNET)
const web3ProviderBSC = new ethers.providers.JsonRpcProvider('https://bsc-dataseed.binance.org/', { name: 'binance', chainId: 56 })
const FromModal = ({ setIsFromModal, setToken0, setTokenSymbol0, setToken0Contract, setTokenDecimals0, dexName, chainId }) => {
  const { Search } = Input
  const [dataTable, setDataTable] = useState([])
  const [filter, setFilter] = useState(false)
  const [filterData, setFilterData] = useState([])

  const chainIdInDec = parseInt(chainId, 16)

  const { data: listToken } = useQuery(
    ['listToken'],
    async() => {
      const data = await get(`dexs/tokens/chainid=${chainIdInDec}`)
      return data?.data.tokens
    }
  )

  useEffect(() => {
    setDataTable(listToken)
  }, [listToken])
  console.log(dataTable)

  const onSearch = (value) =>{
    if (value === '') {
      setFilter(false)
    } else {
      setFilter(true)
      const listData = dataTable?.filter((item) => {
        return item?.name.toLowerCase().includes(value.toLowerCase()) ||
        item?.symbol.toLowerCase().includes(value.toLowerCase()) ||
        item?.address === value
      })
      setFilterData(listData)
    }
  }

  const columns = [
    {
      title: 'Name',
      dataIndex: 'name',
      render: (_, record) => (
        <div className='coin-side' onClick={() => {
          setTokenSymbol0(record?.symbol)
          setTokenDecimals0(record?.decimals)
          setIsFromModal(false)
          // setToken0Address(record?.address)
          const token0 = dexName === 'Uni Swap' || dexName === 'Sushi Swap'
            ? new TokenUni(chainIdInDec, record?.address, record?.decimals, record?.symbol, record?.name)
            : dexName === '1inch Swap'
              ? new TokenUni(chainIdInDec, record?.address, parseInt(record?.decimals), record?.symbol, record?.name)
              : new TokenPk(chainIdInDec, record?.address, parseInt(record?.decimals))
          setToken0(token0)
          const getToken0Contract = () => dexName === 'Uni Swap' || dexName === 'Sushi Swap'
            ? new ethers.Contract(record?.address, ERC20ABI, web3Provider)
            : dexName === '1inch Swap'
              ? new ethers.Contract(record?.address, ERC20ABI, web3Provider)
              : new ethers.Contract(record?.address, BEP20ABI, web3ProviderBSC)
          const token0Contract = getToken0Contract()
          setToken0Contract(token0Contract)
        }}>
          <div className='coin-symbol'>{record?.symbol}</div>
          <div className='coin-name'>{record?.name}</div>
        </div>
      )
    },
    {
      title: 'Balance',
      render: () => (
        <div></div>
      )
    }
  ]

  return (
    <div className='to-modal'>
      <div className='to-modal__title'>Select Coin</div>
      <div className='to-modal__search-bar'>
        <Search
          placeholder='input search text'
          onSearch={onSearch}
          enterButton />
      </div>
      <div className='to-modal__coins--list'>
        {dataTable === undefined || dataTable === null
          ? <BeatLoader />
          : <Table
            columns={columns}
            // dataSource={dataTable && dataTable}
            dataSource={filter ? filterData : dataTable && dataTable}
            scroll={{ y: 240 }}
            pagination={{ hideOnSinglePage: true }}/>
        }
      </div>
    </div>
  )
}

export default FromModal
