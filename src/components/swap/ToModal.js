import React, { useState, useEffect } from 'react'
import { Input, Table } from 'antd'
import { useQuery } from '@tanstack/react-query'
import { Token as TokenUni } from '@uniswap/sdk-core'
import { Token as TokenPk } from '@pancakeswap/sdk'
import { ethers } from 'ethers'
import ERC20ABI from '../../abi/abi.json'
import BEP20ABI from '../../abi/bep20abi.json'
import { get } from '../../api/supportDexService'

const REACT_APP_INFURA_URL_MAINNET = process.env.REACT_APP_INFURA_URL_MAINNET
const web3Provider = new ethers.providers.JsonRpcProvider(REACT_APP_INFURA_URL_MAINNET)
const web3ProviderBSC = new ethers.providers.JsonRpcProvider('https://bsc-dataseed.binance.org/', { name: 'binance', chainId: 56 })
const ToModal = ({ setIsToModal, setTokenSymbol1, setToken1Contract, setToken1, dexName, chainId }) => {
  const { Search } = Input
  const [dataTable, setDataTable] = useState([])
  const [filter, setFilter] = useState(false)
  const [filterData, setFilterData] = useState([])

  const chainIdInDec = parseInt(chainId, 16)

  const { data: listToken } = useQuery(
    ['listToken'],
    async() => {
      const data = await get(`dexs/tokens/chainid=${chainIdInDec}`)
      return data?.data.tokens
    }
  )

  useEffect(() => {
    setDataTable(listToken)
  }, [])

  const onSearch = (value) =>{
    if (value === '') {
      setFilter(false)
    } else {
      setFilter(true)
      const listData = dataTable?.filter((item) => {
        return item?.name.toLowerCase().includes(value.toLowerCase()) ||
        item?.symbol.toLowerCase().includes(value.toLowerCase()) ||
        item?.address === value
      })
      setFilterData(listData)
    }
  }

  const columns = [
    {
      title: 'Name',
      dataIndex: 'name',
      render: (_, record) => (
        <div className='coin-side' onClick={() => {
          setTokenSymbol1(record?.symbol)
          setIsToModal(false)
          // setTokenDecimals1(record?.decimals)
          // setToken1Address(record?.address)
          const token1 = dexName === 'Uni Swap' || dexName === 'Sushi Swap'
            ? new TokenUni(chainIdInDec, record?.address, parseInt(record?.decimals), record?.symbol, record?.name)
            : dexName === '1inch Swap'
              ? new TokenUni(chainIdInDec, record?.address, parseInt(record?.decimals), record?.symbol, record?.name)
              : new TokenPk(chainIdInDec, record?.address, parseInt(record?.decimals))
          console.log(token1)
          setToken1(token1)
          const getToken1Contract = () => dexName === 'Uni Swap' || dexName === 'Sushi Swap'
            ? new ethers.Contract(record?.address, ERC20ABI, web3Provider)
            : dexName === '1inch Swap'
              ? new ethers.Contract(record?.address, ERC20ABI, web3Provider)
              : new ethers.Contract(record?.address, BEP20ABI, web3ProviderBSC)
          const token1Contract = getToken1Contract()
          setToken1Contract(token1Contract)
        }}>
          <div className='coin-symbol'>{record?.symbol}</div>
          <div className='coin-name'>{record?.name}</div>
        </div>
      )
    },
    {
      title: 'Balance',
      render: () => (
        <div>0</div>
      )
    }
  ]

  return (
    <div className='to-modal'>
      <div className='to-modal__title'>Select Coin</div>
      <div className='to-modal__search-bar'>
        <Search
          placeholder='input search text'
          onSearch={onSearch}
          enterButton />
      </div>
      <Table
        columns={columns}
        dataSource={filter ? filterData : dataTable && dataTable}
        scroll={{ y: 240 }}
        pagination={{ hideOnSinglePage: true }}/>
    </div>
  )
}

export default ToModal
