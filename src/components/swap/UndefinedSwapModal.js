import React from 'react'
import { Button } from 'antd'
import { DownOutlined } from '@ant-design/icons'

const UndefinedSwapModal = () => {
  return (
    <div className='uni-swap__content'>
      <div className='swap-page__modal--uni-swap'>
        <div className='swap-page__modal--uni-swap-from'>
          <div className='select-coin-part'>
            <div className='swap-span'>From</div>
            <Button className='swap-coin-btn' disabled>
              Select coin <DownOutlined />
            </Button>
          </div>
        </div>
        <div className='swap-page__modal--uni-swap-to'>
          <div className='select-coin-part'>
            <div className='swap-span'>To</div>
            <Button className='swap-coin-btn' disabled>Select coin <DownOutlined /></Button>
          </div>
        </div>
      </div>
    </div>
  )
}

export default UndefinedSwapModal
