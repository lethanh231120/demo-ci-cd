import React from 'react'
import { Button } from 'antd'

const SelectSwap = (props) => {
  const { symbolNetwork, setDexName, setIsSelectProtocol } = props
  const handleSetDexName = (dexName) => {
    setDexName(dexName)
    setIsSelectProtocol(false)
  }
  return (
    <div className='select-protocol-form'>
      { symbolNetwork === 'ETH' ? (
        <>
          <Button onClick={() => handleSetDexName('Uni Swap')}>
            <span>Swap with UniSwap</span>
            <img src='https://upload.wikimedia.org/wikipedia/commons/thumb/e/e7/Uniswap_Logo.svg/2051px-Uniswap_Logo.svg.png' alt='uniswap-logo'/>
          </Button>
          <Button onClick={() => handleSetDexName('Sushi Swap')}>
            <span>Swap with Sushi Swap</span>
            <img src='https://cryptologos.cc/logos/sushiswap-sushi-logo.png' alt='sushi-swap'/>
          </Button>
          <Button onClick={() => handleSetDexName('1inch Swap')}>
            <span>Swap with 1Inch</span>
            <img src='https://cryptologos.cc/logos/1inch-1inch-logo.png?v=023' alt='1inch-swap'/>
          </Button>
        </>
      ) : (
        <>
          <Button onClick={() => handleSetDexName('Pancake Swap')}>
            <span>Swap with Pancake Swap</span>
            <img src='https://cryptologos.cc/logos/pancakeswap-cake-logo.png' alt='pancake-swap'/>
          </Button>
        </>
      )}
    </div>
  )
}

export default SelectSwap
