import { Button, Input } from 'antd'
import React from 'react'
import './styles.scss'

const AdvanceModal = props => {
  return (
    <div className='advance__modal'>
      <div className='advance__modal--title'>
        Advanced Options
      </div>
      <div className='advance__modal--slippage'>
        <span>Slippage</span>
        <div className='advance__modal--slippage-options'>
          <Button
            onClick={() => props.setSlippageAmount(2)}
          >2%</Button>
          <Button
            onClick={() => props.setSlippageAmount(3)}
          >3%</Button>
          <Input
            defaultValue={0}
            className='inputField'
            placeholder='1.0%'
            value={props.slippageAmount}
            onChange={e => props.setSlippageAmount(e.target.value)}
            addonAfter='%'
          />
        </div>
      </div>
      <div className='advance__modal--deadline'>
        <span>Transaction deadline</span>
        <div className='advance__modal--deadline-options'>
          <Input
            defaultValue={0}
            className='inputField'
            placeholder='10'
            value={props.deadlineMinutes}
            onChange={e => props.setDeadlineMinutes(e.target.value)}
            addonAfter='minutes'
          />
        </div>
      </div>
      <div className='advance__modal--btn'>
        <Button onClick={() => props.setIsModalAdvanced(false)}>Confirm</Button>
      </div>
    </div>
  )
}

export default AdvanceModal
