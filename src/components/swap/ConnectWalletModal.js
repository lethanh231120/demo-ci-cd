import React, { useState } from 'react'
import { RightOutlined } from '@ant-design/icons'
import { useNavigate } from 'react-router-dom'
import ModalHeader from '../modal/onchain-connect/ModalHeader'
import ModalContent from '../modal/onchain-connect/ModalContent'
import { Modal, notification } from 'antd'
import './styles.scss'

export const ConnectWalletModal = ({ setIsConnectWalletModal }) => {
  const [isPlatformModal, setIsPlatformModal] = useState(false)
  const navigate = useNavigate()
  const handleConnectBitcoinWallet = () => {
    navigate('/connect/bitcoin')
  }

  const handleConnectMetaMask = () => {
    if (window.ethereum && window.ethereum.isMetaMask) {
      window.ethereum.request({ method: 'eth_requestAccounts' })
        .then(result => {
          sessionStorage.setItem('address', result[0])
          setIsConnectWalletModal(false)
        })
        .catch(err =>{
          notification.error({
            message: 'Error',
            description: err.message
          })
        })
    } else {
      notification.error({
        message: 'Cannot find meta mask',
        description: 'Cannot find metamask extensions. Please check again and install if not have meta mask.'
      })
    }
  }

  const handleConnectTrustWallet = () => {
    navigate('/connect/trust-wallet')
  }

  const openPlatformModal = () => {
    setIsConnectWalletModal(false)
    setIsPlatformModal(true)
  }

  return (
    <div className='connect-wallet__modal'>
      <div className='connect-wallet__modal-header'>
        <div className='connect-wallet__modal-header-title'>Select Portfolio to Connect</div>
        <div className='connect-wallet__modal-header-description'>Connect your wallets and exchanges to manage all your assets together now.</div>
      </div>
      <div className='connect-wallet__modal-wallet'>
        <div className='connect-wallet__modal-wallet-zone' onClick={handleConnectBitcoinWallet}>
          <img src='https://cdn.pixabay.com/photo/2015/08/27/11/20/bitcoin-910307_1280.png' alt='coinstats-logo' />
          <div>Bitcoin Wallet <RightOutlined /></div>
        </div>

        <div className='connect-wallet__modal-wallet-zone' onClick={handleConnectMetaMask}>
          <img src='coins/metamask.png' alt='eth-logo' />
          <div>&nbsp;&nbsp;MetaMask Wallet <RightOutlined /></div>
        </div>

        <div className='connect-wallet__modal-wallet-zone' onClick={handleConnectTrustWallet}>
          <img src='https://trustwallet.com/assets/images/media/assets/TWT.png' alt='trust-wallet' />
          <div>Trust Wallet <RightOutlined /></div>
        </div>

        <div className='connect-wallet__modal-wallet-zone' onClick={openPlatformModal}>
          <img src='https://www.iconpacks.net/icons/2/free-coin-wallet-icon-2204-thumb.png' alt='coinstats-logo' />
          <div>Connect to other <RightOutlined /></div>
        </div>
      </div>

      <Modal
        visible={isPlatformModal}
        footer={null}
        onOk={() => setIsPlatformModal(false)}
        onCancel={() => setIsPlatformModal(false)}
        className='platform-modal'
      >
        <ModalHeader />
        <ModalContent/>
      </Modal>
    </div>
  )
}
