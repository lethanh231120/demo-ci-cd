import { ethers, BigNumber } from 'ethers'
import { CurrencyAmount, Percent, TradeType } from '@uniswap/sdk-core'
import JSBI from 'jsbi'
import { AlphaRouter } from '@uniswap/smart-order-router'
import { notification } from 'antd'

const V3_SWAP_ROUTER_ADDRESS = process.env.REACT_APP_V3_UNI_SWAP_ROUTER_ADDRESS
const SUSHI_ROUTER_ADDRESS = process.env.REACT_APP_SUSHI_SWAP_ROUTER_ADDRESS
// const PANCAKE_ROUTER_ADDRESS = process.env.REACT_APP_PANCAKE_SWAP_ROUTER_ADDRESS
const REACT_APP_INFURA_URL_MAINNET = process.env.REACT_APP_INFURA_URL_MAINNET
const web3Provider = new ethers.providers.JsonRpcProvider(REACT_APP_INFURA_URL_MAINNET)

export const getPrice = async(inputAmount, slippageAmount, deadline, walletAddress, token0, token1, decimals, dexName) => {
  const router = new AlphaRouter({ chainId: 1, provider: web3Provider })
  const percentSlippage = new Percent(slippageAmount, 100)
  const wei = ethers.utils.parseUnits(inputAmount.toString(), decimals)
  // const wei = inputAmount * Math.pow(10, decimals)
  const currencyAmount = CurrencyAmount.fromRawAmount(token0, JSBI.BigInt(wei))
  // const currencyAmount = CurrencyAmount.fromRawAmount(token0, wei)
  const route = await router.route(
    currencyAmount,
    token1,
    TradeType.EXACT_INPUT,
    {
      recipient: walletAddress,
      slippageTolerance: percentSlippage,
      deadline: deadline
    }
  )

  const transaction = {
    data: route.methodParameters.calldata,
    to: dexName === 'Uni Swap' ? V3_SWAP_ROUTER_ADDRESS : SUSHI_ROUTER_ADDRESS,
    value: BigNumber.from(route.methodParameters.value),
    from: walletAddress,
    gasPrice: BigNumber.from(route.gasPriceWei),
    gasLimit: ethers.utils.hexlify(1000000)
  }

  const quoteAmountOut = route.quote.toFixed(3)
  const ratio = (inputAmount / (route.quote.toFixed(6))).toFixed(9)

  return [
    transaction,
    quoteAmountOut,
    ratio
  ]
}

export const runSwap = async(transaction, contract, dexName) => {
  const provider = new ethers.providers.Web3Provider(window.ethereum)
  const signer = provider.getSigner()
  const approvalAmount = ethers.utils.parseUnits('10', 18).toString()
  await contract.connect(signer).approve(
    dexName === 'Uni Swap' ? V3_SWAP_ROUTER_ADDRESS : SUSHI_ROUTER_ADDRESS,
    approvalAmount
  )
  const sendTxn = await signer.sendTransaction(transaction)

  const reciept = await sendTxn.wait()

  if (reciept) {
    const txnHash = await sendTxn.hash
    await notification.success({
      description: `Navigate to https://mainnet.etherscan.io/txn/${txnHash} to see your transaction`
    })
  }
}

