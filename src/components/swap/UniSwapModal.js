import React, { useEffect, useState } from 'react'
import { Button, Input, Modal } from 'antd'
import FromModal from './FromModal'
import ToModal from './ToModal'
import BeatLoader from 'react-spinners/BeatLoader'
import { DownOutlined } from '@ant-design/icons'
import { getPrice } from './RouterService'
import { ethers } from 'ethers'

const UniSwapModal = (props) => {
  const addressSigner = window.sessionStorage.getItem('address')

  const [tokenSymbol0, setTokenSymbol0] = useState('')
  const [tokenSymbol1, setTokenSymbol1] = useState('')
  const [isFromModal, setIsFromModal] = useState(false)
  const [isToModal, setIsToModal] = useState(false)
  const [tokenDecimals0, setTokenDecimals0] = useState(18)
  const [token0, setToken0] = useState(undefined)
  const [token1, setToken1] = useState(undefined)
  const [token0Contract, setToken0Contract] = useState(undefined)
  const [token1Contract, setToken1Contract] = useState(undefined)
  const [inputAmount, setInputAmount] = useState('')
  const [outputAmount, setOutputAmount] = useState(0)
  const [loading, setLoading] = useState(false)
  const [token0Balance, setToken0Balance] = useState(0)
  const [token1Balance, setToken1Balance] = useState(0)

  const openFromModal = () => {
    setIsFromModal(true)
  }

  const openToModal = () => {
    setIsToModal(true)
  }

  const getSwapPrice = () => {
    setLoading(true)
    getPrice(
      inputAmount && inputAmount,
      props.slippageAmount,
      Math.floor(Date.now() / 1000 + (props.deadlineMinutes * 60)),
      addressSigner,
      token0 && token0,
      token1 && token1,
      tokenDecimals0 && tokenDecimals0,
      props.dexName
    ).then(data => {
      console.log('DATA', data)
      props.setTransaction(data[0])
      setOutputAmount(data[1])
      setLoading(false)
    })
  }

  useEffect(()=>{
    // console.log('Token changed !!!!')
    token0Contract?.balanceOf(addressSigner)
      .then(res => {
        props.setToken0Contract(token0Contract)
        setToken0Balance(Number(ethers.utils.formatEther(res)))
      })
    token1Contract?.balanceOf(addressSigner)
      .then(res => {
        setToken1Balance(Number(ethers.utils.formatEther(res)))
      })
  }, [token0Contract, token1Contract, addressSigner])

  return (
    <div className='uni-swap__content'>
      <div className='swap-page__modal--uni-swap'>
        <div className='swap-page__modal--uni-swap-from'>
          <div className='select-coin-part'>
            <div className='swap-span'>From</div>
            { addressSigner
              ? (
                tokenSymbol0 === '' ? (
                  <Button
                    onClick={openFromModal}
                  >Select coin <DownOutlined /></Button>
                )
                  : (
                    <Button
                      onClick={openFromModal}
                      style={{ background: '#ffa959' }}
                    >{tokenSymbol0}</Button>
                  )
              )
              : (
                <Button className='swap-coin-btn' disabled>
                  Select coin <DownOutlined />
                </Button>
              )}
          </div>
          <div className='quantity-part'>
            <Input
              onPressEnter={getSwapPrice}
              onChange={(e) => setInputAmount(e.target.value)}
              placeholder='Amount'
            />
            <div className='token-balance'>Balance: {token0Balance}</div>
          </div>
        </div>
        <div className='swap-page__modal--uni-swap-to'>
          <div className='select-coin-part'>
            <div className='swap-span'>To</div>
            { addressSigner
              ? (
                tokenSymbol1 === '' ? (
                  <Button
                    onClick={openToModal}
                  >Select coin <DownOutlined /></Button>
                )
                  : (
                    <Button
                      onClick={openToModal}
                      style={{ background: '#ffa959' }}
                    >{tokenSymbol1}</Button>
                  )
              )
              : (
                <Button className='swap-coin-btn' disabled>
                  Select coin <DownOutlined />
                </Button>
              )}
          </div>
          <div className='quantity-part'>
            <div className='token-amount'>{ loading ? <BeatLoader /> : outputAmount}</div>
            <div className='token-balance'>Balance: {token1Balance}</div>
          </div>
        </div>
      </div>

      <Modal
        className='from-modal'
        visible={isFromModal}
        onOk={() => setIsFromModal(false)}
        onCancel={() => setIsFromModal(false)}
        footer={null}
      >
        <FromModal
          dexName={props.dexName}
          setIsFromModal={setIsFromModal}
          setTokenSymbol0={setTokenSymbol0}
          setTokenDecimals0={setTokenDecimals0}
          setToken0Contract={setToken0Contract}
          setToken0={setToken0}
          chainId={props.chainId}
        />
      </Modal>

      <Modal
        className='to-modal'
        visible={isToModal}
        onOk={() => setIsToModal(false)}
        onCancel={() => setIsToModal(false)}
        footer={null}
      >
        <ToModal
          dexName={props.dexName}
          setIsToModal={setIsToModal}
          setTokenSymbol1={setTokenSymbol1}
          setToken1Contract={setToken1Contract}
          setToken1={setToken1}
          chainId={props.chainId}
        />
      </Modal>
    </div>
  )
}

export default UniSwapModal
