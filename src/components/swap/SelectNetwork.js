import React from 'react'
import { Button, notification } from 'antd'

const BSC_MAINNET_CHAINID = process.env.REACT_APP_BSC_MAINNET_CHAINID
const ETHEREUM_MAINNET_CHAINID = process.env.REACT_APP_ETHEREUM_MAINNET_CHAINID

export const SelectNetwork = (props) => {
  const changeNetwork = async(id) => {
    try {
      await window.ethereum.request({
        method: 'wallet_switchEthereumChain',
        params: [{ chainId: id }]
      })
    } catch (error) {
      notification.error({
        message: 'Error',
        description: error.message
      })
    }
  }

  const handleChangeIntoETH = () => {
    changeNetwork(ETHEREUM_MAINNET_CHAINID)
    props.setChainId(ETHEREUM_MAINNET_CHAINID)
    props.setSymbolNetwork('ETH')
    props.setIsSelectNetworkModal(false)
  }

  const handleChangeIntoBNB = () => {
    changeNetwork(BSC_MAINNET_CHAINID)
    props.setChainId(BSC_MAINNET_CHAINID)
    props.setSymbolNetwork('BNB')
    props.setIsSelectNetworkModal(false)
  }

  return (
    <div className='select-network-form'>
      <Button onClick={handleChangeIntoETH}>
        Ethereum Mainnet Network
      </Button>
      <Button onClick={handleChangeIntoBNB}>
        Binance Smart Chain Network
      </Button>
    </div>
  )
}

