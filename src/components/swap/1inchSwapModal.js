import React, { useEffect, useState } from 'react'
import { Button, Input, Modal } from 'antd'
import FromModal from './FromModal'
import ToModal from './ToModal'
import BeatLoader from 'react-spinners/BeatLoader'
import { DownOutlined } from '@ant-design/icons'
import { ethers } from 'ethers'
import axios from 'axios'

const oneInchSwapModal = (props) => {
  const addressSigner = window.sessionStorage.getItem('address')

  const [tokenSymbol0, setTokenSymbol0] = useState('')
  const [tokenSymbol1, setTokenSymbol1] = useState('')
  const [isFromModal, setIsFromModal] = useState(false)
  const [isToModal, setIsToModal] = useState(false)
  const [tokenDecimals0, setTokenDecimals0] = useState(undefined)
  const [tokenDecimals1, setTokenDecimals1] = useState(undefined)
  const [token0, setToken0] = useState(undefined)
  const [token1, setToken1] = useState(undefined)
  const [token0Contract, setToken0Contract] = useState(undefined)
  const [token1Contract, setToken1Contract] = useState(undefined)
  const [inputAmount, setInputAmount] = useState('')
  const [outputAmount, setOutputAmount] = useState(0)
  const [loading, setLoading] = useState(false)
  const [token0Balance, setToken0Balance] = useState(0)
  const [token1Balance, setToken1Balance] = useState(0)
  const [token0Address, setToken0Address] = useState(undefined)
  const [token1Address, setToken1Address] = useState(undefined)
  const [bestQuote, setBestQuote] = useState()

  console.log(token0, token1, tokenDecimals0, token1Balance)
  console.log('DECIMAL S1', tokenDecimals1)

  const openFromModal = () => {
    setIsFromModal(true)
  }

  const openToModal = () => {
    setIsToModal(true)
  }

  const getBestQuote = async(value) => {
    await axios.get(`https://api.1inch.io/v4.0/1/quote?fromTokenAddress=${token0Address && token0Address}&toTokenAddress=${token1Address && token1Address}&amount=${value}`).then(
      result => {
        const toTokenAmountInBigInt = result?.data?.toTokenAmount
        const pow = ethers.utils.parseUnits('1', tokenDecimals1).toString()
        setBestQuote(toTokenAmountInBigInt / pow)
      }
    )
    await setLoading(false)
  }

  const getSwapPrice = () => {
    setLoading(true)
    setOutputAmount(0)
    const amount = ethers.utils.parseUnits(inputAmount && inputAmount, tokenDecimals0)
    getBestQuote(amount)
  }

  useEffect(()=>{
    // console.log('Token changed !!!!')
    token0Contract?.balanceOf(addressSigner)
      .then(res => {
        props.setToken0Contract(token0Contract)
        setToken0Balance(Number(ethers.utils.formatEther(res)))
      })
    token1Contract?.balanceOf(addressSigner)
      .then(res => {
        setToken1Balance(Number(ethers.utils.formatEther(res)))
      })
  }, [token0Contract, token1Contract, addressSigner])

  return (
    <div className='uni-swap__content'>
      <div className='swap-page__modal--uni-swap'>
        <div className='swap-page__modal--uni-swap-from'>
          <div className='select-coin-part'>
            <div className='swap-span'>From</div>
            { addressSigner
              ? (
                tokenSymbol0 === '' ? (
                  <Button
                    onClick={openFromModal}
                  >Select coin <DownOutlined /></Button>
                )
                  : (
                    <Button
                      onClick={openFromModal}
                      style={{ background: '#ffa959' }}
                    >{tokenSymbol0}</Button>
                  )
              )
              : (
                <Button className='swap-coin-btn' disabled>
                  Select coin <DownOutlined />
                </Button>
              )}
          </div>
          <div className='quantity-part'>
            <Input
              onPressEnter={getSwapPrice}
              onChange={(e) => setInputAmount(e.target.value)}
              placeholder='Amount'
            />
            <div className='token-balance'>Balance: {token0Balance}</div>
          </div>
        </div>
        <div className='swap-page__modal--uni-swap-to'>
          <div className='select-coin-part'>
            <div className='swap-span'>To</div>
            { addressSigner
              ? (
                tokenSymbol1 === '' ? (
                  <Button
                    onClick={openToModal}
                  >Select coin <DownOutlined /></Button>
                )
                  : (
                    <Button
                      onClick={openToModal}
                      style={{ background: '#ffa959' }}
                    >{tokenSymbol1}</Button>
                  )
              )
              : (
                <Button className='swap-coin-btn' disabled>
                  Select coin <DownOutlined />
                </Button>
              )}
          </div>
          <div className='quantity-part'>
            <div className='token-amount'>{ loading ? <BeatLoader /> : outputAmount}</div>
            <div className='best-quote'>{loading ? <BeatLoader /> : bestQuote}</div>
          </div>
        </div>
        <div className='swap-page__modal--uni-swap-quote'>

        </div>
      </div>

      <Modal
        className='from-modal'
        visible={isFromModal}
        onOk={() => setIsFromModal(false)}
        onCancel={() => setIsFromModal(false)}
        footer={null}
      >
        <FromModal
          dexName={props.dexName}
          setIsFromModal={setIsFromModal}
          setTokenSymbol0={setTokenSymbol0}
          setTokenDecimals0={setTokenDecimals0}
          setToken0Contract={setToken0Contract}
          setToken0={setToken0}
          setToken0Address={setToken0Address}
        />
      </Modal>

      <Modal
        className='to-modal'
        visible={isToModal}
        onOk={() => setIsToModal(false)}
        onCancel={() => setIsToModal(false)}
        footer={null}
      >
        <ToModal
          dexName={props.dexName}
          setIsToModal={setIsToModal}
          setTokenSymbol1={setTokenSymbol1}
          setToken1Contract={setToken1Contract}
          setToken1={setToken1}
          setToken1Address={setToken1Address}
          setTokenDecimals1={setTokenDecimals1}
        />
      </Modal>
    </div>
  )
}

export default oneInchSwapModal

