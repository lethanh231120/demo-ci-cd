import { ethers } from 'ethers'
import { Fetcher, Route, Trade, TokenAmount, TradeType, Percent } from '@pancakeswap/sdk'
import PANCAKESWAP_ROUTER_ABI from '../../abi/PancakeRouterabi.json'
import { notification } from 'antd'

const PANCAKE_ROUTER_ADDRESS = process.env.REACT_APP_PANCAKE_SWAP_ROUTER_ADDRESS
const web3ProviderBSC = new ethers.providers.JsonRpcProvider('https://bsc-dataseed.binance.org/', { name: 'binance', chainId: 56 })
const PANCAKE_SWAP_CONTRACT = new ethers.Contract(PANCAKE_ROUTER_ADDRESS, PANCAKESWAP_ROUTER_ABI, web3ProviderBSC)

export const getPrice = async(token0, token1, inputAmount, slippageAmount, deadlineTime, walletAddress, decimal) => {
  const pair = await Fetcher.fetchPairData(token0, token1, web3ProviderBSC)
  const route = await new Route([pair], token1)
  let amountIn = ethers.utils.parseUnits(inputAmount.toString(), decimal)
  amountIn = amountIn.toString()
  const slippageTolerance = new Percent(slippageAmount, 100)

  const trade = new Trade(
    route,
    new TokenAmount(token1, amountIn),
    TradeType.EXACT_INPUT
  )

  const amountOutMin = trade.minimumAmountOut(slippageTolerance).raw
  const amountOutMinHex = ethers.BigNumber.from(amountOutMin.toString()).toHexString()
  const path = [token1.address, token0.address]
  const to = walletAddress
  const deadline = Math.floor(Date.now() / 1000) + 60 * deadlineTime
  const value = trade.inputAmount.raw
  // const value = 0.25
  const valueHex = ethers.BigNumber.from(value.toString()).toHexString()

  const amounts = await PANCAKE_SWAP_CONTRACT.getAmountsOut(amountIn, path)
  // Return a copy of transactionRequest, The default implementation calls checkTransaction and resolves to if it is an ENS name, adds gasPrice, nonce, gasLimit and chainId based on the related operations on Signer.
  const rawTxn = await PANCAKE_SWAP_CONTRACT.populateTransaction.swapExactTokensForTokens(valueHex, amountOutMinHex, path, to, deadline)

  return [rawTxn, amounts]
}

export const runSwapBSC = async(contract, transaction) => {
  const provider = new ethers.providers.Web3Provider(window.ethereum)
  const signer = provider.getSigner()
  const approvalAmount = ethers.utils.parseUnits('10', 18).toString()
  await contract.connect(signer).approve(
    PANCAKE_ROUTER_ADDRESS,
    approvalAmount
  )
  // Returns a Promise which resolves to the transaction.
  const sendTxn = await signer.sendTransaction(transaction)

  // Resolves to the TransactionReceipt once the transaction has been included in the chain for x confirms blocks.
  const reciept = await sendTxn.wait()

  if (reciept) {
    const txnHash = await sendTxn.hash
    const blockNumber = await reciept.blockNumber
    await notification.success({
      description: `- Transaction is mined - ' '\n'
      'Transaction Hash:', ${txnHash} '\n' 
      'Block Number: ' ${blockNumber}'\n'
      'Navigate to https://mainnet.etherscan.io/txn/'${txnHash}
      , 'to see your transaction`
    })
  }
}
