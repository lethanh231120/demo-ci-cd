import React, { useEffect, useState, useContext } from 'react'
import { DeleteOutlined, EditOutlined } from '@ant-design/icons'
import { Button, Col, Row, Tabs, Divider, Input, Popconfirm } from 'antd'
import './styles.scss'
import { delConnection } from '../../api/connectService'
// import ModalContent from '../modal/onchain-connect'
import ModalEdit from '../modal/modal-edit/EditConnection'
import { useQueryClient } from '@tanstack/react-query'
import { PlatFormContext } from '../../layouts'
import ModalConnectOther from '../modal/onchain-connect'

const { TabPane } = Tabs

const WalletAddress = (props) => {
  const { holdings, connectionName, setConnectionName, connections, setIsOwnPersentage, isOwnPersentage } = props
  // const [isModalVisible, setIsModalVisible] = useState(false)
  const [isModalEdit, setIsModalEdit] = useState(false)
  const [dataEdit, setDataEdit] = useState(false)
  const [filterConnection, setFilterConnection] = useState()
  const [indexDelete, setIndexDelete] = useState()
  const { Search } = Input
  const queryClient = useQueryClient()
  const state = useContext(PlatFormContext)
  const [isModalConnectWallet, setIsModalConnectWallet] = useState(false)

  const handleConnectPortfolio = () => {
    setIsModalConnectWallet(true)
  }

  useEffect(() => {
    setFilterConnection(connections)
  }, [connections])

  const onChange = (value) => {
    setFilterConnection(holdings && holdings?.assetsData?.filter((item) => {
      return item.connectionName.toLowerCase().includes(value.toLowerCase())
    }))
  }

  const handleDeleteConnection = async(item, index) => {
    // 1 api
    try {
      await delConnection(`connect/connectionId=${item.id}`)
      queryClient.invalidateQueries('connections')
      setIndexDelete(index)
      state.handleSetData({
        addressDelete: item.address,
        listNft: state?.propsImport?.listNft,
        listHolding: state?.propsImport?.listHolding,
        initial: false
      })
    } catch (error) {
      console.log(error)
    }
  }

  useEffect(() => {
    if ((indexDelete !== undefined) && (filterConnection !== undefined)) {
      if ((indexDelete + 1) > filterConnection?.length) {
        setConnectionName((filterConnection[filterConnection?.length - 1])?.id)
      } else {
        filterConnection && setConnectionName(filterConnection[indexDelete]?.id)
      }
    }
  }, [filterConnection, indexDelete])

  const handleEditAddress = (item) => {
    setIsModalEdit(true)
    setDataEdit(item)
  }

  return (
    <div className='sidebar'>
      <Row>
        <Col span={24}>
          <Button onClick={handleConnectPortfolio}>CONNECT PORTFOLIO</Button>
        </Col>
        <Divider />
        <Search
          placeholder= 'input search text'
          onChange={(e) => onChange(e.target.value)}
        />
        <Col span={24}>
          <div className='sidebar-connection'>
            <Tabs tabPosition='right' onTabClick={(e) => setConnectionName(e)} value={connectionName}>
              <TabPane tab='All Assets' key='all' style={{ textAlign: 'left', width: '100%' }}/>
              {filterConnection && filterConnection.length > 0 && filterConnection.map((item, index) => (
                <TabPane
                  tab={
                    <div className='tab-list-item'>
                      <div className='tab-list-item-text'><span>{item.connectionName}</span></div>
                      <div className='tab-list-icon'>
                        <Button onClick={() => {
                          setIsOwnPersentage({
                            isOwnPersentage: !isOwnPersentage,
                            ownPersentage: item?.ownPersentage
                          })
                        }}>Chi tiết</Button>
                        <Popconfirm
                          placement='top'
                          className='popover'
                          title='Are you sure to delete this connection?'
                          onConfirm={() => handleDeleteConnection(item, index)}
                          okText='Yes'
                          cancelText='No'
                        >
                          <DeleteOutlined className='tab-list-icon-item' />
                        </Popconfirm>
                        <EditOutlined className='tab-list-icon-item' onClick={() => handleEditAddress(item)}/>
                      </div>
                    </div>
                  }
                  key={item.id}
                />
              ))}
            </Tabs>
          </div>
        </Col>
      </Row>
      <ModalConnectOther
        isModalConnectWallet={isModalConnectWallet}
        setIsModalConnectWallet={setIsModalConnectWallet}
      />
      <ModalEdit
        isModalEdit={isModalEdit}
        setIsModalEdit={setIsModalEdit}
        dataEdit={dataEdit}
      />
    </div>
  )
}

export default WalletAddress
