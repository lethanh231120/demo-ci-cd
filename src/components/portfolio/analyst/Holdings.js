import React, { useState, useEffect } from 'react'
import { Tabs, Table, Popover } from 'antd'
import { EllipsisOutlined, CaretUpOutlined, CaretDownOutlined } from '@ant-design/icons'
// import { EllipsisOutlined, CaretUpOutlined, CaretDownOutlined } from '@ant-design/icons'
import '../../portfolio/styles.scss'
import { ListGroupNFT } from '../../nft/ListGroupNft'
import { BILLION } from '../../../constants/TypeConstants'
import { LazyLoadImage } from 'react-lazy-load-image-component'
import Chart from '../../chart/LineChart'
import { DEFAULT_PAGE, LIMIT } from '../../../constants/params'
// import _ from 'lodash'

const { TabPane } = Tabs

const Holdings = ({ dataConnection, isGroupNFT, setIsGroupNFT, isLoading }) => {
  const [data, setData] = useState([])
  const [page, setPage] = useState({
    page: DEFAULT_PAGE,
    pageSize: LIMIT
  })

  const [selectedRowKeys, setSelectedRowKeys] = useState([
    'rank',
    'name',
    'price',
    'amount',
    'marketCap',
    'volumn',
    'priceChange1w',
    // 'priceChange7d',
    'total',
    'totalChange24h',
    'priceGraph',
    'percentChange24',
    'priceChange24h',
    'low24h',
    'high24h'
  ])

  const columnsPopover = [
    {
      title: 'Title',
      dataIndex: 'title',
      width: '85%',
      render: (_, record) => (<span style={{ color: '#A8ADB3' }}>{record.title}</span>)
    }
  ]

  const items = [
    {
      key: 'rank',
      title: '#'
    },
    {
      key: 'name',
      title: 'Name'
    },
    {
      key: 'amount',
      title: 'Amount'
    },
    {
      key: 'high24h',
      title: 'High 24h'
    },
    {
      key: 'low24h',
      title: 'Low 24h'
    },
    {
      key: 'percentChange24',
      title: 'Percent Change 24H'
    },
    {
      key: 'priceChange24h',
      title: 'Price Change 24H'
    },
    {
      key: 'total',
      title: 'Total'
    },
    {
      key: 'totalChange24h',
      title: 'Total Change 24H'
    },
    // {
    //   key: 'priceChange7d',
    //   title: '7d Change'
    // },
    {
      key: 'price',
      title: 'Price'
    },
    {
      key: 'marketCap',
      title: 'Market Cap'
    },
    {
      key: 'volumn',
      title: 'Volumn 24h'
    },
    {
      key: 'priceGraph',
      title: 'Price Graph (7d)'
    }
  ]

  useEffect(() => {
    setData(dataConnection && dataConnection?.holdings)
  }, [dataConnection])

  const onSelectChange = (newselectedRowKeys) => {
    setSelectedRowKeys(newselectedRowKeys)
  }

  const rowSelection = {
    selectedRowKeys,
    onChange: onSelectChange
  }

  const renderPrice = (priceCoin) => {
    let price
    if ((priceCoin >= 0) && (priceCoin < 1)) {
      price = priceCoin.toFixed(5).replace(/\d(?=(\d{3})+\.)/g, '$&,')
    }
    if (priceCoin >= 1) {
      price = priceCoin.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')
    }
    return price
  }

  const columns = [
    {
      key: '#',
      title: '#',
      width: '20px',
      hidden: !selectedRowKeys.includes('rank'),
      render: (_, record, index)=>(<span style={{ color: '#A8ADB3' }}>{(page.page - 1) * page.pageSize + index + 1}</span>)
    },
    {
      key: 'name',
      title: <span style={{ textAlign: 'left !important' }}>Name</span>,
      className: 'table-name',
      width: '250px',
      hidden: !selectedRowKeys.includes('name'),
      sorter: (a, b) => a.coinName.toLowerCase().localeCompare(b.coinName.toLowerCase()),
      render: (_, record) => (<div>
        <div className='table-icon-coin'>
          {record.coinImage
            ? (
              <LazyLoadImage src={record.coinImage}/>
            ) : (
              <span className='table-icon-coin-logo'>{record.symbol.slice(0, 3)}</span>
            )}
        </div>
        <div className='table-name-content'>
          <div className='table-name-text'>
            <span>
              {record?.coinName ? record?.coinName : ''}
            </span>
          </div>
          <div className='table-name-symbol'>
            <span>
              {record?.symbol ? record?.symbol.toUpperCase() : ''}
            </span>
          </div>
        </div>
      </div>)
    },
    {
      key: 'price',
      title: 'Price',
      dataIndex: 'price',
      width: '150px',
      sorter: (a, b) => a.price - b.price,
      hidden: !selectedRowKeys.includes('price'),
      render: (_, record) => (<span className='amount'>
        ${renderPrice(record?.price)}
      </span>)
    },
    {
      key: 'amount',
      title: <span style={{ textAlign: 'left !important' }}>Amount</span>,
      width: '120px',
      hidden: !selectedRowKeys.includes('amount'),
      sorter: {
        compare: (a, b) => a.amount - b.amount,
        multiple: 1
      },
      render: (_, record) => (<span className='amount'>
        {(record?.amount * (1 / Math.pow(10, record?.decimals)))?.toFixed(5).replace(/\d(?=(\d{3})+\.)/g, '$&,')}
        {/* {record?.amount} */}
      </span>)
    },
    {
      key: 'total',
      title: 'Total',
      dataIndex: 'total',
      width: '150px',
      sorter: (a, b) => a.total - b.total,
      hidden: !selectedRowKeys.includes('total'),
      render: (_, record) => (<span className='amount'>
        {/* ${record?.total} */}
        ${(record?.total * (1 / Math.pow(10, record?.decimals)))?.toFixed(5).replace(/\d(?=(\d{3})+\.)/g, '$&,')}
      </span>)
    },
    {
      key: 'high24h',
      title: 'High 24h',
      width: '250px',
      hidden: !selectedRowKeys.includes('high24h'),
      render: (_, record) => (
        <div className='price-change'>
          $ {(record?.high24h * (1 / Math.pow(10, record?.decimals)))?.toFixed(5).replace(/\d(?=(\d{3})+\.)/g, '$&,')}
        </div>
      ),
      sorter: (a, b) => a.high24h - b.high24h
    },
    {
      key: 'low24h',
      title: 'Low 24H',
      width: '250px',
      hidden: !selectedRowKeys.includes('low24h'),
      render: (_, record = '12') => (
        <div className='price-change'>
          $ {(record?.low24h * (1 / Math.pow(10, record?.decimals)))?.toFixed(5).replace(/\d(?=(\d{3})+\.)/g, '$&,')}
        </div>
      ),
      sorter: (a, b) => a.low24h - b.low24h
    },
    {
      key: 'priceChange24h',
      title: 'Price Change (24h)',
      sorter: (a, b) => a.priceChange24h - b.priceChange24h,
      width: '200px',
      hidden: !selectedRowKeys.includes('priceChange24h'),
      render: (_, record) => (
        <div className='price-change'
          style={{
            color: record.priceChange24h ? (record.priceChange24h >= 0 ? '#34b349' : '#ff4d4d') : '#34b349'
          }}
        >
          $ {record.priceChange24h ? parseFloat(record.priceChange24h >= 0 ? record.priceChange24h : record.priceChange24h.toString().slice(1)).toFixed(4).replace(/\d(?=(\d{3})+\.)/g, '$&,') : '0.00'}
        </div>
      )
    },
    {
      key: 'percentChange24',
      title: 'Percent 24H',
      sorter: (a, b) => a.priceChangePercentage24h - b.priceChangePercentage24h,
      width: '150px',
      hidden: !selectedRowKeys.includes('percentChange24'),
      render: (_, record) => (
        <div className='price-change'
          style={{
            backgroundColor: record.priceChangePercentage24h ? (record.priceChangePercentage24h >= 0 ? 'rgba(52, 199, 89, 0.1)' : 'rgba(255,53,53,0.1)') : 'rgba(52, 199, 89, 0.1)',
            color: record.priceChangePercentage24h ? (record.priceChangePercentage24h >= 0 ? '#34b349' : '#ff4d4d') : '#34b349'
          }}
        >
          {record.priceChangePercentage24h ? (record.priceChangePercentage24h >= 0 ? <CaretUpOutlined/> : <CaretDownOutlined/>) : <CaretUpOutlined/>}
          {record.priceChangePercentage24h ? parseFloat(record.priceChangePercentage24h >= 0 ? record.priceChangePercentage24h : record.priceChangePercentage24h.toString().slice(1)).toFixed(2) : '0.00'} %
        </div>
      )
    },
    {
      key: 'totalChange24h',
      title: 'Total Change 24h',
      dataIndex: 'totalChange24h',
      width: '250px',
      sorter: (a, b) => a.totalChange24h - b.totalChange24h,
      hidden: !selectedRowKeys.includes('totalChange24h'),
      render: (_, record) => (
        <div className='price-change'
          style={{
            color: record.totalChange24h ? (record.totalChange24h >= 0 ? '#34b349' : '#ff4d4d') : '#34b349'
          }}
        >
          $ {record.totalChange24h ? parseFloat(record.totalChange24h >= 0 ? (record.totalChange24h * (1 / Math.pow(10, record?.decimals))) : (record.totalChange24h * (1 / Math.pow(10, record?.decimals))).toString().slice(1)).toFixed(5).replace(/\d(?=(\d{3})+\.)/g, '$&,') : '0.00'}
        </div>
      )
    },
    // {
    //   key: 'priceChange1w',
    //   title: '7d Change',
    //   sorter: (a, b) => a.percentChange7d - b.percentChange7d,
    //   width: '150px',
    //   hidden: !selectedRowKeys.includes('priceChange7d'),
    //   render: (_, record) => (
    //     <div className='price-change'
    //       style={{
    //         backgroundColor: record.percentChange7d ? (record.percentChange7d >= 0 ? 'rgba(52, 199, 89, 0.1)' : 'rgba(255,53,53,0.1)') : 'rgba(52, 199, 89, 0.1)',
    //         color: record.percentChange7d ? (record.percentChange7d >= 0 ? '#34b349' : '#ff4d4d') : '#34b349'
    //       }}
    //     >
    //       {record.percentChange7d ? (record.percentChange7d >= 0 ? <CaretUpOutlined/> : <CaretDownOutlined/>) : <CaretUpOutlined/>}
    //       {record.percentChange7d ? parseFloat(record.percentChange7d >= 0 ? record.percentChange7d : record.percentChange7d.toString().slice(1)).toFixed(2) : '0.00'} %
    //     </div>
    //   )
    // },
    {
      key: 'marketCap',
      title: 'Market Cap',
      sorter: (a, b) => a.marketCap - b.marketCap,
      width: '150px',
      defaultSortOrder: 'descend',
      hidden: !selectedRowKeys.includes('marketCap'),
      render: (_, record) => (<span style={{ color: '#A8ADB3' }}>
        $ {(record.marketCap * BILLION).toFixed(5).replace(/\d(?=(\d{3})+\.)/g, '$&,')} B
      </span>)
    },
    {
      key: 'volumn',
      title: 'Volumn 24h',
      sorter: (a, b) => a.volumn - b.volumn,
      width: '150px',
      hidden: !selectedRowKeys.includes('volumn'),
      render: (_, record) => (<span style={{ color: '#A8ADB3' }}>
        $ {record?.volumn ? (record?.volumn * BILLION).toFixed(5).replace(/\d(?=(\d{3})+\.)/g, '$&,') : 0} B
      </span>)
    },
    {
      title: 'Price Graph (7d)',
      dataIndex: 'priceGraph',
      width: '150px',
      className: 'table-graph',
      hidden: !selectedRowKeys.includes('priceGraph'),
      render: (_, record) => (<Chart record={record?.priceChart7d} change7d={record.percentChange7d}/>)
    },
    {
      key: 'more',
      title: <Popover
        placement='bottomRight'
        content={(<Table
          showHeader={false}
          scroll={{
            y: 260
          }}
          style={{ maxWidth: '290px' }}
          className='tableabc'
          rowSelection={rowSelection}
          pagination={false}
          columns={columnsPopover}
          dataSource={items}
        >
        </Table>)}
        trigger='click'
      >
        +
      </Popover>,
      className: 'table-plus',
      width: '20px',
      dataIndex: 'key',
      render: (_, record) => (
        <Popover
          placement='bottomRight'
          content={(<Table
            showHeader={false}
            scroll={{
              y: 260
            }}
            style={{ maxWidth: '290px' }}
            className='tableabc'
            rowSelection={rowSelection}
            pagination={false}
            columns={columnsPopover}
            dataSource={items}
          >
          </Table>)}
          trigger='click'
        >
          <EllipsisOutlined className='table-row-item-icon' style={{ color: '#fff', fontSize: '20px' }}/>
        </Popover>
      )
    }
  ].filter(item => !item.hidden)

  return (
    <Tabs defaultActiveKey='1'>
      <TabPane tab='Holdings' key='1'>
        <Table
          loading={isLoading}
          columns={columns}
          dataSource={data}
          scroll={{ x: 'max-content' }}
          showSorterTooltip={false}
          rowKey={(record, index) => `${(page.page - 1) * page.pageSize + index + 1}`}
          pagination={{
            defaultCurrent: page.page,
            pageSize: page.pageSize,
            showSizeChanger: false,
            onChange(current) {
              setPage({ ...page, page: current })
            }
          }}
        />
      </TabPane>
      {/* {(dataConnection && !_.isEmpty(dataConnection?.nft[0])) ? ( */}
      <TabPane tab='NFT' key='2'>
        <ListGroupNFT
          dataConnection={dataConnection}
          isGroupNFT={isGroupNFT}
          setIsGroupNFT={setIsGroupNFT}
          isLoading={isLoading}
        />
      </TabPane>
      {/* ) */}
      {/* : ''} */}
      <TabPane tab='Charts' key='3'>
        Content of Tab Pane 3
      </TabPane>
      <TabPane tab='Transactions' key='4'>
        Content of Tab Pane 4
      </TabPane>
    </Tabs>
  )
}

export default Holdings
