import React from 'react'
import { Col, Row, Select } from 'antd'
import { CaretUpOutlined, CaretDownOutlined } from '@ant-design/icons'
import LineChartAsset from '../../chart/LineChartAsset'

const { Option } = Select
export const HeaderAnalyst = ({ totalValue, handleChangePeriod, dataChart, period, percentInfo }) => {
  return (
    <Col span={24}>
      <Row>
        <Col span='10'>
          <Col span={24}>
            <div className='main-price'>${totalValue?.toFixed(5).replace(/\d(?=(\d{3})+\.)/g, '$&,')}</div>
          </Col>
          <Col span={24}>
            <div className='data-change'>
              <span
                className='change-price-up'
                style={{
                  color: percentInfo ? (percentInfo?.balanceFluctuations >= 0 ? '#34b349' : '#ff4d4d') : '#34b349'
                }}
              >
                $ {percentInfo?.balanceFluctuations ? parseFloat(percentInfo?.balanceFluctuations >= 0 ? percentInfo?.balanceFluctuations : percentInfo?.balanceFluctuations.toString().slice(1)).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,') : 0}
              </span>
              <span>
                <span
                  className='change-price-up'
                  style={{
                    color: percentInfo ? (percentInfo?.balanceFluctuations >= 0 ? '#34b349' : '#ff4d4d') : '#34b349'
                  }}
                >
                  {percentInfo?.balanceFluctuations >= 0 ? <CaretUpOutlined/> : <CaretDownOutlined/>}
                </span>
                <span
                  className='change-price-up'
                  style={{
                    color: percentInfo ? (percentInfo?.balanceFluctuations >= 0 ? '#34b349' : '#ff4d4d') : '#34b349'
                  }}
                >
                  {percentInfo?.percent ? parseFloat(percentInfo?.percent >= 0 ? percentInfo?.percent : percentInfo?.percent.toString().slice(1)) : 0} %
                </span>
              </span>
              <Select
                defaultValue={period}
                onChange={handleChangePeriod}
                color='rgba(0,0,0,.85)'
              >
                <Option value='1h'>1H</Option>
                <Option value='1d'>24H</Option>
                <Option value='1w'>1W</Option>
                <Option value='1m'>1M</Option>
                <Option value='3m'>3M</Option>
                <Option value='6m'>6M</Option>
                <Option value='1y'>1Y</Option>
                <Option value='all'>ALL</Option>
              </Select>
            </div>
          </Col>
        </Col>
        <Col span='14'>
          <div style={{ textAlign: 'right', display: 'flex', justifyContent: 'center' }}>
            <LineChartAsset dataChart={dataChart && dataChart} width={300} height={80}/>
          </div>
        </Col>
      </Row>
    </Col>
  )
}
