import React, { memo } from 'react'
import { Col, Row } from 'antd'
import Holdings from './Holdings'
import '../styles.scss'
import { HeaderAnalyst } from './HeaderAnalyst'

const Analyst = (props) => {
  const { dataConnection, isGroupNFT, setIsGroupNFT, isLoading, handleChangePeriod, dataChart, period, percentInfo } = props

  return (
    <div className='dashboard'>
      <Row>
        <HeaderAnalyst
          totalValue={(dataConnection?.totalValue)}
          // totalValue={(dataConnection?.totalValue)?.toString()?.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')}
          handleChangePeriod={handleChangePeriod}
          dataChart={dataChart}
          percentInfo={percentInfo}
          period={period}
        />
        <Col span={24}>
          <Holdings
            dataConnection={dataConnection}
            isGroupNFT={isGroupNFT}
            setIsGroupNFT={setIsGroupNFT}
            isLoading={isLoading}
          />
        </Col>
      </Row>
    </div>
  )
}

export default memo(Analyst)
