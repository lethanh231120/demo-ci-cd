import React, { memo } from 'react'
import { LineChart, Line, YAxis, XAxis, CartesianGrid, Tooltip } from 'recharts'
import CustomTooltip from './CustomTooltip'
const ChartConnection = ({ dataChart }) => {
  const dataSort = [dataChart && dataChart].flat(1)
  dataSort?.sort((a, b) => a.totalValue - b.totalValue)
  return (
    <LineChart width={300} height={80} data={dataChart}>
      <YAxis domain={[dataSort && parseFloat(dataSort[0]?.totalValue), dataSort && parseFloat(dataSort[dataSort?.length - 1]?.totalValue)]} hide={true}/>
      <XAxis dataKey='timestamp' hide={true}/>
      <CartesianGrid vertical={false} horizontal={false}/>
      <Line
        type='linear'
        dot={false}
        dataKey='totalValue'
        stroke={dataChart && ((parseFloat(dataChart[0]?.totalValue) < parseFloat(dataChart[dataChart.length - 1]?.totalValue)) ? '#6ccf59' : '#ff4d4d')}
        strokeWidth={1}
      />
      <Tooltip
        content={<CustomTooltip payload={dataChart} />}
      />
    </LineChart>
  )
}
export default memo(ChartConnection)
