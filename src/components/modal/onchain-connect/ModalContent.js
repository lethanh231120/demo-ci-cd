import React, { memo } from 'react'
import { Row, Col, Image, Typography } from 'antd'
import { ArrowRightOutlined } from '@ant-design/icons'
import { useNavigate } from 'react-router-dom'
import { wallets } from '../../../utils/connects/OnchainWallet'

const { Text } = Typography

const ModalContent = () => {
  // When u click into a wallet, navigate your page to this wallet's import address page
  const navigate = useNavigate()
  const handleClickWallet = (item) => {
    navigate(`../../../connect/${item?.id}`, { state: { type: 'onchain' }})
  }

  return (
    <div className='content-modal'>
      <Row gutter={12}>
        {wallets.map((item, index) => (
          <Col span={8} key={index}>
            <div className='content-modal-item' onClick={() => handleClickWallet(item)}>
              <div className='content-modal-item-icon'>
                <Image
                  width={40}
                  preview={false}
                  src={item.icon}
                />
                <Text className='content-modal-item-text'>{item.name}</Text>
              </div>
              <ArrowRightOutlined className='content-modal-item-arrow'/>
            </div>
          </Col>
        ))}
      </Row>
    </div>
  )
}
export default memo(ModalContent)
