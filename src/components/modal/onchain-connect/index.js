import React, { memo } from 'react'
import { Modal } from 'antd'
import ModalHeader from './ModalHeader'
import ModalContent from './ModalContent'
import ModalFooter from './ModalFooter'
import './style.scss'

const ModalConnectOnchain = ({ isModalConnectWallet, setIsModalConnectWallet }) => {
  const handleClose = () => {
    setIsModalConnectWallet(false)
  }
  return (
    <Modal
      visible={isModalConnectWallet}
      className='modal-connect'
      onOk={handleClose}
      onCancel={handleClose}
      footer={null}
      bodyStyle={{ overflow: 'hidden' }}
      width={830}
    >
      <ModalHeader />
      <ModalContent />
      <ModalFooter />
    </Modal>
  )
}
export default memo(ModalConnectOnchain)
