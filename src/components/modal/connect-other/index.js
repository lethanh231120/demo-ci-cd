import React from 'react'
import { Modal, Typography, Image } from 'antd'
import { ArrowRightOutlined } from '@ant-design/icons'
import { connectType } from '../../../utils/connects/ConnectType'
import './style.scss'
const { Title, Text } = Typography

const ModalConnectOther = ({ isModalConnectOther, setIsModalConnectOther, handleClickConnectType }) => {
  return (
    <Modal
      visible={isModalConnectOther}
      className='modal-connect-other'
      onOk={() => setIsModalConnectOther(false)}
      onCancel={() => setIsModalConnectOther(false)}
      footer={null}
      bodyStyle={{ overflow: 'hidden' }}
      width={500}
    >
      <Title level={2} className='modal-header-title'>
        Choose an asset type
      </Title>
      <div className='content-modal'>
        {connectType?.map((item, index) => (
          <div key={index} className='content-modal-item' onClick={() => handleClickConnectType(item.id)}>
            <div className='content-modal-item-icon'>
              <Image
                width={50}
                preview={false}
                src={item.icon}
              />
              <Text className='content-modal-item-text'>{item.name}</Text>
            </div>
            <ArrowRightOutlined className='content-modal-item-arrow'/>
          </div>
        ))}
      </div>
    </Modal>
  )
}

export default ModalConnectOther
