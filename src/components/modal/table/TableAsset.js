import React from 'react'
import { Table } from 'antd'
import { useNavigate } from 'react-router-dom'
const TableAsset = ({ columns, loading, data, paginate, scroll, detailCoin }) => {
  const navigate = useNavigate()

  const handleClickItem = (record) => {
    if (detailCoin) {
      navigate(`../../../../coins/${record.coinId}`, { state: { data: record }})
    }
  }

  return (
    <Table
      loading={loading}
      columns={columns}
      dataSource={data}
      scroll={scroll}
      showSorterTooltip={false}
      pagination={paginate}
      onRow={(record) => ({
        onClick: () => { handleClickItem(record) }
      })}
    />
  )
}

export default TableAsset
