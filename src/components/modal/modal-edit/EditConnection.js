import React, { useState, memo } from 'react'
import { Modal, Button, Form, Input } from 'antd'
import { patchConnection } from '../../../api/connectService'
import { useQueryClient } from '@tanstack/react-query'

const ModalEdit = ({ isModalEdit, setIsModalEdit, dataEdit }) => {
  const [loading, setLoading] = useState(false)
  const [form] = Form.useForm()
  const queryClient = useQueryClient()

  const handleOk = () => {
    setIsModalEdit(false)
  }

  const handleCancel = () => {
    setIsModalEdit(false)
  }

  const onFinish = async(values) => {
    const data = {
      ...values,
      'ownPersentage': parseFloat(values?.ownPersentage)
    }
    try {
      setLoading(true)
      const res = await patchConnection(`connect/connectionId=${dataEdit.id}`, data)
      queryClient.invalidateQueries('listConnection')
      setIsModalEdit(false)
      res && setLoading(false)
      form.resetFields()
    } catch (error) {
      console.log(error)
    }
  }

  return (
    <Modal
      visible={isModalEdit}
      onOk={handleOk}
      layout='vertical'
      onCancel={handleCancel}
      footer={null}
      className='modal-style'
      bodyStyle={{ height: '50vh', overflow: 'hidden' }}
      width={400}
    >
      <Form name='basic' form = {form} initialss={{ remember: true }} onFinish={onFinish} autoComplete='off'>
        <Form.Item label='Connection Name' name='connectionName'>
          <Input placeholder={dataEdit && dataEdit.connectionName}/>
        </Form.Item>
        <Form.Item
          label='Ownership rate'
          name='ownPersentage'
        >
          <Input type='number' placeholder={dataEdit && dataEdit.ownPersentage}/>
        </Form.Item>
        <Form.Item >
          <Button type='primary' htmlType='submit' loading={loading}>
            Submit
          </Button>
        </Form.Item>
      </Form>
    </Modal>
  )
}
export default memo(ModalEdit)
