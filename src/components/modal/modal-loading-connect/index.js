import React, { useContext, useEffect, useState } from 'react'
import { Modal } from 'antd'
import { Typography, Button, Space, Spin } from 'antd'
import { postBtc } from '../../../api/bitcoinService'
import { postEvm } from '../../../api/evmService'
import { postXrp } from '../../../api/xrpService'
import { postSolana } from '../../../api/solanaService'
import { getConnection } from '../../../api/connectService'
import { postNft } from '../../../api/nftService'
import { BITCOIN_CHAINID, ETHEREUM_CHAINID, BINANCE_SMART_CHAIN_CHAINID, RIPPLE_CHAINID, SOLANA_CHAINID } from '../../../constants/ChainId'
import { PlatFormContext } from '../../../layouts'
import { useQuery } from '@tanstack/react-query'
import { BINANCE, OFFCHAIN } from '../../../constants/TypeImport'
import { postBinance } from '../../../api/binanceService'
import { config } from '../../../utils/config'
// import axios from 'axios'
const { Title, Text } = Typography

const ModalLoadingConnect = ({ isModalLoading, message, setMessage }) => {
  const [infoCoin, setInfoCoin] = useState()
  const state = useContext(PlatFormContext)
  const [refetchIntervalHolding, setRefetchIntervalHolding] = useState(2000)
  // cap nhat infoCoin theo chainId vua nhap
  useEffect(() => {
    if (state?.propsImport?.type === OFFCHAIN) {
      switch (state?.propsImport.exchange) {
        case BINANCE:
          setInfoCoin({
            method: postBinance,
            chainId: OFFCHAIN,
            endpoint: BINANCE,
            form: state?.propsImport?.form
          })
          break
        default:
          break
      }
    } else {
      switch (state?.propsImport?.chainId) {
        case BITCOIN_CHAINID:
          setInfoCoin({
            address: state?.propsImport?.address,
            chainId: BITCOIN_CHAINID,
            method: postBtc,
            endpoint: 'bitcoin'
          })
          break
        case ETHEREUM_CHAINID:
          setInfoCoin({
            address: state?.propsImport?.address,
            chainId: ETHEREUM_CHAINID,
            method: postEvm,
            endpoint: 'evm',
            nft: true
          })
          break
        case RIPPLE_CHAINID:
          setInfoCoin({
            address: state?.propsImport?.address,
            chainId: RIPPLE_CHAINID,
            method: postXrp,
            endpoint: 'xrp'
          })
          break
        case BINANCE_SMART_CHAIN_CHAINID:
          setInfoCoin({
            address: state?.propsImport?.address,
            chainId: BINANCE_SMART_CHAIN_CHAINID,
            method: postEvm,
            endpoint: 'evm',
            nft: true
          })
          break
        case SOLANA_CHAINID:
          setInfoCoin({
            address: state?.propsImport?.address,
            chainId: SOLANA_CHAINID,
            method: postSolana,
            endpoint: 'solana',
            nft: true
          })
          break
        default:
          break
      }
    }
  }, [state])

  // get holding theo dia chi va chain vua nhap
  const { data } = useQuery(
    ['holding', infoCoin],
    async() => {
      if (infoCoin.chainId === OFFCHAIN) {
        const holding = await infoCoin.method(`${infoCoin.endpoint}/account-info`, infoCoin.form, config)
        return { holding }
      } else {
        if (infoCoin.nft) {
          const holding = await infoCoin.method(`${infoCoin.endpoint}/addresses-holdings`, { 'addresses': [infoCoin?.address] })
          const nft = await postNft('nft/addresses-nfts', { 'addresses': [infoCoin?.address] })
          const connections = await getConnection('connect/current-connections')
          return { holding, nft, connections }
        } else {
          const holding = await infoCoin.method(`${infoCoin.endpoint}/addresses-holdings`, { 'addresses': [infoCoin?.address] })
          const connections = await getConnection('connect/current-connections')
          return { holding, connections }
        }
      }
    },
    {
      refetchInterval: refetchIntervalHolding,
      onSuccess: () => setRefetchIntervalHolding(0)
    }
  )

  // check neu api nft va holding deu co du lieu thi cap nhat lai loading va success
  useEffect(() => {
    if (state?.propsImport?.type === OFFCHAIN) {
      if (data?.holding?.status) {
        state.handleSetData({
          listHolding: [data?.holding?.data],
          form: infoCoin.form,
          loading: false,
          success: true
        })
      }
    } else {
      if (infoCoin?.nft) {
        if (data?.holding?.status && data?.nft?.status) {
          state.handleSetData({
            loading: false,
            success: true,
            listNft: [
              state.propsImport.listNft,
              data?.nft?.data
            ],
            listHolding: [
              state.propsImport.listHolding,
              data?.holding?.data
            ],
            initial: false
          })
        }
      } else {
        if (data?.holding?.status) {
          state.handleSetData({
            loading: false,
            success: true,
            listHolding: [
              state.propsImport.listHolding,
              data?.holding?.data
            ],
            listNft: [
              state.propsImport.listNft
            ],
            initial: false
          })
        }
      }
    }
  }, [data])

  const handleCancel = () => {
    state.handleSetPropsImport({ loading: false })
    setMessage('')
  }

  return (
    <Modal
      visible={isModalLoading}
      footer={null}
      className='modal-style'
      bodyStyle={{ height: '50vh', overflow: 'hidden' }}
      width={520}
      onCancel={handleCancel}
      onOk={message && handleCancel}
      destroyOnClose={true}
      maskClosable={false}
    >
      <Title style={{ fontSize: '28px', color: '#fff', fontWeight: '600', textAlign: 'center', background: 'black' }}>
        It May Take a Few Seconds to Connect Bitcoin Wallet to CoinStats
      </Title>
      <div>
        waitting...
      </div>
      {message !== ''
        ? <Typography className='message-error'>
          <Text type='danger' style={{ color: 'red' }}>
            {state?.propsImport?.messageError ? state?.propsImport?.messageError : (message && message)}
          </Text>
        </Typography>
        : ''
      }
      <div>
        <Space size='middle'>
          <Spin size='large' />
        </Space>
      </div>
      {message && <Button className='button' onClick={handleCancel}>SKIP</Button>}
    </Modal>
  )
}
export default ModalLoadingConnect
