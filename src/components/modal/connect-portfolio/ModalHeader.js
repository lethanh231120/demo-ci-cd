import React, { memo } from 'react'
import { Typography, Input } from 'antd'
const { Title } = Typography
const { Search } = Input
const ModalHeader = () => {
  return (
    <div className='modal-header'>
      <Title className='modal-header-title'>
        More than 70+ platforms supported. Choose and connect in few clicks.
      </Title>
      <Search
        placeholder='input search text'
        // onSearch={onSearch}
        className='input-search'
        style={{
          width: '90%'
        }}
      />
    </div>
  )
}
export default memo(ModalHeader)
