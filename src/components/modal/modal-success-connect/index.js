import React, { useContext } from 'react'
import { Modal, Button, Result } from 'antd'
import { useNavigate } from 'react-router-dom'
import { PlatFormContext } from '../../../layouts'
const ModalSuccessConnect = ({ isModalSuccess, message }) => {
  const navigate = useNavigate()
  const state = useContext(PlatFormContext)

  const handleClickSeePortfolio = () => {
    navigate('../portfolio-manager')
    state.handleSetPropsImport({ success: false })
  }

  return (
    <Modal
      className='reset-password-modal'
      visible={isModalSuccess}
      onOk={handleClickSeePortfolio}
      onCancel={handleClickSeePortfolio}
      footer={null}
    >
      <Result
        status='success'
        title={message}
        extra={[
          <Button className='button' onClick={handleClickSeePortfolio} key='submit'>
            SEE MY ANALYTICS
          </Button>
        ]}
      />
    </Modal>
  )
}
export default ModalSuccessConnect
