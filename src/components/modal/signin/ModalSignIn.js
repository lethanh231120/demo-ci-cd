import React from 'react'
import SignIn from '../../auth/login'
import { Modal } from 'antd'

const ModalSignIn = ({ isModalSignin, handleCancel }) => {
  return (
    <Modal
      visible={isModalSignin}
      footer={null}
      onOk={handleCancel}
      onCancel={handleCancel}
      className='modal-signin'
    >
      <SignIn handleCancel={handleCancel}/>
    </Modal>
  )
}

export default ModalSignIn
