import React, { useEffect, useState } from 'react'
import { Form, Select, Input, Button, Typography, Image, Table, Popover, Modal, notification, Tooltip } from 'antd'
import { EditOutlined, ClearOutlined, SmileOutlined } from '@ant-design/icons'
import Chart from '../chart/LineChart'
import { patch } from '../../api/stockService'
import { useQueryClient } from '@tanstack/react-query'

const { Text } = Typography
const { Option } = Select

const Holdings = (props) => {
  const [form] = Form.useForm()
  const [openModalChange, setOpenModalChange] = useState(false)
  const [loading, setLoading] = useState(false)
  const [itemChange, setItemChange] = useState()
  const queryClient = useQueryClient()
  const [data, setData] = useState([])
  const [selectedRowKeys, setSelectedRowKeys] = useState([
    'rank',
    'symbol',
    'exchange',
    'amount',
    'volume',
    'priceopen',
    'priceclose',
    'pricehigh',
    'pricelow',
    'adjpriceclose',
    'chart'
  ])
  const TYPE_SUBSTRACT = 2

  useEffect(() => {
    setData(props?.dataStock)
  }, [props.dataStock])

  const columnsPopover = [
    {
      title: 'Title',
      dataIndex: 'title',
      width: '85%',
      render: (_, record) => (<span style={{ color: '#A8ADB3' }}>{record.title}</span>)
    }
  ]

  const items = [
    {
      key: 'rank',
      title: '#'
    },
    {
      key: 'symbol',
      title: 'Symbol'
    },
    {
      key: 'exchange',
      title: 'Exchange'
    },
    {
      key: 'amount',
      title: 'Amount'
    },
    {
      key: 'volume',
      title: 'Volume'
    },
    {
      key: 'priceopen',
      title: 'Price Open'
    },
    {
      key: 'priceclose',
      title: 'Price Close'
    },
    {
      key: 'pricehigh',
      title: 'Price High'
    },
    {
      key: 'pricelow',
      title: 'Price Low'
    },
    {
      key: 'adjpriceclose',
      title: 'Price'
    },
    {
      key: 'chart',
      title: 'Chart (30 day)'
    }
  ]

  const onSelectChange = (newselectedRowKeys) => {
    setSelectedRowKeys(newselectedRowKeys)
  }

  const rowSelection = {
    selectedRowKeys,
    onChange: onSelectChange
  }

  const handleDeleteStockConnection = async(record) => {
    try {
      const data = {
        type: TYPE_SUBSTRACT,
        exchange: record.exchange,
        symbol: record.symbol,
        amount: record.amount
      }
      await patch(`stocks/assets/change-amount`, { holdings: [data] })
      openNotification('Delete stock successfully')
      queryClient.invalidateQueries('stockConnections')
    } catch (error) {
      console.log(error)
    }
  }

  const openNotification = (messageRes) => {
    notification.open({
      description: messageRes,
      duration: 2,
      icon: (
        <SmileOutlined
          style={{
            color: '#108ee9'
          }}
        />
      )
    })
  }

  const handleChangeAmount = (record) => {
    setItemChange(record)
    setOpenModalChange(true)
  }

  const columns = [
    {
      key: 'rank',
      title: '#',
      hidden: !selectedRowKeys.includes('rank'),
      width: '20px',
      render: (_, record, index) => (<span style={{ color: '#A8ADB3' }}>{index + 1}</span>)
    },
    {
      key: 'symbol',
      title: 'Symbol',
      width: '100px',
      hidden: !selectedRowKeys.includes('symbol'),
      sorter: (a, b) => a.name.toLowerCase().localeCompare(b.name.toLowerCase()),
      render: (_, record) => (
        <div className='table-name'>
          <span className='table-symbol'>{record?.symbol}</span>
        </div>
      )
    },
    {
      key: 'exchange',
      title: 'Exchange',
      width: '150px',
      hidden: !selectedRowKeys.includes('exchange'),
      sorter: (a, b) => a.exchange.toLowerCase().localeCompare(b.exchange.toLowerCase()),
      render: (_, record) => (
        <div className='table-name'>
          <span className='table-symbol'>{record?.exchange}</span>
        </div>
      )
    },
    {
      key: 'amount',
      title: 'Amount',
      width: '150px',
      hidden: !selectedRowKeys.includes('amount'),
      sorter: (a, b) => parseFloat(a.amount) - parseFloat(b.amount),
      render: (_, record) => (
        <span>{parseInt(record.amount)}</span>
      )
    },
    {
      key: 'adjpriceclose',
      title: 'Curren Price',
      width: '150px',
      hidden: !selectedRowKeys.includes('adjpriceclose'),
      sorter: (a, b) => parseFloat(a.adjpriceclose) - parseFloat(b.adjpriceclose),
      render: (_, record) => (
        <span>{record?.adjpriceclose ? record?.adjpriceclose : 0}</span>
      )
    },
    {
      key: 'volume',
      title: 'Volume',
      width: '150px',
      hidden: !selectedRowKeys.includes('volume'),
      sorter: (a, b) => parseFloat(a.volume) - parseFloat(b.volume),
      render: (_, record) => (
        <span>{record.volume}</span>
      )
    },
    {
      key: 'priceopen',
      title: 'Price Open',
      width: '150px',
      hidden: !selectedRowKeys.includes('priceopen'),
      sorter: (a, b) => parseFloat(a.priceopen) - parseFloat(b.priceopen),
      render: (_, record) => (
        <span>{record.priceopen}</span>
      )
    },
    {
      key: 'priceclose',
      title: 'Price Close',
      width: '150px',
      hidden: !selectedRowKeys.includes('priceclose'),
      sorter: (a, b) => parseFloat(a.priceclose) - parseFloat(b.priceclose),
      render: (_, record) => (
        <span>{record.priceclose}</span>
      )
    },
    {
      key: 'pricehigh',
      title: 'Price High',
      width: '150px',
      hidden: !selectedRowKeys.includes('pricehigh'),
      sorter: (a, b) => parseFloat(a.pricehigh) - parseFloat(b.pricehigh),
      render: (_, record) => (
        <span>{record.pricehigh}</span>
      )
    },
    {
      key: 'pricelow',
      title: 'Price Low',
      width: '150px',
      hidden: !selectedRowKeys.includes('pricelow'),
      sorter: (a, b) => parseFloat(a.pricelow) - parseFloat(b.pricelow),
      render: (_, record) => (
        <span>{record.pricelow}</span>
      )
    },
    {
      key: 'chart',
      title: 'Chart (30 day)',
      width: '150px',
      hidden: !selectedRowKeys.includes('chart'),
      render: (_, record) => (<Chart record={record?.dataChart}/>)
    },
    {
      key: 'more',
      title: <Popover
        placement='bottomRight'
        content={(<Table
          showHeader={false}
          scroll={{
            y: 260
          }}
          style={{ maxWidth: '290px' }}
          className='tableabc'
          rowSelection={rowSelection}
          pagination={false}
          columns={columnsPopover}
          dataSource={items}
        >
        </Table>)}
        trigger='click'
      >
        +
      </Popover>,
      className: 'table-plus',
      width: '20px',
      dataIndex: 'key',
      fixed: 'right',
      render: (_, record) => (<div style={{ display: 'flex', justifyContent: 'space-between' }}>
        <div className='icon-stock' style={{ width: '40px' }} onClick={() => handleChangeAmount(record)}>
          <EditOutlined />
        </div>
        <div className='icon-stock' style={{ width: '40px' }} onClick={() => handleDeleteStockConnection(record)}>
          <Tooltip placement='topRight' title={'Clear amount'}>
            <ClearOutlined />
          </Tooltip>
        </div>
      </div>)
    }
  ].filter(item => !item.hidden)

  const onFinish = async(values) => {
    setLoading(true)
    try {
      await patch(`stocks/assets/change-amount`, values)
      queryClient.invalidateQueries('stockConnections')
      setOpenModalChange(false)
      setLoading(false)
      form.resetFields()
    } catch (error) {
      console.log(error)
      setLoading(false)
    }
  }

  return (
    <div>
      <Table
        loading={props?.loading}
        columns={columns}
        dataSource={data && data}
        scroll={{ x: 'max-content' }}
        showSorterTooltip={false}
        rowKey={(record) => record?.id}
        pagination={{
          defaultCurrent: 1,
          pageSize: 20,
          showSizeChanger: false
        }}
      />
      <Modal
        className='update-stock-modal'
        visible={openModalChange}
        onOk = {() => setOpenModalChange(false)}
        onCancel = {() => setOpenModalChange(false)}
        footer={null}
      >
        <div className='change-amount'>
          <Typography className='platform-title'>
            <Text className='platform-text'>
              Stock Platform
            </Text>
            <Image
              width={30}
              preview={false}
              src='/coins/stock.png'
            />
            <Text style={{ color: 'black', marginLeft: '10px' }}>Update your stock amount</Text>
          </Typography>
          <Form autoComplete='off' layout='vertical' form={form} onFinish={onFinish}>
            <Form.Item
              label='Type'
              name='type'
              rules={[
                {
                  required: true,
                  message: 'Please choose type!'
                }
              ]}
            >
              <Select>
                <Option value={1}>Add</Option>
                <Option value={2}>Substract</Option>
              </Select>
            </Form.Item>
            <Form.Item
              label='Amount'
              name='amount'
              rules={[
                {
                  required: true,
                  message: 'Please enter the number of amount!'
                },
                {
                  pattern: new RegExp(/^[1-9]*$/),
                  message: 'Amount must be integer and bigger than 0!'
                }
              ]}
            >
              <Input placeholder='Amount' type='number'/>
            </Form.Item>
            <Form.Item
              label='Exchanges'
              name='exchange'
              initialValue={`${itemChange?.exchange}`}
              rules={[
                {
                  required: true,
                  message: 'Please choose exchange!'
                }
              ]}
            >
              <Input disabled/>
            </Form.Item>
            <Form.Item
              label='Symbol'
              name='symbol'
              initialValue={`${itemChange?.symbol}`}
              rules={[
                {
                  required: true,
                  message: 'Please choose symbol!'
                }
              ]}
            >
              <Input disabled/>
            </Form.Item>
            <Form.Item shouldUpdate >
              {() => (
                <Button
                  loading={loading}
                  type='primary'
                  htmlType='submit'
                >
                  Update
                </Button>
              )}
            </Form.Item>
          </Form>
        </div>
      </Modal>
    </div>
  )
}

export default Holdings
