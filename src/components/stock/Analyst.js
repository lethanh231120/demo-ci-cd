import React, { memo } from 'react'
import { Col, Row } from 'antd'
import { HeaderAnalyst } from './HeaderAnalyst'
import Holdings from './Holdings'

const Analyst = (props) => {
  const { dataStock, loading, percentInfo, totalValue, handleChange, chartAllStock } = props
  const priceChange = parseFloat('2.36')

  return (
    <div className='dashboard'>
      <Row>
        <HeaderAnalyst
          percentInfo={percentInfo}
          priceChange={priceChange}
          dataStock={dataStock}
          totalValue={totalValue}
          chartAllStock={chartAllStock}
          handleChange={handleChange}
        />
        <Col span={24}>
          <Holdings dataStock = { dataStock } loading={loading} />
        </Col>
      </Row>
    </div>
  )
}

export default memo(Analyst)
