import React, { useState } from 'react'
import { Col, Row, Select, Button, Modal, notification } from 'antd'
import { CaretUpOutlined, CaretDownOutlined, SmileOutlined } from '@ant-design/icons'
import AddStockModal from './AddStockModal'
import './styles.scss'
import { del } from '../../api/stockService'
import { useQueryClient } from '@tanstack/react-query'
import _ from 'lodash'
import LineChartAsset from '../chart/LineChartAsset'

const { Option } = Select
export const HeaderAnalyst = ({ handleChange, dataStock, percentInfo, totalValue, chartAllStock }) => {
  const queryClient = useQueryClient()
  const [isModalAddStock, setIsModalAddStock] = useState(false)
  const handleAddStock = () => {
    setIsModalAddStock(true)
  }
  const handleDeleteAll = async() => {
    try {
      await del('stocks/assets/delete')
      openNotification('Delete all stock successfully')
      queryClient.invalidateQueries('stockConnections')
    } catch (error) {
      console.log(error)
    }
  }

  const openNotification = (messageRes) => {
    notification.open({
      description: messageRes,
      duration: 2,
      icon: (
        <SmileOutlined
          style={{
            color: '#108ee9'
          }}
        />
      )
    })
  }

  return (
    <>
      <Col span={24}>
        <Row>
          <Col span='10'>
            <Col span={24}>
              <div className='main-price'>${totalValue && totalValue?.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')}</div>
            </Col>
            <Col span={24}>
              <div className='data-change'>
                <span
                  className='change-price-up'
                  style={{
                    color: percentInfo ? (percentInfo?.balanceFluctuations >= 0 ? '#34b349' : '#ff4d4d') : '#34b349'
                  }}
                >
                  $ {percentInfo?.balanceFluctuations ? parseFloat(percentInfo?.balanceFluctuations >= 0 ? percentInfo?.balanceFluctuations : percentInfo?.balanceFluctuations.toString().slice(1)).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,') : 0}
                </span>
                <span>
                  <span
                    className='change-price-up'
                    style={{
                      color: percentInfo ? (percentInfo?.balanceFluctuations >= 0 ? '#34b349' : '#ff4d4d') : '#34b349'
                    }}
                  >
                    {percentInfo?.balanceFluctuations >= 0 ? <CaretUpOutlined/> : <CaretDownOutlined/>}
                  </span>
                  <span
                    className='change-price-up'
                    style={{
                      color: percentInfo ? (percentInfo?.balanceFluctuations >= 0 ? '#34b349' : '#ff4d4d') : '#34b349'
                    }}
                  >
                    {percentInfo?.percent ? parseFloat(percentInfo?.percent >= 0 ? percentInfo?.percent : percentInfo?.percent.toString().slice(1)).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,') : 0} %
                  </span>
                </span>
                <Select
                  defaultValue='30'
                  onChange={handleChange}
                  color='rgba(0,0,0,.85)'
                >
                  <Option value='30'>1M</Option>
                  <Option value='60'>3M</Option>
                  <Option value='180'>6M</Option>
                  <Option value='365'>1Y</Option>
                  {/* <Option value='all'>ALL</Option> */}
                </Select>
                <Button onClick={handleAddStock}> Add New Stock </Button>
                {!_.isEmpty(dataStock) && (<Button onClick={handleDeleteAll}> Delete All </Button>)}
              </div>
            </Col>
          </Col>
          <Col span='14'>
            <div style={{ textAlign: 'right', display: 'flex', justifyContent: 'center' }}>
              <LineChartAsset
                dataChart={chartAllStock && [...chartAllStock]?.reverse()}
                width={300}
                height={80}
              />
            </div>
          </Col>
        </Row>
      </Col>
      <Modal
        className='update-stock-modal'
        visible={isModalAddStock}
        onOk = {() => setIsModalAddStock(false)}
        onCancel = {() => setIsModalAddStock(false)}
        footer={null}
      >
        <AddStockModal setIsModalAddStock={setIsModalAddStock}/>
      </Modal>
    </>
  )
}
