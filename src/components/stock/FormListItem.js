import React from 'react'
import { Typography, Form, Select, Row, Col, Input } from 'antd'
import { CloseOutlined, PlusOutlined } from '@ant-design/icons'

const { Option } = Select
const { Text } = Typography

export const FormListItem = ({ handleChooseExchange, exData, stock, exChange, TYPE_ADD_STOCK }) => {
  return (
    <Form.List name='stocks'>
      {(fields, { add, remove }) => {
        return (
          <div>
            {fields.map((field, index) => (
              <div className='stock-form-item' key={field.key}>
                <div className='stock-form-item-icon'>
                  <CloseOutlined onClick={() => remove(field.name)} />
                </div>
                <Row gutter={16}>
                  <Col span={12}>
                    <Form.Item
                      label='Type'
                      name={[index, 'type']}
                      initialValue={TYPE_ADD_STOCK}
                      rules={[
                        {
                          required: true,
                          message: 'Please choose type!'
                        }
                      ]}
                    >
                      <Select disabled>
                        <Option value={1}>Add</Option>
                        <Option value={2}>Substract</Option>
                      </Select>
                    </Form.Item>
                  </Col>
                  <Col span={12}>
                    <Form.Item
                      label='Amount'
                      name={[index, 'amount']}
                      rules={[
                        {
                          required: true,
                          message: 'Please enter the number of amount!'
                        },
                        {
                          pattern: new RegExp(/^[1-9]*$/),
                          message: 'Amount must be integer and bigger than 0!'
                        }
                      ]}
                    >
                      <Input placeholder='Amount' type='number'/>
                    </Form.Item>
                  </Col>
                </Row>
                <Row gutter={16}>
                  <Col span={12}>
                    <Form.Item
                      label='Exchanges'
                      name={[index, 'exchange']}
                      rules={[
                        {
                          required: true,
                          message: 'Please choose exchange!'
                        }
                      ]}
                    >
                      <Select
                        showSearch
                        placeholder='Select your exchanges'
                        optionFilterProp='children'
                        filterOption={(input, option) => option.children.toLowerCase().includes(input.toLowerCase())}
                        onChange={handleChooseExchange}
                      >
                        {
                          exData?.map((item, id) =>
                            <Option key={id} value={item.name}>{item.name}</Option>
                          )
                        }
                      </Select>
                    </Form.Item>
                  </Col>
                  <Col span={12}>
                    <Form.Item
                      label='Symbol'
                      name={[index, 'symbol']}
                      rules={[
                        {
                          required: true,
                          message: 'Please choose symbol!'
                        }
                      ]}
                    >
                      { exChange !== undefined ? (
                        <Select
                          showSearch
                          placeholder='Select your stock'
                          optionFilterProp='children'
                          filterOption={(input, option) => option.children.toLowerCase().includes(input.toLowerCase())}
                        >
                          {
                            stock?.map((item, id) =>
                              <Option key={id} value={item.symbol}>{item.symbol}</Option>
                            )
                          }
                        </Select>
                      ) : (
                        <Select placeholder='Please choose your exchange before' disabled></Select>
                      )
                      }
                    </Form.Item>
                  </Col>
                </Row>
              </div>
            ))}
            <Form.Item>
              <Typography className='stock-button' onClick={() => add()} block icon={<PlusOutlined />}>
                <Text><PlusOutlined /> Add another</Text>
              </Typography>
            </Form.Item>
          </div>
        )
      }}
    </Form.List>
  )
}
