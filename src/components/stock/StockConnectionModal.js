// import React from 'react'
// import { Row, Col, Image, Typography } from 'antd'
// import { ArrowRightOutlined } from '@ant-design/icons'
// import { useNavigate } from 'react-router-dom'
// import { stockPlatforms as platforms } from '../../utils/platforms/StockPlatforms'
// const { Text } = Typography

// export const StockConnectionPlatform = () => {
//   const navigate = useNavigate()
//   const handleClickWallet = () => {
//     navigate(`../../connect/stock`)
//   }

//   return (
//     <div className='content-modal'>
//       <Row gutter={12}>
//         {platforms.map((item, index) => (
//           <Col span={8} key={index}>
//             <div className='content-modal-item' onClick={() => handleClickWallet(item.id)}>
//               <div className='content-modal-item-icon'>
//                 <Image
//                   width={40}
//                   preview={false}
//                   src={item.icon}
//                 />
//                 <Text className='content-modal-item-text'>{item.name}</Text>
//               </div>
//               <ArrowRightOutlined className='content-modal-item-arrow'/>
//             </div>
//           </Col>
//         ))}
//       </Row>
//     </div>
//   )
// }
