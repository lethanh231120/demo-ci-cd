import React from 'react'
import { Modal, Result, Button } from 'antd'

const ModalNoti = ({ handleSignIn, handleCloseOpenNoti, openModalNoti }) => {
  return (
    <Modal
      className='reset-password-modal'
      visible={openModalNoti}
      onOk={handleCloseOpenNoti}
      onCancel={handleCloseOpenNoti}
      footer={null}
    >
      <Result
        status='warning'
        title='You need to login before you can access this page'
        extra={
          <Button type='primary' onClick={handleSignIn}>
            Login
          </Button>
        }
      />
    </Modal>
  )
}

export default ModalNoti
