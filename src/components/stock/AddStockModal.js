import { useQueryClient } from '@tanstack/react-query'
import { Form, Select, Input, Button, Typography, Image, notification, Row, Col } from 'antd'
import React, { useState, useEffect, useContext } from 'react'
import { get, patch } from '../../api/stockService'
import '../form-input/style.scss'
import { FormListItem } from './FormListItem'
import { StockContext } from '../../layouts'

const { Text } = Typography
const { Option } = Select

const AddStockModal = (props) => {
  const TYPE_ADD_STOCK = 1
  const [form] = Form.useForm()
  const queryClient = useQueryClient()
  const [exData, setExData] = useState(undefined)
  const [exChange, setExChange] = useState(undefined)
  const [stock, setStock] = useState(undefined)
  const [loading, setLoading] = useState(false)
  const stockContext = useContext(StockContext)
  console.log('stockContext', stockContext)

  useEffect(()=> {
    const getEx = async() => {
      await get('stocks/exchanges')
        .then(res => {
          setExData(res?.data)
        })
        .catch(err => notification.error({
          message: 'Error',
          description: err.message
        }))
    }
    getEx()
  }, [])

  const handleChooseExchange = (value) => {
    setExChange(value)
  }

  useEffect(()=> {
    const getStock = async() => {
      await get(`stocks/symbols?exchange=${exChange}`)
        .then(res => {
          setStock(res?.data)
        })
        .catch(err => notification.error({
          message: 'Error',
          description: err.message
        }))
    }

    getStock()
  }, [exChange])

  const onFinish = async(values) => {
    setLoading(true)
    let data
    if (values?.stocks) {
      data = [
        values?.stock,
        values?.stocks
      ]
    } else {
      data = [values?.stock]
    }
    try {
      await patch(`stocks/assets/change-amount`, { 'holdings': data?.flat(1) })
      await queryClient.invalidateQueries('stockConnections')
      await form.resetFields()
      await props.setIsModalAddStock(false)
      setLoading(false)
    } catch (error) {
      notification.error({
        message: 'Error',
        description: error.response.data.error
      })
      form.resetFields()
    }
  }

  return (
    <div className='change-amount'>
      <Typography className='platform-title'>
        <Text className='platform-text'>
          Stock Platform
        </Text>
        <Image
          width={30}
          preview={false}
          src='/coins/stock.png'
        />
        <Text style={{ color: 'black', marginLeft: '10px' }}>Update your stock amount</Text>
      </Typography>
      <div className='box-form'>
        <Form
          autoComplete='off'
          layout='vertical'
          form={form}
          onFinish={onFinish}
        >
          <div className='stock-form-item'>
            <Row gutter={16}>
              <Col span={12}>
                <Form.Item
                  label='Type'
                  name={['stock', 'type']}
                  initialValue={TYPE_ADD_STOCK}
                  rules={[
                    {
                      required: true,
                      message: 'Please choose type!'
                    }
                  ]}
                >
                  <Select disabled>
                    <Option value={1}>Add</Option>
                    <Option value={2}>Substract</Option>
                  </Select>
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item
                  label='Amount'
                  name={['stock', 'amount']}
                  rules={[
                    {
                      required: true,
                      message: 'Please enter the number of amount!'
                    },
                    {
                      pattern: new RegExp(/^[1-9]*$/),
                      message: 'Amount must be integer and bigger than 0!'
                    }
                  ]}
                >
                  <Input placeholder='Amount' type='number'/>
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={16}>
              <Col span={12}>
                <Form.Item
                  label='Exchanges'
                  name={['stock', 'exchange']}
                  rules={[
                    {
                      required: true,
                      message: 'Please choose exchange!'
                    }
                  ]}
                >
                  <Select
                    showSearch
                    placeholder='Select your exchanges'
                    optionFilterProp='children'
                    filterOption={(input, option) => option.children.toLowerCase().includes(input.toLowerCase())}
                    onChange={handleChooseExchange}
                  >
                    {
                      exData?.map((item, id) =>
                        <Option key={id} value={item.name}>{item.name}</Option>
                      )
                    }
                  </Select>
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item
                  label='Symbol'
                  name={['stock', 'symbol']}
                  rules={[
                    {
                      required: true,
                      message: 'Please choose symbol!'
                    }
                  ]}
                >
                  { exChange !== undefined ? (
                    <Select
                      showSearch
                      placeholder='Select your stock'
                      optionFilterProp='children'
                      filterOption={(input, option) => option.children.toLowerCase().includes(input.toLowerCase())}
                    >
                      {
                        stock?.map((item, id) =>
                          <Option key={id} value={item.symbol}>{item.symbol}</Option>
                        )
                      }
                    </Select>
                  ) : (
                    <Select placeholder='Please choose your exchange before' disabled></Select>
                  )
                  }
                </Form.Item>
              </Col>
            </Row>
          </div>
          <FormListItem
            handleChooseExchange={handleChooseExchange}
            exData={exData}
            stock={stock}
            exChange={exChange}
            TYPE_ADD_STOCK={TYPE_ADD_STOCK}
          />
          <Form.Item >
            <Button loading={loading} type='primary' htmlType='submit'>Submit</Button>
          </Form.Item>
        </Form>
      </div>
    </div>
  )
}

export default AddStockModal
