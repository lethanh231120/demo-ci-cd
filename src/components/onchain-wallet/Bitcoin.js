import React, { useContext } from 'react'
import { Form } from 'antd'
import PlatformHeader from '../form-input/PlatformHeader'
import ConnectionName from '../form-input/ConnectionName'
import AddressWallet from '../form-input/AddressWallet'
import ButtonSubmit from '../form-input/ButtonSubmit'
import { BITCOIN_CHAINID } from '../../constants/ChainId'
import { postConnection } from '../../api/connectService'
import './platform.scss'
import { PlatFormContext } from '../../layouts'
import { config } from '../../utils/config'
import OwnershipRate from '../form-input/OwnershipRate'
import { ONCHAIN } from '../../constants/TypeImport'

export const Bitcoin = () => {
  const state = useContext(PlatFormContext)
  const [form] = Form.useForm()

  const onFinish = async(values) => {
    try {
      const data = {
        ...values,
        'chainId': BITCOIN_CHAINID,
        'ownPersentage': parseFloat(values?.ownPersentage)
      }
      state.handleSetPropsImport({ loading: true })
      const res = await postConnection('connect/import-connection', data, config)
      res && state.handleSetPropsImport({
        loading: true,
        success: false,
        statusCode: res?.code,
        statusImport: res?.status,
        addressImport: values?.address,
        chainId: BITCOIN_CHAINID,
        type: ONCHAIN
      })
    } catch (error) {
      error?.response?.data && state.handleSetPropsImport({
        messageError: error.response.data.error,
        loading: true,
        statusCode: error.response.data.code
      })
    }
  }

  return (
    <div className='bitcoin'>
      <PlatformHeader src='/coins/bitcoin.png' text='Bitcoin'/>
      <Form
        onFinish={onFinish}
        autoComplete='off'
        layout='vertical'
        form={form}
      >
        <ConnectionName/>
        <OwnershipRate/>
        <AddressWallet/>
        <ButtonSubmit text='Submit'/>
      </Form>
    </div>
  )
}
