import React, { useState, useRef } from 'react'
import './styles.scss'
import { Card, Typography, Button, Skeleton, Modal } from 'antd'
import LineChartAsset from '../chart/LineChartAsset'
import { CaretUpOutlined, CaretDownOutlined } from '@ant-design/icons'
import ModalConnectOnchain from '../modal/onchain-connect'
import ModalConnectOffchain from '../modal/offchain-connect'
import { useNavigate } from 'react-router-dom'
import AddStockModal from '../stock/AddStockModal'

const { Title, Text } = Typography

const PropertyOverview = (props) => {
  const {
    title,
    dataChart,
    width,
    height,
    total,
    isButton,
    titleButton,
    listPeriod,
    type,
    percentInfo,
    setDataChangeByPeriod,
    detail,
    loading,
    data
  } = props
  const navigate = useNavigate()
  const [period, setPeriod] = useState(type === 'stock' ? '30' : '1d')
  const [isModalConnectWallet, setIsModalConnectWallet] = useState(false)
  const [isModalConnectExchange, setIsModalConnectExchange] = useState(false)
  const [isModalAddStock, setIsModalAddStock] = useState(false)

  const divRef = useRef()

  // const [size, setSize] = useState()
  // useEffect(() => {
  //   function updateSize() {
  //     setSize([window.innerWidth, window.innerHeight])
  //     console.log(divRef.current.offsetWidth)
  //     console.log(divRef.current.offsetHeight)
  //   }
  //   window.addEventListener('resize', updateSize)
  // }, [divRef.current])

  const handleChangePeriod = (period) => {
    setPeriod(period)
    setDataChangeByPeriod({
      'key': type,
      'period': period
    })
  }

  const handleImport = (type) => {
    if (type === 'onchain') {
      setIsModalConnectWallet(true)
    }
    if (type === 'offchain') {
      setIsModalConnectExchange(true)
    }
    if (type === 'stock') {
      setIsModalAddStock(true)
    }
  }

  const handleDetailAsset = (link) => {
    navigate(`${link}`, { state: { data }})
  }

  return (
    <div ref={divRef}>
      <Card
        className='overview-card'
        style={{
          width: '100%',
          textAlign: 'left',
          height: '320px'
        }}
      >
        {loading ? (
          <>
            <Skeleton.Button style={{ marginBottom: '10px' }} active={true} size='small' shape='default' block='checked' />
            <Skeleton active />
            <Skeleton active />
          </>
        ) : (
          <>
            <Title level={3}>{title && title}</Title>
            <LineChartAsset dataChart={dataChart} width={width} height={height} percentInfo={percentInfo}/>
            <div className='overview-period'>
              {listPeriod && listPeriod?.map((item) => (
                <div
                  className='overview-period-item'
                  key={item?.period}
                  style={{ color: (period === item?.period) ? '#ff9332' : '' }}
                  onClick={() => handleChangePeriod(item?.period)}
                >
                  {item?.title}
                </div>
              ))}
            </div>
            <div
              style={{ paddingBottom: '10px', cursor: 'pointer' }}
              onClick={() => detail?.isDetail ? handleDetailAsset(detail?.linkUrl) : ''}
            >
              <Text style={{ display: 'block', padding: '10px 0' }}>
              Total : ${total?.toFixed(4).replace(/\d(?=(\d{3})+\.)/g, '$&,')}
              </Text>
              <div>
                <span
                  className='change-price-up'
                  style={{
                    color: percentInfo?.balanceFluctuations >= 0 ? '#34b349' : '#ff4d4d'
                  }}
                >
                  $ {percentInfo?.balanceFluctuations ? parseFloat(percentInfo?.balanceFluctuations >= 0 ? percentInfo?.balanceFluctuations : percentInfo?.balanceFluctuations.toString().slice(1)).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,') : 0}
                </span>
                <span>
                  <span
                    className='change-price-up'
                    style={{
                      color: percentInfo?.balanceFluctuations >= 0 ? '#34b349' : '#ff4d4d'
                    }}
                  >
                    {percentInfo?.balanceFluctuations >= 0 ? <CaretUpOutlined/> : <CaretDownOutlined/>}
                  </span>
                  <span
                    className='change-price-up'
                    style={{
                      color: percentInfo?.balanceFluctuations >= 0 ? '#34b349' : '#ff4d4d'
                    }}
                  >
                    {percentInfo?.percent ? parseFloat(percentInfo?.percent >= 0 ? percentInfo?.percent : percentInfo?.percent.toString().slice(1)) : 0} %
                  </span>
                </span>
              </div>
            </div>
            {isButton ? (
              <Button type='primary' onClick={() => handleImport(type)}>{titleButton && titleButton}</Button>
            ) : ('')}
          </>
        )}
      </Card>
      <ModalConnectOnchain
        isModalConnectWallet={isModalConnectWallet}
        setIsModalConnectWallet={setIsModalConnectWallet}
      />
      <ModalConnectOffchain
        isModalConnectExchange={isModalConnectExchange}
        setIsModalConnectExchange={setIsModalConnectExchange}
      />
      <Modal
        className='update-stock-modal'
        visible={isModalAddStock}
        onOk = {() => setIsModalAddStock(false)}
        onCancel = {() => setIsModalAddStock(false)}
        footer={null}
      >
        <AddStockModal setIsModalAddStock={setIsModalAddStock}/>
      </Modal>
    </div>
  )
}

export default PropertyOverview
