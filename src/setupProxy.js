const { createProxyMiddleware } = require('http-proxy-middleware')
module.exports = function(app) {
  app.use(
    createProxyMiddleware('/accountService', {
      target: 'https://accounts.nika.guru',
      changeOrigin: true,
      pathRewrite: {
        '^/accountService': ''
      },
      headers: {
        Connection: 'keep-alive'
      }
    })
  )
  app.use(
    createProxyMiddleware('/mailService', {
      target: 'https://mail.nika.guru',
      changeOrigin: true,
      pathRewrite: {
        '^/mailService': ''
      },
      headers: {
        Connection: 'keep-alive'
      }
    })
  )
  app.use(
    createProxyMiddleware('/connectService', {
      target: 'https://connect.nika.guru',
      changeOrigin: true,
      pathRewrite: {
        '^/connectService': ''
      },
      headers: {
        Connection: 'keep-alive'
      }
    })
  )
  app.use(
    createProxyMiddleware('/priceService', {
      target: 'https://price.nika.guru',
      changeOrigin: true,
      pathRewrite: {
        '^/priceService': ''
      },
      headers: {
        Connection: 'keep-alive'
      }
    })
  )
  app.use(
    createProxyMiddleware('/bitcoinService', {
      target: 'https://bitcoin.nika.guru',
      changeOrigin: true,
      pathRewrite: {
        '^/bitcoinService': ''
      },
      headers: {
        Connection: 'keep-alive'
      }
    })
  )
  app.use(
    createProxyMiddleware('/evmService', {
      target: 'https://evm.nika.guru',
      changeOrigin: true,
      pathRewrite: {
        '^/evmService': ''
      },
      headers: {
        Connection: 'keep-alive'
      }
    })
  )
  app.use(
    createProxyMiddleware('/solanaService', {
      target: 'https://sol.nika.guru',
      changeOrigin: true,
      pathRewrite: {
        '^/solanaService': ''
      },
      headers: {
        Connection: 'keep-alive'
      }
    })
  )
  app.use(
    createProxyMiddleware('/nftService', {
      target: 'https://nft.nika.guru',
      changeOrigin: true,
      pathRewrite: {
        '^/nftService': ''
      },
      headers: {
        Connection: 'keep-alive'
      }
    })
  )
  app.use(
    createProxyMiddleware('/xrpService', {
      target: 'https://ripple.nika.guru',
      changeOrigin: true,
      pathRewrite: {
        '^/xrpService': ''
      },
      headers: {
        Connection: 'keep-alive'
      }
    })
  )
  app.use(
    createProxyMiddleware('/stockService', {
      target: 'https://stocks.nika.guru',
      changeOrigin: true,
      pathRewrite: {
        '^/stockService': ''
      },
      headers: {
        Connection: 'keep-alive'
      }
    })
  )
  app.use(
    createProxyMiddleware('/coinService', {
      target: 'https://coin-info.nika.guru',
      changeOrigin: true,
      pathRewrite: {
        '^/coinService': ''
      },
      headers: {
        Connection: 'keep-alive'
      }
    })
  )
  app.use(
    createProxyMiddleware('/binanceService', {
      // target: 'https://6dc3-58-187-250-48.ap.ngrok.io/', 
      target: 'https://binance.nika.guru',
      changeOrigin: true,
      pathRewrite: {
        '^/binanceService': ''
      },
      headers: {
        Connection: 'keep-alive'
      }
    })
  )
  app.use(
    createProxyMiddleware('/supportEarnService', {
      target: 'https://support-earn.nika.guru',
      changeOrigin: true,
      pathRewrite: {
        '^/supportEarnService': ''
      },
      headers: {
        Connection: 'keep-alive'
      }
    })
  )
  app.use(
    createProxyMiddleware('/supportDexService', {
      target: 'https://support-dex.nika.guru',
      changeOrigin: true,
      pathRewrite: {
        '^/supportDexService':''
      },
      headers: {
        Connection: 'keep-alive'
      }
    })
  )
}
