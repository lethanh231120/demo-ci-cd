import React, { useState } from 'react'
import { Typography, Row, Col, Image, Button } from 'antd'
import { ArrowRightOutlined } from '@ant-design/icons'
import './index.scss'
import { useNavigate } from 'react-router-dom'
import ServiceTable from '../cryptocurrencies/ServiceTable'
import StockInfo from '../stock/StockInfo'
import { wallets } from '../../utils/connects/OnchainWallet'
import ModalConnectOther from '../../components/modal/connect-other'
import ModalConnectOnchain from '../../components/modal/onchain-connect'
import ModalConnectOffchain from '../../components/modal/offchain-connect'

const { Title, Text } = Typography

export default function Home() {
  const [isModalConnectOther, setIsModalConnectOther] = useState(false)
  const [isModalConnectWallet, setIsModalConnectWallet] = useState(false)
  const [isModalConnectExchange, setIsModalConnectExchange] = useState(false)
  const navigate = useNavigate()
  const paramsForHomePage = {
    skip: 0,
    limit: 5
  }
  // get top 3 platform
  const list = wallets.slice(0, 3)

  const handelClickConnect = () => {
    // navigate('../connect-portfolio')
    setIsModalConnectOther(true)
  }

  const handleConnectWallet = (id) => {
    navigate(`../connect/${id}`)
  }

  const handleNavigateToStockInfo = () => {
    navigate('../stock-info')
  }
  const handleNavigateToCryptoCurrencies = () => {
    navigate('../cryptocurrencies')
  }
  const handleClickConnectType = (type) => {
    setIsModalConnectOther(false)
    if (type === 'onchain') {
      setIsModalConnectWallet(true)
    }
    if (type === 'offchain') {
      setIsModalConnectExchange(true)
    }
    if (type === 'stock') {
      navigate('../stock')
    }
  }

  return (
    <div className='home-container'>
      <Typography className='home-content-header'>
        <Title className='home-header-title'>
          Manage Your Crypto and DeFi Portfolio From One Place
        </Title>
        <Text className='home-header-text'>
          Securely connect the portfolio you’re using to start.
        </Text>
      </Typography>
      <Row>
        <Col span={8} offset={8}>
          <Row gutter={24}>
            {list && list.map((item, index) => (
              <Col span={8} key={index} onClick={() => handleConnectWallet(item.id)}>
                <div className='home-platform-item'>
                  <Image width={80} preview={false} src={item.icon}/>
                  <Text className='home-platform-item-name'>{item.name}</Text>
                  <Text className='home-platform-item-text'>
                    Connect
                    <ArrowRightOutlined className='home-platform-item-icon'/>
                  </Text>
                </div>
              </Col>
            ))}
          </Row>
        </Col>
      </Row>
      <Title style={{ color: '#A8ADB3', fontSize: '18px', padding: '30px 0' }}>Or</Title>
      <button onClick={handelClickConnect} className='button'>CONNECT ORTHER</button>
      <div className='homepage__short-coins--list'>
        <div className='coins--table'>
          <Title style={{ color: '#fff', textAlign: 'left' }}>Top coins</Title>
          <ServiceTable paramsForHomePage={paramsForHomePage} isAction={false}/>
        </div>
        <div>
          <Button type='primary' onClick={handleNavigateToCryptoCurrencies}>See More</Button>
        </div>
      </div>
      <div className='homepage__stock--table'>
        <div className='stock--table'>
          <Title style={{ color: '#fff', textAlign: 'left' }}>Stock Info</Title>
          <StockInfo/>
        </div>
        <div>
          <Button type='primary' onClick={handleNavigateToStockInfo}>See More</Button>
        </div>
      </div>
      <ModalConnectOther
        isModalConnectOther={isModalConnectOther}
        setIsModalConnectOther={setIsModalConnectOther}
        handleClickConnectType={handleClickConnectType}
      />
      <ModalConnectOnchain
        isModalConnectWallet={isModalConnectWallet}
        setIsModalConnectWallet={setIsModalConnectWallet}
      />
      <ModalConnectOffchain
        isModalConnectExchange={isModalConnectExchange}
        setIsModalConnectExchange={setIsModalConnectExchange}
      />
    </div>
  )
}
