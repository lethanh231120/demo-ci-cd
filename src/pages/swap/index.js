import { Button, Modal } from 'antd'
import React, { useState } from 'react'
import './styles.scss'
import { DownOutlined } from '@ant-design/icons'
import AdvanceModal from '../../components/swap/AdvanceModal'
import { ConnectWalletModal } from '../../components/swap/ConnectWalletModal'
import { addressFormat } from '../../utils/parseFloat'
import SelectSwap from '../../components/swap/SelectSwap'
import { SelectNetwork } from '../../components/swap/SelectNetwork'
import UniSwapModal from '../../components/swap/UniSwapModal'
import PancakeSwapModal from '../../components/swap/PancakeSwapModal'
import OneInchSwapModal from '../../components/swap/1inchSwapModal'
import UndefinedSwapModal from '../../components/swap/UndefinedSwapModal'
import { runSwapBSC } from '../../components/swap/PancakeRouterService'
import { runSwap } from '../../components/swap/RouterService'

const Swap = () => {
  const [isModalAdvanced, setIsModalAdvanced] = useState(false)
  const [isConnectWalletModal, setIsConnectWalletModal] = useState(false)
  const [isSelectProtocol, setIsSelectProtocol] = useState(false)
  const [isSelectNetworkModal, setIsSelectNetworkModal] = useState(false)
  const [chainId, setChainId] = useState(undefined)
  const [token0Contract, setToken0Contract] = useState(undefined)
  const [transaction, setTransaction] = useState(undefined)
  const [slippageAmount, setSlippageAmount] = useState('')
  const [deadlineMinutes, setDeadlineMinutes] = useState('')
  const [dexName, setDexName] = useState(undefined)
  const [symbolNetwork, setSymbolNetwork] = useState(undefined)

  // const dexName = window.sessionStorage.getItem('DEX')

  const handleChangeId = () => {
    window.ethereum.request({ method: 'eth_chainId' })
      .then(() => {
        console.log(chainId)
        setDexName(undefined)
      })
  }

  window.ethereum && window.ethereum.on('chainChanged', handleChangeId)

  const onClick = () => {
    setIsConnectWalletModal(true)
  }

  const openSelectProtocol = () => {
    setIsSelectProtocol(true)
  }

  const openSelectNetworkModal = () => {
    setIsSelectNetworkModal(true)
  }

  const addressSigner = window.sessionStorage.getItem('address')

  const handleDisconnect = () =>{
    window.sessionStorage.removeItem('address')
    window.sessionStorage.removeItem('DEX')
    window.location.reload()
  }

  return (
    <div className='swap-page__container'>
      <div className='swap-page__modal'>
        <div className='swap-page__modal--title'>
          <span>Swap</span>
        </div>
        <>
          {addressSigner
            ? (<div className='swap-page__modal--wallet-info'>
              <div className='swap-page__modal--wallet-info--left'>
                <img
                  src='coins/metamask.png'
                  alt='Coin logo' />
                <div className='swap-page__modal--wallet-info--left-no-img'>
                  <div className='swap-page__modal--wallet-info--name'>Metamask Wallet</div>
                  <div className='swap-page__modal--wallet-info-props'>
                    <span className='swap-page__modal--wallet-info-props--address'>{addressFormat(addressSigner)}</span>
                    <span className='swap-page__modal--wallet-info-props--status' onClick={handleDisconnect}>Connected</span>
                  </div>
                </div>
              </div>
              <div className='change-wallet--arrow'>
                <Button onClick={openSelectNetworkModal}>
                  {
                    symbolNetwork === undefined ? 'Network' : symbolNetwork
                  }
                </Button>
              </div>
            </div>)
            : (<div className='swap-page__modal--connect-wallet'>
              <div className='connect-wallet-span'>Connect Your Wallet</div>
              <Button onClick={onClick}>Connect</Button>
            </div>)
          }
        </>
        <div className='swap-page__modal--swap'>
          <div className='swap-page__modal--swap__protocol'>
            <div className='swap-span'>Select Protocol</div>
            { symbolNetwork === undefined
              ? <Button disabled>Select Protocol</Button>
              : <Button onClick={openSelectProtocol}>
                {dexName === undefined ? 'Select Protocol' : dexName}</Button>
            }
          </div>
          {
            dexName === null || dexName === undefined ? (
              <UndefinedSwapModal
                setDexName={setDexName}
              />
            ) : dexName === 'Uni Swap' || dexName === 'Sushi Swap' ? (
              <UniSwapModal
                dexName={dexName}
                slippageAmount={slippageAmount}
                deadlineMinutes={deadlineMinutes}
                setTransaction={setTransaction}
                setToken0Contract={setToken0Contract}
                chainId={chainId}
              />
            ) : dexName === 'Pancake Swap' ? (
              <PancakeSwapModal
                dexName={dexName}
                slippageAmount={slippageAmount}
                deadlineMinutes={deadlineMinutes}
                setTransaction={setTransaction}
                setToken0Contract={setToken0Contract}
                chainId={chainId}
              />
            ) : (
              <OneInchSwapModal
                dexName={dexName}
                slippageAmount={slippageAmount}
                deadlineMinutes={deadlineMinutes}
                setTransaction={setTransaction}
                setToken0Contract={setToken0Contract}
                chainId={chainId}
              />
            )
          }
        </div>
        <div className='swap-page__modal--advanced-btn'>
          <Button onClick={() => setIsModalAdvanced(true)}>Advanced <DownOutlined /></Button>
        </div>
        <div className='swap-page__modal--submit-btn'>
          {
            addressSigner
              ? (
                dexName === 'uni-swap' || dexName === 'sushi-swap'
                  ? <Button
                    onClick={ () => runSwap(transaction && transaction, token0Contract && token0Contract) }
                  >Swap</Button>
                  : <Button
                    onClick={ () => runSwapBSC(token0Contract && token0Contract, transaction && transaction) }
                  >Swap</Button>
              ) : (
                <Button disabled>Swap</Button>
              )
          }
        </div>
        <div className='swap-page__modal--footer'>
          <h6>Swap by the best price from all sources</h6>
          <h6>The quote includes 0.5% <a>CoinStats Defi Swap fee</a></h6>
        </div>
      </div>
      <Modal
        className='advance-modal'
        visible={isModalAdvanced}
        onOk={() => setIsModalAdvanced(false)}
        onCancel={() => setIsModalAdvanced(false)}
        footer={null}
      >
        <AdvanceModal
          setIsModalAdvanced={setIsModalAdvanced}
          slippageAmount={slippageAmount}
          setSlippageAmount={setSlippageAmount}
          deadlineMinutes={deadlineMinutes}
          setDeadlineMinutes={setDeadlineMinutes}
        />
      </Modal>

      <Modal
        className='connect-wallet-modal'
        visible={isConnectWalletModal}
        onOk={() => setIsConnectWalletModal(false)}
        onCancel={() => setIsConnectWalletModal(false)}
        footer={null}
      >
        <ConnectWalletModal
          setIsConnectWalletModal={setIsConnectWalletModal}
        />
      </Modal>
      <Modal
        className='swap-select-modal'
        visible={isSelectProtocol}
        onOk={() => setIsSelectProtocol(false)}
        onCancel={() => setIsSelectProtocol(false)}
        footer={null}
        closable={false}
      >
        <SelectSwap symbolNetwork={symbolNetwork} setIsSelectProtocol={setIsSelectProtocol} setDexName={setDexName}/>
      </Modal>
      <Modal
        className='select-network-modal'
        visible={isSelectNetworkModal}
        onOk={() => setIsSelectNetworkModal(false)}
        onCancel={() => setIsSelectNetworkModal(false)}
        footer={null}
        closable={false}
      >
        <SelectNetwork setIsSelectNetworkModal={setIsSelectNetworkModal} setSymbolNetwork={setSymbolNetwork} setChainId={setChainId}/>
      </Modal>
    </div >
  )
}
export default Swap
