import React, { useEffect, useState } from 'react'
import Dashboard from './Dashboard'
import EmptyConnect from './EmptyConnect'
import { useQuery } from '@tanstack/react-query'
import { getConnection } from '../../api/connectService'
import { getCookie, STORAGEKEY } from '../../utils/storage'
import { useNavigate } from 'react-router-dom'
import ModalSignIn from '../../components/modal/signin/ModalSignIn'
import ModalNoti from './ModalNoti'
import { get } from '../../api/stockService'
import _ from 'lodash'

const index = () => {
  const [openModalNoti, setOpanModalNoti] = useState(false)
  const [isModalSignin, setIsModalSignin] = useState(false)
  const navigate = useNavigate()
  const token = getCookie(STORAGEKEY.ACCESS_TOKEN)

  const { data: connections } = useQuery(
    ['connections'],
    async() => {
      const res = await getConnection('connect/current-connections')
      return res
    }
  )

  // get all stock holdings
  const { data: stocks } = useQuery(
    ['stockConnections'],
    async() => {
      const stockConnections = await get('stocks/assets/amount')
      if (stockConnections?.data?.holdings !== null) {
        return stockConnections?.data?.holdings
      } else {
        return []
      }
    }
  )

  const handleCloseOpenNoti = () => {
    setOpanModalNoti(false)
    navigate('../../')
  }

  useEffect(() => {
    setOpanModalNoti(!token)
  }, [token])

  const handleCancel = () => {
    setIsModalSignin(false)
    navigate('../../')
  }

  const handleSignIn = () => {
    setIsModalSignin(true)
    setOpanModalNoti(false)
  }

  return (
    <>
      {token ? (
        <div>
          {((connections?.data === null) && (_.isEmpty(stocks)))
            ? (<EmptyConnect/>)
            : (<Dashboard
              connections={connections?.data}
              stocks={stocks}
            />
            )}
        </div>
      ) : (
        <ModalNoti handleSignIn={handleSignIn} handleCloseOpenNoti={handleCloseOpenNoti} openModalNoti={openModalNoti}/>
      )}
      <ModalSignIn isModalSignin={isModalSignin} handleCancel={handleCancel}/>
    </>
  )
}

export default index
