import React, { useState, useEffect } from 'react'
import PropertyOverview from '../../../../components/property-overview/PropertyOverview'
import _ from 'lodash'
import { get } from '../../../../api/stockService'
// import { useQuery } from '@tanstack/react-query'
import { totalPercent } from '../../../../utils/parseFloat'

const AssetStock = ({ stocks, setInfoStock, chartAllStock }) => {
  const [dataChangeByPeriod, setDataChangeByPeriod] = useState({ 'period': '30' })
  // const [chartAllStock, setChartAllStock] = useState([])
  const [dataStock, setDataStock] = useState()
  const [loading, setLoading] = useState(false)
  const [totalValue, setTotalValue] = useState()
  const [percentInfo, setPercentInfo] = useState()

  console.log(loading)
  // xu ly cho stock
  useEffect(() => {
    const getData = async() => {
      if (!_.isEmpty(stocks)) {
        setLoading(true)
        const listHoldingStock = []
        await Promise.all(stocks?.map(async(item) => {
          const listDataChart = []
          // call api lay thong tin stock theo exchang va symbol
          const data = await get(`stocks/prices/eod?exchange=${item?.exchange}&symbol=${item?.symbol}`)
          // map list stock va tao moi item bang cach gop thong tin cua item va itemChart
          data?.data?.forEach((itemChart) => {
            const newItem = {
              ...itemChart,
              'amount': item?.amount,
              'connectionId': item?.id
            }
            listDataChart.push(newItem)
          })
          listHoldingStock.push(listDataChart)
        }))
        if (!_.isEmpty(listHoldingStock)) {
          const listDataChart = []
          // map danh sach data cu tung ma stock va gop gia lai va luu vao mang dataChart
          listHoldingStock?.forEach((item) => {
            const dataChart = []
            item?.forEach((itemInChart) => {
              dataChart.push({ 'price': itemInChart?.adjpriceclose, 'eoddate': itemInChart?.eoddate })
            })
            const newItem = {
              ...item[0],
              'dataChart': dataChart
            }
            listDataChart.push(newItem)
          })
          if (!_.isEmpty(listDataChart)) {
            setDataStock(listDataChart)
            setLoading(false)
          }
        }
      } else {
        setDataStock()
        setLoading(false)
        setPercentInfo()
      }
    }
    getData()
  }, [stocks])

  useEffect(() => {
    if (dataStock) {
      let totalValue = 0
      dataStock?.forEach((item) => {
        totalValue += item?.adjpriceclose * parseFloat(item?.amount)
      })
      if (totalValue !== undefined) {
        setTotalValue(totalValue)
        setInfoStock({
          data: dataStock,
          totalValue: totalValue
        })
      }
    }
  }, [dataStock])

  useEffect(() => {
    if (chartAllStock) {
      const balanceFluctuations = chartAllStock ? (chartAllStock[0]?.totalValue - chartAllStock[chartAllStock.length - 1]?.totalValue) : 0
      const percent = balanceFluctuations ? totalPercent(balanceFluctuations, (chartAllStock && chartAllStock[0]?.totalValue)).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,') : 0
      setPercentInfo({
        balanceFluctuations: balanceFluctuations,
        percent: percent
      })
    }
  }, [chartAllStock])

  return (
    <PropertyOverview
      title='Stock'
      dataChart={chartAllStock}
      width={200}
      height={80}
      total={totalValue || 0}
      isButton={true}
      data={dataStock}
      titleButton='Add Stock'
      detail={{ 'isDetail': true, 'linkUrl': 'detail-stock' }}
      listPeriod={[
        { 'title': '1M', 'period': '30' },
        { 'title': '3M', 'period': '90' },
        { 'title': '6M', 'period': '180' },
        { 'title': '1Y', 'period': '365' }
      ]}
      loading={loading}
      type='stock'
      percentInfo={percentInfo}
      setDataChangeByPeriod={setDataChangeByPeriod}
      dataChangeByPeriod={dataChangeByPeriod}
    />
  )
}

export default AssetStock
