import React, { useState, useEffect, useContext } from 'react'
import PropertyOverview from '../../../../components/property-overview/PropertyOverview'
import {
  BITCOIN_CHAINID,
  ETHEREUM_CHAINID,
  BINANCE_SMART_CHAIN_CHAINID,
  SOLANA_CHAINID,
  RIPPLE_CHAINID
} from '../../../../constants/ChainId'
import _ from 'lodash'
import { postBtc } from '../../../../api/bitcoinService'
import { postXrp } from '../../../../api/xrpService'
import { postEvm } from '../../../../api/evmService'
import { postNft } from '../../../../api/nftService'
import { postSolana } from '../../../../api/solanaService'
import { totalPercent } from '../../../../utils/parseFloat'
import { useQuery } from '@tanstack/react-query'
import { FIVE_MINUTE, ONE_HOUR, ONE_DAY } from '../../../../constants/params'
import { PlatFormContext } from '../../../../layouts'

const AssetOnChain = ({ infoOnChange, setInfoOnchange, connections, setInfoNft }) => {
  const [dataChart, setDataChart] = useState([])
  const [percentInfo, setPercentInfo] = useState()
  const [listChartByAddress, setListChartByAddress] = useState([])
  const [chartAllAssetByPeriod, setChartAllAssetByPeriod] = useState([])
  const [groupChainId, setGroupChainid] = useState()
  const [dataChangeByPeriod, setDataChangeByPeriod] = useState({ 'period': '1d' })
  const [listConnection, setListConnection] = useState()
  const [listChart24h, setListChart24h] = useState([])

  const state = useContext(PlatFormContext)

  useEffect(() => {
    setInfoOnchange({
      loading: true
    })
    const nft = []
    const groupCoin = []
    // group address by chainId
    const groupBychainId = connections && connections.reduce((group, connection) => {
      const { chainId } = connection
      group[chainId] = group[chainId] ?? []
      group[chainId].push(connection.address)
      return group
    }, {})
    // get addresses by chainId eth or sol and push nft
    groupBychainId && Object.keys(groupBychainId).forEach(function(key) {
      if (key === ETHEREUM_CHAINID || key === SOLANA_CHAINID || key === BINANCE_SMART_CHAIN_CHAINID) {
        nft.push(...groupBychainId[key])
      }
      // custom lai du lieu
      groupCoin.push({
        'chainId': key,
        'address': groupBychainId[key]
      })
    })
    setGroupChainid(groupCoin)
    // neu la trang thai khoi tao, bat dau vao trang
    if (state?.propsImport?.initial) {
      // kiểm tra nếu mảng groupCoin khác rỗng
      if (!_.isEmpty(groupCoin)) {
        const getData = async() => {
          // get info chainId 1 or 56
          // merge all eth and bsc addresses into one array
          const listChain = groupCoin && groupCoin?.filter((item) => (item?.chainId === ETHEREUM_CHAINID || item?.chainId === BINANCE_SMART_CHAIN_CHAINID))
          const addressEVM = []
          listChain?.forEach((item) => {
            addressEVM.push(...item.address)
          })
          // call api các holding dùng promise.all để bắt buộc có dữ liệu mới tiếp tục thực hiện logic
          const [holdingsBtc, holdingsXrp, holdingsEvm] = await Promise.all([
            // get data holding btc chainId = 0
            postBtc('bitcoin/addresses-holdings',
              { 'addresses': groupCoin && groupCoin?.find((item) => (item.chainId === BITCOIN_CHAINID))?.address }
            ).then(res => res.data).catch(error => console.log(error)),
            postXrp('xrp/addresses-holdings',
              { 'addresses': groupCoin && groupCoin?.find((item) => (item.chainId === RIPPLE_CHAINID))?.address }
            ).then(res => res.data).catch(error => console.log(error)),
            postEvm('evm/addresses-holdings', { 'addresses': addressEVM })
              .then(res => res.data).catch(error => console.log(error))
          ])

          // kiểm tra nếu có dữ liệu của nft
          if (!_.isEmpty(nft)) {
            // call api lấy danh sách nft theo địa chỉ của connections
            const listNft = await postNft('nft/addresses-nfts', { 'addresses': nft })
            // cập nhật thông tin cho context propsImport
            state.handleSetData({
              ...state?.propsImport,
              listHolding: [
                holdingsBtc && holdingsBtc,
                holdingsXrp && holdingsXrp,
                holdingsEvm && holdingsEvm
              ],
              listNft: listNft?.data,
              initial: false
            })
          } else {
            // cập nhật lại thông tin của context propsImport
            state.handleSetData({
              ...state?.propsImport,
              listHolding: [
                holdingsBtc && holdingsBtc,
                holdingsXrp && holdingsXrp,
                holdingsEvm && holdingsEvm
              ],
              initial: false
            })
          }
        }
        getData()
      }
    }
  }, [connections])

  // Tính tổng tiền
  const totalMoney = (total, currenValue) => {
    const value = total + parseFloat(currenValue?.totalValue)
    return value
  }

  const caclTotal = (total, currenValue) => {
    const value = total + parseFloat(currenValue?.amount * currenValue?.price * (1 / Math.pow(10, currenValue?.decimals)))
    return value
  }

  // get chart total by address connection
  useQuery(
    ['listChart', dataChangeByPeriod, groupChainId, connections],
    async() => {
      if (groupChainId) {
        // call chart cua tat ca dia chi
        const data = await Promise.all(groupChainId?.map(async(item) => {
          if (item?.chainId === BITCOIN_CHAINID) {
            const data = await postBtc(`bitcoin/addresses-charts/period=${dataChangeByPeriod.period}`, { 'addresses': item?.address })
            return data?.data
          }
          if (item?.chainId === ETHEREUM_CHAINID) {
            const data = await postEvm(`evm/addresses-charts/period=${dataChangeByPeriod.period}`, { 'addresses': item?.address })
            return data?.data
          }
          if (item?.chainId === RIPPLE_CHAINID) {
            const data = await postXrp(`xrp/addresses-charts/period=${dataChangeByPeriod.period}`, { 'addresses': item?.address })
            return data?.data
          }
          if (item?.chainId === BINANCE_SMART_CHAIN_CHAINID) {
            const data = await postEvm(`evm/addresses-charts/period=${dataChangeByPeriod.period}`, { 'addresses': item?.address })
            return data?.data
          }
          if (item?.chainId === SOLANA_CHAINID) {
            const data = await postSolana(`solana/addresses-charts/period=${dataChangeByPeriod.period}`, { 'addresses': item?.address })
            return data?.data
          }
        }))
        setListChartByAddress(data?.flat(1))
        if (dataChangeByPeriod?.period === '1d') {
          setListChart24h(data?.flat(1))
        }
      }
    },
    {
      refetchInterval: dataChangeByPeriod?.period === '1h' ? FIVE_MINUTE : ((dataChangeByPeriod?.period === '1d' || dataChangeByPeriod?.period === '1w') ? ONE_HOUR : ONE_DAY)
    }
  )

  // xu ly cho holding
  useEffect(() => {
    const connection = []

    // gộp thoong tin cua connection voi thong tin cua listholding
    const newListHolding = [...state.propsImport.listHolding]
    connections?.forEach((itemConnect, index) => {
      if (!_.isEmpty(newListHolding)) {
        const itemHolding = newListHolding?.find((item) => {
          if (item?.address) {
            return (itemConnect?.address === item?.address?.address)
          }
        })
        if (itemHolding) {
          const totalValue = itemHolding?.holdings?.reduce(caclTotal, 0)
          const newConnect = {
            index: index,
            ...itemHolding.address,
            connectionName: itemConnect?.connectionName,
            ownPersentage: itemConnect?.ownPersentage,
            id: itemConnect?.id,
            holdings: itemHolding?.holdings,
            totalValue: totalValue
          }
          connection.push(newConnect)
        }
      }
    })

    // cap nhat thong tin chart cho connection va thong tin chi tiet cac dong coin cho holdings
    connection?.forEach((item, index) => {
      const chart24h = listChart24h?.find((item24h) => (item24h?.address?.address === item?.address))
      const itemChart = listChartByAddress?.find((itemChart) => (itemChart?.address?.address === item?.address))
      if (itemChart?.addressChart) {
        const newChart = [...itemChart.addressChart]
        // cap nhat chart theo ownPercentage
        itemChart?.addressChart?.map((itemChart, index) => {
          newChart[index] = {
            ...itemChart,
            totalValue: parseFloat(itemChart?.totalValue) * (item?.ownPersentage / 100)
          }
        })
        connection[index] = {
          ...item,
          totalValue: parseFloat(item?.totalValue) * (item?.ownPersentage / 100),
          index: index,
          dataChart: newChart,
          chart24h: chart24h.addressChart
        }
      }
      if (itemChart?.addressChart === null) {
        connection[index] = {
          ...item,
          totalValue: parseFloat(item?.totalValue) * (item?.ownPersentage / 100)
        }
      }
    })

    setListConnection(connection)
    // Tính tổng totalValue
    const totalValue = connection?.reduce(totalMoney, 0)

    !_.isEmpty(connection) && setInfoOnchange({
      ...infoOnChange,
      'totalValue': totalValue,
      'listConnect': connection
    })

    // gộp nfts của từng item
    const listNft = [...state.propsImport.listNft]
    state?.propsImport?.listNft?.map((item, index) => {
      const nftItem = connections?.find((itemConnect) => (itemConnect?.address === item?.address?.address))
      if (nftItem) {
        listNft[index] = {
          nfts: item?.nfts,
          address: {
            ...item.address,
            connectionName: nftItem?.connectionName,
            ownPersentage: nftItem?.ownPersentage
          }
        }
      }
    })
    // cap nhat thong tin cua nft ra component allasset
    !_.isEmpty(listNft) && setInfoNft(listNft)
  }, [state?.propsImport, listChartByAddress, connections, listChart24h])

  // tinh chart
  useEffect(() => {
    if (!_.isEmpty(listConnection)) {
      const chartAllAsset = []
      let maxLength = 0
      let itemMaxLength
      listConnection?.map((item) => {
        if (item?.dataChart?.length > maxLength) {
          maxLength = item?.dataChart?.length
          itemMaxLength = item?.dataChart
        }
      })
      for (let i = 0; i < maxLength; ++i) {
        let total = 0
        let timestamp
        // tong tat ca cac ban ghi co cung index trong danh sach dataChart cua cac dia chi
        // vi tai cac vi tri co index cung nhau o cac mang, thi timestamp deu giong nhau
        listConnection && listConnection?.forEach((chartItem) => {
          if (chartItem?.dataChart) {
            total += parseFloat(((chartItem?.dataChart !== null) && (chartItem?.dataChart[i] !== undefined))
              ? chartItem?.dataChart[i]?.totalValue : 0)
            // check neu ton tai timestamp thi lay timestamp
            // neu khong ton tai thi lay timestamp cua ban ghi thu i trong itemMaxLength
            timestamp = ((chartItem?.dataChart !== null) && (chartItem?.dataChart[i] !== undefined)) ? chartItem?.dataChart[i]?.timestamp : itemMaxLength[i]?.timestamp
          }
        })
        chartAllAsset.push({ 'totalValue': total, timestamp })
      }
      const newChartAll = [...chartAllAssetByPeriod]
      const indexChartByPeriod = chartAllAssetByPeriod?.findIndex((item) => (item?.period === dataChangeByPeriod?.period))
      if (indexChartByPeriod !== -1) {
        newChartAll[indexChartByPeriod] = {
          'period': dataChangeByPeriod?.period,
          'dataChart': chartAllAsset
        }
        setChartAllAssetByPeriod(newChartAll)
        setDataChart(newChartAll[indexChartByPeriod]?.dataChart)
        setInfoOnchange({
          ...infoOnChange,
          loading: false
        })
      }

      if (indexChartByPeriod === -1) {
        setChartAllAssetByPeriod([
          ...chartAllAssetByPeriod,
          {
            'period': dataChangeByPeriod?.period,
            'dataChart': chartAllAsset
          }
        ])
        setDataChart(chartAllAsset)
        setInfoOnchange({
          ...infoOnChange,
          loading: false
        })
      }
    }
  }, [listChartByAddress, dataChangeByPeriod, listConnection])

  // tinh percent info
  useEffect(() => {
    const balanceFluctuations = dataChart && (dataChart[0]?.totalValue - dataChart[dataChart.length - 1]?.totalValue)
    const percent = balanceFluctuations ? totalPercent(balanceFluctuations, (dataChart && dataChart[0]?.totalValue)).toFixed(4).replace(/\d(?=(\d{3})+\.)/g, '$&,') : 0
    setPercentInfo({
      balanceFluctuations: balanceFluctuations,
      percent: percent
    })
  }, [dataChart])

  return (
    <PropertyOverview
      title='Onchain Wallet'
      dataChart={dataChart && [...dataChart]?.reverse()}
      width={200}
      height={80}
      total={infoOnChange?.totalValue}
      isButton={true}
      titleButton='Add Address'
      detail={{ 'isDetail': true, 'linkUrl': 'onchain' }}
      listPeriod={[
        { 'title': '1H', 'period': '1h' },
        { 'title': '1D', 'period': '1d' },
        { 'title': '1M', 'period': '1m' },
        { 'title': 'ALL', 'period': 'all' }
      ]}
      loading={infoOnChange?.loading}
      type='onchain'
      percentInfo={percentInfo}
      setDataChangeByPeriod={setDataChangeByPeriod}
      data={infoOnChange}
    />
  )
}

export default AssetOnChain
