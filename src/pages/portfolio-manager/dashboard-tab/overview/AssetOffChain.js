import React, { useEffect, useState } from 'react'
import { postBinance } from '../../../../api/binanceService'
import PropertyOverview from '../../../../components/property-overview/PropertyOverview'
import { totalPercent } from '../../../../utils/parseFloat'

const AssetOffChain = (props) => {
  const { listAccounts, setInfoOffchain } = props
  const [dataChangeByPeriod, setDataChangeByPeriod] = useState({ 'period': '1d' })
  const [totalOffchainChart, setTotalOffchainChart] = useState()
  const [percentInfo, setPercentInfo] = useState()
  // get data chart offchain
  const listAccountChart = listAccounts?.map(async(account) => {
    const dataForm = {
      'accounts': [{
        apiKey: account.apiKey,
        secretKey: account.secretKey
      }]
    }
    const accountChartPoint = await postBinance(`binance/charts/period=${dataChangeByPeriod?.period}`, dataForm)
    return accountChartPoint.data
  })

  useEffect(() => {
    const allOffchainChartArr = []
    const dataChartOffchainAccountArr = []
    Promise.all(listAccountChart).then(res => {
      for (let i = 0; i < res.length; i++) {
        dataChartOffchainAccountArr.push({
          ...res[i][0]
        })
        for (let j = 0; j < res[i][0].accountValueRepo.accountChart.length; j++) {
          allOffchainChartArr.push(
            res[i][0]?.accountValueRepo.accountChart[j]
          )
        }
      }
      setInfoOffchain(dataChartOffchainAccountArr)
      const data = []
      const holder = allOffchainChartArr.reduce((prev, { timestamp, totalValue }) => {
        prev[timestamp] = prev[timestamp] ? prev[timestamp] + parseFloat(totalValue) : parseFloat(totalValue)
        return prev
      }, {})
      for (const i in holder) {
        data.push({ totalValue: holder[i], timestamp: i })
      }
      // console.log('DATACHART', data)
      const balanceFluctuations = (parseFloat(data[0]?.totalValue) - parseFloat(data[data.length - 1]?.totalValue)) || 0
      const percent = balanceFluctuations ? totalPercent(balanceFluctuations, (data && data[0]?.totalValue)).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,') : 0
      setPercentInfo({
        balanceFluctuations: balanceFluctuations,
        percent: percent
      })
      setTotalOffchainChart(data.reverse())
    })
    console.log('DATAAAAAAA', dataChartOffchainAccountArr)
  }, [listAccounts])

  return (
    <>
      { listAccounts === null
        ? <PropertyOverview
          title='Offchain Wallet'
          dataChart={totalOffchainChart}
          width={200}
          height={80}
          total={parseFloat(props.total)}
          isButton={true}
          titleButton='Add Account'
          detail={{ 'isDetail': false }}
          listPeriod={[
            { 'title': '1H', 'period': '1h' },
            { 'title': '1D', 'period': '1d' },
            { 'title': '1M', 'period': '1m' },
            { 'title': 'ALL', 'period': 'all' }
          ]}
          type='offchain'
          percentInfo={percentInfo}
          setDataChangeByPeriod={setDataChangeByPeriod}
        />
        : <PropertyOverview
          title='Offchain Wallet'
          dataChart={totalOffchainChart}
          width={200}
          height={80}
          total={parseFloat(props.total)}
          isButton={true}
          titleButton='Add Account'
          detail={{ 'isDetail': true, 'linkUrl': 'offchain' }}
          listPeriod={[
            { 'title': '1H', 'period': '1h' },
            { 'title': '1D', 'period': '1d' },
            { 'title': '1M', 'period': '1m' },
            { 'title': 'ALL', 'period': 'all' }
          ]}
          type='offchain'
          percentInfo={percentInfo}
          setDataChangeByPeriod={setDataChangeByPeriod}
        />
      }
    </>
  )
}

export default AssetOffChain
