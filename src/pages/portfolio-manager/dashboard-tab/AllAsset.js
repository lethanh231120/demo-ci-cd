import React, { useState, useEffect } from 'react'
import PropertyOverview from '../../../components/property-overview/PropertyOverview'
import { Row, Col } from 'antd'
// import PieChartAsset from '../../../components/chart/PieChart'
import OnchainAssetTable from './table/OnchainAssetTable'
import OffchainAssetTable from './table/OffchainAssetTable'
import StockAssetTable from './table/StockAssetTable'
import AssetStock from './overview/AssetStock'
import AssetOnChain from './overview/AssetOnChain'
import AssetOffChain from './overview/AssetOffChain'
import { useQuery } from '@tanstack/react-query'
import { get as getStock } from '../../../api/stockService'
import { getConnection } from '../../../api/connectService'
import { postBinance } from '../../../api/binanceService'

const dataChart = [
  { totalValue: 5520785486.214152, timestamp: '2022-09-18T08:50:00Z' },
  { totalValue: 5521803264.503245, timestamp: '2022-09-18T08:45:00Z' },
  { totalValue: 5520507332.999393, timestamp: '2022-09-18T08:40:00Z' },
  { totalValue: 5521595852.674142, timestamp: '2022-09-18T08:35:00Z' },
  { totalValue: 5520186812.497448, timestamp: '2022-09-18T08:30:00Z' },
  { totalValue: 5520476283.461426, timestamp: '2022-09-18T08:25:00Z' },
  { totalValue: 5517070240.168135, timestamp: '2022-09-18T08:20:00Z' },
  { totalValue: 5509848022.339613, timestamp: '2022-09-18T08:15:00Z' },
  { totalValue: 5512754221.32966, timestamp: '2022-09-18T08:10:00Z' },
  { totalValue: 5510609803.097517, timestamp: '2022-09-18T08:05:00Z' },
  { totalValue: 5515307590.970818, timestamp: '2022-09-18T08:00:00Z' },
  { totalValue: 5520997205.68044, timestamp: '2022-09-18T07:55:00Z' }
]

const AllAsset = ({ stocks, connections, setInfoNft }) => {
  const [infoStock, setInfoStock] = useState()
  const [infoOnChange, setInfoOnchange] = useState({ loading: true })
  const [dataChangeByPeriod, setDataChangeByPeriod] = useState({ 'period': '30' })
  const [chartAllStock, setChartAllStock] = useState([])
  const [infoOffchain, setInfoOffchain] = useState()
  const [total, setTotal] = useState(0)
  const [dataOffchain, setDataOffChain] = useState()
  // get data offchain here
  const { data: listAccounts } = useQuery(
    ['listAccounts'],
    async() => {
      const account = await getConnection('connect/current-accounts/cexName=binance')
      return account.data
    }
  )
  const listAccountInfo = listAccounts?.map(async(account) => {
    const dataForm = {
      'accounts': [{
        apiKey: account.apiKey,
        secretKey: account.secretKey
      }]
    }
    const accountInfo = await postBinance('binance/account-info', dataForm)
    return accountInfo?.data
  })
  useEffect(() => {
    const merged = []
    Promise?.all(listAccountInfo).then(res => {
      let totalValue = 0
      for (let i = 0; i < res.length; i++) {
        totalValue += parseFloat(res[i][0]?.account?.totalValue)
        merged.push({
          ...res[i][0],
          ...listAccounts[i],
          ...infoOffchain[i] && infoOffchain[i]
        })
      }
      setTotal(totalValue)
      setDataOffChain(merged)
    })
  }, [listAccounts, infoOffchain])
  // asset stock here
  // get chart by period
  useQuery(
    ['chartByPeriod', dataChangeByPeriod],
    async() => {
      const chart = await getStock(`stocks/assets/history?limit=${dataChangeByPeriod?.period}`)
      if (chart?.data?.totalAssets !== null) {
        const newChart = [...chart.data.totalAssets]
        chart?.data?.totalAssets?.map((item, index) => {
          newChart[index] = {
            accountId: item?.accountId,
            id: item?.id,
            totalValue: item?.total,
            timestamp: item?.createdDate
          }
        })
        setChartAllStock(newChart.reverse())
      } else {
        setChartAllStock([])
      }
    }
  )

  return (
    <>
      <Row gutter={60} style={{ marginTop: '50px' }}>
        <Col span={16}>
          <PropertyOverview
            title={'All Asset'}
            dataChart={dataChart}
            width={500}
            height={100}
            total={5510609803.097517}
            detail={{ 'isDetail': false }}
            isButton={false}
            titleButton=''
            listPeriod={[
              { 'title': '1H', 'period': '1h' },
              { 'title': '1D', 'period': '1d' },
              { 'title': '7D', 'period': '7d' },
              { 'title': '1M', 'period': '1m' },
              { 'title': '3M', 'period': '3m' },
              { 'title': '6M', 'period': '6m' },
              { 'title': 'ALL', 'period': 'all' }
            ]}
            type='all'
            balanceFluctuations={5509848.2131231}
            percent={2.45}
            setDataChangeByPeriod={setDataChangeByPeriod}
          />
        </Col>
        <Col span={8}>
          {/* <PieChartAsset width={400} height={400}/> */}
        </Col>
      </Row>
      <Row gutter={32} style={{ marginTop: '50px' }}>
        <Col span={8}>
          <AssetOnChain
            setInfoOnchange={setInfoOnchange}
            connections={connections}
            setInfoNft={setInfoNft}
            infoOnChange={infoOnChange}
          />
        </Col>
        <Col span={8}>
          <AssetOffChain
            total={total && total}
            listAccounts={listAccounts && listAccounts}
            setInfoOffchain = {setInfoOffchain}
          />
        </Col>
        <Col span={8}>
          <AssetStock
            infoStock={infoStock}
            stocks={stocks}
            setInfoStock={setInfoStock}
            chartAllStock = {chartAllStock && chartAllStock}
          />
        </Col>
      </Row>
      <OnchainAssetTable
        isButtonDetail={true}
        loading={infoOnChange?.loading}
        data={infoOnChange && infoOnChange?.listConnect?.slice(0, 5)}
        type='onchain'
        isListAction={false}
        paginate={false}
        scroll=''
      />
      <OffchainAssetTable
        isButtonDetail={true}
        dataOffchain={dataOffchain && dataOffchain}
        // loading={loading}
        // data={data}
      />
      <StockAssetTable
        stocks={stocks}
        isButtonDetail={true}
        setInfoStock={setInfoStock}
        // loading={loading}
        // data={data}
      />
    </>
  )
}

export default AllAsset
