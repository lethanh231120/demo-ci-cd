import React, { useState, useContext, memo } from 'react'
import { Typography } from 'antd'
import TableAsset from '../../../../components/modal/table/TableAsset'
import { Button, Popconfirm } from 'antd'
import { useNavigate } from 'react-router-dom'
import Chart from '../../../../components/chart/LineChart'
import './style.scss'
import { EditOutlined, DeleteOutlined, LineChartOutlined, HeartOutlined } from '@ant-design/icons'
import { delConnection } from '../../../../api/connectService'
import ModalEdit from '../../../../components/modal/modal-edit/EditConnection'
import { PlatFormContext } from '../../../../layouts'
import { useQueryClient } from '@tanstack/react-query'

const { Title } = Typography

const OnchainAssetTable = ({ loading, data, isButtonDetail, type, isListAction, paginate, scroll }) => {
  const [isModalEdit, setIsModalEdit] = useState(false)
  const [dataEdit, setDataEdit] = useState(false)
  const navigate = useNavigate()
  const queryClient = useQueryClient()
  const state = useContext(PlatFormContext)

  const handleDeleteConnection = async(record) => {
    try {
      await delConnection(`connect/connectionId=${record.id}`)
      queryClient.invalidateQueries('listConnection')
      state.handleSetData({
        addressDelete: record.address,
        listNft: state?.propsImport?.listNft,
        listHolding: state?.propsImport?.listHolding,
        initial: false
      })
    } catch (error) {
      console.log(error)
    }
  }

  const handleEditAddress = (record) => {
    setIsModalEdit(true)
    setDataEdit(record)
  }

  const handleClickItem = (record) => {
    if (type === 'onchain') {
      navigate(`${record?.id}`, { state: { data: record }})
    }
  }

  const columns = [
    {
      key: 'index',
      title: '#',
      width: '20px',
      render: (_, record) => (<span style={{ color: '#A8ADB3' }}>{record?.index + 1}</span>)
    },
    {
      key: 'connectionName',
      title: 'Name',
      width: '200px',
      sorter: (a, b) => a.connectionName.toLowerCase().localeCompare(b.connectionName.toLowerCase()),
      render: (_, record) => (
        <div className='table-name'>
          <span className='table-symbol'>{record?.connectionName}</span>
        </div>
      )
    },
    {
      key: 'total',
      title: 'Curren Total',
      width: '150px',
      sorter: (a, b) => a.totalValue.toLowerCase().localeCompare(b.totalValue.toLowerCase()),
      render: (_, record) => (
        <div className='table-name'>
          <span className='table-symbol'>{record?.totalValue}</span>
        </div>
      )
    },
    {
      key: 'total1d',
      title: 'Total 1D',
      width: '150px',
      // sorter: (a, b) => parseFloat(a.amount) - parseFloat(b.amount),
      render: (_, record) => (
        <>
          {/* <span>{parseInt(record.amount)}</span> */}
        </>
      )
    },
    {
      key: 'total7d',
      title: 'Total 7D',
      width: '150px',
      // sorter: (a, b) => parseFloat(a.adjpriceclose) - parseFloat(b.adjpriceclose),
      render: (_, record) => (
        <>
          {/* <span>{record?.adjpriceclose ? record?.adjpriceclose : 0}</span> */}
        </>
      )
    },
    {
      key: 'chart',
      title: 'Price Chart 24H',
      width: '150px',
      render: (_, record) => (<Chart record={record?.chart24h}/>)
    },
    {
      key: 'more',
      title: '',
      className: 'table-plus',
      width: '20px',
      dataIndex: 'key',
      render: (_, record) => (
        <>
          {isListAction && (
            <div className='onchain-table-action'>
              <div className='onchain-table-action-icon'>
                <HeartOutlined/>
              </div>
              <div className='onchain-table-action-icon'>
                <LineChartOutlined
                  onClick={() => handleClickItem(record)}
                />
              </div>
              <div
                className='onchain-table-action-icon'
                onClick={() => handleEditAddress(record)}
              >
                <EditOutlined />
              </div>
              <Popconfirm
                placement='top'
                className='onchain-table-action-icon'
                title='Are you sure to delete this connection?'
                onConfirm={() => handleDeleteConnection(record)}
                okText='Yes'
                cancelText='No'
              >
                <DeleteOutlined className='tab-list-icon-item' />
              </Popconfirm>
            </div>
          )}
        </>
      )
    }
  ].filter(item => !item.hidden)

  const handleNavigate = () => {
    navigate('onchain')
  }

  return (
    <div className='onchain-table'>
      <Title level={2} style={{ color: '#fff', textAlign: 'left' }}>{isButtonDetail && 'Onchain Wallet'}</Title>
      <TableAsset
        columns={columns}
        data={data}
        loading={loading}
        type={type}
        paginate={paginate}
        scroll={scroll}
      />
      {isButtonDetail && (
        <Button
          style={{ marginTop: '20px' }}
          type='primary'
          onClick={handleNavigate}
          disabled={loading}
        >
            Detail
        </Button>
      )}
      <ModalEdit
        isModalEdit={isModalEdit}
        setIsModalEdit={setIsModalEdit}
        dataEdit={dataEdit}
      />
    </div>
  )
}

export default memo(OnchainAssetTable)
