import React from 'react'
import TableAsset from '../../../../components/modal/table/TableAsset'
import './style.scss'

const ListCoinTable = ({ columns, data, pagination, scroll, detailCoin }) => {
  return (
    <div className='onchain-table'>
      <TableAsset
        columns={columns}
        data={data}
        paginate={pagination}
        scroll={scroll}
        detailCoin={detailCoin}
      />
    </div>
  )
}

export default ListCoinTable
