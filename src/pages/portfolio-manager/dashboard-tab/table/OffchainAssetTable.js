import React, { useEffect, useState } from 'react'
import { Table, Typography } from 'antd'
import TableAsset from '../../../../components/modal/table/TableAsset'
import { Button, Popconfirm } from 'antd'
import { useNavigate } from 'react-router-dom'
import { EditOutlined, DeleteOutlined, LineChartOutlined } from '@ant-design/icons'
import './stock.scss'
import { useQueryClient } from '@tanstack/react-query'
import { delConnection } from '../../../../api/connectService'
import Chart from '../../../../components/chart/LineChart'
const { Title } = Typography

const OffchainAssetTable = ({ isButtonDetail, dataOffchain, loading, dataForOffchainDetail, dataForSingleOffchainAccount }) => {
  const [data, setData] = useState()
  const dataForTable = []
  const queryClient = useQueryClient()
  useEffect(() => {
    if (dataOffchain) {
      dataOffchain?.map((connection) => {
        // console.log('CONNECTION', connection.account)
        dataForTable.push({ name: connection?.connectionName,
          totalValue: connection?.account?.totalValue,
          totalValueAll: connection?.account?.totalValueAll,
          totalValue1h: connection?.account?.totalValue1h,
          totalValue4h: connection?.account?.totalValue4h,
          totalValue1d: connection?.account?.totalValue1d,
          totalValue7d: connection?.account?.totalValue7d,
          totalValue1m: connection?.account?.totalValue1m,
          totalValue3m: connection?.account?.totalValue3m,
          totalValue6m: connection?.account?.totalValue6m,
          totalValue1y: connection?.account?.totalValue1y,
          valueChart: connection?.accountValueRepo?.accountChart,
          children: [{
            name: 'Spot',
            totalValue: connection?.spotAccount?.subAccount?.totalValue,
            totalValueAll: connection?.spotAccount?.subAccount?.totalValueAll,
            totalValue1h: connection?.spotAccount?.subAccount?.totalValue1h,
            totalValue4h: connection?.spotAccount?.subAccount?.totalValue4h,
            totalValue1d: connection?.spotAccount?.subAccount?.totalValue1d,
            totalValue7d: connection?.spotAccount?.subAccount?.totalValue7d,
            totalValue1m: connection?.spotAccount?.subAccount?.totalValue1m,
            totalValue3m: connection?.spotAccount?.subAccount?.totalValue3m,
            totalValue6m: connection?.spotAccount?.subAccount?.totalValue6m,
            totalValue1y: connection?.spotAccount?.subAccount?.totalValue1y,
            valueChart: connection?.spotValueRepo?.spotChart
          }, {
            name: 'Margin Cross',
            totalValue: connection?.marginCrossAccount?.subAccount?.totalValue,
            totalValueAll: connection?.marginCrossAccount?.subAccount?.totalValueAll,
            totalValue1h: connection?.marginCrossAccount?.subAccount?.totalValue1h,
            totalValue4h: connection?.marginCrossAccount?.subAccount?.totalValue4h,
            totalValue1d: connection?.marginCrossAccount?.subAccount?.totalValue1d,
            totalValue7d: connection?.marginCrossAccount?.subAccount?.totalValue7d,
            totalValue1m: connection?.marginCrossAccount?.subAccount?.totalValue1m,
            totalValue3m: connection?.marginCrossAccount?.subAccount?.totalValue3m,
            totalValue6m: connection?.marginCrossAccount?.subAccount?.totalValue6m,
            totalValue1y: connection?.marginCrossAccount?.subAccount?.totalValue1y,
            valueChart: connection?.magrinCrossValueRepo?.magrinCrossChart
          }, {
            name: 'Margin Isolated',
            totalValue: connection?.marginIsolatedAccount?.subAccount?.totalValue,
            totalValueAll: connection?.marginIsolatedAccount?.subAccount?.totalValueAll,
            totalValue1h: connection?.marginIsolatedAccount?.subAccount?.totalValue1h,
            totalValue4h: connection?.marginIsolatedAccount?.subAccount?.totalValue4h,
            totalValue1d: connection?.marginIsolatedAccount?.subAccount?.totalValue1d,
            totalValue7d: connection?.marginIsolatedAccount?.subAccount?.totalValue7d,
            totalValue1m: connection?.marginIsolatedAccount?.subAccount?.totalValue1m,
            totalValue3m: connection?.marginIsolatedAccount?.subAccount?.totalValue3m,
            totalValue6m: connection?.marginIsolatedAccount?.subAccount?.totalValue6m,
            totalValue1y: connection?.marginIsolatedAccount?.subAccount?.totalValue1y,
            valueChart: connection?.magrinIsolatedValueRepo?.magrinIsolatedChart
          }, {
            name: 'Saving',
            totalValue: connection?.savingsAccount?.subAccount?.totalValue,
            totalValueAll: connection?.savingsAccount?.subAccount?.totalValueAll,
            totalValue1h: connection?.savingsAccount?.subAccount?.totalValue1h,
            totalValue4h: connection?.savingsAccount?.subAccount?.totalValue4h,
            totalValue1d: connection?.savingsAccount?.subAccount?.totalValue1d,
            totalValue7d: connection?.savingsAccount?.subAccount?.totalValue7d,
            totalValue1m: connection?.savingsAccount?.subAccount?.totalValue1m,
            totalValue3m: connection?.savingsAccount?.subAccount?.totalValue3m,
            totalValue6m: connection?.savingsAccount?.subAccount?.totalValue6m,
            totalValue1y: connection?.savingsAccount?.subAccount?.totalValue1y,
            valueChart: connection?.savingValueRepo?.savingChart
          }, {
            name: 'Future - Stable part',
            totalValue: connection?.usdsMFutureAccount?.subAccount?.totalValue,
            totalValueAll: connection?.usdsMFutureAccount?.subAccount?.totalValueAll,
            totalValue1h: connection?.usdsMFutureAccount?.subAccount?.totalValue1h,
            totalValue4h: connection?.usdsMFutureAccount?.subAccount?.totalValue4h,
            totalValue1d: connection?.usdsMFutureAccount?.subAccount?.totalValue1d,
            totalValue7d: connection?.usdsMFutureAccount?.subAccount?.totalValue7d,
            totalValue1m: connection?.usdsMFutureAccount?.subAccount?.totalValue1m,
            totalValue3m: connection?.usdsMFutureAccount?.subAccount?.totalValue3m,
            totalValue6m: connection?.usdsMFutureAccount?.subAccount?.totalValue6m,
            totalValue1y: connection?.usdsMFutureAccount?.subAccount?.totalValue1y,
            valueChart: connection?.usDsMFutureValueRepo?.usDsMFutureChart
          }, {
            name: 'Future - Coins part',
            totalValue: connection?.coinMFutureAccount?.subAccount?.totalValue,
            totalValueAll: connection?.coinMFutureAccount?.subAccount?.totalValueAll,
            totalValue1h: connection?.coinMFutureAccount?.subAccount?.totalValue1h,
            totalValue4h: connection?.coinMFutureAccount?.subAccount?.totalValue4h,
            totalValue1d: connection?.coinMFutureAccount?.subAccount?.totalValue1d,
            totalValue7d: connection?.coinMFutureAccount?.subAccount?.totalValue7d,
            totalValue1m: connection?.coinMFutureAccount?.subAccount?.totalValue1m,
            totalValue3m: connection?.coinMFutureAccount?.subAccount?.totalValue3m,
            totalValue6m: connection?.coinMFutureAccount?.subAccount?.totalValue6m,
            totalValue1y: connection?.coinMFutureAccount?.subAccount?.totalValue1y,
            valueChart: connection?.coinMFutureValueRepo?.coinMFutureChart
          }]
        })
        console.log('data', dataForTable)
        setData(dataForTable)
      })
    }
  }, [dataOffchain])
  const navigate = useNavigate()

  const handleClickItem = (record) => {
    navigate(`../${record.id}`, { state: { data: record }})
  }

  const handleDeleteAccount = async(record) => {
    try {
      await delConnection(`connect/cexId=${record.id}`)
      queryClient.invalidateQueries('listAccounts')
    } catch (error) {
      console.log(error)
    }
  }

  const columnsForShortTable = [
    {
      key: 'rank',
      title: '#',
      width: '20px'
      // render: (_, record, index) => (<span style={{ color: '#A8ADB3' }}>{index + 1}</span>)
    },
    {
      key: 'name',
      title: 'Name',
      width: '300px',
      sorter: (a, b) => a.name.toLowerCase().localeCompare(b.name.toLowerCase()),
      render: (_, record) => (
        <div className='table-name'>
          <span className='table-symbol'>{record?.name}</span>
        </div>
      )
    },
    {
      key: 'total',
      title: 'Total',
      with: '150px',
      sorter: (a, b) => parseFloat(a.totalValue) - parseFloat(b.totalValue),
      render: (_, record) => (
        <span>{parseFloat(record?.totalValue).toFixed(2)}</span>
      )
    },
    {
      key: 'totalall',
      title: 'Total All',
      width: '150px',
      sorter: (a, b) => parseFloat(a.totalValueAll) - parseFloat(b.totalValueAll),
      render: (_, record) => (
        <>
          <span>{parseFloat(record?.totalValueAll).toFixed(2)}</span>
        </>
      )
    },
    {
      key: 'chart',
      title: 'Price Chart 24H',
      width: '150px',
      render: (_, record) => (<Chart record={record?.valueChart}/>)
    }
  ]

  const columns = [
    {
      key: 'rank',
      title: '#',
      width: '20px'
      // render: (_, record, index) => (<span style={{ color: '#A8ADB3' }}>{index + 1}</span>)
    },
    {
      key: 'name',
      title: 'Name',
      width: '300px',
      sorter: (a, b) => a.name.toLowerCase().localeCompare(b.name.toLowerCase()),
      render: (_, record) => (
        <div className='table-name'>
          <span className='table-symbol'>{record?.name}</span>
        </div>
      )
    },
    {
      key: 'total',
      title: 'Total',
      with: '150px',
      sorter: (a, b) => parseFloat(a.totalValue) - parseFloat(b.totalValue),
      render: (_, record) => (
        <span>{parseFloat(record?.totalValue).toFixed(2)}</span>
      )
    },
    {
      key: 'total1h',
      title: 'Total 1h',
      width: '150px',
      sorter: (a, b) => parseFloat(a.totalValue1h) - parseFloat(b.totalValue1h),
      render: (_, record) => (
        <span>{parseFloat(record?.totalValue1h).toFixed(2)}</span>
      )
    },
    {
      key: 'total4h',
      title: 'Total 4h',
      width: '150px',
      sorter: (a, b) => parseFloat(a.totalValue4h) - parseFloat(b.totalValue4h),
      render: (_, record) => (
        <>
          <span>{parseFloat(record.totalValue4h).toFixed(2)}</span>
        </>
      )
    },
    {
      key: 'total1d',
      title: 'Total 1D',
      width: '150px',
      sorter: (a, b) => parseFloat(a.totalValue1d) - parseFloat(b.totalValue1d),
      render: (_, record) => (
        <>
          <span>{parseFloat(record.totalValue1d).toFixed(2)}</span>
        </>
      )
    },
    {
      key: 'total7d',
      title: 'Total 7D',
      width: '150px',
      sorter: (a, b) => parseFloat(a.totalValue7d) - parseFloat(b.totalValue7d),
      render: (_, record) => (
        <>
          <span>{parseFloat(record.totalValue7d).toFixed(2)}</span>
        </>
      )
    },
    {
      key: 'total1m',
      title: 'Total 1M',
      width: '150px',
      sorter: (a, b) => parseFloat(a.totalValue1m) - parseFloat(b.totalValue1m),
      render: (_, record) => (
        <>
          <span>{parseFloat(record.totalValue1m).toFixed(2)}</span>
        </>
      )
    },
    {
      key: 'total3m',
      title: 'Total 3M',
      width: '150px',
      sorter: (a, b) => parseFloat(a.totalValue3m) - parseFloat(b.totalValue3m),
      render: (_, record) => (
        <>
          <span>{parseFloat(record.totalValue3m).toFixed(2)}</span>
        </>
      )
    },
    {
      key: 'total6m',
      title: 'Total 6M',
      width: '150px',
      sorter: (a, b) => parseFloat(a.totalValue6m) - parseFloat(b.totalValue6m),
      render: (_, record) => (
        <>
          <span>{parseFloat(record.totalValue6m).toFixed(2)}</span>
        </>
      )
    },
    {
      key: 'total1y',
      title: 'Total 1Y',
      width: '150px',
      sorter: (a, b) => parseFloat(a.totalValue1y) - parseFloat(b.totalValue1y),
      render: (_, record) => (
        <>
          <span>{parseFloat(record.totalValue1y).toFixed(2)}</span>
        </>
      )
    },
    {
      key: 'totalall',
      title: 'Total All',
      width: '150px',
      sorter: (a, b) => parseFloat(a.totalValueAll) - parseFloat(b.totalValueAll),
      render: (_, record) => (
        <>
          <span>{parseFloat(record?.totalValueAll).toFixed(2)}</span>
        </>
      )
    },
    {
      key: 'chart',
      title: 'Price Chart 24H',
      width: '150px',
      render: (_, record) => (<Chart record={record?.valueChart}/>)
    },
    {
      key: 'more',
      title: '',
      className: 'table-plus',
      width: '20px',
      dataIndex: 'key',
      render: (_, record) => (
        <>
          <div className='onchain-table-action'>
            <div className='onchain-table-action-icon'>
              <LineChartOutlined
                onClick={() => handleClickItem(record)}
              />
            </div>
            <div
              className='onchain-table-action-icon'
              // onClick={() => handleEditAddress(record)}
            >
              <EditOutlined />
            </div>
            <Popconfirm
              placement='top'
              className='onchain-table-action-icon'
              title='Are you sure to delete this connection?'
              onConfirm={() => handleDeleteAccount(record)}
              okText='Yes'
              cancelText='No'
            >
              <DeleteOutlined className='tab-list-icon-item' />
            </Popconfirm>
          </div>
        </>
      )
    }
  ].filter(item => !item.hidden)

  const columnsForSingleAccount = [
    {
      key: 'rank',
      title: '#',
      width: '20px',
      render: (_, record, index) => (<span style={{ color: '#A8ADB3' }}>{index + 1}</span>)
    },
    {
      key: 'name',
      title: 'Name',
      width: '300px',
      sorter: (a, b) => a.asset.toLowerCase().localeCompare(b.asset.toLowerCase()),
      render: (_, record) => (
        <div className='table-name'>
          <span className='table-symbol'>{record?.asset}</span>
        </div>
      )
    },
    {
      key: 'free',
      title: 'Free',
      width: '150px',
      sorter: (a, b) => parseFloat(a.free) - parseFloat(b.free),
      render: (_, record) => (
        <span>{record?.free ? parseFloat(record?.free).toFixed(2) : parseFloat(record?.amount).toFixed(2)}</span>
      )
    },
    {
      key: 'locked',
      title: 'Locked',
      width: '150px',
      sorter: (a, b) => parseFloat(a.locked) - parseFloat(b.locked),
      render: (_, record) => (
        <span>{parseFloat(record?.locked).toFixed(2)}</span>
      )
    },
    {
      key: 'prices',
      title: 'Prices',
      width: '150px',
      sorter: (a, b) => parseFloat(a.price) - parseFloat(b.price),
      render: (_, record) => (
        <span>{parseFloat(record?.price).toFixed(2)}</span>
      )
    }
  ].filter(item => !item.hidden)

  return (
    <div className='offchain__table--container'>
      <Title level={2} style={{ color: '#fff', textAlign: 'left', marginTop: '50px' }}>{isButtonDetail && 'Offchain Wallet'}</Title>
      { dataForOffchainDetail !== undefined
        ? <TableAsset
          columns={columns}
          data={dataForOffchainDetail}
          loading={loading}
        />
        : dataForSingleOffchainAccount !== undefined
          ? <TableAsset
            columns={columnsForSingleAccount}
            data={dataForSingleOffchainAccount?.holdings}
            loading={loading}
          />
          : <Table
            loading={loading}
            columns={columnsForShortTable}
            dataSource={data}
            scroll={{ x: 'max-content' }}
            showSorterTooltip={false}
            rowKey={(record) => record?.id}
            pagination={false}
          />
      }
      {isButtonDetail && (
        <Button
          style={{ marginTop: '20px' }}
          type='primary'
          onClick={() => navigate('offchain')}
        >
          Detail
        </Button>
      )}
    </div>
  )
}

export default OffchainAssetTable
