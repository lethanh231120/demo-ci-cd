import React, { useState, useEffect } from 'react'
import { Typography, Button, Tooltip, notification, Modal, Table } from 'antd'
import TableAsset from '../../../../components/modal/table/TableAsset'
import { useNavigate } from 'react-router-dom'
import Chart from '../../../../components/chart/LineChart'
import { EditOutlined, ClearOutlined, SmileOutlined } from '@ant-design/icons'
import './style.scss'
import ModalChangeAmount from '../../detail-stock/ModalChangeAmount'
import { patch, get } from '../../../../api/stockService'

const { Title } = Typography

const StockAssetTable = ({ isButtonDetail, stocks, loading, dataForDetailStockTable }) => {
  const [itemChange, setItemChange] = useState()
  const [dataForTable, setDataForTable] = useState([])
  const [openModalChange, setOpenModalChange] = useState(false)
  const TYPE_SUBSTRACT = 2
  const navigate = useNavigate()

  // get data for stock table
  const listSymbolPriceInfo = stocks?.map(async(symbol) => {
    const data = await get(`stocks/prices/eod?limit=30&exchange=${symbol.exchange}&symbol=${symbol.symbol}`)
    return data.data
  })

  useEffect(() => {
    Promise?.all(listSymbolPriceInfo).then(res => {
      const listDataChart = []
      const dataTable = []
      res?.forEach((item) => {
        const dataChart = []
        item?.forEach((itemInChart) => {
          dataChart.push({ 'price': itemInChart?.adjpriceclose, 'eoddate': itemInChart?.eoddate })
        })
        const newItem = {
          ...item[0],
          'dataChart': dataChart
        }
        listDataChart.push(newItem)
      })
      for (let i = 0; i < stocks?.length; i++) {
        dataTable.push({
          ...stocks[i],
          ...(listDataChart.find((itmInner) => itmInner.symbol === stocks[i].symbol)) }
        )
      }
      setDataForTable(dataTable)
    })
  }, [stocks])

  const handleChangeAmount = (record) => {
    setItemChange(record)
    setOpenModalChange(true)
  }

  const openNotification = (messageRes) => {
    notification.open({
      description: messageRes,
      duration: 2,
      icon: (
        <SmileOutlined
          style={{
            color: '#108ee9'
          }}
        />
      )
    })
  }

  const handleDeleteStockConnection = async(record) => {
    try {
      const data = {
        type: TYPE_SUBSTRACT,
        exchange: record.exchange,
        symbol: record.symbol,
        amount: record.amount
      }
      await patch(`stocks/assets/change-amount`, { holdings: [data] })
      openNotification('Delete stock successfully')
      navigate('../')
    } catch (error) {
      console.log(error)
    }
  }

  const columns = [
    {
      key: 'rank',
      title: '#',
      width: '20px',
      render: (_, record, index) => (<span style={{ color: '#A8ADB3' }}>{index + 1}</span>)
    },
    {
      key: 'symbol',
      title: 'Symbol',
      width: '100px',
      sorter: (a, b) => a.symbol.toLowerCase().localeCompare(b.symbol.toLowerCase()),
      render: (_, record) => (
        <div className='table-name'>
          <span className='table-symbol'>{record?.symbol}</span>
        </div>
      )
    },
    {
      key: 'exchange',
      title: 'Exchange',
      width: '150px',
      sorter: (a, b) => a.exchange.toLowerCase().localeCompare(b.exchange.toLowerCase()),
      render: (_, record) => (
        <div className='table-name'>
          <span className='table-symbol'>{record?.exchange}</span>
        </div>
      )
    },
    {
      // key: 'adjpriceclose',
      title: 'Curren Price',
      width: '150px',
      sorter: (a, b) => parseFloat(a.adjpriceclose) - parseFloat(b.adjpriceclose),
      render: (_, record) => (
        <span>{record?.adjpriceclose ? record?.adjpriceclose : 0}</span>
      )
    },
    {
      // key: 'amount',
      title: 'Amount',
      width: '150px',
      sorter: (a, b) => parseFloat(a.amount) - parseFloat(b.amount),
      render: (_, record) => (
        <span>{record?.amount ? parseInt(record?.amount) : 0}</span>
      )
    },
    {
      title: 'Total Value',
      width: '150px',
      sorter: (a, b) => parseFloat(a.amount) * a.adjpriceclose - parseFloat(b.amount) * b.adjpriceclose,
      render: (_, record) => (
        <span>
          {record?.adjpriceclose ? (record?.adjpriceclose * (record?.amount ? parseFloat(record?.amount) : 0)).toFixed(2) : 0}
        </span>
      )
    },
    {
      // key: 'volume',
      title: 'Volume',
      width: '150px',
      sorter: (a, b) => parseFloat(a.volume) - parseFloat(b.volume),
      render: (_, record) => (
        <span>{record.volume}</span>
      )
    },
    {
      key: 'priceopen',
      title: 'Price Open',
      width: '150px',
      sorter: (a, b) => parseFloat(a.priceopen) - parseFloat(b.priceopen),
      render: (_, record) => (
        <span>{record.priceopen}</span>
      )
    },
    {
      key: 'priceclose',
      title: 'Price Close',
      width: '150px',
      sorter: (a, b) => parseFloat(a.priceclose) - parseFloat(b.priceclose),
      render: (_, record) => (
        <span>{record.priceclose}</span>
      )
    },
    {
      key: 'pricehigh',
      title: 'Price High',
      width: '150px',
      sorter: (a, b) => parseFloat(a.pricehigh) - parseFloat(b.pricehigh),
      render: (_, record) => (
        <span>{record.pricehigh}</span>
      )
    },
    {
      key: 'pricelow',
      title: 'Price Low',
      width: '150px',
      sorter: (a, b) => parseFloat(a.pricelow) - parseFloat(b.pricelow),
      render: (_, record) => (
        <span>{record.pricelow}</span>
      )
    },
    {
      key: 'chart',
      title: 'Chart (30 day)',
      width: '150px',
      render: (_, record) => (<Chart record={record?.dataChart}/>)
    },
    {
      key: 'more',
      title: '',
      className: 'table-plus',
      width: '20px',
      dataIndex: 'key',
      fixed: 'right',
      render: (_, record) => (<div style={{ display: 'flex', justifyContent: 'space-between' }}>
        <div className='icon-stock' style={{ width: '40px' }}>
          <EditOutlined onClick={() => handleChangeAmount(record)}/>
        </div>
        <div className='icon-stock' style={{ width: '40px' }}>
          <Tooltip placement='topRight' title={'Clear amount'}>
            <ClearOutlined onClick={() => handleDeleteStockConnection(record)}/>
          </Tooltip>
        </div>
      </div>)
    }
  ]

  const columnsForShortTable = [
    {
      key: 'rank',
      title: '#',
      width: '20px',
      render: (_, record, index) => (<span style={{ color: '#A8ADB3' }}>{index + 1}</span>)
    },
    {
      key: 'symbol',
      title: 'Symbol',
      width: '100px',
      sorter: (a, b) => a.symbol.toLowerCase().localeCompare(b.symbol.toLowerCase()),
      render: (_, record) => (
        <div className='table-name'>
          <span className='table-symbol'>{record?.symbol}</span>
        </div>
      )
    },
    {
      key: 'exchange',
      title: 'Exchange',
      width: '150px',
      sorter: (a, b) => a.exchange.toLowerCase().localeCompare(b.exchange.toLowerCase()),
      render: (_, record) => (
        <div className='table-name'>
          <span className='table-symbol'>{record?.exchange}</span>
        </div>
      )
    },
    {
      // key: 'adjpriceclose',
      title: 'Curren Price',
      width: '150px',
      sorter: (a, b) => parseFloat(a.adjpriceclose) - parseFloat(b.adjpriceclose),
      render: (_, record) => (
        <span>{record?.adjpriceclose ? record?.adjpriceclose : 0}</span>
      )
    },
    {
      // key: 'amount',
      title: 'Amount',
      width: '150px',
      sorter: (a, b) => parseFloat(a.amount) - parseFloat(b.amount),
      render: (_, record) => (
        <span>{record?.amount ? parseInt(record?.amount) : 0}</span>
      )
    },
    {
      title: 'Total Value',
      width: '150px',
      sorter: (a, b) => parseFloat(a.amount) * a.adjpriceclose - parseFloat(b.amount) * b.adjpriceclose,
      render: (_, record) => (
        <span>
          {record?.adjpriceclose ? (record?.adjpriceclose * (record?.amount ? parseFloat(record?.amount) : 0)).toFixed(2) : 0}
        </span>
      )
    },
    {
      key: 'chart',
      title: 'Chart (30 day)',
      width: '150px',
      render: (_, record) => (<Chart record={record?.dataChart}/>)
    }
  ]

  return (
    <>
      <div className='stock-table'>
        <Title level={2} style={{ color: '#fff', textAlign: 'left', marginTop: '50px' }}>{isButtonDetail && 'Stock'}</Title>
        { dataForDetailStockTable !== undefined
          ? <TableAsset
            columns={columns}
            data={dataForDetailStockTable}
            loading={loading}
          />
          : <Table
            loading={loading}
            columns={columnsForShortTable}
            dataSource={dataForTable}
            scroll={{ x: 'max-content' }}
            showSorterTooltip={false}
            rowKey={(record) => record?.id}
            pagination={false}
          />
        }
        {isButtonDetail && (
          <Button
            style={{ marginTop: '20px' }}
            type='primary'
            onClick={() => navigate('detail-stock')}
          >
            Detail
          </Button>
        )}
      </div>
      <Modal
        className='update-stock-modal'
        visible={openModalChange}
        onOk = {() => setOpenModalChange(false)}
        onCancel = {() => setOpenModalChange(false)}
        footer={null}
      >
        <ModalChangeAmount
          itemChange={itemChange}
          setOpenModalChange={setOpenModalChange}
        />
      </Modal>
    </>
  )
}

export default StockAssetTable
