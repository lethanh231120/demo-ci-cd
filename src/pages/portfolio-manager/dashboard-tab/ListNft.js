import React, { useEffect, useState } from 'react'
import NFT from '../../../components/nft/NFT'
import { Row, Col, Typography } from 'antd'
import './nft.scss'
import DetailConnectionNft from '../detail-connection-nft/DetailConnectionNft'
import _ from 'lodash'
import { BINANCE_SMART_CHAIN_CHAINID, ETHEREUM_CHAINID, SOLANA_CHAINID } from '../../../constants/ChainId'
import { post } from '../../../api/coinPriceService'
import { IPFS } from '../../../constants/CheckIPFS'
import { IpfsImage } from 'react-ipfs-image'
import { LazyLoadImage } from 'react-lazy-load-image-component'

const { Text, Title } = Typography
const ListNFT = ({ infoNft }) => {
  const [isGroupNFT, setIsGroupNFT] = useState(false)
  const [currentConnection, setCurrenConnection] = useState()
  const [allNft, setAllNft] = useState()
  const [totalValue, setTotalValue] = useState()
  const [listAllImage, setListAllImage] = useState()
  const [image, setImage] = useState()
  const [indexImg, setIndexImg] = useState(0)

  const handleBack = () => {
    setIsGroupNFT(false)
  }

  useEffect(() => {
    const calculateAllNft = async() => {
      const listChainId = []
      const listCoinId = []

      // group list chain id
      const groupByChainId = _.groupBy(infoNft, (item) => {
        if (item?.address?.chainId) {
          return item?.address?.chainId
        }
      })

      // format data
      groupByChainId && Object.keys(groupByChainId).forEach(function(key) {
        (key !== 'undefined') && listChainId.push({
          chainId: key,
          connections: groupByChainId[key]
        })
      })

      // get list coinId
      listChainId.forEach((item) => {
        switch (item?.chainId) {
          case ETHEREUM_CHAINID:
            listCoinId.push('ethereum')
            break
          case BINANCE_SMART_CHAIN_CHAINID:
            listCoinId.push('bnb-chain')
            break
          case SOLANA_CHAINID:
            listCoinId.push('solana')
            break
          default:
            break
        }
      })

      // get price coin info of nft
      if (!_.isEmpty(listCoinId)) {
        const listAllNft = []
        const listImageAll = []
        const listCoinInfo = await post('price/info/list', { 'listCoinId': listCoinId })
        listChainId.forEach((item) => {
          // tim trong danh sach coin info nhung dong coin có chainId tưng tự
          const infoNativeCoin = listCoinInfo?.data?.coins?.find((itemCoin) => {
            let data
            switch (item?.chainId) {
              case ETHEREUM_CHAINID:
                data = itemCoin?.coinId === 'ethereum'
                break
              case BINANCE_SMART_CHAIN_CHAINID:
                data = itemCoin?.coinId === 'bnb-chain'
                break
              case SOLANA_CHAINID:
                data = itemCoin?.coinId === 'solana'
                break
              default:
                break
            }
            return data
          })
          const listConnection = []
          // gộp tát cả cac nft group trong 1 connection lai voi nhau
          item?.connections?.forEach((connection) => {
            const nfts = []
            const listImageNftItem = []
            Object.keys(connection?.nfts).forEach(function(key) {
              (key !== 'undefined') && nfts.push(connection?.nfts[key])
              connection?.nfts[key]?.forEach((itemNft) => {
                if (itemNft?.nftInfo?.metadata !== '' && itemNft?.nftInfo?.metadata !== undefined && JSON.parse(itemNft?.nftInfo?.metadata)?.image && JSON.parse(itemNft?.nftInfo?.metadata)?.image !== '') {
                  listImageAll?.push(JSON.parse(itemNft?.nftInfo?.metadata)?.image)
                  listImageNftItem?.push(JSON.parse(itemNft?.nftInfo?.metadata)?.image)
                }
              })
            })
            listConnection.push({
              address: {
                ...connection.address,
                price: infoNativeCoin?.price
              },
              nfts: nfts?.flat(1),
              listImageNftItem: listImageNftItem
            })
          })
          listAllNft.push(listConnection)
        })
        setListAllImage(listImageAll)
        setAllNft(listAllNft?.flat(1))
      }
    }
    calculateAllNft()
  }, [])

  useEffect(() => {
    const countNft = allNft?.reduce((total, curren) => {
      return total + curren?.nfts?.length
    }, 0)
    const totalNft = allNft?.reduce((total, curren) => {
      const totalValue = total + parseFloat(curren?.address?.totalValue) * curren?.address?.price
      return totalValue
    }, 0)
    setTotalValue({
      totalNft: totalNft,
      countNft: countNft
    })
  }, [allNft])

  useEffect(() => {
    setTimeout(function() {
      if (indexImg < listAllImage?.length - 1) {
        let urlImage
        if (listAllImage[indexImg] && listAllImage[indexImg].slice(0, 7) === IPFS) {
          urlImage = <div className='ant-image'><IpfsImage hash={listAllImage[indexImg]}/></div>
        }
        if (listAllImage[indexImg] && listAllImage[indexImg].slice(0, 7) !== IPFS) {
          urlImage = <LazyLoadImage alt='image-nft' src={listAllImage[indexImg]} width= '100%' height= '70%' delayTime='500' effect='blur'/>
        }
        if (!listAllImage[indexImg]) {
          urlImage = <LazyLoadImage alt='image-nft' src='/coins/nft.png' width= '100%' height= '70%' delayTime='500' effect='blur' />
        }
        setImage(urlImage)
        setIndexImg(indexImg + 1)
      }
      if (indexImg === listAllImage?.length - 1) {
        setIndexImg(0)
      }
    }, 2000)
  })

  return (
    <>
      {isGroupNFT ? (
        <DetailConnectionNft
          handleBack={handleBack}
          currentConnection={currentConnection}
        />
      ) : (<>
        <div className='nft-header'>
          <Row gutter={16}>
            <Col span={16}>
              <div className='nft-header-total'>
                <Text>Total Worth</Text>
                <Title level={2}>$ {totalValue?.totalNft} </Title>
              </div>
              <div className='nft-header-count'>
                <Text>NFT Count</Text>
                <Title level={2}>{totalValue?.countNft}</Title>
              </div>
            </Col>
            <Col span={8}>
              <div style={{ width: '200px', height: '200px' }}>
                {image}
              </div>
            </Col>
          </Row>
        </div>
        <div style={{ padding: '50px 0' }}>
          <Row gutter={[16, 16]}>
            {allNft?.map((item, index) => (
              <>
                {(item?.nfts?.length > 0) && (
                  <Col span={6} key={index}>
                    <NFT
                      num={item?.nfts?.length}
                      total={parseFloat(item?.address?.totalValue) * item?.address?.price}
                      connectionName={item?.address?.connectionName}
                      name=''
                      type='connections'
                      setIsGroupNFT={setIsGroupNFT}
                      setCurrenConnection={setCurrenConnection}
                      data={item}
                      urlImage={item?.listImageNftItem}
                    />
                  </Col>
                )}
              </>
            ))}
          </Row>
        </div>
      </>
      )}
    </>
  )
}

export default ListNFT
