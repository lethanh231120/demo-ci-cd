import React, { useEffect, useState } from 'react'
import { ArrowLeftOutlined } from '@ant-design/icons'
import { Typography, Row, Col } from 'antd'
import NFT from '../../../components/nft/NFT'
import _ from 'lodash'

const { Text, Title } = Typography

const DetailConnectionNft = ({ handleBack, currentConnection }) => {
  const [listNft, setListNft] = useState()
  const [totalValue, setTotalValue] = useState()

  useEffect(() => {
    const listNft = []
    currentConnection?.data?.nfts?.map((item) => {
      listNft.push({
        connectionName: currentConnection?.connectionName,
        name: item?.nftInfo?.name,
        image: (item.nftInfo.metadata !== '' && item.nftInfo.metadata !== undefined)
          ? (JSON.parse(item?.nftInfo?.metadata)?.image ? JSON.parse(item?.nftInfo?.metadata)?.image : JSON.parse(item?.nftInfo?.metadata)?.image_url) : '',
        total: (parseFloat(item?.nftInfo?.price
          ? item?.nftInfo?.price : 0) * (item?.nftInfo?.amount
          ? item?.nftInfo?.amount : 0) * currentConnection?.data?.address?.price),
        data: item
      })
    })
    !_.isEmpty(setListNft(listNft))
  }, [currentConnection])

  useEffect(() => {
    const total = listNft?.reduce((total, curren) => {
      return total + parseFloat(curren?.total)
    }, 0)
    setTotalValue({
      totalValue: total,
      count: listNft?.length
    })
  }, [listNft])

  return (
    <>
      <div className='nft-title'>
        <div className='nft-title-back' onClick={handleBack}>
          <ArrowLeftOutlined/>
        </div>
      </div>
      <div className='nft-header'>
        <div className='nft-header-total'>
          <Text>Total Worth</Text>
          <Title level={2}>
            $ {totalValue?.totalValue}
          </Title>
        </div>
        <div className='nft-header-count'>
          <Text>NFT Count</Text>
          <Title level={2}>
            {totalValue?.count}
          </Title>
        </div>
      </div>
      <div style={{ padding: '50px 0' }}>
        <Row gutter={[16, 16]}>
          {listNft?.map((itemNft, index) => (
            <Col span={6} key={index}>
              <NFT
                total={itemNft?.total}
                connectionName={itemNft?.connectionName}
                name={itemNft?.name}
                type='item'
                urlImage={itemNft?.image}
                data={itemNft?.data}
                price={currentConnection?.data?.address?.price}
              />
            </Col>
          ))}
        </Row>
      </div>
    </>
  )
}

export default DetailConnectionNft
