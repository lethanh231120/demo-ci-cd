import React, { useState, useEffect, useContext } from 'react'
import PropertyOverview from '../../../components/property-overview/PropertyOverview'
import { Row, Col, Button } from 'antd'
import OnchainAssetTable from '../dashboard-tab/table/OnchainAssetTable'
import { useNavigate } from 'react-router-dom'
import { useQuery } from '@tanstack/react-query'
import {
  BITCOIN_CHAINID,
  ETHEREUM_CHAINID,
  BINANCE_SMART_CHAIN_CHAINID,
  SOLANA_CHAINID,
  RIPPLE_CHAINID
} from '../../../constants/ChainId'
import { FIVE_MINUTE, ONE_HOUR, ONE_DAY } from '../../../constants/params'
import { postBtc } from '../../../api/bitcoinService'
import { postXrp } from '../../../api/xrpService'
import { postEvm } from '../../../api/evmService'
import { postSolana } from '../../../api/solanaService'
import { totalPercent } from '../../../utils/parseFloat'
import { getConnection } from '../../../api/connectService'
import { PlatFormContext } from '../../../layouts'
import _ from 'lodash'
import { DEFAULT_PAGE, LIMIT } from '../../../constants/params'
import PieChartAsset from '../../../components/chart/PieChart'

const DetailOnchain = () => {
  const [loading, setLoading] = useState({
    chart: true,
    table: true
  })
  const [dataChangeByPeriod, setDataChangeByPeriod] = useState({ 'period': '1d' })
  const [listChartByAddress, setListChartByAddress] = useState([])
  const [chartAllAssetByPeriod, setChartAllAssetByPeriod] = useState([])
  const [dataChart, setDataChart] = useState()
  const [percentInfo, setPercentInfo] = useState()
  const [totalValue, setTotalValue] = useState()
  const [groupChainId, setGroupChainId] = useState()
  const [listHoldingDirecft, setListHoldingDirect] = useState([])
  const [listConnection, setListConnection] = useState()
  const [connections, setConnections] = useState()
  const [listChart24h, setListChart24h] = useState([])
  const [dataPieChart, setDataPieChart] = useState([])

  const [page, setPage] = useState({
    page: DEFAULT_PAGE,
    pageSize: LIMIT
  })

  const state = useContext(PlatFormContext)

  useQuery(
    ['listConnection'],
    async() => {
      const res = await getConnection('connect/current-connections')
      res && setConnections(res?.data)
    }
  )

  const navigate = useNavigate()

  // group connection theo chainId
  useEffect(() => {
    if (connections) {
      const nft = []
      const groupCoin = []
      // group address by chainId
      const groupBychainId = connections?.reduce((group, connection) => {
        const { chainId } = connection
        group[chainId] = group[chainId] ?? []
        group[chainId].push(connection.address)
        return group
      }, {})
      // get addresses by chainId eth or sol and push nft
      groupBychainId && Object.keys(groupBychainId).forEach(function(key) {
        if (key === ETHEREUM_CHAINID || key === SOLANA_CHAINID || key === BINANCE_SMART_CHAIN_CHAINID) {
          nft.push(...groupBychainId[key])
        }
        // custom lai du lieu
        groupCoin.push({
          'chainId': key,
          'address': groupBychainId[key]
        })
      })
      setGroupChainId(groupCoin)

      const getData = async() => {
        // get info chainId 1 or 56
        // merge all eth and bsc addresses into one array
        const listChain = groupCoin?.filter((item) => (item?.chainId === ETHEREUM_CHAINID || item?.chainId === BINANCE_SMART_CHAIN_CHAINID))
        const addressEVM = []
        listChain?.forEach((item) => {
          addressEVM.push(...item.address)
        })
        // call api các holding dùng promise.all để bắt buộc có dữ liệu mới tiếp tục thực hiện logic
        const [holdingsBtc, holdingsXrp, holdingsEvm] = await Promise.all([
          // get data holding btc chainId = 0
          postBtc('bitcoin/addresses-holdings',
            { 'addresses': groupCoin && groupCoin?.find((item) => (item.chainId === BITCOIN_CHAINID))?.address }
          ).then(res => res.data).catch(error => console.log(error)),
          postXrp('xrp/addresses-holdings',
            { 'addresses': groupCoin && groupCoin?.find((item) => (item.chainId === RIPPLE_CHAINID))?.address }
          ).then(res => res.data).catch(error => console.log(error)),
          postEvm('evm/addresses-holdings', { 'addresses': addressEVM })
            .then(res => res.data).catch(error => console.log(error))
        ])

        setListHoldingDirect([
          holdingsBtc && holdingsBtc,
          holdingsXrp && holdingsXrp,
          holdingsEvm && holdingsEvm
        ])
      }
      !_.isEmpty(groupCoin) && getData()
    }
  }, [connections])

  useEffect(() => {
    if (state?.propsImport?.addressDelete) {
      // neu co addressDelete dia chi connection muon xoa
      // kiểm tra xem có tồn tại listHoldingDirecft hay không
      if (listHoldingDirecft) {
        // Tìm index của item có địa chỉ muốn xóa trong danh sách listHoldingDirecft
        const index = listHoldingDirecft?.findIndex((item) => (item?.address?.address === state?.propsImport?.addressDelete))
        // xóa item có index vưa tìm được trong danh sách listHoldingDirecft
        listHoldingDirecft?.splice(index, 1)
      }
    }
  }, [state?.propsImport?.addressDelete])

  // Tính tổng tiền
  const totalMoney = (total, currenValue) => {
    const value = total + parseFloat(currenValue?.totalValue)
    return value
  }

  const caclTotal = (total, currenValue) => {
    const value = total + parseFloat(currenValue?.amount * currenValue?.price * (1 / Math.pow(10, currenValue?.decimals)))
    return value
  }

  // call chart cua tat ca dia chi
  useQuery(
    ['listChart', dataChangeByPeriod, groupChainId],
    async() => {
      if (groupChainId) {
        const data = await Promise.all(groupChainId?.map(async(item) => {
          if (item?.chainId === BITCOIN_CHAINID) {
            const data = await postBtc(`bitcoin/addresses-charts/period=${dataChangeByPeriod.period}`, { 'addresses': item?.address })
            return data?.data
          }
          if (item?.chainId === ETHEREUM_CHAINID) {
            const data = await postEvm(`evm/addresses-charts/period=${dataChangeByPeriod.period}`, { 'addresses': item?.address })
            return data?.data
          }
          if (item?.chainId === RIPPLE_CHAINID) {
            const data = await postXrp(`xrp/addresses-charts/period=${dataChangeByPeriod.period}`, { 'addresses': item?.address })
            return data?.data
          }
          if (item?.chainId === BINANCE_SMART_CHAIN_CHAINID) {
            const data = await postEvm(`evm/addresses-charts/period=${dataChangeByPeriod.period}`, { 'addresses': item?.address })
            return data?.data
          }
          if (item?.chainId === SOLANA_CHAINID) {
            const data = await postSolana(`solana/addresses-charts/period=${dataChangeByPeriod.period}`, { 'addresses': item?.address })
            return data?.data
          }
        }))
        setListChartByAddress(data?.flat(1))
        if (dataChangeByPeriod?.period === '1d') {
          setListChart24h(data?.flat(1))
        }
      }
    },
    {
      refetchInterval: dataChangeByPeriod?.period === '1h' ? FIVE_MINUTE : ((dataChangeByPeriod?.period === '1d' || dataChangeByPeriod?.period === '1w') ? ONE_HOUR : ONE_DAY)
    }
  )

  // xu ly cho holding
  useEffect(() => {
    if (connections) {
      const connection = []

      if (!_.isEmpty(listHoldingDirecft?.flat(1))) {
        // tinh totalvalue cua list coin direct
        const totalValueDirect = listHoldingDirecft?.flat(1)?.reduce((total, curren) => {
          const totalDirect = total + parseFloat(curren?.address ? curren?.address?.totalValue : 0)
          return totalDirect
        }, 0)
        const indexPieChart = dataPieChart.findIndex((item) => (item?.name === 'Direct'))
        if (indexPieChart !== -1) {
          const newPieChart = [...dataPieChart]
          newPieChart[indexPieChart] = {
            'name': 'Direct',
            'totalValue': totalValueDirect
          }
          setDataPieChart(newPieChart)
        }
        if (indexPieChart === -1) {
          setDataPieChart([
            ...dataPieChart,
            {
              'name': 'Direct',
              'totalValue': totalValueDirect
            }
          ])
        }

        // gộp thoong tin cua connection voi thong tin cua listholdingDirecft
        const newListHoldingDirecft = [...listHoldingDirecft.flat(1)]
        connections?.forEach((itemConnect, index) => {
          if (!_.isEmpty(newListHoldingDirecft)) {
            const itemHolding = newListHoldingDirecft?.find((item) => {
              if (item?.address) {
                return (itemConnect?.address === item?.address?.address)
              }
            })
            if (itemHolding) {
              const totalValue = itemHolding?.holdings?.reduce(caclTotal, 0)
              const newConnect = {
                index: index,
                ...itemHolding.address,
                connectionName: itemConnect?.connectionName,
                ownPersentage: itemConnect?.ownPersentage,
                id: itemConnect?.id,
                holdings: itemHolding?.holdings,
                totalValue: totalValue * (itemConnect?.ownPersentage / 100)
              }
              connection.push(newConnect)
            }
          }
        })
      }

      // cap nhat thong tin chart cho connection va thong tin chi tiet cac dong coin cho holdings
      connection?.forEach((item, index) => {
        const chart24h = listChart24h?.find((item24h) => (item24h?.address?.address === item?.address))
        const itemChart = listChartByAddress?.find((itemChart) => (itemChart?.address?.address === item?.address))
        if (itemChart?.addressChart && chart24h?.addressChart) {
          const newChart = [...itemChart.addressChart]
          // cap nhat chart theo ownPercentage
          itemChart?.addressChart?.map((itemChart, index) => {
            newChart[index] = {
              ...itemChart,
              totalValue: parseFloat(itemChart?.totalValue) * (item?.ownPersentage / 100)
            }
          })
          connection[index] = {
            ...item,
            index: index,
            dataChart: newChart,
            chart24h: chart24h.addressChart
          }
        }
        if (itemChart?.addressChart === null || chart24h?.addressChart === null) {
          connection[index] = {
            ...item
          }
        }
      })
      // Tính tổng totalValue
      if (!_.isEmpty(connection)) {
        const totalValue = connection?.reduce(totalMoney, 0)
        setTotalValue(totalValue)
        setListConnection(connection)
        setLoading({
          ...loading,
          table: false
        })
      }
    }
  }, [listHoldingDirecft, listChartByAddress, connections, listChart24h])

  // tinh chart
  useEffect(() => {
    if (listConnection) {
      const chartAllAsset = []
      let maxLength = 0
      let itemMaxLength
      listConnection?.map((item) => {
        if (item?.dataChart?.length > maxLength) {
          maxLength = item?.dataChart?.length
          itemMaxLength = item?.dataChart
        }
      })
      for (let i = 0; i < maxLength; ++i) {
        let total = 0
        let timestamp
        // tong tat ca cac ban ghi co cung index trong danh sach dataChart cua cac dia chi
        // vi tai cac vi tri co index cung nhau o cac mang, thi timestamp deu giong nhau
        listConnection && listConnection?.forEach((chartItem) => {
          if (chartItem?.dataChart) {
            total += parseFloat(((chartItem?.dataChart !== null) && (chartItem?.dataChart[i] !== undefined))
              ? chartItem?.dataChart[i]?.totalValue : 0)
            // check neu ton tai timestamp thi lay timestamp
            // neu khong ton tai thi lay timestamp cua ban ghi thu i trong itemMaxLength
            timestamp = ((chartItem?.dataChart !== null) && (chartItem?.dataChart[i] !== undefined)) ? chartItem?.dataChart[i]?.timestamp : itemMaxLength[i]?.timestamp
          }
        })
        chartAllAsset.push({ 'totalValue': total, timestamp })
      }
      const newChartAll = [...chartAllAssetByPeriod]
      const indexChartByPeriod = chartAllAssetByPeriod?.findIndex((item) => (item?.period === dataChangeByPeriod?.period))
      if (indexChartByPeriod !== -1) {
        newChartAll[indexChartByPeriod] = {
          'period': dataChangeByPeriod?.period,
          'dataChart': chartAllAsset
        }
        setChartAllAssetByPeriod(newChartAll)
        setDataChart(newChartAll[indexChartByPeriod]?.dataChart)
        setLoading({
          ...loading,
          chart: false
        })
      }

      if (indexChartByPeriod === -1) {
        setChartAllAssetByPeriod([
          ...chartAllAssetByPeriod,
          {
            'period': dataChangeByPeriod?.period,
            'dataChart': chartAllAsset
          }
        ])
        setDataChart(chartAllAsset)
        setLoading({
          ...loading,
          chart: false
        })
      }
    }
  }, [dataChangeByPeriod, listConnection])

  // tinh percent info
  useEffect(() => {
    const balanceFluctuations = dataChart && (dataChart[0]?.totalValue - dataChart[dataChart.length - 1]?.totalValue)
    const percent = balanceFluctuations ? totalPercent(balanceFluctuations, (dataChart && dataChart[0]?.totalValue)).toFixed(4).replace(/\d(?=(\d{3})+\.)/g, '$&,') : 0
    setPercentInfo({
      balanceFluctuations: balanceFluctuations,
      percent: percent
    })
  }, [dataChart])

  const handleBack = () => {
    navigate(-1)
  }

  return (
    <>
      <div className='detail-onchain'>
        <Button onClick={handleBack}>Back</Button>
      </div>
      <Row gutter={60} style={{ marginTop: '50px' }}>
        <Col span={16}>
          <PropertyOverview
            title={'Onchain Wallet'}
            dataChart={dataChart && [...dataChart]?.reverse()}
            width={500}
            height={100}
            total={totalValue}
            detail={{ 'isDetail': false }}
            isButton={false}
            titleButton=''
            listPeriod={[
              { 'title': '1H', 'period': '1h' },
              { 'title': '1D', 'period': '1d' },
              { 'title': '7D', 'period': '7d' },
              { 'title': '1M', 'period': '1m' },
              { 'title': '3M', 'period': '3m' },
              { 'title': '6M', 'period': '6m' },
              { 'title': 'ALL', 'period': 'all' }
            ]}
            type='all'
            percentInfo={percentInfo}
            setDataChangeByPeriod={setDataChangeByPeriod}
            loading={loading?.chart}
          />
        </Col>
        <Col span={8}>
          <PieChartAsset data={dataPieChart && dataPieChart} sizeForPieChart={150} />
        </Col>
      </Row>
      <OnchainAssetTable
        isButtonDetail={false}
        data={listConnection && listConnection}
        loading={loading?.table}
        isListAction={true}
        type='onchain'
        paginate={{
          defaultCurrent: page.page,
          pageSize: page.pageSize,
          showSizeChanger: false,
          onChange(current) {
            setPage({ ...page, page: current })
          }
        }}
        detailCoin={false}
      />
    </>
  )
}

export default DetailOnchain
