import React, { useState, useEffect } from 'react'
import { Row, Col, Button, Popover, Table, Popconfirm } from 'antd'
import PropertyOverview from '../../../../components/property-overview/PropertyOverview'
import { useNavigate, useLocation } from 'react-router-dom'
import ListCoinTable from '../../dashboard-tab/table/ListCoinTable'
import { LIMIT, DEFAULT_PAGE } from '../../../../constants/params'
import { LazyLoadImage } from 'react-lazy-load-image-component'
import { DeleteOutlined, HeartOutlined, CaretUpOutlined, CaretDownOutlined } from '@ant-design/icons'
import { BILLION } from '../../../../constants/TypeConstants'
import Chart from '../../../../components/chart/LineChart'
import {
  BITCOIN_CHAINID,
  ETHEREUM_CHAINID,
  BINANCE_SMART_CHAIN_CHAINID,
  SOLANA_CHAINID,
  RIPPLE_CHAINID
} from '../../../../constants/ChainId'
import { postBtc } from '../../../../api/bitcoinService'
import { postXrp } from '../../../../api/xrpService'
import { postEvm } from '../../../../api/evmService'
import { postSolana } from '../../../../api/solanaService'
import { totalPercent } from '../../../../utils/parseFloat'
import PieChartAsset from '../../../../components/chart/PieChart'

const ListCoinDirectConnection = () => {
  const { state } = useLocation()
  const { data, holdings } = state
  const [dataChangeByPeriod, setDataChangeByPeriod] = useState({ 'period': '1d' })
  const [selectedRowKeys, setSelectedRowKeys] = useState([
    'rank',
    'name',
    'price',
    'amount',
    'total',
    'marketCap',
    // 'volumn',
    'priceChange1w',
    // 'totalChange24h',
    'percentChange24',
    'priceGraph'
    // 'priceChange24h',
    // 'low24h',
    // 'high24h'
  ])

  const [page, setPage] = useState({
    page: DEFAULT_PAGE,
    pageSize: LIMIT
  })
  const [dataChart, setDataChart] = useState()
  const [percentInfo, setPercentInfo] = useState()
  const [dataChartByPeriod, setDataChartByPeriod] = useState([])
  const [totalValue, setTotalValue] = useState()
  const [dataPieChart, setDataPieChart] = useState()
  const [isTotalGreaterThan10, setIsTotalGreaterThan10] = useState(false)
  const [listCoin, setListCoin] = useState(holdings)

  const navigate = useNavigate()

  const columnsPopover = [
    {
      title: 'Title',
      dataIndex: 'title',
      width: '85%',
      render: (_, record) => (<span style={{ color: '#A8ADB3' }}>{record.title}</span>)
    }
  ]

  const onSelectChange = (newselectedRowKeys) => {
    setSelectedRowKeys(newselectedRowKeys)
  }

  const rowSelection = {
    selectedRowKeys,
    onChange: onSelectChange
  }

  const items = [
    {
      key: 'rank',
      title: '#'
    },
    {
      key: 'name',
      title: 'Name'
    },
    {
      key: 'amount',
      title: 'Amount'
    },
    {
      key: 'high24h',
      title: 'High 24h'
    },
    {
      key: 'low24h',
      title: 'Low 24h'
    },
    {
      key: 'percentChange24',
      title: 'Percent Change 24H'
    },
    {
      key: 'priceChange24h',
      title: 'Price Change 24H'
    },
    {
      key: 'total',
      title: 'Total'
    },
    {
      key: 'totalChange24h',
      title: 'Total Change 24H'
    },
    {
      key: 'price',
      title: 'Price'
    },
    {
      key: 'marketCap',
      title: 'Market Cap'
    },
    // {
    //   key: 'volumn',
    //   title: 'Volumn 24h'
    // },
    {
      key: 'priceGraph',
      title: 'Price Graph (7d)'
    }
  ]

  const renderPrice = (record) => {
    let price
    if ((record?.price >= 0) && (record?.price < 1)) {
      price = record?.price.toFixed(5).replace(/\d(?=(\d{3})+\.)/g, '$&,')
    }
    if (record?.price >= 1) {
      price = record?.price.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')
    }
    return price
  }

  useEffect(() => {
    const getData = async() => {
      let dataChart
      switch (data?.chainId) {
        case BITCOIN_CHAINID:
          dataChart = await postBtc(`bitcoin/addresses-charts/period=${dataChangeByPeriod.period}`,
            { 'addresses': [data.address] }
          )
          break
        case ETHEREUM_CHAINID:
          dataChart = await postEvm(`evm/addresses-charts/period=${dataChangeByPeriod.period}`,
            { 'addresses': [data.address] }
          )
          break
        case RIPPLE_CHAINID:
          dataChart = await postXrp(`xrp/addresses-charts/period=${dataChangeByPeriod.period}`,
            { 'addresses': [data.address] }
          )
          break
        case BINANCE_SMART_CHAIN_CHAINID:
          dataChart = await postEvm(`evm/addresses-charts/period=${dataChangeByPeriod.period}`,
            { 'addresses': [data.address] }
          )
          break
        case SOLANA_CHAINID:
          dataChart = await postSolana(`solana/addresses-charts/period=${dataChangeByPeriod.period}`,
            { 'addresses': [data.address] }
          )
          break
        default:
          break
      }
      const itemPeriod = dataChartByPeriod && dataChartByPeriod?.findIndex((itemPeriod) => (itemPeriod?.period === dataChangeByPeriod?.period))
      if (itemPeriod !== -1) {
        const newDataChart = [...dataChartByPeriod]
        newDataChart[itemPeriod] = {
          'period': dataChangeByPeriod?.period,
          'dataChart': dataChart?.data[0]?.addressChart
        }
        setDataChartByPeriod(newDataChart)
      }
      if (itemPeriod === -1) {
        setDataChartByPeriod([
          ...dataChartByPeriod,
          {
            'period': dataChangeByPeriod?.period,
            'dataChart': dataChart?.data[0]?.addressChart
          }
        ])
      }
    }
    getData()
  }, [dataChangeByPeriod])

  useEffect(() => {
    const chart = dataChartByPeriod?.find((item) => (item?.period === dataChangeByPeriod?.period))
    if (chart?.dataChart) {
      if (chart?.dataChart === null) {
        setDataChart([])
      } else {
        const newChat = [...chart.dataChart]
        chart?.dataChart?.forEach((item, index) => {
          newChat[index] = {
            ...item,
            totalValue: parseFloat(item?.totalValue) * (data?.ownPersentage / 100)
          }
        })
        setDataChart(newChat)
      }
    }
  }, [dataChartByPeriod])

  // Tính tổng tiền
  const totalMoney = (total, currenValue) => {
    const value = total + parseFloat(currenValue?.total)
    return value
  }

  useEffect(() => {
    const total = listCoin?.reduce(totalMoney, 0)
    setTotalValue(total)
    const dataPie = []
    const newListHolding = listCoin?.sort((a, b) => (a.total * (1 / Math.pow(10, a.decimals))) - (b.total * (1 / Math.pow(10, a.decimals))))
    const listReverse = newListHolding?.reverse()
    const listMore = listReverse?.slice(5, listReverse?.length)
    const totalListMore = listMore?.reduce((total, curren) => {
      return total + (curren?.total * (1 / Math.pow(10, curren?.decimals)))
    }, 0)
    dataPie.push({
      'name': 'More Coin',
      'totalValue': totalListMore
    })
    const top10 = listReverse?.slice(0, 5)
    top10?.forEach((item) => {
      dataPie.push({
        'name': item?.coinName?.charAt(0).toUpperCase() + item?.coinName?.slice(1),
        'totalValue': item?.total * (1 / Math.pow(10, item?.decimals))
      })
    })
    setDataPieChart(dataPie)
  }, [listCoin])

  const columns = [
    {
      key: '#',
      title: '#',
      width: '20px',
      hidden: !selectedRowKeys.includes('rank'),
      render: (_, record, index)=>(<span style={{ color: '#A8ADB3' }}>{(page.page - 1) * page.pageSize + index + 1}</span>)
    },
    {
      key: 'name',
      title: <span style={{ textAlign: 'left !important' }}>Name</span>,
      className: 'table-name',
      width: '250px',
      hidden: !selectedRowKeys.includes('name'),
      sorter: (a, b) => a.coinName.toLowerCase().localeCompare(b.coinName.toLowerCase()),
      render: (_, record) => (<div>
        <div className='table-icon-coin'>
          {record.coinImage
            ? (
              <LazyLoadImage src={record.coinImage}/>
            ) : (
              <span className='table-icon-coin-logo'>{record.symbol.slice(0, 3)}</span>
            )}
        </div>
        <div className='table-name-content'>
          <div className='table-name-text'>
            <span>
              {record?.coinName ? record?.coinName : ''}
            </span>
          </div>
          <div className='table-name-symbol'>
            <span>
              {record?.symbol ? record?.symbol.toUpperCase() : ''}
            </span>
          </div>
        </div>
      </div>)
    },
    {
      key: 'price',
      title: 'Price',
      dataIndex: 'price',
      width: '150px',
      sorter: (a, b) => a.price - b.price,
      hidden: !selectedRowKeys.includes('price'),
      render: (_, record) => (<span className='amount'>
        ${renderPrice(record)}
      </span>)
    },
    {
      key: 'amount',
      title: <span style={{ textAlign: 'left !important' }}>Amount</span>,
      width: '120px',
      hidden: !selectedRowKeys.includes('amount'),
      sorter: {
        compare: (a, b) => a.amount - b.amount,
        multiple: 1
      },
      render: (_, record) => (<span className='amount'>
        {(record?.amount * (1 / Math.pow(10, record?.decimals)))?.toFixed(5).replace(/\d(?=(\d{3})+\.)/g, '$&,')}
        {/* {record?.amount} */}
      </span>)
    },
    {
      key: 'total',
      title: 'Total',
      dataIndex: 'total',
      width: '150px',
      sorter: (a, b) => a.total - b.total,
      hidden: !selectedRowKeys.includes('total'),
      render: (_, record) => (<span className='amount'>
        {/* ${record?.total} */}
        ${(record?.total * (1 / Math.pow(10, record?.decimals)))?.toFixed(5).replace(/\d(?=(\d{3})+\.)/g, '$&,')}
      </span>)
    },
    {
      key: 'high24h',
      title: 'High 24h',
      width: '250px',
      hidden: !selectedRowKeys.includes('high24h'),
      render: (_, record) => (
        <div className='price-change'>
          $ {(record?.high24h * (1 / Math.pow(10, record?.decimals)))?.toFixed(5).replace(/\d(?=(\d{3})+\.)/g, '$&,')}
        </div>
      ),
      sorter: (a, b) => a.high24h - b.high24h
    },
    {
      key: 'low24h',
      title: 'Low 24H',
      width: '250px',
      hidden: !selectedRowKeys.includes('low24h'),
      render: (_, record = '12') => (
        <div className='price-change'>
          $ {(record?.low24h * (1 / Math.pow(10, record?.decimals)))?.toFixed(5).replace(/\d(?=(\d{3})+\.)/g, '$&,')}
        </div>
      ),
      sorter: (a, b) => a.low24h - b.low24h
    },
    {
      key: 'priceChange24h',
      title: 'Price Change (24h)',
      sorter: (a, b) => a.priceChange24h - b.priceChange24h,
      width: '200px',
      hidden: !selectedRowKeys.includes('priceChange24h'),
      render: (_, record) => (
        <div className='price-change'
          style={{
            color: record.priceChange24h ? (record.priceChange24h >= 0 ? '#34b349' : '#ff4d4d') : '#34b349'
          }}
        >
          $ {record.priceChange24h ? parseFloat(record.priceChange24h >= 0 ? record.priceChange24h : record.priceChange24h.toString().slice(1)).toFixed(4).replace(/\d(?=(\d{3})+\.)/g, '$&,') : '0.00'}
        </div>
      )
    },
    {
      key: 'percentChange24',
      title: 'Percent 24H',
      sorter: (a, b) => a.priceChangePercentage24h - b.priceChangePercentage24h,
      width: '150px',
      hidden: !selectedRowKeys.includes('percentChange24'),
      render: (_, record) => (
        <div className='price-change'
          style={{
            backgroundColor: record.priceChangePercentage24h ? (record.priceChangePercentage24h >= 0 ? 'rgba(52, 199, 89, 0.1)' : 'rgba(255,53,53,0.1)') : 'rgba(52, 199, 89, 0.1)',
            color: record.priceChangePercentage24h ? (record.priceChangePercentage24h >= 0 ? '#34b349' : '#ff4d4d') : '#34b349'
          }}
        >
          {record.priceChangePercentage24h ? (record.priceChangePercentage24h >= 0 ? <CaretUpOutlined/> : <CaretDownOutlined/>) : <CaretUpOutlined/>}
          {record.priceChangePercentage24h ? parseFloat(record.priceChangePercentage24h >= 0 ? record.priceChangePercentage24h : record.priceChangePercentage24h.toString().slice(1)).toFixed(2) : '0.00'} %
        </div>
      )
    },
    {
      key: 'totalChange24h',
      title: 'Total Change 24h',
      dataIndex: 'totalChange24h',
      width: '250px',
      sorter: (a, b) => a.totalChange24h - b.totalChange24h,
      hidden: !selectedRowKeys.includes('totalChange24h'),
      render: (_, record) => (
        <div className='price-change'
          style={{
            color: record.totalChange24h ? (record.totalChange24h >= 0 ? '#34b349' : '#ff4d4d') : '#34b349'
          }}
        >
          $ {record.totalChange24h ? parseFloat(record.totalChange24h >= 0 ? (record.totalChange24h * (1 / Math.pow(10, record?.decimals))) : (record.totalChange24h * (1 / Math.pow(10, record?.decimals))).toString().slice(1)).toFixed(5).replace(/\d(?=(\d{3})+\.)/g, '$&,') : '0.00'}
        </div>
      )
    },
    {
      key: 'marketCap',
      title: 'Market Cap',
      sorter: (a, b) => a.marketCap - b.marketCap,
      width: '150px',
      defaultSortOrder: 'descend',
      hidden: !selectedRowKeys.includes('marketCap'),
      render: (_, record) => (<span style={{ color: '#A8ADB3' }}>
        $ {(record.marketCap * BILLION).toFixed(5).replace(/\d(?=(\d{3})+\.)/g, '$&,')} B
      </span>)
    },
    {
      key: 'volumn',
      title: 'Volumn 24h',
      sorter: (a, b) => a.volumn - b.volumn,
      width: '150px',
      hidden: !selectedRowKeys.includes('volumn'),
      render: (_, record) => (<span style={{ color: '#A8ADB3' }}>
        $ {record?.volumn ? (record?.volumn * BILLION).toFixed(5).replace(/\d(?=(\d{3})+\.)/g, '$&,') : 0} B
      </span>)
    },
    {
      title: 'Price Graph (7d)',
      dataIndex: 'priceGraph',
      width: '150px',
      className: 'table-graph',
      hidden: !selectedRowKeys.includes('priceGraph'),
      render: (_, record) => (<Chart record={record?.priceChart7d} change7d={record.percentChange7d}/>)
    },
    {
      key: 'more',
      title: <Popover
        placement='bottomRight'
        content={(<Table
          showHeader={false}
          scroll={{
            y: 260
          }}
          style={{ maxWidth: '290px' }}
          className='tableabc'
          rowSelection={rowSelection}
          pagination={false}
          columns={columnsPopover}
          dataSource={items}
        >
        </Table>)}
        trigger='click'
      >
        +
      </Popover>,
      className: 'table-plus',
      width: '20px',
      dataIndex: 'key',
      render: (_, record) => (
        <div className='onchain-table-action'>
          <div className='onchain-table-action-icon'>
            <HeartOutlined />
          </div>
          <Popconfirm
            placement='top'
            className='onchain-table-action-icon'
            title='Are you sure to delete this connection?'
            okText='Yes'
            cancelText='No'
          >
            <DeleteOutlined className='tab-list-icon-item' />
          </Popconfirm>
        </div>
      )
    }
  ].filter(item => !item.hidden)

  const handleBack = () => {
    navigate(-1)
  }

  useEffect(() => {
    if (isTotalGreaterThan10) {
      const listCoinGreaterThan10 = listCoin?.filter((item) => (item?.total * (1 / Math.pow(10, item?.decimals)) > 1))
      setListCoin(listCoinGreaterThan10)
    }
  }, [isTotalGreaterThan10])

  // tinh percent info
  useEffect(() => {
    const balanceFluctuations = dataChart && (dataChart[0]?.totalValue - dataChart[dataChart.length - 1]?.totalValue)
    const percent = balanceFluctuations ? totalPercent(balanceFluctuations, (dataChart && dataChart[0]?.totalValue)).toFixed(4).replace(/\d(?=(\d{3})+\.)/g, '$&,') : 0
    setPercentInfo({
      balanceFluctuations: balanceFluctuations,
      percent: percent
    })
  }, [dataChart])

  return (
    <>
      <div className='detail-onchain'>
        <Button onClick={handleBack}>Back</Button>
        <Button onClick={() => setIsTotalGreaterThan10(!isTotalGreaterThan10)}>List coin has total greater than 1$ </Button>
      </div>
      <Row gutter={60} style={{ marginTop: '50px' }}>
        <Col span={16}>
          <PropertyOverview
            title='List Coin Direct'
            dataChart={dataChart && [...dataChart]?.reverse()}
            width={500}
            height={100}
            total={totalValue}
            detail={{ 'isDetail': false }}
            isButton={false}
            titleButton=''
            listPeriod={[
              { 'title': '1H', 'period': '1h' },
              { 'title': '1D', 'period': '1d' },
              { 'title': '7D', 'period': '7d' },
              { 'title': '1M', 'period': '1m' },
              { 'title': '3M', 'period': '3m' },
              { 'title': '6M', 'period': '6m' },
              { 'title': 'ALL', 'period': 'all' }
            ]}
            type=''
            percentInfo={percentInfo}
            setDataChangeByPeriod={setDataChangeByPeriod}
          />
        </Col>
        <Col span={8}>
          <PieChartAsset data={dataPieChart && dataPieChart} sizeForPieChart={150} />
        </Col>
      </Row>
      <div style={{ marginTop: '50px' }}>
        <ListCoinTable
          columns={columns}
          data={listCoin}
          pagination={{
            defaultCurrent: page.page,
            pageSize: page.pageSize,
            showSizeChanger: false,
            onChange(current) {
              setPage({ ...page, page: current })
            }
          }}
          detailCoin={true}
          scroll={{ x: 'max-content' }}
        />
      </div>
    </>
  )
}

export default ListCoinDirectConnection
