import React, { useEffect, useState } from 'react'
import { Row, Col, Button, Popconfirm } from 'antd'
import PropertyOverview from '../../../components/property-overview/PropertyOverview'
import Chart from '../../../components/chart/LineChart'
import { BILLION } from '../../../constants/TypeConstants'
import { ArrowDownOutlined, ArrowUpOutlined } from '@ant-design/icons'
import { useNavigate } from 'react-router-dom'
import './style.scss'
import { useLocation } from 'react-router-dom'
import {
  BITCOIN_CHAINID,
  ETHEREUM_CHAINID,
  BINANCE_SMART_CHAIN_CHAINID,
  SOLANA_CHAINID,
  RIPPLE_CHAINID
} from '../../../constants/ChainId'
import { postBtc } from '../../../api/bitcoinService'
import { postXrp } from '../../../api/xrpService'
import { postEvm } from '../../../api/evmService'
import { postSolana } from '../../../api/solanaService'
import { totalPercent } from '../../../utils/parseFloat'
import ListCoinTable from '../dashboard-tab/table/ListCoinTable'
import { LazyLoadImage } from 'react-lazy-load-image-component'
import { LIMIT, DEFAULT_PAGE } from '../../../constants/params'
// import { EllipsisOutlined, CaretUpOutlined, CaretDownOutlined } from '@ant-design/icons'
import { DeleteOutlined, HeartOutlined, CaretUpOutlined, CaretDownOutlined } from '@ant-design/icons'

import { Table, Popover } from 'antd'
import { bigNumber, smallNumber } from '../../../utils/number/formatNum'
import { useQuery } from '@tanstack/react-query'
import _ from 'lodash'
import { post } from '../../../api/coinPriceService'
import PieChartAsset from '../../../components/chart/PieChart'

const DetailConnection = () => {
  const [dataChangeByPeriod, setDataChangeByPeriod] = useState({ 'period': '1d' })
  const [selectedRowKeys, setSelectedRowKeys] = useState([
    'rank',
    'name',
    'price',
    'amount',
    'marketCap',
    // 'volumn',
    // 'priceChange1w',
    // 'priceChange7d',
    'total',
    // 'totalChange24h',
    'priceGraph'
    // 'percentChange24',
    // 'priceChange24h'
    // 'low24h',
    // 'high24h'
  ])
  const [page, setPage] = useState({
    page: DEFAULT_PAGE,
    pageSize: LIMIT
  })
  const [dataChart, setDataChart] = useState()
  const [percentInfo, setPercentInfo] = useState()
  const [totalValue, setTotalValue] = useState()
  const [dataChartByPeriod, setDataChartByPeriod] = useState([])
  const [isTotalGreaterThan10, setIsTotalGreaterThan10] = useState(false)

  const [allHolding, setAllHolding] = useState()
  const [listCoinDirect, setListCoinDirect] = useState()
  // const [listCoinInDirect, setListCoinInDirect] = useState()

  const [listCoinInfo, setListCoinInfo] = useState()
  const [allCoinAffterGroup, setAllCoinAffterGroup] = useState()
  const [holdings, setHoldings] = useState()

  const navigate = useNavigate()

  const { state } = useLocation()
  const { data } = state

  // call holding
  useQuery(
    ['connections'],
    async() => {
      let holdings
      switch (data?.chainId) {
        case BITCOIN_CHAINID:
          holdings = await postBtc('bitcoin/addresses-holdings', { 'addresses': [data?.address] })
          break
        case ETHEREUM_CHAINID:
          holdings = await postEvm('evm/addresses-holdings', { 'addresses': [data?.address] })
          break
        case RIPPLE_CHAINID:
          holdings = await postXrp('xrp/addresses-holdings', { 'addresses': [data?.address] })
          break
        case BINANCE_SMART_CHAIN_CHAINID:
          holdings = await postEvm('evm/addresses-holdings', { 'addresses': [data?.address] })
          break
        case SOLANA_CHAINID:
          holdings = await postSolana(`solana/addresses-holdings`, { 'addresses': [data?.address] })
          break
        default:
          break
      }
      if (holdings) {
        setListCoinDirect(holdings?.data[0]?.holdings)
        setAllHolding([
          // list coin direct
          ...holdings.data[0].holdings
          // list coin indirect
        ])
      }
    }
  )

  useEffect(() => {
    const allCoinAffterGroup = []
    // group tất cả các holding theo coinId
    const groupByCoinId = _.groupBy(allHolding, (item) => {
      if (item?.coinId) {
        return item?.coinId
      }
    })

    // foreach object groupByCoinId
    groupByCoinId && Object.keys(groupByCoinId).forEach(function(key) {
      // check nếu key khác undefined tức là tồn tại coinId
      if (key !== 'undefined') {
        // tinh tổng amount của đồng coin đó
        const amount = groupByCoinId[key].reduce((total, item) => {
          return total + parseFloat(item.amount)
        }, 0)
        // cập nhật lại thông tin đồng coin với amount lấy tu danh sach dong coin co coinId giong nhau
        // lấy thông tin 1 đồng coin bất ky trong list co coinId giong nhau va cap nhật amount cho dong coin
        const itemInfo = {
          ...groupByCoinId[key][0],
          amount: bigNumber(amount)
        }
        // thêm đồng coin vào danh sách allCoinAffterGroup
        allCoinAffterGroup.push(itemInfo)
      } else {
        // check nếu key la undefined tức là khong có coinid
        // group danh sách coin theo coinName
        const groupByCoinName = _.groupBy(groupByCoinId[key], (item) => {
          if (item?.coinName) {
            return item?.coinName
          }
        })
        // forEach danh sách vua được group theo coinName
        groupByCoinName && Object.keys(groupByCoinName).forEach(function(key) {
          // check neu tồn tại coinName
          if (key !== 'undefined') {
            // tinh tổng amount của tất cả các dồng coin co cùng coinName
            const amount = groupByCoinName[key].reduce((total, item) => {
              return total + parseInt(item.amount)
            }, 0)
            // Tạo thông tin của đồng coin
            // Lấy tất cả thông tin 1 đồng coin bất ky trong 1 list coin co cung coinName
            // gan lai amout la tổng số lương của list coin
            const itemInfo = {
              ...groupByCoinName[key][0],
              amount: bigNumber(amount)
            }
            // Thêm coin vao danh sách allCoinAffterGroup
            allCoinAffterGroup.push(itemInfo)
          }
        })
      }
    })

    if (!_.isEmpty(allCoinAffterGroup)) {
      const getCoinInfo = async() => {
        const listCoinId = []
        // lấy tất cả holdings
        allCoinAffterGroup?.forEach((item) => {
          if (item?.coinId) {
            listCoinId.push(item?.coinId)
          }
        })
        setAllCoinAffterGroup(allCoinAffterGroup)
        // check neu list coinId co du lieu thi moi call
        if (!_.isEmpty(listCoinId)) {
          // call api list coin info theo danh sach coinid
          const listCoin = await post('price/info/list', { 'listCoinId': listCoinId })
          setListCoinInfo(listCoin?.data?.coins)
        }
      }
      getCoinInfo()
    }
  }, [allHolding])

  useEffect(() => {
    if (listCoinInfo) {
      const newAll = [...allCoinAffterGroup]
      const newDirect = [...listCoinDirect]
      allCoinAffterGroup?.forEach((itemHolding, index) => {
        const itemAll = listCoinInfo?.find((item) => (item?.coinId === itemHolding?.coinId))
        const amount = bigNumber(parseFloat(itemHolding?.amount)) * (data?.ownPersentage / 100)
        const price = smallNumber(itemHolding?.price)
        if (itemAll) {
          const total = itemAll?.price * amount
          const totalChange24h = (itemAll?.priceChange24h) * amount
          const high24h = itemAll?.high24h * amount
          const low24h = itemAll?.low24h * amount
          newAll[index] = {
            ...itemAll,
            ...itemHolding,
            'amount': amount,
            'total': total,
            'price': Number(price),
            'totalChange24h': totalChange24h,
            'high24h': high24h,
            'low24h': low24h
          }
        }
        if (itemAll === undefined) {
          const total = itemHolding?.price ? itemHolding?.price * amount : 0
          const newCoin = {
            ...itemHolding,
            total: total,
            amount: amount,
            price: Number(price) ? Number(price) : 0,
            totalChange24h: 0,
            ath: 0,
            athChangePercentage: 0,
            atl: 0,
            atlChangePercentage: 0,
            circulatingSupply: 0,
            coinImage: '',
            fullyDilutedValuation: 0,
            high24h: 0,
            low24h: 0,
            marketCap: 0,
            marketCapChange24h: 0,
            marketCapChangePercentage24h: 0,
            maxSupply: 0,
            priceChange24h: 0,
            priceChangePercentage24h: 0,
            totalSupply: 0,
            totalVolume: 0,
            priceChart7d: []
          }
          newAll[index] = newCoin
        }
      })
      listCoinDirect?.forEach((itemHolding, index) => {
        const itemDirect = listCoinInfo?.find((item) => (item?.coinId === itemHolding?.coinId))
        const amount = bigNumber(parseFloat(itemHolding?.amount)) * (data?.ownPersentage / 100)
        const price = smallNumber(itemHolding?.price)
        if (itemDirect) {
          const total = itemDirect?.price * amount
          const totalChange24h = (itemDirect?.priceChange24h) * amount
          const high24h = itemDirect?.high24h * amount
          const low24h = itemDirect?.low24h * amount
          newDirect[index] = {
            ...itemDirect,
            ...itemHolding,
            'amount': amount,
            'total': total,
            'price': Number(price),
            'totalChange24h': totalChange24h,
            'high24h': high24h,
            'low24h': low24h
          }
        }
        if (itemDirect === undefined) {
          const total = itemHolding?.price ? itemHolding?.price * amount : 0
          const newCoin = {
            ...itemHolding,
            total: total,
            amount: amount,
            price: Number(price) ? Number(price) : 0,
            totalChange24h: 0,
            ath: 0,
            athChangePercentage: 0,
            atl: 0,
            atlChangePercentage: 0,
            circulatingSupply: 0,
            coinImage: '',
            fullyDilutedValuation: 0,
            high24h: 0,
            low24h: 0,
            marketCap: 0,
            marketCapChange24h: 0,
            marketCapChangePercentage24h: 0,
            maxSupply: 0,
            priceChange24h: 0,
            priceChangePercentage24h: 0,
            totalSupply: 0,
            totalVolume: 0,
            priceChart7d: []
          }
          newDirect[index] = newCoin
        }
      })
      // setListCoinDirect(newDirect)
      // setAllHolding(newAll)
      setHoldings({
        all: newAll,
        direct: newDirect
      })
    }
  }, [listCoinInfo, allCoinAffterGroup, listCoinDirect])

  useEffect(() => {
    const listCoinGreaterThan10 = holdings?.all?.filter((item) => (item?.total * (1 / Math.pow(10, item?.decimals)) > 1))
    setHoldings({
      ...holdings,
      all: listCoinGreaterThan10
    })
  }, [isTotalGreaterThan10])

  // call data chart
  useEffect(() => {
    const getData = async() => {
      let dataChart
      switch (data?.chainId) {
        case BITCOIN_CHAINID:
          dataChart = await postBtc(`bitcoin/addresses-charts/period=${dataChangeByPeriod.period}`,
            { 'addresses': [data.address] }
          )
          break
        case ETHEREUM_CHAINID:
          dataChart = await postEvm(`evm/addresses-charts/period=${dataChangeByPeriod.period}`,
            { 'addresses': [data.address] }
          )
          break
        case RIPPLE_CHAINID:
          dataChart = await postXrp(`xrp/addresses-charts/period=${dataChangeByPeriod.period}`,
            { 'addresses': [data.address] }
          )
          break
        case BINANCE_SMART_CHAIN_CHAINID:
          dataChart = await postEvm(`evm/addresses-charts/period=${dataChangeByPeriod.period}`,
            { 'addresses': [data.address] }
          )
          break
        case SOLANA_CHAINID:
          dataChart = await postSolana(`solana/addresses-charts/period=${dataChangeByPeriod.period}`,
            { 'addresses': [data.address] }
          )
          break
        default:
          break
      }
      const itemPeriod = dataChartByPeriod && dataChartByPeriod?.findIndex((itemPeriod) => (itemPeriod?.period === dataChangeByPeriod?.period))
      if (itemPeriod !== -1) {
        const newDataChart = [...dataChartByPeriod]
        newDataChart[itemPeriod] = {
          'period': dataChangeByPeriod?.period,
          'dataChart': dataChart?.data[0]?.addressChart
        }
        setDataChartByPeriod(newDataChart)
      }
      if (itemPeriod === -1) {
        setDataChartByPeriod([
          ...dataChartByPeriod,
          {
            'period': dataChangeByPeriod?.period,
            'dataChart': dataChart?.data[0]?.addressChart
          }
        ])
      }
    }
    getData()
  }, [dataChangeByPeriod])

  useEffect(() => {
    const chart = dataChartByPeriod?.find((item) => (item?.period === dataChangeByPeriod?.period))
    if (chart?.dataChart) {
      if (chart?.dataChart === null) {
        setDataChart([])
      } else {
        const newChat = [...chart.dataChart]
        chart?.dataChart?.forEach((item, index) => {
          newChat[index] = {
            ...item,
            totalValue: parseFloat(item?.totalValue) * (data?.ownPersentage / 100)
          }
        })
        setDataChart(newChat)
      }
    }
  }, [dataChartByPeriod])

  // Tính tổng tiền
  const totalMoney = (total, currenValue) => {
    const value = total + parseFloat(currenValue?.total * (1 / Math.pow(10, currenValue?.decimals)))
    return value
  }

  useEffect(() => {
    // Tính tổng tiền
    const total = holdings?.all?.reduce(totalMoney, 0)
    const totalDirect = holdings?.direct?.reduce(totalMoney, 0)
    setTotalValue([
      {
        'name': 'all',
        'totalValue': total
      },
      {
        'name': 'direct',
        'totalValue': totalDirect
      }
    ])
  }, [holdings])

  useEffect(() => {
    const balanceFluctuations = dataChart && (dataChart[0]?.totalValue - dataChart[dataChart.length - 1]?.totalValue)
    const percent = balanceFluctuations ? totalPercent(balanceFluctuations, (dataChart && dataChart[0]?.totalValue)).toFixed(4).replace(/\d(?=(\d{3})+\.)/g, '$&,') : 0
    setPercentInfo({
      balanceFluctuations: balanceFluctuations,
      percent: percent
    })
  }, [dataChart])

  const handleDetail = (endpoind) => {
    if (endpoind === 'direct') {
      navigate('direct', { state: { data: data, holdings: holdings?.direct }})
    }
    if (endpoind === 'indirect') {
      navigate('indirect', { state: { data: data }})
    }
  }

  const columnsPopover = [
    {
      title: 'Title',
      dataIndex: 'title',
      width: '85%',
      render: (_, record) => (<span style={{ color: '#A8ADB3' }}>{record.title}</span>)
    }
  ]

  const onSelectChange = (newselectedRowKeys) => {
    setSelectedRowKeys(newselectedRowKeys)
  }

  const rowSelection = {
    selectedRowKeys,
    onChange: onSelectChange
  }

  const items = [
    {
      key: 'rank',
      title: '#'
    },
    {
      key: 'name',
      title: 'Name'
    },
    {
      key: 'amount',
      title: 'Amount'
    },
    {
      key: 'high24h',
      title: 'High 24h'
    },
    {
      key: 'low24h',
      title: 'Low 24h'
    },
    {
      key: 'percentChange24',
      title: 'Percent Change 24H'
    },
    {
      key: 'priceChange24h',
      title: 'Price Change 24H'
    },
    {
      key: 'total',
      title: 'Total'
    },
    {
      key: 'totalChange24h',
      title: 'Total Change 24H'
    },
    // {
    //   key: 'priceChange7d',
    //   title: '7d Change'
    // },
    {
      key: 'price',
      title: 'Price'
    },
    {
      key: 'marketCap',
      title: 'Market Cap'
    },
    {
      key: 'volumn',
      title: 'Volumn 24h'
    },
    {
      key: 'priceGraph',
      title: 'Price Graph (7d)'
    }
  ]

  const columns = [
    {
      key: '#',
      title: '#',
      width: '20px',
      hidden: !selectedRowKeys.includes('rank'),
      render: (_, record, index)=>(<span style={{ color: '#A8ADB3' }}>{(page.page - 1) * page.pageSize + index + 1}</span>)
    },
    {
      key: 'name',
      title: <span style={{ textAlign: 'left !important' }}>Name</span>,
      className: 'table-name',
      width: '250px',
      hidden: !selectedRowKeys.includes('name'),
      sorter: (a, b) => a.coinName.toLowerCase().localeCompare(b.coinName.toLowerCase()),
      render: (_, record) => (<div>
        <div className='table-icon-coin'>
          {record.coinImage
            ? (
              <LazyLoadImage src={record.coinImage}/>
            ) : (
              <span className='table-icon-coin-logo'>{record.symbol.slice(0, 3)}</span>
            )}
        </div>
        <div className='table-name-content'>
          <div className='table-name-text'>
            <span>
              {record?.coinName ? record?.coinName : ''}
            </span>
          </div>
          <div className='table-name-symbol'>
            <span>
              {record?.symbol ? record?.symbol.toUpperCase() : ''}
            </span>
          </div>
        </div>
      </div>)
    },
    {
      key: 'price',
      title: 'Price',
      dataIndex: 'price',
      width: '150px',
      sorter: (a, b) => a.price - b.price,
      hidden: !selectedRowKeys.includes('price'),
      render: (_, record) => (<span className='amount'>
        ${renderPrice(record)}
      </span>)
    },
    {
      key: 'amount',
      title: <span style={{ textAlign: 'left !important' }}>Amount</span>,
      width: '120px',
      hidden: !selectedRowKeys.includes('amount'),
      sorter: {
        compare: (a, b) => a.amount - b.amount,
        multiple: 1
      },
      render: (_, record) => (<span className='amount'>
        {(record?.amount * (1 / Math.pow(10, record?.decimals)))?.toFixed(5).replace(/\d(?=(\d{3})+\.)/g, '$&,')}
        {/* {record?.amount} */}
      </span>)
    },
    {
      key: 'total',
      title: 'Total',
      dataIndex: 'total',
      width: '150px',
      defaultSortOrder: 'descend',
      sorter: (a, b) => a.total - b.total,
      hidden: !selectedRowKeys.includes('total'),
      render: (_, record) => (<span className='amount'>
        {/* ${record?.total} */}
        ${(record?.total * (1 / Math.pow(10, record?.decimals)))?.toFixed(5).replace(/\d(?=(\d{3})+\.)/g, '$&,')}
      </span>)
    },
    {
      key: 'high24h',
      title: 'High 24h',
      width: '250px',
      hidden: !selectedRowKeys.includes('high24h'),
      render: (_, record) => (
        <div className='price-change'>
          $ {(record?.high24h * (1 / Math.pow(10, record?.decimals)))?.toFixed(5).replace(/\d(?=(\d{3})+\.)/g, '$&,')}
        </div>
      ),
      sorter: (a, b) => a.high24h - b.high24h
    },
    {
      key: 'low24h',
      title: 'Low 24H',
      width: '250px',
      hidden: !selectedRowKeys.includes('low24h'),
      render: (_, record = '12') => (
        <div className='price-change'>
          $ {(record?.low24h * (1 / Math.pow(10, record?.decimals)))?.toFixed(5).replace(/\d(?=(\d{3})+\.)/g, '$&,')}
        </div>
      ),
      sorter: (a, b) => a.low24h - b.low24h
    },
    {
      key: 'priceChange24h',
      title: 'Price Change (24h)',
      sorter: (a, b) => a.priceChange24h - b.priceChange24h,
      width: '200px',
      hidden: !selectedRowKeys.includes('priceChange24h'),
      render: (_, record) => (
        <div className='price-change'
          style={{
            color: record.priceChange24h ? (record.priceChange24h >= 0 ? '#34b349' : '#ff4d4d') : '#34b349'
          }}
        >
          $ {record.priceChange24h ? parseFloat(record.priceChange24h >= 0 ? record.priceChange24h : record.priceChange24h.toString().slice(1)).toFixed(4).replace(/\d(?=(\d{3})+\.)/g, '$&,') : '0.00'}
        </div>
      )
    },
    {
      key: 'percentChange24',
      title: 'Percent 24H',
      sorter: (a, b) => a.priceChangePercentage24h - b.priceChangePercentage24h,
      width: '150px',
      hidden: !selectedRowKeys.includes('percentChange24'),
      render: (_, record) => (
        <div className='price-change'
          style={{
            backgroundColor: record.priceChangePercentage24h ? (record.priceChangePercentage24h >= 0 ? 'rgba(52, 199, 89, 0.1)' : 'rgba(255,53,53,0.1)') : 'rgba(52, 199, 89, 0.1)',
            color: record.priceChangePercentage24h ? (record.priceChangePercentage24h >= 0 ? '#34b349' : '#ff4d4d') : '#34b349'
          }}
        >
          {record.priceChangePercentage24h ? (record.priceChangePercentage24h >= 0 ? <CaretUpOutlined/> : <CaretDownOutlined/>) : <CaretUpOutlined/>}
          {record.priceChangePercentage24h ? parseFloat(record.priceChangePercentage24h >= 0 ? record.priceChangePercentage24h : record.priceChangePercentage24h.toString().slice(1)).toFixed(2) : '0.00'} %
        </div>
      )
    },
    {
      key: 'totalChange24h',
      title: 'Total Change 24h',
      dataIndex: 'totalChange24h',
      width: '250px',
      sorter: (a, b) => a.totalChange24h - b.totalChange24h,
      hidden: !selectedRowKeys.includes('totalChange24h'),
      render: (_, record) => (
        <div className='price-change'
          style={{
            color: record.totalChange24h ? (record.totalChange24h >= 0 ? '#34b349' : '#ff4d4d') : '#34b349'
          }}
        >
          $ {record.totalChange24h ? parseFloat(record.totalChange24h >= 0 ? (record.totalChange24h * (1 / Math.pow(10, record?.decimals))) : (record.totalChange24h * (1 / Math.pow(10, record?.decimals))).toString().slice(1)).toFixed(5).replace(/\d(?=(\d{3})+\.)/g, '$&,') : '0.00'}
        </div>
      )
    },
    {
      key: 'marketCap',
      title: 'Market Cap',
      sorter: (a, b) => a.marketCap - b.marketCap,
      width: '150px',
      hidden: !selectedRowKeys.includes('marketCap'),
      render: (_, record) => (<span style={{ color: '#A8ADB3' }}>
        $ {(record.marketCap * BILLION).toFixed(5).replace(/\d(?=(\d{3})+\.)/g, '$&,')} B
      </span>)
    },
    {
      key: 'volumn',
      title: 'Volumn 24h',
      sorter: (a, b) => a.volumn - b.volumn,
      width: '150px',
      hidden: !selectedRowKeys.includes('volumn'),
      render: (_, record) => (<span style={{ color: '#A8ADB3' }}>
        $ {record?.volumn ? (record?.volumn * BILLION).toFixed(5).replace(/\d(?=(\d{3})+\.)/g, '$&,') : 0} B
      </span>)
    },
    {
      title: 'Price Graph (7d)',
      dataIndex: 'priceGraph',
      width: '150px',
      className: 'table-graph',
      hidden: !selectedRowKeys.includes('priceGraph'),
      render: (_, record) => (<Chart record={record?.priceChart7d} change7d={record.percentChange7d}/>)
    },
    {
      key: 'more',
      title: <Popover
        placement='bottomRight'
        content={(<Table
          showHeader={false}
          scroll={{
            y: 260
          }}
          style={{ maxWidth: '290px' }}
          className='tableabc'
          rowSelection={rowSelection}
          pagination={false}
          columns={columnsPopover}
          dataSource={items}
        >
        </Table>)}
        trigger='click'
      >
        +
      </Popover>,
      className: 'table-plus',
      width: '20px',
      dataIndex: 'key',
      render: (_, record) => (
        <div className='onchain-table-action'>
          <div className='onchain-table-action-icon'>
            <HeartOutlined />
          </div>
          <Popconfirm
            placement='top'
            className='onchain-table-action-icon'
            title='Are you sure to delete this connection?'
            okText='Yes'
            cancelText='No'
          >
            <DeleteOutlined className='tab-list-icon-item' />
          </Popconfirm>
        </div>
      )
    }
  ].filter(item => !item.hidden)

  const renderPrice = (record) => {
    let price
    if ((record?.price >= 0) && (record?.price < 1)) {
      price = record?.price.toFixed(5).replace(/\d(?=(\d{3})+\.)/g, '$&,')
    }
    if (record?.price >= 1) {
      price = record?.price.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')
    }
    return price
  }

  const handleBack = () => {
    navigate(-1)
  }

  return (
    <>
      <div className='detail-onchain'>
        <Button onClick={handleBack}>Back</Button>
        <Button onClick={() => setIsTotalGreaterThan10(!isTotalGreaterThan10)}>List coin has total greater than 1$ </Button>
      </div>
      <Row gutter={60} style={{ marginTop: '50px' }} justify='space-around' align='middle'>
        <Col span={12}>
          <PropertyOverview
            title={data?.connectionName}
            dataChart={dataChart && [...dataChart]?.reverse()}
            width={500}
            height={100}
            total={totalValue && totalValue[0]?.totalValue}
            detail={{ 'isDetail': false }}
            isButton={false}
            titleButton=''
            listPeriod={[
              { 'title': '1H', 'period': '1h' },
              { 'title': '1D', 'period': '1d' },
              { 'title': '7D', 'period': '7d' },
              { 'title': '1M', 'period': '1m' },
              { 'title': '3M', 'period': '3m' },
              { 'title': '6M', 'period': '6m' },
              { 'title': 'ALL', 'period': 'all' }
            ]}
            type='all'
            percentInfo={percentInfo}
            setDataChangeByPeriod={setDataChangeByPeriod}
          />
        </Col>
        <Col span={12}>
          <Row gutter={12}>
            <Col span={14}>
              <PieChartAsset data={totalValue && totalValue} sizeForPieChart={150} />
            </Col>
            <Col span={8}>
              <div
                className='onchain-profit'
                onClick={() => handleDetail('direct')}
              >
                <ArrowDownOutlined className='onchain-profit-icon'/>
                <div className='onchain-profit-title'>Direct</div>
                <div className='onchain-profit-percent'>20%</div>
              </div>
              <div
                className='onchain-profit'
                onClick={() => handleDetail('indirect')}
              >
                <ArrowUpOutlined className='onchain-profit-icon'/>
                <div className='onchain-profit-title'>Indirect</div>
                <div className='onchain-profit-percent'>20%</div>
              </div>
            </Col>
          </Row>
        </Col>
      </Row>
      <ListCoinTable
        data={holdings?.all}
        columns={columns}
        pagination={{
          defaultCurrent: page.page,
          pageSize: page.pageSize,
          showSizeChanger: false,
          onChange(current) {
            setPage({ ...page, page: current })
          }
        }}
        scroll={{ x: 'max-content' }}
        detailCoin={true}
      />
    </>
  )
}

export default DetailConnection
