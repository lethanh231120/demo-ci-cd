import React, { useState } from 'react'
import { Row, Col, Button } from 'antd'
import PropertyOverview from '../../../../components/property-overview/PropertyOverview'
import TableAsset from '../../../../components/modal/table/TableAsset'
import { useNavigate, useLocation } from 'react-router-dom'
const dataChart = [
  { totalValue: 5520785486.214152, timestamp: '2022-09-18T08:50:00Z' },
  { totalValue: 5521803264.503245, timestamp: '2022-09-18T08:45:00Z' },
  { totalValue: 5520507332.999393, timestamp: '2022-09-18T08:40:00Z' },
  { totalValue: 5521595852.674142, timestamp: '2022-09-18T08:35:00Z' },
  { totalValue: 5520186812.497448, timestamp: '2022-09-18T08:30:00Z' },
  { totalValue: 5520476283.461426, timestamp: '2022-09-18T08:25:00Z' },
  { totalValue: 5517070240.168135, timestamp: '2022-09-18T08:20:00Z' },
  { totalValue: 5509848022.339613, timestamp: '2022-09-18T08:15:00Z' },
  { totalValue: 5512754221.32966, timestamp: '2022-09-18T08:10:00Z' },
  { totalValue: 5510609803.097517, timestamp: '2022-09-18T08:05:00Z' },
  { totalValue: 5515307590.970818, timestamp: '2022-09-18T08:00:00Z' },
  { totalValue: 5520997205.68044, timestamp: '2022-09-18T07:55:00Z' }
]

const ListCoinIndirectConnection = () => {
  const { state } = useLocation()
  const { data, holdings } = state
  const [dataChangeByPeriod, setDataChangeByPeriod] = useState({ 'period': '1d' })
  const [isTotalGreaterThan10, setIsTotalGreaterThan10] = useState(false)

  console.log(dataChangeByPeriod)
  console.log(holdings)
  const navigate = useNavigate()
  const columns = [
    {
      key: 'rank',
      title: '#',
      width: '20px'
      // render: (_, record, index) => (<span style={{ color: '#A8ADB3' }}>{index + 1}</span>)
    },
    {
      key: 'name',
      title: 'Name',
      width: '100px',
      sorter: (a, b) => a.name.toLowerCase().localeCompare(b.name.toLowerCase()),
      render: (_, record) => (
        <div className='table-name'>
          {/* <span className='table-symbol'>{record?.name}</span> */}
        </div>
      )
    },
    {
      key: 'price',
      title: 'Price',
      width: '150px',
      // sorter: (a, b) => a.exchange.toLowerCase().localeCompare(b.exchange.toLowerCase()),
      render: (_, record) => (
        <div className='table-name'>
          {/* <span className='table-symbol'>{record?.exchange}</span> */}
        </div>
      )
    },
    {
      key: 'total',
      title: 'Total',
      width: '150px',
      // sorter: (a, b) => parseFloat(a.amount) - parseFloat(b.amount),
      render: (_, record) => (
        <>
          {/* <span>{parseInt(record.amount)}</span> */}
        </>
      )
    },
    {
      key: 'percentChange',
      title: 'Change',
      width: '150px',
      // sorter: (a, b) => parseFloat(a.adjpriceclose) - parseFloat(b.adjpriceclose),
      render: (_, record) => (
        <>
          {/* <span>{record?.adjpriceclose ? record?.adjpriceclose : 0}</span> */}
        </>
      )
    },
    {
      key: 'amount',
      title: 'Amount',
      width: '150px',
      sorter: (a, b) => parseFloat(a.total1m) - parseFloat(b.total1m),
      render: (_, record) => (
        <>
          {/* <span>{record.volume}</span> */}
        </>
      )
    },
    {
      key: 'pricegraph',
      title: 'Price Graph',
      width: '150px',
      sorter: (a, b) => parseFloat(a.total3m) - parseFloat(b.total3m),
      render: (_, record) => (
        <>
          {/* <span>{record.total3m}</span> */}
        </>
      )
    }
  ]

  const handleBack = () => {
    navigate(-1)
  }

  return (
    <>
      <div className='detail-onchain'>
        <Button onClick={handleBack}>Back</Button>
        <Button onClick={() => setIsTotalGreaterThan10(!isTotalGreaterThan10)}>List coin has total greater than 1$ </Button>
      </div>
      <Row gutter={60} style={{ marginTop: '50px' }}>
        <Col span={16}>
          <PropertyOverview
            title='List Coin Indirect'
            dataChart={dataChart}
            width={500}
            height={100}
            total={5510609803.097517}
            detail={{ 'isDetail': false }}
            isButton={false}
            titleButton=''
            listPeriod={[
              { 'title': '1H', 'period': '1h' },
              { 'title': '1D', 'period': '1d' },
              { 'title': '7D', 'period': '7d' },
              { 'title': '1M', 'period': '1m' },
              { 'title': '3M', 'period': '3m' },
              { 'title': '6M', 'period': '6m' },
              { 'title': 'ALL', 'period': 'all' }
            ]}
            type='all'
            balanceFluctuations={5509848.2131231}
            percent={2.45}
            setDataChangeByPeriod={setDataChangeByPeriod}
          />
        </Col>
        <Col span={8}>
          {/* <PieChartAsset width={400} height={400}/> */}
        </Col>
      </Row>
      <div style={{ marginTop: '50px' }}>
        <TableAsset
          columns={columns}
          data={data}
          // loading={loading}
        />
      </div>
    </>
  )
}

export default ListCoinIndirectConnection
