import React, { useState } from 'react'
import { Row, Col } from 'antd'
import PropertyOverview from '../../../../components/property-overview/PropertyOverview'
import { useLocation } from 'react-router-dom'
import TableAsset from '../../../../components/modal/table/TableAsset'

const ListCoinIndirectAccount = () => {
  const [dataChangeByPeriod, setDataChangeByPeriod] = useState({ 'period': '1d' })
  const [dataForTable, setDataForTable] = useState([])
  const [dataChart, setDataChart] = useState([])
  console.log(dataChangeByPeriod)
  const { state } = useLocation()
  const { data } = state
  const total = parseFloat(data.children[3].totalValue)
  useState(() => {
    setDataForTable(data.inDirect)
    const chartArr = []
    chartArr.push(...data.children[0].valueChart)
    setDataChart(chartArr)
  }, [data])
  const columns = [
    {
      key: 'rank',
      title: '#',
      width: '20px',
      render: (_, record, index) => (<span style={{ color: '#A8ADB3' }}>{index + 1}</span>)
    },
    {
      key: 'name',
      title: 'Name',
      width: '300px',
      sorter: (a, b) => a.asset.toLowerCase().localeCompare(b.asset.toLowerCase()),
      render: (_, record) => (
        <div className='table-name'>
          <span className='table-symbol'>{record?.asset}</span>
        </div>
      )
    },
    {
      key: 'free',
      title: 'Free',
      width: '150px',
      sorter: (a, b) => parseFloat(a.free) - parseFloat(b.free),
      render: (_, record) => (
        <span>{record?.free ? parseFloat(record?.free).toFixed(2) : parseFloat(record?.amount).toFixed(2)}</span>
      )
    },
    {
      key: 'locked',
      title: 'Locked',
      width: '150px',
      sorter: (a, b) => parseFloat(a.locked) - parseFloat(b.locked),
      render: (_, record) => (
        <span>{record?.locked ? parseFloat(record?.locked).toFixed(2) : 0}</span>
      )
    },
    {
      key: 'prices',
      title: 'Prices',
      width: '150px',
      sorter: (a, b) => parseFloat(a.price) - parseFloat(b.price),
      render: (_, record) => (
        <span>{parseFloat(record?.price).toFixed(2)}</span>
      )
    }
  ]

  return (
    <>
      <Row gutter={60} style={{ marginTop: '50px' }}>
        <Col span={16}>
          <PropertyOverview
            title='List Coin Indirect'
            dataChart={dataChart}
            width={500}
            height={100}
            total={total}
            detail={{ 'isDetail': false }}
            isButton={false}
            titleButton=''
            listPeriod={[
              { 'title': '1H', 'period': '1h' },
              { 'title': '1D', 'period': '1d' },
              { 'title': '7D', 'period': '7d' },
              { 'title': '1M', 'period': '1m' },
              { 'title': '3M', 'period': '3m' },
              { 'title': '6M', 'period': '6m' },
              { 'title': 'ALL', 'period': 'all' }
            ]}
            type='all'
            balanceFluctuations={5509848.2131231}
            percent={2.45}
            setDataChangeByPeriod={setDataChangeByPeriod}
          />
        </Col>
        <Col span={8}>
          {/* <PieChartAsset width={400} height={400}/> */}
        </Col>
      </Row>
      <div style={{ marginTop: '50px' }}>
        <TableAsset
          columns={columns}
          data={dataForTable}
          // loading={loading}
        />
      </div>
    </>
  )
}

export default ListCoinIndirectAccount
