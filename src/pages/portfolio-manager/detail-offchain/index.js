import React, { useState, useEffect } from 'react'
import PropertyOverview from '../../../components/property-overview/PropertyOverview'
import { Row, Col } from 'antd'
import OffchainAssetTable from '../dashboard-tab/table/OffchainAssetTable'
import { postBinance } from '../../../api/binanceService'
import { getConnection } from '../../../api/connectService'
import { useQuery } from '@tanstack/react-query'
import PieChartAsset from '../../../components/chart/PieChart'
import _ from 'lodash'
import { totalPercent } from '../../../utils/parseFloat'
import { useNavigate } from 'react-router-dom'

const DetailOffchain = () => {
  const [dataChangeByPeriod, setDataChangeByPeriod] = useState({ 'period': '1d' })
  const [total, setTotal] = useState(0)
  const [dataOffchain, setDataOffChain] = useState()
  const [data, setData] = useState()
  const [dataForPie, setDataForPie] = useState()
  const [dataOffchainWithChart, setDataOffchainWithChart] = useState()
  const [dataChart, setDataChart] = useState()
  const [percentInfo, setPercentInfo] = useState()
  const navigate = useNavigate()

  const { data: listAccounts } = useQuery(
    ['listAccounts'],
    async() => {
      const account = await getConnection('connect/current-accounts/cexName=binance')
      return account.data
    }
  )
  const listAccountInfo = listAccounts?.map(async(account) => {
    const dataForm = {
      'accounts': [{
        apiKey: account.apiKey,
        secretKey: account.secretKey
      }]
    }
    const accountInfo = await postBinance('binance/account-info', dataForm)
    return accountInfo?.data
  })
  useEffect(() => {
    if (listAccounts === null) {
      navigate('../..')
    }
    const merged = []
    Promise.all(listAccountInfo).then(res => {
      let totalValue = 0
      for (let i = 0; i < res.length; i++) {
        totalValue += parseFloat(res[i][0]?.account?.totalValue)
        merged.push({
          ...res[i][0],
          ...listAccounts[i]
        })
      }
      setTotal(totalValue)
      setDataOffChain(merged)
    })
  }, [listAccounts])

  const dataForTable = []
  const dataForPieArr = []
  // get data chart offchain
  const listAccountChart = listAccounts?.map(async(account) => {
    const dataForm = {
      'accounts': [{
        apiKey: account.apiKey,
        secretKey: account.secretKey
      }]
    }
    const accountChartPoint = await postBinance(`binance/charts/period=${dataChangeByPeriod?.period}`, dataForm)
    return accountChartPoint.data
  })

  useEffect(() => {
    const allOffchainChartArr = []
    const dataChartOffchainAccountArr = []
    const merged = []
    const data = []
    Promise.all(listAccountChart).then(res => {
      for (let i = 0; i < res.length; i++) {
        dataChartOffchainAccountArr.push({ ...res[i][0] })
        for (let j = 0; j < res[i][0].accountValueRepo.accountChart.length; j++) {
          allOffchainChartArr.push(
            res[i][0]?.accountValueRepo.accountChart[j]
          )
        }
      }
      for (let i = 0; i < dataOffchain?.length; i++) {
        merged.push({
          ...dataOffchain[i],
          ...dataChartOffchainAccountArr[i]
        })
      }
      const holder = allOffchainChartArr.reduce((prev, { timestamp, totalValue }) => {
        prev[timestamp] = prev[timestamp] ? prev[timestamp] + parseFloat(totalValue) : parseFloat(totalValue)
        return prev
      }, {})
      for (const i in holder) {
        data.push({ totalValue: holder[i], timestamp: i })
      }
      // set percent info
      const balanceFluctuations = (parseFloat(data[0]?.totalValue) - parseFloat(data[data.length - 1]?.totalValue)) || 0
      const percent = balanceFluctuations ? totalPercent(balanceFluctuations, (data && data[0]?.totalValue)).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,') : 0
      setPercentInfo({
        balanceFluctuations: balanceFluctuations,
        percent: percent
      })
      setDataChart(data.reverse())
      setDataOffchainWithChart(merged)
    })
  }, [listAccounts, dataOffchain])

  useEffect(() => {
    const holdings = []
    const directHoldings = []
    const inDirectHoldings = []
    dataOffchainWithChart && dataOffchainWithChart?.map((connection) => {
      const isolatedAssetHoldings = []
      const spotHoldings = []
      const marginCrossHoldings = []
      const coinMFutureHoldings = []
      const usdsMFutureHoldings = []
      const savingHoldings = []
      parseFloat(connection?.spotAccount?.subAccount?.totalValue) > 0 ? spotHoldings.push(...connection.spotAccount.spots || []) : spotHoldings
      parseFloat(connection?.marginCrossAccount?.subAccount?.totalValue) > 0 ? marginCrossHoldings.push(...connection.marginCrossAccount.userAssets || []) : marginCrossHoldings
      parseFloat(connection?.coinMFutureAccount?.subAccount?.totalValue) > 0 ? coinMFutureHoldings.push(...connection.coinMFutureAccount.coinMFutures || []) : coinMFutureHoldings
      parseFloat(connection?.usdsMFutureAccount?.subAccount?.totalValue) > 0 ? usdsMFutureHoldings.push(...connection.usdsMFutureAccount.usdsMFutures || []) : usdsMFutureHoldings
      parseFloat(connection?.savingsAccount?.subAccount?.totalValue) > 0 ? savingHoldings.push(...connection.savingsAccount.positionAmountVos || []) : savingHoldings
      connection?.marginIsolatedAccount?.assets !== null &&
      connection?.marginIsolatedAccount?.assets?.map((item) => {
        isolatedAssetHoldings.push(item.quoteAsset)
      })
      holdings.push(
        ...isolatedAssetHoldings,
        ...spotHoldings,
        ...marginCrossHoldings,
        ...coinMFutureHoldings,
        ...usdsMFutureHoldings,
        ...savingHoldings
      )
      directHoldings.push(
        ...isolatedAssetHoldings,
        ...spotHoldings,
        ...marginCrossHoldings,
        ...coinMFutureHoldings,
        ...usdsMFutureHoldings
      )
      inDirectHoldings.push(...savingHoldings)
      const allCoinAffterGroup = []
      const allDirectCoinAfterGroup = []
      // group tất cả các holding theo coinId
      const groupByCoinId = _.groupBy(holdings && holdings, (item) => {
        if (item?.asset) {
          return item?.asset
        }
      })
      // foreach object groupByCoinId
      groupByCoinId && Object.keys(groupByCoinId).forEach(function(key) {
        // check nếu key khác undefined tức là tồn tại coinId
        const free = groupByCoinId[key].reduce((total, item) => {
          return total + parseFloat(item?.free ? item.free : item.balance)
        }, 0)
        const locked = groupByCoinId[key].reduce((total, item) => {
          return total + parseFloat(item?.locked ? item?.locked : 0)
        }, 0)
        // cập nhật lại thông tin đồng coin với amount lấy tu danh sach dong coin co coinId giong nhau
        // lấy thông tin 1 đồng coin bất ky trong list co coinId giong nhau va cap nhật amount cho dong coin
        const itemInfo = {
          ...groupByCoinId[key][0],
          free: free,
          locked: locked
        }
        // thêm đồng coin vào danh sách allCoinAffterGroup
        allCoinAffterGroup.push(itemInfo)
      })

      // group direct holding by coinId
      const groupDirectByCoinId = _.groupBy(directHoldings && directHoldings, (item) => {
        if (item?.asset) {
          return item?.asset
        }
      })
      groupDirectByCoinId && Object.keys(groupDirectByCoinId).forEach(function(key) {
        const free = groupDirectByCoinId[key].reduce((total, item) => {
          return total + parseFloat(item?.free ? item.free : item.balance)
        }, 0)
        const locked = groupDirectByCoinId[key].reduce((total, item) => {
          return total + parseFloat(item?.locked ? item?.locked : 0)
        }, 0)
        const itemInfo = {
          ...groupDirectByCoinId[key][0],
          free: free,
          locked: locked
        }
        allDirectCoinAfterGroup.push(itemInfo)
      })

      dataForTable.push({
        name: connection?.connectionName || 'Binance',
        id: connection?.id,
        totalValue: connection?.account?.totalValue,
        totalValueAll: connection?.account?.totalValueAll,
        totalValue1h: connection?.account?.totalValue1h,
        totalValue4h: connection?.account?.totalValue4h,
        totalValue1d: connection?.account?.totalValue1d,
        totalValue7d: connection?.account?.totalValue7d,
        totalValue1m: connection?.account?.totalValue1m,
        totalValue3m: connection?.account?.totalValue3m,
        totalValue6m: connection?.account?.totalValue6m,
        totalValue1y: connection?.account?.totalValue1y,
        holdings: allCoinAffterGroup,
        directs: allDirectCoinAfterGroup,
        inDirect: inDirectHoldings,
        valueChart: connection?.accountValueRepo?.accountChart,
        children: [{
          name: 'Spot',
          totalValue: connection?.spotAccount?.subAccount?.totalValue,
          totalValueAll: connection?.spotAccount?.subAccount?.totalValueAll,
          totalValue1h: connection?.spotAccount?.subAccount?.totalValue1h,
          totalValue4h: connection?.spotAccount?.subAccount?.totalValue4h,
          totalValue1d: connection?.spotAccount?.subAccount?.totalValue1d,
          totalValue7d: connection?.spotAccount?.subAccount?.totalValue7d,
          totalValue1m: connection?.spotAccount?.subAccount?.totalValue1m,
          totalValue3m: connection?.spotAccount?.subAccount?.totalValue3m,
          totalValue6m: connection?.spotAccount?.subAccount?.totalValue6m,
          totalValue1y: connection?.spotAccount?.subAccount?.totalValue1y,
          holdings: connection.coinMFutureAccount.coinMFutures !== null ? connection.coinMFutureAccount.coinMFutures : [],
          valueChart: connection?.spotValueRepo?.spotChart
        }, {
          name: 'Margin Cross',
          totalValue: connection?.marginCrossAccount?.subAccount?.totalValue,
          totalValueAll: connection?.marginCrossAccount?.subAccount?.totalValueAll,
          totalValue1h: connection?.marginCrossAccount?.subAccount?.totalValue1h,
          totalValue4h: connection?.marginCrossAccount?.subAccount?.totalValue4h,
          totalValue1d: connection?.marginCrossAccount?.subAccount?.totalValue1d,
          totalValue7d: connection?.marginCrossAccount?.subAccount?.totalValue7d,
          totalValue1m: connection?.marginCrossAccount?.subAccount?.totalValue1m,
          totalValue3m: connection?.marginCrossAccount?.subAccount?.totalValue3m,
          totalValue6m: connection?.marginCrossAccount?.subAccount?.totalValue6m,
          totalValue1y: connection?.marginCrossAccount?.subAccount?.totalValue1y,
          holdings: connection.marginCrossAccount.userAssets !== null ? connection.marginCrossAccount.userAssets : [],
          valueChart: connection?.magrinCrossValueRepo?.magrinCrossChart
        }, {
          name: 'Margin Isolated',
          totalValue: connection?.marginIsolatedAccount?.subAccount?.totalValue,
          totalValueAll: connection?.marginIsolatedAccount?.subAccount?.totalValueAll,
          totalValue1h: connection?.marginIsolatedAccount?.subAccount?.totalValue1h,
          totalValue4h: connection?.marginIsolatedAccount?.subAccount?.totalValue4h,
          totalValue1d: connection?.marginIsolatedAccount?.subAccount?.totalValue1d,
          totalValue7d: connection?.marginIsolatedAccount?.subAccount?.totalValue7d,
          totalValue1m: connection?.marginIsolatedAccount?.subAccount?.totalValue1m,
          totalValue3m: connection?.marginIsolatedAccount?.subAccount?.totalValue3m,
          totalValue6m: connection?.marginIsolatedAccount?.subAccount?.totalValue6m,
          totalValue1y: connection?.marginIsolatedAccount?.subAccount?.totalValue1y,
          holdings: isolatedAssetHoldings !== null ? isolatedAssetHoldings : [],
          valueChart: connection?.magrinIsolatedValueRepo?.magrinIsolatedChart
        }, {
          name: 'Saving',
          totalValue: connection?.savingsAccount?.subAccount?.totalValue,
          totalValueAll: connection?.savingsAccount?.subAccount?.totalValueAll,
          totalValue1h: connection?.savingsAccount?.subAccount?.totalValue1h,
          totalValue4h: connection?.savingsAccount?.subAccount?.totalValue4h,
          totalValue1d: connection?.savingsAccount?.subAccount?.totalValue1d,
          totalValue7d: connection?.savingsAccount?.subAccount?.totalValue7d,
          totalValue1m: connection?.savingsAccount?.subAccount?.totalValue1m,
          totalValue3m: connection?.savingsAccount?.subAccount?.totalValue3m,
          totalValue6m: connection?.savingsAccount?.subAccount?.totalValue6m,
          totalValue1y: connection?.savingsAccount?.subAccount?.totalValue1y,
          holdings: connection.savingsAccount.positionAmountVos !== null ? connection.savingsAccount.positionAmountVos : [],
          valueChart: connection?.savingValueRepo?.savingChart
        }, {
          name: 'Future - Stable part',
          totalValue: connection?.usdsMFutureAccount?.subAccount?.totalValue,
          totalValueAll: connection?.usdsMFutureAccount?.subAccount?.totalValueAll,
          totalValue1h: connection?.usdsMFutureAccount?.subAccount?.totalValue1h,
          totalValue4h: connection?.usdsMFutureAccount?.subAccount?.totalValue4h,
          totalValue1d: connection?.usdsMFutureAccount?.subAccount?.totalValue1d,
          totalValue7d: connection?.usdsMFutureAccount?.subAccount?.totalValue7d,
          totalValue1m: connection?.usdsMFutureAccount?.subAccount?.totalValue1m,
          totalValue3m: connection?.usdsMFutureAccount?.subAccount?.totalValue3m,
          totalValue6m: connection?.usdsMFutureAccount?.subAccount?.totalValue6m,
          totalValue1y: connection?.usdsMFutureAccount?.subAccount?.totalValue1y,
          holdings: connection.usdsMFutureAccount.usdsMFutures !== null ? connection.usdsMFutureAccount.usdsMFutures : [],
          valueChart: connection?.usDsMFutureValueRepo?.usDsMFutureChart
        }, {
          name: 'Future - Coins part',
          totalValue: connection?.coinMFutureAccount?.subAccount?.totalValue,
          totalValueAll: connection?.coinMFutureAccount?.subAccount?.totalValueAll,
          totalValue1h: connection?.coinMFutureAccount?.subAccount?.totalValue1h,
          totalValue4h: connection?.coinMFutureAccount?.subAccount?.totalValue4h,
          totalValue1d: connection?.coinMFutureAccount?.subAccount?.totalValue1d,
          totalValue7d: connection?.coinMFutureAccount?.subAccount?.totalValue7d,
          totalValue1m: connection?.coinMFutureAccount?.subAccount?.totalValue1m,
          totalValue3m: connection?.coinMFutureAccount?.subAccount?.totalValue3m,
          totalValue6m: connection?.coinMFutureAccount?.subAccount?.totalValue6m,
          totalValue1y: connection?.coinMFutureAccount?.subAccount?.totalValue1y,
          holdings: connection.coinMFutureAccount.coinMFutures !== null ? connection.coinMFutureAccount.coinMFutures : [],
          valueChart: connection?.coinMFutureValueRepo?.coinMFutureChart
        }]
      })
      dataForPieArr.push({
        name: `${connection?.connectionName}`,
        totalValue: parseFloat(connection?.account?.totalValue)
      })
      setData(dataForTable)
      setDataForPie(dataForPieArr)
    })
  }, [dataOffchainWithChart])
  return (
    <>
      <Row gutter={60} style={{ marginTop: '50px' }}>
        <Col span={16}>
          <PropertyOverview
            title={'Offchain Wallet'}
            dataChart={dataChart}
            width={500}
            height={100}
            total={parseFloat(total && total)}
            detail={{ 'isDetail': false }}
            isButton={false}
            titleButton=''
            listPeriod={[
              { 'title': '1H', 'period': '1h' },
              { 'title': '1D', 'period': '1d' },
              { 'title': '7D', 'period': '7d' },
              { 'title': '1M', 'period': '1m' },
              { 'title': '3M', 'period': '3m' },
              { 'title': '6M', 'period': '6m' },
              { 'title': 'ALL', 'period': 'all' }
            ]}
            type='all'
            percentInfo={percentInfo}
            setDataChangeByPeriod={setDataChangeByPeriod}
          />
        </Col>
        <Col span={8}>
          <PieChartAsset width={400} height={400} data={dataForPie && dataForPie} sizeForPieChart={150}/>
        </Col>
      </Row>
      <OffchainAssetTable
        isButtonDetail={false}
        dataForOffchainDetail={data}
      />
    </>
  )
}

export default DetailOffchain
