import React, { useEffect, useState } from 'react'
import { Row, Col } from 'antd'
import PropertyOverview from '../../../components/property-overview/PropertyOverview'
import OffchainAssetTable from '../dashboard-tab/table/OffchainAssetTable'
import { ArrowDownOutlined, ArrowUpOutlined } from '@ant-design/icons'
import PieChartAsset from '../../../components/chart/PieChart'
import './style.scss'
import { useLocation, useNavigate } from 'react-router-dom'
import { totalPercent } from '../../../utils/parseFloat'

const DetailAccount = () => {
  const [dataChangeByPeriod, setDataChangeByPeriod] = useState({ 'period': '1d' })
  const [dataForPie, setDataForPie] = useState([])
  const [directPercent, setDirectPercent] = useState()
  const [inDirectPercent, setInDirectPercent] = useState()
  const [percentInfo, setPercentInfo] = useState()

  console.log(dataChangeByPeriod)

  const { state } = useLocation()
  const { data } = state
  console.log('DATAAAA', data)
  const { totalValue } = data
  const navigate = useNavigate()

  const handleDetail = (endpoint) => {
    if (endpoint === 'direct') {
      navigate(`../direct`, { state: { data: data }})
    }
    if (endpoint === 'indirect') {
      navigate(`../indirect`, { state: { data: data }})
    }
  }
  useEffect(() => {
    const dataForPieArr = []
    dataForPieArr.push({
      name: 'In Direct',
      totalValue: parseFloat(data?.children[3]?.totalValue)
    }, {
      name: 'Direct',
      totalValue: parseFloat(parseFloat(data.totalValue) - parseFloat(data?.children[3]?.totalValue))
    })
    // set percent info
    const balanceFluctuations = (parseFloat(data.valueChart[data.valueChart.length - 1]?.totalValue) - parseFloat(data.valueChart[0].totalValue)) || 0
    const percent = balanceFluctuations ? totalPercent(balanceFluctuations, (data && data.valueChart[0]?.totalValue)).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,') : 0
    setPercentInfo({
      balanceFluctuations: balanceFluctuations,
      percent: percent
    })
    setDataForPie(dataForPieArr)
    setInDirectPercent(parseFloat(data?.children[3]?.totalValue) * 100 / parseFloat(data.totalValue))
    setDirectPercent(parseFloat(parseFloat(data.totalValue) - parseFloat(data?.children[3]?.totalValue)) * 100 / parseFloat(data.totalValue))
  }, [data])
  return (
    <>
      <Row gutter={60} style={{ marginTop: '50px' }} justify='space-around' align='middle'>
        <Col span={12}>
          <PropertyOverview
            title={data.name}
            width={500}
            height={100}
            dataChart={data.valueChart}
            total={parseFloat(totalValue && totalValue)}
            detail={{ 'isDetail': false }}
            isButton={false}
            titleButton=''
            listPeriod={[
              { 'title': '1H', 'period': '1h' },
              { 'title': '1D', 'period': '1d' },
              { 'title': '7D', 'period': '7d' },
              { 'title': '1M', 'period': '1m' },
              { 'title': '3M', 'period': '3m' },
              { 'title': '6M', 'period': '6m' },
              { 'title': 'ALL', 'period': 'all' }
            ]}
            type='all'
            setDataChangeByPeriod={setDataChangeByPeriod}
            percentInfo={percentInfo}
          />
        </Col>
        <Col span={12}>
          <Row gutter={12}>
            <Col span={14}>
              <PieChartAsset width={400} height={400} data={dataForPie && dataForPie} sizeForPieChart={150}/>
            </Col>
            <Col span={8}>
              <div
                className='offchain-profit'
                onClick={() => handleDetail('direct')}
              >
                <ArrowDownOutlined className='offchain-profit-icon'/>
                <div className='offchain-profit-title'>Direct</div>
                <div className='offchain-profit-percent'>{parseFloat(directPercent).toFixed(2)}%</div>
              </div>
              {
                parseFloat(inDirectPercent) > 0
                  ? <div
                    className='offchain-profit'
                    onClick={() => handleDetail('indirect')}
                  >
                    <ArrowUpOutlined className='offchain-profit-icon'/>
                    <div className='offchain-profit-title'>Indirect</div>
                    <div className='offchain-profit-percent'>{parseFloat(inDirectPercent).toFixed(2)}%</div>
                  </div>
                  : ''
              }
            </Col>
          </Row>
        </Col>
      </Row>
      <OffchainAssetTable
        isButtonDetail={false}
        dataForSingleOffchainAccount={data}
      />
    </>
  )
}

export default DetailAccount
