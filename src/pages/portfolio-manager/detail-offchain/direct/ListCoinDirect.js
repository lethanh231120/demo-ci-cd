import React, { useState } from 'react'
import { Row, Col } from 'antd'
import PropertyOverview from '../../../../components/property-overview/PropertyOverview'
import { useLocation } from 'react-router-dom'
import TableAsset from '../../../../components/modal/table/TableAsset'
import PieChartAsset from '../../../../components/chart/PieChart'

const ListCoinIndirectAccount = () => {
  const [dataForTable, setDataForTable] = useState([])
  const [dataChart, setDataChart] = useState([])
  const [dataForPie, setDataForPie] = useState([])
  const { state } = useLocation()
  const { data } = state
  console.log('DATA', data)
  const total = (parseFloat(data.totalValue) - parseFloat(data.children[3].totalValue))
  useState(() => {
    let chartArr = []
    const allResArr = []
    const dataForPieArr = []
    setDataForTable(data.directs)
    chartArr.push(
      data?.children[0]?.valueChart,
      data?.children[1]?.valueChart,
      data?.children[2]?.valueChart,
      data?.children[4]?.valueChart,
      data?.children[5]?.valueChart
    )
    chartArr = chartArr.filter((element) => {
      return element !== undefined
    })
    for (let i = 0; i < chartArr.length; i++) {
      allResArr.push(...chartArr[i])
    }
    const dataArr = []
    const holder = allResArr.reduce((prev, { timestamp, totalValue }) => {
      prev[timestamp] = prev[timestamp] ? prev[timestamp] + parseFloat(totalValue) : parseFloat(totalValue)
      return prev
    }, {})
    for (const i in holder) {
      dataArr.push({ totalValue: holder[i], timestamp: i })
    }
    for (let i = 0; i < data.directs.length; i++) {
      dataForPieArr.push({
        name: data.directs[i].asset,
        totalValue: parseFloat(data.directs[i].price) * data.directs[i].free
      })
    }
    setDataForPie(dataForPieArr)
    setDataChart(dataArr)
  }, [data])
  const columns = [
    {
      key: 'rank',
      title: '#',
      width: '20px',
      render: (_, record, index) => (<span style={{ color: '#A8ADB3' }}>{index + 1}</span>)
    },
    {
      key: 'name',
      title: 'Name',
      width: '300px',
      sorter: (a, b) => a.asset.toLowerCase().localeCompare(b.asset.toLowerCase()),
      render: (_, record) => (
        <div className='table-name'>
          <span className='table-symbol'>{record?.asset}</span>
        </div>
      )
    },
    {
      key: 'free',
      title: 'Free',
      width: '150px',
      sorter: (a, b) => parseFloat(a.free) - parseFloat(b.free),
      render: (_, record) => (
        <span>{record?.free ? parseFloat(record?.free).toFixed(2) : parseFloat(record?.amount).toFixed(2)}</span>
      )
    },
    {
      key: 'locked',
      title: 'Locked',
      width: '150px',
      sorter: (a, b) => parseFloat(a.locked) - parseFloat(b.locked),
      render: (_, record) => (
        <span>{record?.locked ? parseFloat(record?.locked).toFixed(2) : 0}</span>
      )
    },
    {
      key: 'prices',
      title: 'Prices',
      width: '150px',
      sorter: (a, b) => parseFloat(a.price) - parseFloat(b.price),
      render: (_, record) => (
        <span>{parseFloat(record?.price).toFixed(2)}</span>
      )
    }
  ]

  return (
    <>
      <Row gutter={60} style={{ marginTop: '50px' }}>
        <Col span={16}>
          <PropertyOverview
            title='List Coin Direct'
            dataChart={dataChart}
            width={500}
            height={100}
            total={total}
            detail={{ 'isDetail': false }}
            isButton={false}
            titleButton=''
            type='all'
          />
        </Col>
        <Col span={8}>
          <PieChartAsset width={400} height={400} data={dataForPie && dataForPie} sizeForPieChart={150}/>
        </Col>
      </Row>
      <div style={{ marginTop: '50px' }}>
        <TableAsset
          columns={columns}
          data={dataForTable}
          // loading={loading}
        />
      </div>
    </>
  )
}

export default ListCoinIndirectAccount
