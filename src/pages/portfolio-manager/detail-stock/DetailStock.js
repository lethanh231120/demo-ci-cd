import React, { useState, useEffect } from 'react'
import StockAssetTable from '../dashboard-tab/table/StockAssetTable'
import { Row, Col, Button, Modal } from 'antd'
import PropertyOverview from '../../../components/property-overview/PropertyOverview'
import { ArrowUpOutlined } from '@ant-design/icons'
import './style.scss'
import { useNavigate } from 'react-router-dom'
import { useQuery } from '@tanstack/react-query'
import { get } from '../../../api/stockService'
import AddStockModal from '../../../components/stock/AddStockModal'
import PieChartAsset from '../../../components/chart/PieChart'

const DetailStock = () => {
  const [dataChangeByPeriod, setDataChangeByPeriod] = useState({ 'period': '30' })
  // const { state } = useLocation()
  // const { percentInfo } = state
  const [chartAllStock, setChartAllStock] = useState()
  const [isModalAddStock, setIsModalAddStock] = useState(false)
  const [dataForDetailStockTable, setDataForDetailStockTable] = useState([])
  const [dataForPie, setDataForPie] = useState([])
  const navigate = useNavigate()

  // get chart by period
  useQuery(
    ['chartByPeriod', dataChangeByPeriod],
    async() => {
      const chart = await get(`stocks/assets/history?limit=${dataChangeByPeriod?.period}`)
      if (chart?.data?.totalAssets !== null) {
        const newChart = [...chart.data.totalAssets]
        chart?.data?.totalAssets?.map((item, index) => {
          newChart[index] = {
            accountId: item?.accountId,
            id: item?.id,
            totalValue: item?.total,
            timestamp: item?.createdDate
          }
        })
        setChartAllStock(newChart.reverse())
      } else {
        setChartAllStock([])
      }
    }
  )

  // get data for table and pie chart
  const { data: listAssets } = useQuery(
    ['listAssets'], async() => {
      const res = await get(`stocks/assets/amount`)
      return res?.data.holdings
    }
  )

  const listSymbolPriceInfo = listAssets?.map(async(symbol) => {
    const data = await get(`stocks/prices/eod?limit=30&exchange=${symbol.exchange}&symbol=${symbol.symbol}`)
    return data.data
  })

  useEffect(() => {
    Promise?.all(listSymbolPriceInfo).then(res => {
      const listDataChart = []
      const dataTable = []
      const dataForPieArr = []
      res?.forEach((item) => {
        const dataChart = []
        item?.forEach((itemInChart) => {
          dataChart.push({ 'price': itemInChart?.adjpriceclose, 'eoddate': itemInChart?.eoddate })
        })
        const newItem = {
          ...item[0],
          'dataChart': dataChart
        }
        listDataChart.push(newItem)
      })
      for (let i = 0; i < listAssets?.length; i++) {
        dataTable.push({
          ...listAssets[i],
          ...(listDataChart.find((itmInner) => itmInner.symbol === listAssets[i].symbol)) }
        )
      }
      for (let i = 0; i < dataTable?.length; i++) {
        dataForPieArr.push({
          name: dataTable[i].symbol,
          totalValue: parseFloat((parseInt(dataTable[i].amount) * dataTable[i].adjpriceclose).toFixed(2))
        })
      }
      setDataForDetailStockTable(dataTable)
      setDataForPie(dataForPieArr)
    })
  }, [listAssets])

  console.log(dataForPie)
  const handleBack = () => {
    navigate(-1)
  }

  const handleAddStock = () => {
    setIsModalAddStock(true)
  }

  const dataForProfit = []
  let total = 0
  const holder = dataForDetailStockTable.reduce((prev, { exchange, amount, adjpriceclose }) => {
    prev[exchange] = prev[exchange] ? prev[exchange] + parseFloat((parseInt(amount) * adjpriceclose).toFixed(2)) : parseInt(amount) * adjpriceclose
    return prev
  }, {})
  for (const i in holder) {
    dataForProfit.push({ exchange: i, value: holder[i] })
    total += holder[i]
  }

  return (
    <>
      <div className='detail-stock'>
        <Button onClick={handleBack}>Back</Button>
        <Button onClick={handleAddStock}>Add New Stock</Button>
      </div>
      <Row gutter={60} style={{ marginTop: '50px' }} justify='space-around' align='middle'>
        <Col span={12}>
          <PropertyOverview
            title='Stock'
            dataChart={chartAllStock}
            width={500}
            height={100}
            total={total}
            detail={{ 'isDetail': false }}
            isButton={false}
            titleButton=''
            listPeriod={[
              { 'title': '1M', 'period': '30' },
              { 'title': '3M', 'period': '90' },
              { 'title': '6M', 'period': '180' },
              { 'title': '1Y', 'period': '365' }
            ]}
            // data={dataForDetailStockTable}
            type='stock'
            // percentInfo={percentInfo}
            setDataChangeByPeriod={setDataChangeByPeriod}
          />
        </Col>
        <Col span={12}>
          <Row gutter={12}>
            <Col span={14}>
              <PieChartAsset data={dataForPie && dataForPie} sizeForPieChart={150} />
            </Col>
            <Col span={8}>
              {dataForProfit?.map((item, id) => (
                <div key={id} className='stock-profit'>
                  <ArrowUpOutlined className='stock-profit-icon'/>
                  <div className='stock-profit-title'>{item?.exchange}</div>
                  <div className='stock-profit-percent'>{(item?.value * 100 / total).toFixed(2)}%</div>
                </div>
              ))}
            </Col>
          </Row>
        </Col>
      </Row>
      <StockAssetTable
        isButtonDetail={false}
        dataForDetailStockTable={dataForDetailStockTable}
        setDataForPie={setDataForPie}
      />
      <Modal
        className='update-stock-modal'
        visible={isModalAddStock}
        onOk = {() => setIsModalAddStock(false)}
        onCancel = {() => setIsModalAddStock(false)}
        footer={null}
      >
        <AddStockModal setIsModalAddStock={setIsModalAddStock}/>
      </Modal>
    </>
  )
}

export default DetailStock
