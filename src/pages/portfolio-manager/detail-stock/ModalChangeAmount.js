import React, { useState } from 'react'
import { Form, Select, Input, Button, Typography, Image } from 'antd'
import { patch } from '../../../api/stockService'
const { Text } = Typography
const { Option } = Select
import { useQueryClient } from '@tanstack/react-query'

const ModalChangeAmount = ({ itemChange, setOpenModalChange }) => {
  const [loading, setLoading] = useState(false)
  const [form] = Form.useForm()
  const queryClient = useQueryClient()

  const onFinish = async(values) => {
    setLoading(true)
    try {
      await patch(`stocks/assets/change-amount`, { holdings: [values] })
      queryClient.invalidateQueries('listAssets')
      setOpenModalChange(false)
      setLoading(false)
      form.resetFields()
    } catch (error) {
      console.log(error)
      setLoading(false)
    }
  }

  return (
    <div className='change-amount'>
      <Typography className='platform-title'>
        <Text className='platform-text'>
            Stock Platform
        </Text>
        <Image
          width={30}
          preview={false}
          src='/coins/stock.png'
        />
        <Text style={{ color: 'black', marginLeft: '10px' }}>Update your stock amount</Text>
      </Typography>
      <Form autoComplete='off' layout='vertical' form={form} onFinish={onFinish}>
        <Form.Item
          label='Type'
          name='type'
          rules={[
            {
              required: true,
              message: 'Please choose type!'
            }
          ]}
        >
          <Select>
            <Option value={1}>Add</Option>
            <Option value={2}>Substract</Option>
          </Select>
        </Form.Item>
        <Form.Item
          label='Amount'
          name='amount'
          rules={[
            {
              required: true,
              message: 'Please enter the number of amount!'
            },
            {
              pattern: new RegExp(/^[0-9]*$/),
              message: 'Amount must be integer and bigger than 0!'
            }
          ]}
        >
          <Input placeholder='Amount' type='number'/>
        </Form.Item>
        <Form.Item
          label='Exchanges'
          name='exchange'
          initialValue={`${itemChange?.exchange}`}
          rules={[
            {
              required: true,
              message: 'Please choose exchange!'
            }
          ]}
        >
          <Input disabled/>
        </Form.Item>
        <Form.Item
          label='Symbol'
          name='symbol'
          initialValue={`${itemChange?.symbol}`}
          rules={[
            {
              required: true,
              message: 'Please choose symbol!'
            }
          ]}
        >
          <Input disabled/>
        </Form.Item>
        <Form.Item shouldUpdate >
          {() => (
            <Button
              loading={loading}
              type='primary'
              htmlType='submit'
            >
                Update
            </Button>
          )}
        </Form.Item>
      </Form>
    </div>
  )
}

export default ModalChangeAmount
