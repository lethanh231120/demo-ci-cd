import React, { useState } from 'react'
import { Tabs } from 'antd'
import AllAsset from './dashboard-tab/AllAsset'
import ListNFT from './dashboard-tab/ListNft'
import _ from 'lodash'

const { TabPane } = Tabs

const Dashboard = ({ connections, stocks }) => {
  const [infoNft, setInfoNft] = useState()
  return (
    <div style={{ overflowY: 'hidden', padding: '50px 10%' }}>
      <Tabs defaultActiveKey='1'>
        <TabPane tab='All asset' key='1'>
          <AllAsset
            stocks={stocks}
            connections={connections}
            setInfoNft={setInfoNft}
          />
        </TabPane>
        {!_.isEmpty(infoNft) && (
          <TabPane tab='NFT' key='2'>
            <ListNFT infoNft={infoNft} />
          </TabPane>
        )}
      </Tabs>
    </div>
  )
}

export default Dashboard
