import React from 'react'
import WalletHandle from '../../components/WalletHandler/WalletHandle'

const ConnectMetaMask = () => {
  return (
    <div>
      <WalletHandle />
    </div>
  )
}

export default ConnectMetaMask
