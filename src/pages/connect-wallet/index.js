import React, { useEffect, useState, useContext } from 'react'
import ModalLoadingConnect from '../../components/modal/modal-loading-connect'
import ModalSuccessConnect from '../../components/modal/modal-success-connect'
import { wallets } from '../../utils/connects/OnchainWallet'
import { exchanges } from '../../utils/connects/Exchange'
import { useParams, useLocation } from 'react-router-dom'
import { PlatFormContext } from '../../layouts'
import { connectStatus } from '../../constants/statusCode'
const ConnectWallet = () => {
  const state = useContext(PlatFormContext)
  const { platformId } = useParams()
  const data = useLocation()
  const [isModalLoading, setIsModalLoading] = useState(false)
  const [isModalSuccess, setIsModalSuccess] = useState(false)
  const [message, setMessage] = useState('')

  useEffect(() => {
    setIsModalLoading(state.propsImport.isLoading)
    setIsModalSuccess(state.propsImport.isSuccess)
    connectStatus.map((item) => {
      if (item.code === state.propsImport.code) {
        if (state.propsImport.code === 'B.CON.201.C1') {
          setMessage({
            message: item.message
          })
        }
      }
      if (item.code === state.propsImport.code) {
        if (state.propsImport.code !== 'B.CON.201.C1') {
          setMessage({
            error: item.message
          })
        }
      }
    })
  }, [state])

  const render = () => {
    let formImport
    if (data?.state?.type === 'onchain') {
      formImport = wallets.find((item) => {
        if (platformId === item.id) {
          return (item)
        }
      })
    }
    if (data?.state?.type === 'offchain') {
      formImport = exchanges.find((item) => {
        if (platformId === item.id) {
          return (item)
        }
      })
    }
    return formImport
  }

  return (
    <div>
      {render().platform}
      <ModalLoadingConnect
        isModalLoading={isModalLoading}
        message={message?.error}
        setMessage={setMessage}
      />
      <ModalSuccessConnect
        isModalSuccess={isModalSuccess}
        message={message?.message}
      />
    </div>
  )
}
export default ConnectWallet
