import React, { useState, useEffect } from 'react'
import Analyst from '../../components/stock/Analyst'
import { get } from '../../api/stockService'
import { useQuery } from '@tanstack/react-query'
import { getCookie, STORAGEKEY } from '../../utils/storage'
import { useNavigate } from 'react-router-dom'
import ModalSignIn from '../../components/modal/signin/ModalSignIn'
import ModalNoti from '../../components/stock/ModalNoti'
import _ from 'lodash'
import { totalPercent } from '../../utils/parseFloat'

export const Stock = () => {
  const [isModalSignin, setIsModalSignin] = useState(false)
  const [stockConnection, setStockConnection] = useState([])
  const [openModalNoti, setOpanModalNoti] = useState(false)
  const [chartAllStock, setChartAllStock] = useState([])
  const [dataStock, setDataStock] = useState()
  const [loading, setLoading] = useState(false)
  const [totalValue, setTotalValue] = useState()
  const [percentInfo, setPercentInfo] = useState()
  const [infoChart, setInfoChart] = useState('30')

  const navigate = useNavigate()
  const token = getCookie(STORAGEKEY.ACCESS_TOKEN)

  const handleCloseOpenNoti = () => {
    setOpanModalNoti(false)
    navigate('../../')
  }

  const handleSignIn = () => {
    setOpanModalNoti(false)
    setIsModalSignin(true)
  }

  const handleCancel = () => {
    setIsModalSignin(false)
  }

  // get all stock connections
  useQuery(
    ['stockConnections'],
    async() => {
      const stockConnections = await get('stocks/assets/amount')
      if (stockConnections?.data?.holdings !== null) {
        setStockConnection(stockConnections?.data?.holdings)
      } else {
        setStockConnection([])
      }
    }
  )

  // get chart by period
  useQuery(
    ['chartByPeriod', infoChart],
    async() => {
      const chart = await get(`stocks/assets/history?limit=${infoChart}`)
      if (chart?.data?.totalAssets !== null) {
        const newChart = [...chart.data.totalAssets]
        chart?.data?.totalAssets?.map((item, index) => {
          newChart[index] = {
            accountId: item?.accountId,
            id: item?.id,
            totalValue: item?.total,
            timestamp: item?.createdDate
          }
        })
        setChartAllStock(newChart)
      } else {
        setChartAllStock([])
      }
    }
  )

  useEffect(() => {
    !token && setOpanModalNoti(true)
  }, [token])

  useEffect(() => {
    const getData = async() => {
      if (!_.isEmpty(stockConnection)) {
        setLoading(true)
        const listHoldingStock = []
        await Promise.all(stockConnection?.map(async(item) => {
          const listDataChart = []
          // call api lay thong tin stock theo exchang va symbol
          const data = await get(`stocks/prices/eod?exchange=${item?.exchange}&symbol=${item?.symbol}`)
          // map list stock va tao moi item bang cach gop thong tin cua item va itemChart
          data?.data?.forEach((itemChart) => {
            const newItem = {
              ...itemChart,
              'amount': item?.amount,
              'connectionId': item?.id
            }
            listDataChart.push(newItem)
          })
          listHoldingStock.push(listDataChart)
        }))
        if (!_.isEmpty(listHoldingStock)) {
          const listDataChart = []
          // map danh sach data cu tung ma stock va gop gia lai va luu vao mang dataChart
          listHoldingStock?.forEach((item) => {
            const dataChart = []
            item?.forEach((itemInChart) => {
              dataChart.push({ 'price': itemInChart?.adjpriceclose, 'eoddate': itemInChart?.eoddate })
            })
            const newItem = {
              ...item[0],
              'dataChart': dataChart
            }
            listDataChart.push(newItem)
          })
          console.log('listDataChart', listDataChart)
          if (!_.isEmpty(listDataChart)) {
            setDataStock(listDataChart)
            setLoading(false)
          }
        }
      } else {
        setDataStock()
        setLoading(false)
        setPercentInfo()
      }
    }
    getData()
  }, [stockConnection])

  useEffect(() => {
    if (dataStock) {
      let totalValue = 0
      dataStock?.forEach((item) => {
        console.log(item)
        totalValue += item?.adjpriceclose * parseFloat(item?.amount)
      })
      if (totalValue) {
        setTotalValue(totalValue)
      }
      // let maxLength = 0
      // let itemMaxLength
      // dataStock?.map((item) => {
      //   // check neu address co dataChart
      //   if (item?.dataChart) {
      //     if (item?.dataChart?.length > maxLength) {
      //       // gan maxLength cho item co dataChart nhieu ban ghi nhat
      //       maxLength = item?.dataChart?.length
      //       // gan itemMaxLength cho dataChart co nhieu ban ghi nhat
      //       itemMaxLength = item?.dataChart
      //     }
      //   }
      // })
      // // khoi tao danh sach chart cua all asset
      // const listChart = []
      // // lap cho i chay tu 0 den maxLength - 1
      // // khoi tao total
      // let total = 0
      // let eoddate
      // for (let i = 0; i < maxLength; ++i) {
      //   dataStock?.forEach((itemChart) => {
      //     // tinh total
      //     // tong tat ca cac ban ghi co cung index trong danh sach dataChart cua cac dia chi
      //     // vi tai cac vi tri co index cung nhau o cac mang, thi timestamp deu giong nhau
      //     total += parseFloat(((itemChart?.dataChart !== null) && (itemChart?.dataChart[i] !== undefined)) ? itemChart?.dataChart[i]?.price : 0)
      //     // check neu ton tai timestamp thi lay timestamp
      //     // neu khong ton tai thi lay timestamp cua ban ghi thu i trong itemMaxLength
      //     eoddate = ((itemChart?.dataChart !== null) && (itemChart?.dataChart[i] !== undefined)) ? itemChart?.dataChart[i]?.eoddate : itemMaxLength[i]?.eoddate
      //   })
      //   listChart.push({ 'totalValue': total, 'timestamp': eoddate })
      // }
      // setTotalValue(listChart)
    }
  }, [dataStock])

  useEffect(() => {
    if (chartAllStock) {
      const balanceFluctuations = chartAllStock && (chartAllStock[0]?.totalValue - chartAllStock[chartAllStock.length - 1]?.totalValue)
      const percent = balanceFluctuations ? totalPercent(balanceFluctuations, (chartAllStock && chartAllStock[0]?.totalValue)).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,') : 0
      setPercentInfo({
        balanceFluctuations: balanceFluctuations,
        percent: percent
      })
    }
  }, [chartAllStock])

  const handleChange = (value) => {
    setInfoChart(value)
  }

  return (
    <>
      { token ? (
        <div style={{ overflowY: 'hidden' }}>
          <Analyst
            dataStock={dataStock && dataStock}
            loading={loading}
            percentInfo={percentInfo}
            chartAllStock={chartAllStock}
            totalValue={totalValue}
            handleChange={handleChange}
          />
        </div>
      ) : (
        <ModalNoti handleSignIn={handleSignIn} handleCloseOpenNoti={handleCloseOpenNoti} openModalNoti={openModalNoti}/>
      )}
      <ModalSignIn isModalSignin={isModalSignin} handleCancel={handleCancel}/>
    </>
  )
}
