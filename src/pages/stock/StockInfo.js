// import { useQuery } from '@tanstack/react-query'
import { Table } from 'antd'
import React, { useState } from 'react'
import { get } from '../../api/stockService'
import Chart from '../../components/chart/LineChart'

const StockInfo = () => {
  const [data, setData] = useState(undefined)
  // const [selectedRowKeys, setSelectedRowKeys] = useState([
  //   'rank',
  //   'symbol',
  //   'exchange',
  //   'volume',
  //   'priceopen',
  //   'priceclose',
  //   'pricehigh',
  //   'pricelow',
  //   'adjpriceclose',
  //   'chart'
  // ])

  // const columnsPopover = [
  //   {
  //     title: 'Title',
  //     dataIndex: 'title',
  //     width: '85%',
  //     render: (_, record) => (<span style={{ color: '#A8ADB3' }}>{record.title}</span>)
  //   }
  // ]

  // const items = [
  //   {
  //     key: 'rank',
  //     title: '#'
  //   },
  //   {
  //     key: 'symbol',
  //     title: 'Symbol'
  //   },
  //   {
  //     key: 'exchange',
  //     title: 'Exchange'
  //   },
  //   {
  //     key: 'volume',
  //     title: 'Volume'
  //   },
  //   {
  //     key: 'priceopen',
  //     title: 'Price Open'
  //   },
  //   {
  //     key: 'priceclose',
  //     title: 'Price Close'
  //   },
  //   {
  //     key: 'pricehigh',
  //     title: 'Price High'
  //   },
  //   {
  //     key: 'pricelow',
  //     title: 'Price Low'
  //   },
  //   {
  //     key: 'adjpriceclose',
  //     title: 'Price'
  //   },
  //   {
  //     key: 'chart',
  //     title: 'Chart (30 day)'
  //   }
  // ]

  // const onSelectChange = (newselectedRowKeys) => {
  //   setSelectedRowKeys(newselectedRowKeys)
  // }

  // const rowSelection = {
  //   selectedRowKeys,
  //   onChange: onSelectChange
  // }

  const columns = [
    {
      key: 'rank',
      title: '#',
      // hidden: !selectedRowKeys.includes('rank'),
      // width: '20px',
      render: (_, record, index) => (<span style={{ color: '#A8ADB3' }}>{index + 1}</span>)
    },
    {
      key: 'symbol',
      title: 'Symbol',
      // width: '100px',
      // hidden: !selectedRowKeys.includes('symbol'),
      sorter: (a, b) => a.name.toLowerCase().localeCompare(b.name.toLowerCase()),
      render: (_, record) => (
        <div className='table-name'>
          <span className='table-symbol'>{record?.symbol}</span>
        </div>
      )
    },
    {
      key: 'exchange',
      title: 'Exchange',
      // width: '150px',
      // hidden: !selectedRowKeys.includes('exchange'),
      sorter: (a, b) => a.exchange.toLowerCase().localeCompare(b.exchange.toLowerCase()),
      render: (_, record) => (
        <div className='table-name'>
          <span className='table-symbol'>{record?.exchange}</span>
        </div>
      )
    },
    {
      key: 'adjpriceclose',
      title: 'Curren Price',
      // width: '150px',
      // hidden: !selectedRowKeys.includes('adjpriceclose'),
      sorter: (a, b) => parseFloat(a.adjpriceclose) - parseFloat(b.adjpriceclose),
      render: (_, record) => (
        <span>{record?.adjpriceclose ? record?.adjpriceclose : 0}</span>
      )
    },
    {
      key: 'volume',
      title: 'Volume',
      // width: '150px',
      // hidden: !selectedRowKeys.includes('volume'),
      sorter: (a, b) => parseFloat(a.volume) - parseFloat(b.volume),
      render: (_, record) => (
        <span>{record.volume}</span>
      )
    },
    {
      key: 'priceopen',
      title: 'Price Open',
      // width: '150px',
      // hidden: !selectedRowKeys.includes('priceopen'),
      sorter: (a, b) => parseFloat(a.priceopen) - parseFloat(b.priceopen),
      render: (_, record) => (
        <span>{record.priceopen}</span>
      )
    },
    {
      key: 'priceclose',
      title: 'Price Close',
      // width: '150px',
      // hidden: !selectedRowKeys.includes('priceclose'),
      sorter: (a, b) => parseFloat(a.priceclose) - parseFloat(b.priceclose),
      render: (_, record) => (
        <span>{record.priceclose}</span>
      )
    },
    {
      key: 'pricehigh',
      title: 'Price High',
      // width: '150px',
      // hidden: !selectedRowKeys.includes('pricehigh'),
      sorter: (a, b) => parseFloat(a.pricehigh) - parseFloat(b.pricehigh),
      render: (_, record) => (
        <span>{record.pricehigh}</span>
      )
    },
    {
      key: 'pricelow',
      title: 'Price Low',
      // width: '150px',
      // hidden: !selectedRowKeys.includes('pricelow'),
      sorter: (a, b) => parseFloat(a.pricelow) - parseFloat(b.pricelow),
      render: (_, record) => (
        <span>{record.pricelow}</span>
      )
    },
    {
      key: 'chart',
      title: 'Chart (30 day)',
      // width: '150px',
      // hidden: !selectedRowKeys.includes('chart'),
      render: (_, record) => (<Chart record={record?.dataChart}/>)
    }
    // {
    //   key: 'more',
    //   title: <Popover
    //     placement='bottomRight'
    //     content={(<Table
    //       showHeader={false}
    //       scroll={{
    //         y: 260
    //       }}
    //       style={{ maxWidth: '290px' }}
    //       className='tableabc'
    //       rowSelection={rowSelection}
    //       pagination={false}
    //       columns={columnsPopover}
    //       dataSource={items}
    //     >
    //     </Table>)}
    //     trigger='click'
    //   >
    //     +
    //   </Popover>,
    //   className: 'table-plus',
    //   width: '20px',
    //   dataIndex: 'key',
    //   fixed: 'right',
    //   render: (_, record) => (<div style={{ display: 'flex', justifyContent: 'space-between' }}>
    //   </div>)
    // }
  ].filter(item => !item.hidden)
  // const listSymbols = []
  // const listAllSymbolPriceInfo = []
  const listActionStock = [
    { symbol: 'AAA', exchange: 'NYSE ARCA' },
    { symbol: 'AA', exchange: 'NYSE' },
    { symbol: 'A', exchange: 'NYSE' }
  ]
  // const { data: listExchange } = useQuery(
  //   ['listExchange'],
  //   async() => {
  //     const data = await get('stocks/exchanges')
  //     return data?.data
  //   }
  // )

  // get data by list exchange

  // listExchange?.map(ex => {
  //   const { data: listSymbolPerExchange } = useQuery(
  //     ['listSymbolPerExchange', ex],
  //     async() => {
  //       const data = await get(`stocks/symbols?exchange=${ex.name}`)
  //       return data.data
  //     }
  //   )
  //   listSymbols.push(...listSymbolPerExchange)
  // })

  // get data by exchange NYSE
  // const { data: listSymbolPerExchange } = useQuery(
  //   ['listSymbolPerExchange'],
  //   async() => {
  //     const data = await get(`stocks/symbols?exchange=NYSE`)
  //     return data.data
  //   }
  // )
  // console.log('LIST SYMBOL', listSymbolPerExchange)

  // get data price info for all stock in ex: NYSE

  const listSymbolPriceInfo = listActionStock?.map(async(symbol) => {
    const data = await get(`stocks/prices/eod?limit=30&exchange=${symbol.exchange}&symbol=${symbol.symbol}`)
    return data.data
  })

  Promise.all(listSymbolPriceInfo).then(res => {
    const listDataChart = []
    res?.forEach((item) => {
      const dataChart = []
      item?.forEach((itemInChart) => {
        dataChart.push({ 'price': itemInChart?.adjpriceclose, 'eoddate': itemInChart?.eoddate })
      })
      const newItem = {
        ...item[0],
        'dataChart': dataChart
      }
      listDataChart.push(newItem)
    })
    setData(listDataChart)
  })

  return (
    <div className='stock__information--container'>
      <Table
        dataSource={data && data}
        columns={columns}
        scroll={{ x: 'max-content' }}
        rowKey={(record) => record?.id}
        pagination={false}
      />
    </div>
  )
}

export default StockInfo

