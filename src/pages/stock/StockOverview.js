import { useQuery } from '@tanstack/react-query'
import React, { useState, useEffect } from 'react'
import { get } from '../../api/stockService'
import Chart from '../../components/chart/LineChart'
import { Table } from 'antd'
import PieChartAsset from '../../components/chart/PieChart'
import './styles.scss'

const StockOverview = () => {
  const [data, setData] = useState([])
  const [dataForPie, setDataForPie] = useState([])
  const sizeForPieChart = 150
  const columns = [
    {
      key: 'rank',
      title: '#',
      width: '20px',
      render: (_, record, index) => (<span style={{ color: '#A8ADB3' }}>{index + 1}</span>)
    },
    {
      key: 'symbol',
      title: 'Symbol',
      width: '100px',
      sorter: (a, b) => a.symbol.toLowerCase().localeCompare(b.symbol.toLowerCase()),
      render: (_, record) => (
        <div className='table-name'>
          <span className='table-symbol'>{record?.symbol}</span>
        </div>
      )
    },
    {
      key: 'exchange',
      title: 'Exchange',
      width: '150px',
      sorter: (a, b) => a.exchange.toLowerCase().localeCompare(b.exchange.toLowerCase()),
      render: (_, record) => (
        <div className='table-name'>
          <span className='table-symbol'>{record?.exchange}</span>
        </div>
      )
    },
    {
      key: 'amount',
      title: 'Amount',
      width: '150px',
      sorter: (a, b) => parseInt(a.amount) - parseInt(b.amount),
      render: (_, record) => (
        <span>{record?.amount ? record?.amount : 0}</span>
      )
    },
    {
      key: 'adjpriceclose',
      title: 'Curren Price',
      width: '150px',
      sorter: (a, b) => parseFloat(a.adjpriceclose) - parseFloat(b.adjpriceclose),
      render: (_, record) => (
        <span>{record?.adjpriceclose ? record?.adjpriceclose : 0}</span>
      )
    },
    {
      key: 'total',
      title: 'Total',
      width: '150px',
      sorter: (a, b) => parseFloat(a.adjpriceclose * a.amount) - parseFloat(b.adjpriceclose * b.amount),
      render: (_, record) => (
        <span>{record?.adjpriceclose && record?.amount ? record?.adjpriceclose * record?.amount : 0}</span>
      )
    },
    {
      key: 'volume',
      title: 'Volume',
      width: '150px',
      sorter: (a, b) => parseFloat(a.volume) - parseFloat(b.volume),
      render: (_, record) => (
        <span>{record.volume}</span>
      )
    },
    {
      key: 'chart',
      title: 'Chart (30 day)',
      width: '150px',
      render: (_, record) => (<Chart record={record?.dataChart}/>)
    }
  ].filter(item => !item.hidden)

  const { data: listAssets } = useQuery(
    ['listAssets'],
    async() => {
      const data = await get(`stocks/assets/amount`)
      return data?.data?.holdings
    }
  )

  const listSymbolPriceInfo = listAssets?.map(async(symbol) => {
    const data = await get(`stocks/prices/eod?limit=30&exchange=${symbol.exchange}&symbol=${symbol.symbol}`)
    return data.data
  })

  useEffect(() => {
    Promise?.all(listSymbolPriceInfo).then(res => {
      const listDataChart = []
      const dataTable = []
      const dataForPieArr = []
      res?.forEach((item) => {
        const dataChart = []
        item?.forEach((itemInChart) => {
          dataChart.push({ 'price': itemInChart?.adjpriceclose, 'eoddate': itemInChart?.eoddate })
        })
        const newItem = {
          ...item[0],
          'dataChart': dataChart
        }
        listDataChart.push(newItem)
      })
      for (let i = 0; i < listAssets?.length; i++) {
        dataTable.push({
          ...listAssets[i],
          ...(listDataChart.find((itmInner) => itmInner.symbol === listAssets[i].symbol)) }
        )
      }
      for (let i = 0; i < dataTable?.length; i++) {
        dataForPieArr.push({
          name: dataTable[i].symbol,
          totalValue: parseFloat((parseInt(dataTable[i].amount) * dataTable[i].adjpriceclose).toFixed(2))
        })
      }
      setData(dataTable)
      setDataForPie(dataForPieArr)
    })
  }, [listAssets])

  return (
    <div className='stock__overview--container'>
      <h1>Stock overview</h1>
      <PieChartAsset data={dataForPie && dataForPie} sizeForPieChart={sizeForPieChart}/>
      <Table
        dataSource={data && data}
        columns={columns}
        scroll={{ x: 'max-content' }}
        rowKey={(record) => record?.id}
        pagination={false}
      />
    </div>
  )
}

export default StockOverview
