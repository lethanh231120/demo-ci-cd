import React, { useEffect, useState } from 'react'
import Connections from './Connections'
import DashBoard from './DashBoard'
// import { Modal, Result, Button } from 'antd'
import { useQuery } from '@tanstack/react-query'
import { getConnection } from '../../api/connectService'
import { getCookie, STORAGEKEY } from '../../utils/storage'
import { useNavigate } from 'react-router-dom'
import ModalSignIn from '../../components/modal/signin/ModalSignIn'
import ModalNoti from './ModalNoti'
const index = () => {
  const [openModalNoti, setOpanModalNoti] = useState(false)
  const [isModalSignin, setIsModalSignin] = useState(false)
  const navigate = useNavigate()
  const token = getCookie(STORAGEKEY.ACCESS_TOKEN)

  const { data: connections } = useQuery(
    ['connections'],
    async() => {
      const res = await getConnection('connect/current-connections')
      return res
    }
  )

  const handleCloseOpenNoti = () => {
    setOpanModalNoti(false)
    navigate('../../')
  }

  useEffect(() => {
    setOpanModalNoti(!token)
  }, [token])

  const handleCancel = () => {
    setIsModalSignin(false)
    navigate('../../')
  }

  const handleSignIn = () => {
    setIsModalSignin(true)
    setOpanModalNoti(false)
  }

  return (
    <>
      {token ? (
        <div>
          {connections?.data === null
            ? (<DashBoard/>)
            : (<Connections connections={connections?.data}/>)}
        </div>
      ) : (
        <ModalNoti handleSignIn={handleSignIn} handleCloseOpenNoti={handleCloseOpenNoti} openModalNoti={openModalNoti}/>
      )}
      <ModalSignIn isModalSignin={isModalSignin} handleCancel={handleCancel}/>
    </>
  )
}

export default index
