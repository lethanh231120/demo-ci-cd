import React, { useState, useEffect, useContext } from 'react'
import { Row, Col } from 'antd'
import WalletAddress from '../../components/portfolio/WalletAddress'
import Analyst from '../../components/portfolio/analyst/Analyst'
import {
  BITCOIN_CHAINID,
  ETHEREUM_CHAINID,
  BINANCE_SMART_CHAIN_CHAINID,
  SOLANA_CHAINID,
  RIPPLE_CHAINID
} from '../../constants/ChainId'
import _ from 'lodash'
import { postBtc } from '../../api/bitcoinService'
import { postXrp } from '../../api/xrpService'
import { postEvm } from '../../api/evmService'
import { postNft } from '../../api/nftService'
import { postSolana } from '../../api/solanaService'
import { post } from '../../api/coinPriceService'
import { totalPercent } from '../../utils/parseFloat'
import { useQuery } from '@tanstack/react-query'
import { FIVE_MINUTE, ONE_HOUR, ONE_DAY } from '../../constants/params'
import { PlatFormContext } from '../../layouts'
import { bigNumber, smallNumber } from '../../utils/number/formatNum'

const Connections = ({ connections }) => {
  const [connectionName, setConnectionName] = useState('all')
  const [isOwnPersentage, setIsOwnPersentage] = useState({
    isOwnPersentage: false,
    ownPersentage: 100
  })
  const [dataConnection, setDataConnection] = useState()
  const [isGroupNFT, setIsGroupNFT] = useState(false)
  const [listCoinInfo, setListCoinInfo] = useState()
  const [infoChart, setInfoChart] = useState({ period: '1h' })
  const [dataChart, setDataChart] = useState([])
  const [isLoading, setIsLoading] = useState(true)
  const [percentInfo, setPercentInfo] = useState()
  const [listChartByAddress, setListChartByAddress] = useState([])
  const [chartAllAssetByPeriod, setChartAllAssetByPeriod] = useState()
  const state = useContext(PlatFormContext)

  useEffect(() => {
    const nft = []
    const groupCoin = []
    // group address by chainId
    const groupBychainId = connections && connections.reduce((group, connection) => {
      const { chainId } = connection
      group[chainId] = group[chainId] ?? []
      group[chainId].push(connection.address)
      return group
    }, {})
    // get addresses by chainId eth or sol and push nft
    groupBychainId && Object.keys(groupBychainId).forEach(function(key) {
      if (key === ETHEREUM_CHAINID || key === SOLANA_CHAINID || key === BINANCE_SMART_CHAIN_CHAINID) {
        nft.push(...groupBychainId[key])
      }
      // custom lai du lieu
      groupCoin.push({
        'chainId': key,
        'address': groupBychainId[key]
      })
    })
    // neu la trang thai khoi tao, bat dau vao trang
    if (state?.propsImport?.initial) {
      // kiểm tra nếu mảng groupCoin khác rỗng
      if (!_.isEmpty(groupCoin)) {
        const getData = async() => {
          // get info chainId 1 or 56
          // merge all eth and bsc addresses into one array
          const listChain = groupCoin && groupCoin?.filter((item) => (item?.chainId === ETHEREUM_CHAINID || item?.chainId === BINANCE_SMART_CHAIN_CHAINID))
          const addressEVM = []
          listChain?.forEach((item) => {
            addressEVM.push(...item.address)
          })
          // call api các holding dùng promise.all để bắt buộc có dữ liệu mới tiếp tục thực hiện logic
          const [holdingsBtc, holdingsXrp, holdingsEvm] = await Promise.all([
            // get data holding btc chainId = 0
            postBtc('bitcoin/addresses-holdings',
              { 'addresses': groupCoin && groupCoin?.find((item) => (item.chainId === BITCOIN_CHAINID))?.address }
            ).then(res => res.data).catch(error => console.log(error)),
            postXrp('xrp/addresses-holdings',
              { 'addresses': groupCoin && groupCoin?.find((item) => (item.chainId === RIPPLE_CHAINID))?.address }
            ).then(res => res.data).catch(error => console.log(error)),
            postEvm('evm/addresses-holdings', { 'addresses': addressEVM })
              .then(res => res.data).catch(error => console.log(error))
          ])

          // kiểm tra nếu có dữ liệu của nft
          if (!_.isEmpty(nft)) {
            // call api lấy danh sách nft theo địa chỉ của connections
            const listNft = await postNft('nft/addresses-nfts', { 'addresses': nft })
            // cập nhật thông tin cho context propsImport
            state.handleSetData({
              ...state?.propsImport,
              listHolding: [
                holdingsBtc && holdingsBtc,
                holdingsXrp && holdingsXrp,
                holdingsEvm && holdingsEvm
              ],
              listNft: listNft?.data,
              initial: false
            })
          } else {
            // cập nhật lại thông tin của context propsImport
            state.handleSetData({
              ...state?.propsImport,
              listHolding: [
                holdingsBtc && holdingsBtc,
                holdingsXrp && holdingsXrp,
                holdingsEvm && holdingsEvm
              ],
              initial: false
            })
          }
        }
        getData()
      }
    } else {
      // neu co addressDelete dia chi connection muon xoa
      if (state?.propsImport?.addressDelete) {
        // kiểm tra nếu tồn tại danh sách nft trên context
        if (state?.propsImport?.listNft) {
          // tìm kiếm địa chỉ của item có address muốn xóa trong danh sách listNft ở context
          const index = state?.propsImport?.listNft.findIndex((item) => (item?.address?.address === state?.propsImport?.addressDelete))
          // xóa item có chỉ số index trong mảng listNft
          state?.propsImport?.listNft.splice(index, 1)
        }
        // kiểm tra xem có tồn tại listHolding hay không
        if (state?.propsImport?.listHolding) {
          // Tìm index của item có địa chỉ muốn xóa trong danh sách listHolding
          const index = state?.propsImport?.listHolding?.findIndex((item) => (item?.address?.address === state?.propsImport?.addressDelete))
          // xóa item có index vưa tìm được trong danh sách listHolding
          state?.propsImport?.listHolding?.splice(index, 1)
        }
      }
    }
  }, [connections])

  useEffect(() => {
    const getCoinInfo = async() => {
      const listCoinId = []
      const holding = []
      // lấy tất cả holdings
      state?.propsImport?.listHolding?.forEach((item) => {
        item && holding.push(item.holdings)
      })
      // group các holdings theo coinId
      const groupByCoinId = _.groupBy(holding?.flat(1), (item) => {
        if (item?.coinId) {
          return item?.coinId
        }
      })
      // gan danh sach cac dong coin ton tai coinId vao list de call api
      groupByCoinId && Object.keys(groupByCoinId).forEach(function(key) {
        (key !== 'undefined') && listCoinId.push(key)
      })

      // check neu list coinId co du lieu thi moi call
      if (!_.isEmpty(listCoinId)) {
        // call api list coin info theo danh sach coinid
        const listCoin = await post('price/info/list', { 'listCoinId': listCoinId })
        setListCoinInfo(listCoin?.data?.coins)
      }
    }
    getCoinInfo()
  }, [state?.propsImport?.listHolding])

  const handleChangePeriod = (value) => {
    setInfoChart({
      ...infoChart,
      period: value
    })
  }

  // cập nhật lại trạng thái hiển thị tất cả các list nft hay là hiển thị tất cả các nft trong 1 list nft
  useEffect(() => {
    setIsGroupNFT(false)
    setPercentInfo()
    setDataChart([])
    setIsOwnPersentage({
      ...isOwnPersentage,
      isOwnPersentage: false
    })
    if (connectionName !== 'all') {
      // tim connection co id la connectionName
      const connectionItem = connections?.find((item) => item.id === parseFloat(connectionName))
      setInfoChart({
        ...infoChart,
        address: connectionItem?.address,
        chainId: connectionItem?.chainId
      })
    }
  }, [connectionName])

  // Tính tổng tiền
  const totalMoney = (total, currenValue) => {
    const price = currenValue?.price ? currenValue?.price : 0
    const amount = currenValue?.amount ? currenValue?.amount * (1 / Math.pow(10, currenValue.decimals)) : 0
    const money = price * amount
    const value = total + money
    return value
  }

  // xu ly cho holding
  useEffect(() => {
    if (connectionName !== 'all') {
      // tim connection co id la connectionName
      const connectionItem = connections?.find((item) => item.id === parseInt(connectionName))

      // tim holding theo address cua connectionItem
      const holdingItem = state?.propsImport?.listHolding?.find((item) => item?.address?.address === connectionItem?.address)
      // tim nft theo address cua connection
      const nftItem = state?.propsImport?.listNft?.find((item) => item?.address?.address === connectionItem?.address)

      // tinh tong total value
      const totalValue = holdingItem?.holdings?.reduce(totalMoney, 0)
      if (listCoinInfo) {
        const newListHolding = [...holdingItem.holdings]
        // cập nhật thông tin cho từng holding 1 theo coins info và holding
        holdingItem?.holdings && holdingItem?.holdings.map((itemHolding, index) => {
          const dataItemDetail = listCoinInfo && listCoinInfo.find((item) => (itemHolding.coinId === item.coinId))
          const price = smallNumber(itemHolding?.price)
          if (dataItemDetail) {
            const amount = isOwnPersentage?.isOwnPersentage
              ? (smallNumber(parseFloat(itemHolding?.amount) * (connectionItem?.ownPersentage / 100)))
              : smallNumber(parseFloat(itemHolding?.amount))
            const total = dataItemDetail?.price * amount
            const totalChange24h = dataItemDetail?.priceChange24h * amount
            const high24h = dataItemDetail?.high24h * amount
            const low24h = dataItemDetail?.low24h * amount
            newListHolding[index] = {
              ...dataItemDetail,
              ...itemHolding,
              'total': total,
              'amount': amount,
              'price': Number(price),
              'totalChange24h': totalChange24h,
              'high24h': high24h,
              'low24h': low24h
            }
          }
          if (dataItemDetail === undefined) {
            const amount = isOwnPersentage?.isOwnPersentage
              ? (smallNumber(parseFloat(itemHolding?.amount) * (connectionItem?.ownPersentage / 100)))
              : smallNumber(parseFloat(itemHolding?.amount))
            const total = itemHolding?.price ? itemHolding?.price * amount : 0
            const newCoin = {
              ...itemHolding,
              amount: amount,
              total: total,
              price: Number(price) ? Number(price) : 0,
              totalChange24h: 0,
              ath: 0,
              athChangePercentage: 0,
              atl: 0,
              atlChangePercentage: 0,
              circulatingSupply: 0,
              coinImage: '',
              fullyDilutedValuation: 0,
              high24h: 0,
              low24h: 0,
              marketCap: 0,
              marketCapChange24h: 0,
              marketCapChangePercentage24h: 0,
              maxSupply: 0,
              priceChange24h: 0,
              priceChangePercentage24h: 0,
              totalSupply: 0,
              totalVolume: 0,
              priceChart7d: []
            }
            newListHolding[index] = newCoin
          }
        })

        newListHolding && setIsLoading(false)

        setDataConnection({
          'holdings': newListHolding,
          'nft': [nftItem?.nfts],
          'totalValue': totalValue * (isOwnPersentage?.isOwnPersentage ? isOwnPersentage?.ownPersentage / 100 : 1)
        })
      }
    } else {
      const allHolding = []
      const allNft = []
      const allCoinAffterGroup = []

      // gộp holding của từng item vào allHolding
      state?.propsImport?.listHolding?.forEach((item) => {
        item && allHolding.push(item?.holdings)
      })

      // group tất cả các holding theo coinId
      const groupByCoinId = _.groupBy(allHolding.flat(1), (item) => {
        if (item?.coinId) {
          return item?.coinId
        }
      })

      // foreach object groupByCoinId
      groupByCoinId && Object.keys(groupByCoinId).forEach(function(key) {
        // check nếu key khác undefined tức là tồn tại coinId
        if (key !== 'undefined') {
          // tinh tổng amount của đồng coin đó
          const amount = groupByCoinId[key].reduce((total, item) => {
            return total + parseFloat(item.amount)
          }, 0)
          // cập nhật lại thông tin đồng coin với amount lấy tu danh sach dong coin co coinId giong nhau
          // lấy thông tin 1 đồng coin bất ky trong list co coinId giong nhau va cap nhật amount cho dong coin
          const itemInfo = {
            ...groupByCoinId[key][0],
            amount: bigNumber(amount)
          }
          // thêm đồng coin vào danh sách allCoinAffterGroup
          allCoinAffterGroup.push(itemInfo)
        } else {
          // check nếu key la undefined tức là khong có coinid
          // group danh sách coin theo coinName
          const groupByCoinName = _.groupBy(groupByCoinId[key], (item) => {
            if (item?.coinName) {
              return item?.coinName
            }
          })
          // forEach danh sách vua được group theo coinName
          groupByCoinName && Object.keys(groupByCoinName).forEach(function(key) {
            // check neu tồn tại coinName
            if (key !== 'undefined') {
              // tinh tổng amount của tất cả các dồng coin co cùng coinName
              const amount = groupByCoinName[key].reduce((total, item) => {
                return total + parseInt(item.amount)
              }, 0)
              // Tạo thông tin của đồng coin
              // Lấy tất cả thông tin 1 đồng coin bất ky trong 1 list coin co cung coinName
              // gan lai amout la tổng số lương của list coin
              const itemInfo = {
                ...groupByCoinName[key][0],
                amount: bigNumber(amount)
              }
              // Thêm coin vao danh sách allCoinAffterGroup
              allCoinAffterGroup.push(itemInfo)
            }
          })
        }
      })

      // gộp nfts của từng item vào allNft
      state?.propsImport?.listNft?.map((item) => {
        allNft.push(item?.nfts)
      })

      if (listCoinInfo) {
        // Tính tổng total của all holding
        // const totalValue = allHolding?.flat(1)?.reduce(totalMoney, 0)
        const bcd = (total, currenValue) => {
          const value = total + parseFloat(currenValue?.address ? currenValue?.address?.totalValue : 0)
          return value
        }
        const totalValue = state?.propsImport?.listHolding?.reduce(bcd, 0)
        // lặp danh sách allCoinAffterGroup (coin đang sở hữu) và danh sách listCoinInfo (thông tin chi tiết các đồng coin)
        !_.isEmpty(allCoinAffterGroup) && allCoinAffterGroup.map((itemHolding, index) => {
          const dataItemDetail = listCoinInfo && listCoinInfo.find((item) => (itemHolding.coinId === item.coinId))
          const amount = bigNumber(parseFloat(itemHolding?.amount))
          const price = smallNumber(itemHolding?.price)
          if (dataItemDetail) {
            const total = dataItemDetail?.price * amount
            const totalChange24h = (dataItemDetail?.priceChange24h) * amount
            const high24h = dataItemDetail?.high24h * amount
            const low24h = dataItemDetail?.low24h * amount
            allCoinAffterGroup[index] = {
              ...dataItemDetail,
              ...itemHolding,
              'amount': amount,
              'total': total,
              'price': Number(price),
              'totalChange24h': totalChange24h,
              'high24h': high24h,
              'low24h': low24h
            }
          }
          if (dataItemDetail === undefined) {
            const total = itemHolding?.price ? itemHolding?.price * amount : 0
            const newCoin = {
              ...itemHolding,
              total: total,
              amount: amount,
              price: Number(price) ? Number(price) : 0,
              totalChange24h: 0,
              ath: 0,
              athChangePercentage: 0,
              atl: 0,
              atlChangePercentage: 0,
              circulatingSupply: 0,
              coinImage: '',
              fullyDilutedValuation: 0,
              high24h: 0,
              low24h: 0,
              marketCap: 0,
              marketCapChange24h: 0,
              marketCapChangePercentage24h: 0,
              maxSupply: 0,
              priceChange24h: 0,
              priceChangePercentage24h: 0,
              totalSupply: 0,
              totalVolume: 0,
              priceChart7d: []
            }
            allCoinAffterGroup[index] = newCoin
          }
        })

        !_.isEmpty(allCoinAffterGroup) && setIsLoading(false)
        !_.isEmpty(allCoinAffterGroup) && setDataConnection({
          'holdings': allCoinAffterGroup,
          'nft': allNft,
          'totalValue': totalValue
        })
      }
    }
  }, [connectionName, listCoinInfo, state?.propsImport?.listHolding, isOwnPersentage])

  // xu ly cho chart
  // get chart total by address connection
  useQuery(
    ['listChart', infoChart, connectionName, connections, isOwnPersentage],
    async() => {
      if (connectionName !== 'all') {
        // tim trong listChartAll item co period === period vua duoc select
        const periodInListChartAll = listChartByAddress.find((item) => (item.period === infoChart?.period))
        // neu ton tai periodInListChartAll trong listChartAll thi lay thong tin tu listChartAll
        // ton tai period
        if (periodInListChartAll) {
          // check trong infoChart co address dk chon hay khong
          // neu co thi thuc hien
          // neu k co thi doi khi nao co
          if (infoChart?.address) {
            const addressItem = periodInListChartAll?.dataChart?.find((item) => (item.address === infoChart?.address))
            // neu connection co dia chi vua chon nam trong listChartAll thi lay du lieu chart
            if (addressItem) {
              const newListChart = [...addressItem.dataChart]
              addressItem?.dataChart?.forEach((item, index) => {
                newListChart[index] = {
                  ...item,
                  totalValue: parseFloat(item?.totalValue) * (isOwnPersentage?.isOwnPersentage ? isOwnPersentage?.ownPersentage / 100 : 1)
                }
              })
              setDataChart(newListChart)
            }
            // neu connection co address vua chon khong nam trong listChartAll thi call api
            if (addressItem === undefined) {
              let dataChart
              switch (infoChart?.chainId) {
                case BITCOIN_CHAINID:
                  dataChart = await postBtc(`bitcoin/addresses-charts/period=${infoChart.period}`,
                    { 'addresses': [infoChart.address] }
                  )
                  break
                case ETHEREUM_CHAINID:
                  dataChart = await postEvm(`evm/addresses-charts/period=${infoChart.period}`,
                    { 'addresses': [infoChart.address] }
                  )
                  break
                case RIPPLE_CHAINID:
                  dataChart = await postXrp(`xrp/addresses-charts/period=${infoChart.period}`,
                    { 'addresses': [infoChart.address] }
                  )
                  break
                case BINANCE_SMART_CHAIN_CHAINID:
                  dataChart = await postEvm(`evm/addresses-charts/period=${infoChart.period}`,
                    { 'addresses': [infoChart.address] }
                  )
                  break
                case SOLANA_CHAINID:
                  dataChart = await postSolana(`solana/addresses-charts/period=${infoChart.period}`,
                    { 'addresses': [infoChart.address] }
                  )
                  break
                default:
                  break
              }
              if (dataChart) {
                const newListChartAll = [...listChartByAddress]
                // tim index cua item co period === period vua duoc select
                const indexInListChartAll = newListChartAll.findIndex((item) => (item.period === infoChart?.period))
                newListChartAll[indexInListChartAll] = {
                  'period': newListChartAll[indexInListChartAll]?.period,
                  'dataChart': [
                    ...listChartByAddress[indexInListChartAll].dataChart,
                    {
                      'address': infoChart?.address,
                      'dataChart': dataChart?.data[0]?.addressChart
                    }
                  ]
                }
                // cap nhat lai listChartAll
                setListChartByAddress(newListChartAll)
                setDataChart(dataChart?.data[0]?.addressChart)
              }
            }
          }
        }
        // neu khong co thi call api moi
        if (periodInListChartAll === undefined) {
          let dataChart
          switch (infoChart?.chainId) {
            case BITCOIN_CHAINID:
              dataChart = await postBtc(`bitcoin/addresses-charts/period=${infoChart.period}`,
                { 'addresses': [infoChart.address] }
              )
              break
            case ETHEREUM_CHAINID:
              dataChart = await postEvm(`evm/addresses-charts/period=${infoChart.period}`,
                { 'addresses': [infoChart.address] }
              )
              break
            case RIPPLE_CHAINID:
              dataChart = await postXrp(`xrp/addresses-charts/period=${infoChart.period}`,
                { 'addresses': [infoChart.address] }
              )
              break
            case BINANCE_SMART_CHAIN_CHAINID:
              dataChart = await postEvm(`evm/addresses-charts/period=${infoChart.period}`,
                { 'addresses': [infoChart.address] }
              )
              break
            case SOLANA_CHAINID:
              dataChart = await postSolana(`solana/addresses-charts/period=${infoChart.period}`,
                { 'addresses': [infoChart.address] }
              )
              break
            default:
              break
          }
          if (dataChart) {
            setDataChart(dataChart?.data[0]?.addressChart)
            setListChartByAddress([
              ...listChartByAddress,
              {
                'period': infoChart?.period,
                'dataChart': [{ 'address': infoChart?.address, 'dataChart': dataChart?.data[0]?.addressChart }]
              }
            ])
          }
        }
      } else {
        if (connections) {
          console.log(1111111)
          // call chart cua tat ca connections va gop lai 1 mang lon
          const data = await Promise.all(connections?.map(async(item) => {
            if (item?.chainId === BITCOIN_CHAINID) {
              const data = await postBtc(`bitcoin/addresses-charts/period=${infoChart.period}`, { 'addresses': [item?.address] })
              return { 'address': item?.address, 'dataChart': data?.data[0]?.addressChart }
            }
            if (item?.chainId === ETHEREUM_CHAINID) {
              const data = await postEvm(`evm/addresses-charts/period=${infoChart.period}`, { 'addresses': [item?.address] })
              return { 'address': item?.address, 'dataChart': data?.data[0]?.addressChart }
            }
            if (item?.chainId === RIPPLE_CHAINID) {
              const data = await postXrp(`xrp/addresses-charts/period=${infoChart.period}`, { 'addresses': [item?.address] })
              return { 'address': item?.address, 'dataChart': data?.data[0]?.addressChart }
            }
            if (item?.chainId === BINANCE_SMART_CHAIN_CHAINID) {
              const data = await postEvm(`evm/addresses-charts/period=${infoChart.period}`, { 'addresses': [item?.address] })
              return { 'address': item?.address, 'dataChart': data?.data[0]?.addressChart }
            }
            if (item?.chainId === SOLANA_CHAINID) {
              const data = await postSolana(`solana/addresses-charts/period=${infoChart.period}`, { 'addresses': [item?.address] })
              return { 'address': item?.address, 'dataChart': data?.data[0]?.addressChart }
            }
          }))
          // clone new data
          const newDataClone = [...listChartByAddress]
          console.log('newDataClone', newDataClone)
          // tim trong mang chart cua cac address da ton tai period === infoChart?.period chua
          const dataInListChartAll = newDataClone?.find((item) => (item?.period === infoChart?.period))
          console.log('dataInListChartAll', dataInListChartAll)
          // neu da ton tai item co period thi cap nhat lai thong tin
          if (dataInListChartAll) {
            // tim index cua item co period === period vua select
            const indexInListChartAll = newDataClone?.findIndex((item) => (item?.period === infoChart?.period))
            // gan gia tri cua item co index vua tim dk trong listChartAll = thong tin moi vi realtime
            newDataClone[indexInListChartAll] = {
              'period': infoChart.period,
              'dataChart': data.flat(1)
            }
            setListChartByAddress(newDataClone)
          }
          // neu chua ton tai thi them moi vao
          if (dataInListChartAll === undefined) {
            // them moi du lieu vao listChartAll
            setListChartByAddress([
              ...listChartByAddress,
              {
                'period': infoChart.period,
                'dataChart': data.flat(1)
              }
            ])
          }
        }
      }
    },
    {
      refetchInterval: infoChart?.period === '1h' ? FIVE_MINUTE : ((infoChart?.period === '1d' || infoChart?.period === '1w') ? ONE_HOUR : ONE_DAY)
    }
  )

  console.log('dataChart', dataChart)
  useEffect(() => {
    if (connectionName === 'all') {
      const chartAllAsset = []
      listChartByAddress?.map((item) => {
        let maxLength = 0
        // khai bao address co dataChart.length lon nhat de sd khi
        // address.dataChart.length < maxLength thi lay timestamp cua maxLength
        // address1.dataChart co 10 ban ghi
        // address2.dataChart co 5 ban ghi
        // address3.dataChart co 2 ban ghi
        let itemMaxLength
        // check neu address co dataChart
        if (item?.dataChart) {
          item?.dataChart?.map((charItem) => {
            // check neu dataChart.length > maxLength
            if (charItem?.dataChart?.length > maxLength) {
              // gan maxLength cho item co dataChart nhieu ban ghi nhat
              maxLength = charItem?.dataChart?.length
              // gan itemMaxLength cho dataChart co nhieu ban ghi nhat
              itemMaxLength = charItem?.dataChart
            }
          })
        }
        // khoi tao danh sach chart cua all asset
        const listChart = []
        // lap cho i chay tu 0 den maxLength - 1
        for (let i = 0; i < maxLength; ++i) {
          // khoi tao total
          let total = 0
          let timestamp
          // lap mang item.dataChart
          // tinh total
          // tong tat ca cac ban ghi co cung index trong danh sach dataChart cua cac dia chi
          // vi tai cac vi tri co index cung nhau o cac mang, thi timestamp deu giong nhau
          item?.dataChart && item?.dataChart?.forEach((chartItem) => {
            total += parseFloat(((chartItem?.dataChart !== null) && (chartItem?.dataChart[i] !== undefined)) ? chartItem?.dataChart[i]?.totalValue : 0)
            // check neu ton tai timestamp thi lay timestamp
            // neu khong ton tai thi lay timestamp cua ban ghi thu i trong itemMaxLength
            timestamp = ((chartItem?.dataChart !== null) && (chartItem?.dataChart[i] !== undefined)) ? chartItem?.dataChart[i]?.timestamp : itemMaxLength[i]?.timestamp
          })
          listChart.push({ 'totalValue': total, timestamp })
        }
        chartAllAsset.push({ 'period': item?.period, 'dataChart': listChart })
      })
      setChartAllAssetByPeriod(chartAllAsset)
    }
  }, [listChartByAddress, infoChart, connectionName])

  useEffect(() => {
    if (connectionName === 'all') {
      const chartAll = !_.isEmpty(chartAllAssetByPeriod) && chartAllAssetByPeriod?.find((item) => (item?.period === infoChart?.period))
      setDataChart(chartAll?.dataChart)
    }
  }, [chartAllAssetByPeriod, connectionName])

  useEffect(() => {
    if (connectionName === 'all') {
      const balanceFluctuations = dataChart && (dataChart[0]?.totalValue - dataChart[dataChart.length - 1]?.totalValue)
      const percent = balanceFluctuations ? totalPercent(balanceFluctuations, (dataChart && dataChart[0]?.totalValue)).toFixed(4).replace(/\d(?=(\d{3})+\.)/g, '$&,') : 0
      setPercentInfo({
        balanceFluctuations: balanceFluctuations,
        percent: percent
      })
    } else {
      let totalValue = 0
      let totalChange24 = 0
      dataConnection?.holdings?.forEach((current) => {
        totalValue += current?.total * (1 / Math.pow(10, current?.decimals))
        totalChange24 += current?.totalChange24h * (1 / Math.pow(10, current?.decimals))
      })
      if (totalValue && totalChange24) {
        const percent = totalChange24 ? totalPercent(totalChange24, totalValue * (isOwnPersentage?.ownPersentage / 100)).toFixed(4).replace(/\d(?=(\d{3})+\.)/g, '$&,') : 0
        setPercentInfo({
          balanceFluctuations: totalChange24,
          percent: percent
        })
      }
    }
  }, [dataChart, connectionName])

  return (
    <div style={{ overflowY: 'hidden' }}>
      <Row>
        <Col span={6}>
          <WalletAddress
            connectionName={connectionName}
            connections={connections && connections}
            setConnectionName={setConnectionName}
            setIsOwnPersentage={setIsOwnPersentage}
            isOwnPersentage={isOwnPersentage?.isOwnPersentage}
            setDataConnection={setDataConnection}
          />
        </Col>
        <Col span={18}>
          <Analyst
            period={infoChart?.period}
            handleChangePeriod={handleChangePeriod}
            dataChart={dataChart && [...dataChart]?.reverse()}
            percentInfo={percentInfo}
            dataConnection={dataConnection && dataConnection}
            setIsGroupNFT={setIsGroupNFT}
            isGroupNFT={isGroupNFT}
            isLoading={isLoading}
          />
        </Col>
      </Row>
    </div>
  )
}

export default Connections
