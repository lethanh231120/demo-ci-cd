import React from 'react'
import { Row, Col, Image } from 'antd'
import { SyncOutlined, AppstoreOutlined, LockOutlined, PlusOutlined } from '@ant-design/icons'
import './style.scss'
import { useNavigate } from 'react-router-dom'

const DashBoard = () => {
  const navigate = useNavigate()
  return (
    <div className='dashboard-portfolio'>
      <div className='dashboard-portfolio-connect'>
        <div className='dashboard-portfolio-connect-header'>
          <div className='dashboard-portfolio-connect-title'>Connect Exchange / Wallet</div>
          <div className='dashboard-portfolio-connect-content'>
            From Wallets Like Your Ethereum Wallet Or Exchanges Like Binance. Track All Your Crypto In One Place - On One Screen.
          </div>
        </div>
        <div className='dashboard-portfolio-connect-list'>
          <Row gutter={12}>
            <Col xl={{ span: 6 }} lg={{ span: 8 }} md={{ span: 12 }} sm={{ span: 12 }} xs={{ span: 24 }}>
              <div className='dashboard-portfolio-connect-item'>
                <div className='dashboard-portfolio-connect-item-title'>
                  <div className='dashboard-portfolio-connect-item-icon'><SyncOutlined/></div>
                  <div className='dashboard-portfolio-connect-item-text'>Connect Exchange</div>
                </div>
                <div className='dashboard-portfolio-connect-item-content'>
                  Automatically sync your balances and trades from your exchanges accounts. See profit loss, get order fill notifications and much more.
                </div>
              </div>
            </Col>
            <Col xl={{ span: 6 }} lg={{ span: 8 }} md={{ span: 12 }} sm={{ span: 12 }} xs={{ span: 24 }}>
              <div className='dashboard-portfolio-connect-item' onClick={() => navigate('../../connect-portfolio')}>
                <div className='dashboard-portfolio-connect-item-title'>
                  <div className='dashboard-portfolio-connect-item-icon'><SyncOutlined/></div>
                  <div className='dashboard-portfolio-connect-item-text'>Connect Wallet</div>
                </div>
                <div className='dashboard-portfolio-connect-item-content'>
                  Sync your wallet balances and transactions. Get notifications on new transactions and portfolio overview.
                </div>
              </div>
            </Col>
            <Col xl={{ span: 6 }} lg={{ span: 8 }} md={{ span: 12 }} sm={{ span: 12 }} xs={{ span: 24 }}>
              <div className='dashboard-portfolio-connect-item' onClick={() => navigate('../../connect/metamask')}>
                <div className='dashboard-portfolio-connect-item-title'>
                  <div className='dashboard-portfolio-connect-item-icon'>
                    <Image src='/coins/metamask.png' preview={false}/>
                  </div>
                  <div className='dashboard-portfolio-connect-item-text'>Connect to MetaMask</div>
                </div>
                <div className='dashboard-portfolio-connect-item-content'>
                  Get your Metamask balances and transactions into one place.
                </div>
              </div>
            </Col>
            <Col xl={{ span: 6 }} lg={{ span: 8 }} md={{ span: 12 }} sm={{ span: 12 }} xs={{ span: 24 }}>
              <div className='dashboard-portfolio-connect-item'>
                <div className='dashboard-portfolio-connect-item-title'>
                  <div className='dashboard-portfolio-connect-item-icon'><AppstoreOutlined /></div>
                  <div className='dashboard-portfolio-connect-item-text'>Connect to Ledger</div>
                </div>
                <div className='dashboard-portfolio-connect-item-content'>
                  Access all your Ledger accounts and transactions, get notifications.
                </div>
              </div>
            </Col>
            <Col xl={{ span: 6 }} lg={{ span: 8 }} md={{ span: 12 }} sm={{ span: 12 }} xs={{ span: 24 }}>
              <div className='dashboard-portfolio-connect-item'>
                <div className='dashboard-portfolio-connect-item-title'>
                  <div className='dashboard-portfolio-connect-item-icon'><LockOutlined /></div>
                  <div className='dashboard-portfolio-connect-item-text'>Connect to Trezor</div>
                </div>
                <div className='dashboard-portfolio-connect-item-content'>
                  Setup and connect your Trezor Hardware wallet to keep an eye on your holdings.
                </div>
              </div>
            </Col>
          </Row>
        </div>
      </div>
      <div className='dashboard-portfolio-transaction'>
        <div className='dashboard-portfolio-connect-header'>
          <div className='dashboard-portfolio-connect-title'>Manual Transactions</div>
        </div>
        <div className='dashboard-portfolio-connect-list'>
          <Row gutter={16}>
            <Col span={6}>
              <div className='dashboard-portfolio-connect-item'>
                <div className='dashboard-portfolio-connect-item-title'>
                  <div className='dashboard-portfolio-connect-item-icon'><PlusOutlined /></div>
                  <div className='dashboard-portfolio-connect-item-text'>Add Manual Transaction</div>
                </div>
                <div className='dashboard-portfolio-connect-item-content'>
                  Add manual crypto buy or sell transactions for manual portfolio tracking.
                </div>
              </div>
            </Col>
            <Col span={6}>
              <div className='dashboard-portfolio-connect-item'>
                <div className='dashboard-portfolio-connect-item-title'>
                  <div className='dashboard-portfolio-connect-item-icon'><PlusOutlined /></div>
                  <div className='dashboard-portfolio-connect-item-text'>Add Manual Portfolio</div>
                </div>
                <div className='dashboard-portfolio-connect-item-content'>
                  Add a manual crypto portfolio for manual portfolio tracking.
                </div>
              </div>
            </Col>
          </Row>
        </div>
      </div>
    </div>
  )
}

export default DashBoard
