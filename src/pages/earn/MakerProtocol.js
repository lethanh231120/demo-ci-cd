import React, { useEffect, useState, useRef, useContext } from 'react'
import { Tabs, Card, Form, Input, Button, Typography, notification, Slider, Modal, Spin, Select } from 'antd'
import './style.scss'
import { ethers, utils } from 'ethers'

import DSRManagement from '../../abi/DSRManagementAbi.json'
import ERC20_ABI from '../../abi/ERC20.json'
import McdPotAbi from '../../abi/McdPotAbi.json'
import Weth9Abi from '../../abi/weth9Abi.json'
import CDPManagementAbi from '../../abi/CDPManagementAbi.json'
// import MCD_JOIN_MAKER_ABI from '../../abi/MCD_JOIN_MAKER_ABI.json'
import MCD_VAT_ABI from '../../abi/MCD_VAT_ABI.json'
// import MCD_JOIN_DAI_ABI from '../../abi/MCD_JOIN_DAI_ABI.json'
// import MCD_DAI_ABI from '../../abi/MCD_DAI_ABI.json'

import { numFixed27 } from '../../utils/number/formatNum'
import { ONE_HOUR, ONE_DAY, ONE_MONTH, TWO_MONTHS, THREE_MONTHS, ONE_YEAR } from '../../constants/params'
import { AccountMetamask } from '../../layouts'
import { LoadingOutlined } from '@ant-design/icons'
// import _ from 'lodash'

const { Text } = Typography
const { TabPane } = Tabs
const { Option } = Select
const provider = ((window.ethereum != null) ? new ethers.providers.Web3Provider(window.ethereum) : ethers.getDefaultProvider())
// const ETHEREUM_KOVAN_CHAINID = process.env.REACT_APP_ETHEREUM_KOVAN_CHAINID
// const CDP_MANAGER_ADD = process.env.REACT_APP_CDP_MANAGER_ADD
// const BAT_ADD = process.env.REACT_APP_BAT_ADD
const WETH9_ADD = process.env.REACT_APP_WETH9_ADD
const MCD_VAT = process.env.REACT_APP_MCD_VAT_ADD
const WETH_ADD = process.env.REACT_APP_WETH_ADD
const DSR_MANAGEMENT = process.env.REACT_APP_DSR_MANAGERMENT
// const MCD_JOIN_DAI = process.env.REACT_APP_MCD_JOIN_DAI
// const MCD_DAI = process.env.REACT_APP_MCD_DAI
// const MCD_POT = process.env.REACT_APP_MCD_POT
// const INFU_URL = process.env.REACT_APP_INFURA_URL_KOVAN
// const ILK_REGISTRY = process.env.REACT_APP_ILK_REGISTRY

// 0xB4FBF271143F4FBf7B91A5ded31805e42b2208d6
// 0xd0A1E359811322d97991E03f863a0C30C2cF029C
const MakerProtocol = () => {
  const formRef = useRef(null)
  const [errorMessage, setErrorMessage] = useState()
  const [dsrManagerContract, setDsrManagerContract] = useState()
  const [weth9Contract, setWeth9Contract] = useState()
  const [daiContract, setDaiContract] = useState()
  const [numDaiInWallet, setNumDaiInWallet] = useState()
  const [numDaiInDsr, setNumDaiInDsr] = useState()
  const [numEthInWallet, setNumEthInWallet] = useState()
  const [percent, setPercent] = useState(0)
  const [valueInput, setValueInput] = useState()
  const [interest, setInterest] = useState()
  const [statusGetData, setStatusGetData] = useState(false)
  const [tab, setTab] = useState('deposit')
  const [accounts, setAccounts] = useState()
  const [loading, setLoading] = useState(false)
  const [form] = Form.useForm()
  const accountsMetamask = useContext(AccountMetamask)
  const chainId = accountsMetamask?.chainId && accountsMetamask?.chainId
  const [tokenErc20, setTokenErc20] = useState()
  const [openFormWETH, setOpenFormWETH] = useState(false)

  const signer = provider.getSigner()
  const infuprovider = new ethers.providers.JsonRpcProvider('https://kovan.infura.io/v3/f946e1a9058841b0a21b80df53cb249a')
  const infuprovider1 = new ethers.providers.JsonRpcProvider('https://mainnet.infura.io/v3/11dc11f9b794405eaf7ddc74ac49c04f')

  // tao smart contract cua dcr theo signer
  const cdpManager = new ethers.Contract('0xdcBf58c9640A7bd0e062f8092d70fb981Bb52032', CDPManagementAbi, signer)
  // const cdpManager = new ethers.Contract(CDP_MANAGER_ADD, CDPManagementAbi, signer)
  // tao smart contract cua dcr theo infuria
  // const cdpManagerInfu = new ethers.Contract('0xdcBf58c9640A7bd0e062f8092d70fb981Bb52032', CDPManagementAbi, infuprovider)
  // const cdpManagerInfu = new ethers.Contract(CDP_MANAGER_ADD, CDPManagementAbi, infuprovider)
  // tao smart contract cua mcd_vat
  const mcdVatContract = new ethers.Contract(MCD_VAT, MCD_VAT_ABI, signer)
  console.log(mcdVatContract)
  // tao smart contract cua mcd_join_dai
  // const mcdJoinDaiSmartcontract = new ethers.Contract(MCD_JOIN_DAI, MCD_JOIN_DAI_ABI, signer)
  // tao smartcontract cua mcd_dai
  // const mcdDaiContract = new ethers.Contract(MCD_DAI, MCD_DAI_ABI, signer)
  // tao smart contarct cua dồng erc20
  // const createContractERC20 = (ercAddress) => {
  //   return new ethers.Contract(ercAddress, ERC20_ABI, signer)
  // }

  const changeNetwork = async(id) => {
    try {
      await window.ethereum.request({
        method: 'wallet_switchEthereumChain',
        params: [{ chainId: id }]
      })
    } catch (error) {
      notification.error({
        message: 'Error',
        description: error.message
      })
    }
  }

  const errorMesage = () => {
    Modal.error({
      title: 'Oops, Something Went Wrong',
      content: (
        <div className='modal'>
          <p>MetaMask Extension Not Found</p>
        </div>
      ),
      onOk() {}
    })
  }

  useEffect(() => {
    setAccounts(accountsMetamask?.accounts && accountsMetamask?.accounts[0])
  }, [accountsMetamask])

  useEffect(() => {
    setTokenErc20(accountsMetamask?.ilks[0])
  }, [accountsMetamask?.ilks])

  useEffect(() => {
    const getData = async() => {
      if (accounts) {
        if (parseInt(chainId, 16) === 5) {
          try {
            setLoading(true)
            // tao contarct cua dsr
            const dsrToken = new ethers.Contract(DSR_MANAGEMENT, DSRManagement, signer)
            dsrToken && setDsrManagerContract(dsrToken)
            // tao smart contract cua dsr theo infuria
            const dsrToken2 = new ethers.Contract(DSR_MANAGEMENT, DSRManagement, infuprovider)
            // lay so luong dai hien co trong dsr
            dsrToken2.callStatic.daiBalance(accounts)
              .then(res => setNumDaiInDsr(Number.parseFloat(res.toBigInt().toString()) * (1 / Math.pow(10, 18))))
            // lay dia chi cua dong dai
            const addressDai = await dsrToken2.dai()
            // tao smart contract cua dong dai
            // const daiToken = new ethers.Contract(addressDai, ERC20_ABI, signer)
            const daiToken = new ethers.Contract(addressDai, ERC20_ABI, infuprovider)
            setDaiContract(daiToken)
            // lay so luong dong dai co trong metamask
            daiToken.balanceOf(accounts)
              .then(res => setNumDaiInWallet(Number.parseFloat(res.toBigInt().toString()) * (1 / Math.pow(10, 18))))
            // tao smart contract cho pot
            const mcdPotToken = new ethers.Contract('0x197E90f9FAD81970bA7976f33CbD77088E5D7cf7', McdPotAbi, infuprovider1)
            mcdPotToken.dsr()
              .then(res => setInterest(Number.parseFloat(res.toBigInt().toString()) * (1 / Math.pow(10, 27))))
            setStatusGetData(false)
            setLoading(false)
            // tao smart contract cua weth9 de swap tu eth sang weth
            const weth9Token = new ethers.Contract(WETH9_ADD, Weth9Abi, signer)
            setWeth9Contract(weth9Token)
            // lay so luong eth trong vi metamask
            const numEth = await window.ethereum.request({ method: 'eth_getBalance', params: [accounts, 'latest'] })
            setNumEthInWallet(ethers.utils.formatEther(numEth))
          } catch (error) {
            return errorMesage('Error', error.message)
          }
        } else {
          // changeNetwork(ETHEREUM_KOVAN_CHAINID)
          changeNetwork('0x5')
        }
      }
    }
    getData()
  }, [statusGetData === true, accounts])

  useEffect(() => {
    if (tab === 'deposit_collateral') {
      // check neu balance cua token vua dk chon la 0 thi hien thi loi
      if (tokenErc20 && tokenErc20?.balance === 0) {
        setErrorMessage('You need to have at least 1 token in your wallet for this function')
      }
    }
  }, [tokenErc20, tab])

  // const getData = async() => {
  // lay phan tram lai theo tg
  // const chi = await dsrToken2.callStatic.pieOf(accounts[0])
  // console.log(Number.parseFloat(chi.toBigInt()) * (1 / Math.pow(10, 27)) * 60 * 60 * 24 * 365)
  // }

  const onFinish = async(values) => {
    // yeu cau cap quyen
    try {
      if (tab === 'deposit') {
        setLoading(true)
        // tinh so luong dong DAI gui vao
        const amount = parseFloat(values.number) * Math.pow(10, 18)
        // yeu cau cap quyen cho so luong DAI vua nhap
        const approve = await daiContract.approve(DSR_MANAGEMENT, `${amount}`)
        // doi khi nao approve thuc hien xong
        const receipt = await approve.wait()
        if (receipt) {
          // gui dai vao smart contract de lay lai
          const success = await dsrManagerContract.join(accounts, `${amount}`)
          const receipt = await success.wait()
          receipt && setStatusGetData(true)
          receipt && setLoading(false)
        }
      }
      if (tab === 'deposit_collateral') {
        if (openFormWETH?.form) {
          setLoading(true)
          // wrap eth to weth
          const deposit = await weth9Contract.deposit({ from: accounts, value: utils.parseEther(values?.number) })
          // doi thuc hien xong deposit moi thuc hien tiep
          const receipt = await deposit.wait()
          if (receipt) {
            setOpenFormWETH({
              ...openFormWETH,
              form: false
            })
            setLoading(false)
          }
        } else {
          // the chap
          // mo cdp voi dong coin duoc chon (weth)
          // const open = await cdpManager.open(tokenErc20?.ilk, accounts)
          // const open = await cdpManager.open('0x4554482d43000000000000000000000000000000000000000000000000000000', accounts)
          // const receipt = await open.wait()
          // if (receipt) {
          //   // lấy cdpId
          const cdpId = await cdpManager.last(accounts)
          console.log(Number.parseInt(cdpId.toBigInt().toString()))
          // const cdpId = cdpManagerInfu.last(accounts)
          // const urnAdd = await cdpManager.urns(Number.parseInt(cdpId.toBigInt().toString()))
          // const urnAdd = await cdpManagerInfu.urns(Number.parseInt(cdpId.toBigInt().toString()))
          //   // tao smart contract dong coin vua mo
          //   // lay gem
          //   // const erc20Token = new ethers.Contract(tokenErc20?.info?.gem, Weth9Abi, signer)
          // const erc20Token = new ethers.Contract('0xB4FBF271143F4FBf7B91A5ded31805e42b2208d6', Weth9Abi, signer)
          //   // const erc20Contract = createContractERC20(tokenErc20?.info?.gem)
          //   // const abiJoin = new ethers.Contract(tokenErc20?.info[6], MCD_JOIN_MAKER_ABI, signer)
          // const abiJoin = new ethers.Contract('0x16e6490744d4B3728966f8e72416c005EB3dEa79', MCD_JOIN_MAKER_ABI, signer)
          //   // const amount = parseFloat(values.number) * Math.pow(10, 18)
          // const amount = parseInt(7) * Math.pow(10, 18)
          //   // yeu cau cap quyen
          // const approve = await erc20Token.approve('0x16e6490744d4B3728966f8e72416c005EB3dEa79', `${amount}`)
          //   // const approve = await erc20Token.approve(tokenErc20?.info[6], `${amount}`)
          //   const receiptApprove = await approve.wait()
          //   if (receiptApprove) {
          //     // di chuyen dong coin sang bo thong bao
          // const receiptJoin = await abiJoin.join(urnAdd, `${amount}`)
          // const receipt = await receiptJoin.wait()
          // if (receipt) {
          // khoa so luong coin va rut ra so luong dai DAI

          // convert number -1/e
          // const toPlainString = (num) => {
          //   return ('' + +num).replace(/(-?)(\d*)\.?(\d*)e([+-]\d+)/,
          //     function(a, b, c, d, e) {
          //       return e < 0
          //         ? b + '0.' + Array(1 - e - c.length).join(0) + c + d
          //         : b + c + d + Array(e - d.length + 1).join(0)
          //     })
          // }
          // format number  1*e^
          // Number(7 * Math.pow(10, 18)).toLocaleString('fullwide', { useGrouping: false })
          // await cdpManager.frob(Number.parseInt(cdpId.toBigInt().toString()), `${Number(7 * Math.pow(10, 18)).toLocaleString('fullwide', { useGrouping: false })}`, `${Number(5000 * Math.pow(10, 18)).toLocaleString('fullwide', { useGrouping: false })}`)
          //       // chuyen DAI tu dia chi urn sang dia chi vi
          // console.log(toPlainString(Number(5000 * Math.pow(10, 18) * (1 / Math.pow(10, 45)))))
          // cdpManager.move(Number.parseInt(cdpId.toBigInt().toString()), accounts, `${Number(5000 * Math.pow(10, 45)).toLocaleString('fullwide', { useGrouping: false })}`)
          //       // Phê duyệt bộ điều hợp mã thông báo Dai trong Vat để đúc Dai
          // await mcdVatContract.hope(MCD_JOIN_DAI)
          //       // Mint ERC-20 Dai đến địa chỉ ví của bạn
          // console.log(Number(5000 * Math.pow(10, 18)).toLocaleString('fullwide', { useGrouping: false }))
          // await mcdJoinDaiSmartcontract.exit(accounts, `${Number(5000 * Math.pow(10, 18)).toLocaleString('fullwide', { useGrouping: false })}`)
          // }
          // }
          // }

          // tra no
          // tinh so tien phai trả
          // const abc = await mcdVatContract.ilks('0x4554482d43000000000000000000000000000000000000000000000000000000')
          // console.log(Number(abc.rate.toBigInt()).toLocaleString('fullwide', { useGrouping: false }) * (1 / Math.pow(10, 27)))
          // const numberDai = Number(abc.rate.toBigInt()).toLocaleString('fullwide', { useGrouping: false }) * (1 / Math.pow(10, 27)) * 5000
          // console.log(numberDai)
          // yeu cau cap quyen sd cho dai
          // await mcdDaiContract.approve(MCD_JOIN_DAI, Number(Math.ceil(numberDai) * Math.pow(10, 18)).toLocaleString('fullwide', { useGrouping: false }))
          // chuyen dai sang daiToken adapter
          // await mcdJoinDaiSmartcontract.join(urnAdd, Number(Math.ceil(numberDai) * Math.pow(10, 18)).toLocaleString('fullwide', { useGrouping: false }))
          // tra no dai
          // cdpManager.frob(Number.parseInt(cdpId.toBigInt().toString()), `${toPlainString(-Number(7 * Math.pow(10, 18)).toLocaleString('fullwide', { useGrouping: false }))}`, `${toPlainString(-Number(5000 * Math.pow(10, 18)).toLocaleString('fullwide', { useGrouping: false }))}`)
          // mo khoa tai san the chap va tra ve vi
          console.log(cdpManager)
          // cdpManager.flux(Number.parseInt(cdpId.toBigInt().toString()), accounts, `${Number(7 * Math.pow(10, 18)).toLocaleString('fullwide', { useGrouping: false })}`)
          // rut weth ve dia chi vi
          // abiJoin.exit(accounts, `${Number(7 * Math.pow(10, 18)).toLocaleString('fullwide', { useGrouping: false })}`)
        }
      }
    } catch (error) {
      setErrorMessage('Unable to perform this action, Please try again later')
      setLoading(false)
    }
  }

  const onFinishExit = async(values) => {
    try {
      // tinh so luong dong dai muon rut
      const amount = parseFloat(values.number) * Math.pow(10, 18)
      // rut so luong dai nho hon so luong dang co trong dsrmanager
      if (percent < 100) {
        setLoading(true)
        // gui yeu cau rut dai
        const statusExit = await dsrManagerContract.exit(accounts, `${amount}`)
        const receipt = await statusExit.wait()
        receipt && setStatusGetData(true)
        receipt && setLoading(false)
      }
      // neu so luong nhap bang so luong dang co trong dsrmanager
      if (percent === 100) {
        setLoading(true)
        // gui yeu cau rut tat ca
        const statusExitAll = await dsrManagerContract.exitAll(accounts)
        const receipt = await statusExitAll.wait()
        receipt && setStatusGetData(true)
        receipt && setLoading(false)
      }
    } catch (error) {
      setErrorMessage('Unable to perform this action, Please try again later')
      setLoading(false)
    }
  }

  const handleChangeForm = (values) => {
    if (tab === 'deposit') {
      // check có dai trong ví hay khong
      if (numDaiInWallet !== undefined) {
        setValueInput(values.number)
        const percent = ((values.number) / numDaiInWallet)
        setPercent(parseFloat(percent * 100))
        setErrorMessage('')
        // neu so luong nhap lon hon so luong co trong ví thì hien thi loi
        if (values.number > numDaiInWallet) {
          setErrorMessage('You cannot deposit more assets than the amount in your wallet')
        }
        // bat buoc nhap so luong lon hon 0
        if (values.number === '0') {
          setErrorMessage('This vault type requires a minimum of 1.00 Dai to be generated')
        }
      }
    }
    if (tab === 'exit') {
      // check co dai trong dsr hay khong
      if (numDaiInDsr !== undefined) {
        setValueInput(values.number)
        const percent = ((values.number) / numDaiInDsr)
        setPercent(parseFloat(percent * 100))
        setErrorMessage('')
        // kiem tra so luong nhap lon hon so luong co thì hien thi loi
        if (values.number > numDaiInDsr) {
          setErrorMessage('You cannot deposit more assets than the amount in your wallet')
        }
        // yeu cau so luong nhap phai lon hon 0
        if (values.number === '0') {
          setErrorMessage('This vault type requires a minimum of 1.00 Dai to be generated')
        }
      }
    }
    if (tab === 'deposit_collateral') {
      // kiem tra dang ơ man hinh nao
      // neu openFormWETH?.form === true thi dang o man hinh swap ETH sang WETH
      // neu false thì dang ơ man hinh the chap
      if (openFormWETH?.form) {
        setErrorMessage('')
        // kiem tra co eth trong vi metamask hay k
        if (numEthInWallet !== undefined) {
          setValueInput(values.number)
          const percent = ((values.number) / tokenErc20?.balance)
          setPercent(parseFloat(percent * 100))
          setErrorMessage('')
          // neu so luong nhap lon hon so luong có trong vi thi hien thị lỗi
          if (values.number > numEthInWallet) {
            setErrorMessage('You cannot deposit more assets than the amount in your wallet')
          }
        }
      } else {
        // kiem tra token vua duoc chon co so luong !== undefined hay khong
        if (tokenErc20?.balance !== undefined) {
          setValueInput(values.number)
          const percent = ((values.number) / tokenErc20?.balance)
          setPercent(parseFloat(percent * 100))
          setErrorMessage('')
          // neu so luong nhap lon hon so luong có thì hiển thị lỗi
          if (values.number > tokenErc20?.balance) {
            setErrorMessage('You cannot deposit more assets than the amount in your wallet')
          }
          // bat buoc phai nhap so luong lon hon 0
          if (values.number === '0') {
            setErrorMessage('This vault type requires a minimum of 1.00 Dai to be generated')
          }
        }
      }
    }
  }

  const handleChangeAmount = (value) => {
    setErrorMessage('')
    setPercent(value)
    // check dang o tab nao
    // neu tab === deposit. lay so luong co trong ví
    // neu tab === exit. lay so luong co trong dsr
    // neu tab === deposit_collateral. lay so luong co cua token duoc chon trong ví

    const valueInput = (tab === 'deposit')
      ? ((numDaiInWallet / 100) * value)
      : ((tab === 'exit') ? ((numDaiInDsr / 100) * value)
        : (tab === 'deposit_collateral') ? ((tokenErc20?.balance / 100) * value)
          : 0)

    if (tab === 'deposit') {
      // bat buoc phai có so luong dong trong vi > 0 va so luong nhap > 0
      if (numDaiInWallet === 0) {
        setErrorMessage('You cannot deposit more assets than the amount in your wallet')
      }
      if (value === 0) {
        setErrorMessage('This vault type requires a minimum of 1.00 Dai to be generated')
      }
    }
    if (tab === 'exit') {
      // yeu cau so luong co trong dsr > 0 va so luong nhap > 0
      if (numDaiInDsr === 0) {
        setErrorMessage('You cannot deposit more assets than the amount in your wallet')
      }
      if (value === 0) {
        setErrorMessage('This vault type requires a minimum of 1.00 Dai to be generated')
      }
    }
    if (tab === 'deposit_collateral') {
      // yeu cau so luong cua token  > 0 va so luong nhap  > 0
      if (tokenErc20?.balance === 0) {
        setErrorMessage('You cannot deposit more assets than the amount in your wallet')
      }
      if (value === 0) {
        setErrorMessage('This vault type requires a minimum of 1.00 Dai to be generated')
      }
    }

    setValueInput(valueInput)
    formRef.current?.setFieldsValue({
      number: valueInput
    })
  }

  const handleChangeTab = (activeKey) => {
    setPercent(0)
    setErrorMessage()
    setValueInput()
    form.resetFields()
    setTab(activeKey)
    setLoading(false)
  }

  const handleChangeSelect = (value) => {
    const dataItem = accountsMetamask?.ilks[value]
    setTokenErc20(dataItem)
  }

  useEffect(() => {
    // kiem tra neu thong tin cua token hien tai === vơi đia chi cua weth
    // so luong co cua token === 0
    // hien thi button để swap tu eth sang weth
    if (tokenErc20?.info?.gem === WETH_ADD) {
      if (tokenErc20?.balance === 0) {
        setOpenFormWETH({
          ...openFormWETH,
          button: true
        })
      }
    }
  }, [tokenErc20])

  const handleSwapWETH = () => {
    setOpenFormWETH({
      ...openFormWETH,
      form: true
    })
    setErrorMessage('')
  }

  return (
    <div className='maker'>
      <Tabs
        centered={true}
        type='card'
        defaultActiveKey={tab}
        onChange={handleChangeTab}
      >
        <TabPane tab='Deposit' key='deposit'>
          <Card style={{ width: '100%' }}>
            <Form
              ref={formRef}
              name='basic'
              initialValues={{ remember: true }}
              onFinish={onFinish}
              autoComplete='off'
              layout='vertical'
              form={form}
              style={{ textAlign: 'left' }}
              onValuesChange = {(values) => handleChangeForm(values)}
            >
              <Text>Your Wallet:
                <span style={{ marginLeft: '10px' }}>
                  {(numDaiInWallet !== undefined && !loading)
                    ? numDaiInWallet
                    : (<Spin indicator={<LoadingOutlined style={{ fontSize: 20 }} spin />} />)}
                </span>
              </Text>
              <Form.Item label='Deposit DAI' name='number'>
                <Input placeholder='0 DAI' type='number' disabled={!(numDaiInWallet !== undefined)}/>
              </Form.Item>
              <Slider value={percent} onChange={handleChangeAmount} disabled={!(numDaiInWallet !== undefined)} />{percent} %
              {errorMessage !== ''
                ? <Typography className='message-error'>
                  <Text type='danger'>{errorMessage && errorMessage}</Text>
                </Typography>
                : ''
              }
              <div className='saving'>
                <div className='saving-duration'>
                  <Text>Duration</Text>
                  <div>After 1 hour</div>
                  <div>After 1 day</div>
                  <div>After 30 days</div>
                  <div>After 60 days</div>
                  <div>After 90 days</div>
                  <div>After 1 year</div>
                </div>
                <div className='saving-value'>
                  <Text>Net Value</Text>
                  <div>
                    {valueInput ? numFixed27(valueInput * Math.pow(interest, ONE_HOUR)) : 0}
                  </div>
                  <div>
                    {valueInput ? numFixed27(valueInput * Math.pow(interest, ONE_DAY)) : 0}
                  </div>
                  <div>
                    {valueInput ? numFixed27(valueInput * Math.pow(interest, ONE_MONTH)) : 0}
                  </div>
                  <div>
                    {valueInput ? numFixed27(valueInput * Math.pow(interest, TWO_MONTHS)) : 0}
                  </div>
                  <div>
                    {valueInput ? numFixed27(valueInput * Math.pow(interest, THREE_MONTHS)) : 0}
                  </div>
                  <div>
                    {valueInput ? numFixed27(valueInput * Math.pow(interest, ONE_YEAR)) : 0}
                  </div>
                </div>
              </div>
              <Form.Item >
                <Button type='primary' htmlType='submit' loading={loading} disabled={!percent}>Submit</Button>
              </Form.Item>
            </Form>
          </Card>
        </TabPane>
        <TabPane tab='Exit' key='exit'>
          <Card style={{ width: '100%' }}>
            <Form
              ref={formRef}
              name='basic'
              initialValues={{ remember: true }}
              onFinish={onFinishExit}
              autoComplete='off'
              layout='vertical'
              form={form}
              style={{ textAlign: 'left' }}
              onValuesChange = {(values) => handleChangeForm(values)}
            >
              <Text>
                Amount DAI:
                <span style={{ marginLeft: '10px' }}>
                  {(numDaiInDsr !== undefined && !loading)
                    ? numDaiInDsr
                    : (<Spin indicator={<LoadingOutlined style={{ fontSize: 20 }} spin />} />)}
                </span>
              </Text>
              <Form.Item label='Exit DAI' name='number'>
                <Input placeholder='0 DAI' type='number' disabled={!(numDaiInDsr !== undefined)}/>
              </Form.Item>
              <Slider value={percent} onChange={handleChangeAmount} disabled={!(numDaiInDsr !== undefined)} />{percent} %
              {errorMessage !== ''
                ? <Typography className='message-error'>
                  <Text type='danger'>{errorMessage && errorMessage}</Text>
                </Typography>
                : ''
              }
              <Form.Item >
                <Button type='primary' loading={loading} htmlType='submit' disabled={!percent}>Submit</Button>
              </Form.Item>
            </Form>
          </Card>
        </TabPane>
        <TabPane tab='Deposit Collateral' key='deposit_collateral'>
          {/* <TabPane tab='Deposit Collateral' key='deposit_collateral' disabled={_.isEmpty(accountsMetamask?.ilks)}> */}
          <Card style={{ width: '100%' }}>
            <Form
              ref={formRef}
              name='basic'
              initialValues={{ remember: true }}
              onFinish={onFinish}
              autoComplete='off'
              layout='vertical'
              form={form}
              style={{ textAlign: 'left' }}
              onValuesChange = {(values) => handleChangeForm(values)}
            >
              {openFormWETH?.form ? (
                <>
                  <Text>
                    Amount ETH:
                    <span style={{ marginLeft: '10px' }}>
                      {(numEthInWallet !== undefined)
                        ? numEthInWallet
                        : 0}
                    </span>
                  </Text>
                  <Form.Item label='Amount' name='number'>
                    <Input placeholder='0 ETH' type='number'/>
                  </Form.Item>
                  {errorMessage !== ''
                    ? <Typography className='message-error'>
                      <Text type='danger'>{errorMessage && errorMessage}</Text>
                    </Typography>
                    : ''
                  }
                  <Form.Item >
                    <Button loading={loading} type='primary' htmlType='submit'>Submit</Button>
                  </Form.Item>
                </>
              ) : (
                <>
                  <Text>
                    Amount {tokenErc20 && tokenErc20?.info?.symbol}:
                    <span style={{ marginLeft: '10px' }}>
                      {(tokenErc20 !== undefined)
                        ? tokenErc20?.balance
                        : 0}
                    </span>
                  </Text>
                  <Form.Item label='Select'>
                    <Select
                      showSearch
                      defaultValue={0}
                      placeholder='Search to Select'
                      onChange={handleChangeSelect}
                      filterOption={(input, option) => option.children.includes(input)}
                      filterSort={(optionA, optionB) =>
                        optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
                      }
                    >
                      {accountsMetamask?.ilks && accountsMetamask?.ilks?.map((item, index) => (
                        <Option value={index} key={index}>
                          {item?.info?.name}
                        </Option>
                      ))}
                    </Select>
                  </Form.Item>
                  <Form.Item label='Amount' name='number'>
                    <Input placeholder={`0 ${tokenErc20?.info?.symbol}`} type='number' disabled={tokenErc20?.balance === 0}/>
                  </Form.Item>
                  <Slider value={percent} onChange={handleChangeAmount} disabled={(!(tokenErc20?.balance !== undefined)) || (tokenErc20?.balance === 0)} />{percent} %
                  {errorMessage !== ''
                    ? <Typography className='message-error'>
                      <Text type='danger'>{errorMessage && errorMessage}</Text>
                    </Typography>
                    : ''
                  }
                  {openFormWETH?.button && <a onClick={handleSwapWETH}>Get WETH</a>}
                  <Form.Item >
                    <Button type='primary' loading={loading} htmlType='submit'>Submit</Button>
                    {/* <Button type='primary' loading={loading} htmlType='submit' disabled={!percent}>Submit</Button> */}
                  </Form.Item>
                </>
              )}
            </Form>
          </Card>
        </TabPane>
      </Tabs>
    </div>
  )
}

export default MakerProtocol
