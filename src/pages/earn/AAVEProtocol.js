import { Button, Modal, Table } from 'antd'
import { ethers } from 'ethers'
import React, { useEffect, useState } from 'react'
import aaveAddressesProviderAbi from '../../abi/aaveAddressesProviderAbi.json'
import aaveLendingPoolAbi from '../../abi/aaveLendingPool.json'
import ERC20abi from '../../abi/ERC20.json'
import PriceOracleAbi from '../../abi/PriceOracleAbi.json'
import BorrowModal from '../../components/earn/AAVEProtocol/BorrowModal'
import DepositModal from '../../components/earn/AAVEProtocol/DepositModal'
import './style.scss'

const LENDING_POOL_PROVIDER_ADDRESSES = process.env.REACT_APP_LENDING_POOL_PROVIDER_KOVAN_ADDRESSES
const PRICE_ORACLE_ADDRESS = process.env.REACT_APP_PRICE_ORACLE_ADDRESS
const web3Provider = new ethers.providers.JsonRpcProvider(process.env.REACT_APP_INFURA_URL_KOVAN)
const AAVEProtocol = (props) => {
  const [lendingPoolProviderAddress, setLendingPoolProviderAddress] = useState('')
  const [availableBorrowETH, setAvailableBorrowETH] = useState(0)
  const [dataForDepositTable1, setDataForDepositTable1] = useState([])
  const [dataForDepositTable2, setDataForDepositTable2] = useState([])
  const [dataForBorrow, setDataForBorrow] = useState([])
  const [dataForBorrow2, setDataForBorrow2] = useState([])
  const [dataForDepositModal, setDataForDepositModal] = useState(undefined)
  const [dataForBorrowModal, setDataForBorrowModal] = useState(undefined)
  const [isDepositModal, setIsDepositModal] = useState(false)
  const [isBorrowModal, setIsBorrowModal] = useState(false)

  const secPerYear = 60 * 60 * 24 * 365 // 1 year have 60 * 60 * 24 * 365 seconds
  const lendingPoolProviderAddressContract = new ethers.Contract(LENDING_POOL_PROVIDER_ADDRESSES, aaveAddressesProviderAbi, web3Provider)
  lendingPoolProviderAddressContract.getLendingPool().then(
    res => {
      setLendingPoolProviderAddress(res)
    }
  )

  const onBehalfOf = props.addressSigner
  const tokens = [{
    name: 'WETH', decimal: 18, address: '0xd0A1E359811322d97991E03f863a0C30C2cF029C'
  }, {
    name: 'WBTC', decimal: 8, address: '0xD1B98B6607330172f1D991521145A22BCe793277'
  }, {
    name: 'DAI', decimal: 18, address: '0xFf795577d9AC8bD7D90Ee22b6C1703490b6512FD'
  }, {
    name: 'AAVE', decimal: 18, address: '0xB597cd8D3217ea6477232F9217fa70837ff667Af'
  }]

  const allBalance = tokens.map(async(token) => {
    const tokenContract = new ethers.Contract(token.address, ERC20abi, web3Provider)
    const balance = tokenContract.balanceOf(onBehalfOf)
    return balance
  })

  useEffect(() => {
    const resInDec = []
    Promise.all(allBalance).then(res => {
      const merge = []
      for (let i = 0; i < res.length; i++) {
        const balanceInDec = res[i]._hex / Math.pow(10, tokens[i].decimal)
        resInDec.push({ balance: balanceInDec })
      }

      for (let i = 0; i < tokens.length; i++) {
        merge.push({
          ...tokens[i],
          ...resInDec[i]
        })
      }
      setDataForDepositTable1(merge)
    })
  }, [])

  const allReserveDataOfTokens = tokens.map(async(token) => {
    const lendingPoolContractForGetReserveData = new ethers.Contract(lendingPoolProviderAddress && lendingPoolProviderAddress, aaveLendingPoolAbi, web3Provider)
    const reserveData = lendingPoolContractForGetReserveData.getReserveData(token.address.toString())
    return reserveData
  })

  useEffect(() => {
    const currentLiquidityRateOfTokens = []
    const currentAPYofTokens = []
    const stableBorrowRateOfTokens = []
    const stableAPYOfTokens = []
    const variableBorrowRateOfTokens = []
    const variableAPYOfTokens = []
    const merge = []
    const merge2 = []
    Promise.all(allReserveDataOfTokens).then(res => {
      for (let i = 0; i < res.length; i++) {
        const currentLiquidityRate = res[i].currentLiquidityRate.toString() * Math.pow(10, -27)
        const currentStableBorrowRate = res[i].currentStableBorrowRate.toString() * Math.pow(10, -27)
        const currentVariableBorrowRate = res[i].currentVariableBorrowRate.toString() * Math.pow(10, -27)
        currentLiquidityRateOfTokens.push(currentLiquidityRate)
        stableBorrowRateOfTokens.push(currentStableBorrowRate)
        variableBorrowRateOfTokens.push(currentVariableBorrowRate)
      }
      for (let i = 0; i < stableBorrowRateOfTokens.length; i++) {
        const currentAPY = (1 + stableBorrowRateOfTokens[i] / secPerYear) ** secPerYear - 1
        const currentAPYToFix = currentAPY.toFixed(2)
        stableAPYOfTokens.push({ sAPY: currentAPYToFix })
      }
      for (let i = 0; i < currentLiquidityRateOfTokens.length; i++) {
        const currentAPY = (1 + currentLiquidityRateOfTokens[i] / secPerYear) ** secPerYear - 1
        const currentAPYToFix = currentAPY.toFixed(2)
        currentAPYofTokens.push({ APY: currentAPYToFix })
      }
      for (let i = 0; i < variableBorrowRateOfTokens.length; i++) {
        const currentAPY = (1 + variableBorrowRateOfTokens[i] / secPerYear) ** secPerYear - 1
        const currentAPYToFix = currentAPY.toFixed(2)
        variableAPYOfTokens.push({ vAPY: currentAPYToFix })
      }
      // console.log('currentAPYofTokens', currentAPYofTokens)
      for (let i = 0; i < currentAPYofTokens.length; i++) {
        merge.push({
          ...dataForDepositTable1[i],
          ...currentAPYofTokens[i]
        })
      }

      for (let i = 0; i < tokens.length; i++) {
        merge2.push({
          ...tokens[i],
          ...stableAPYOfTokens[i],
          ...variableAPYOfTokens[i]
        })
      }
      setDataForDepositTable2(merge)
      setDataForBorrow(merge2)
    })
  }, [dataForDepositTable1])

  const getAvailableInETH = async() => {
    const ETHDecimal = 18
    const lendingPoolContractForGetAvailableAmount = new ethers.Contract(lendingPoolProviderAddress && lendingPoolProviderAddress, aaveLendingPoolAbi, web3Provider)
    await lendingPoolContractForGetAvailableAmount.getUserAccountData(onBehalfOf).then(
      res => {
        const availableBorrowsETH = res.availableBorrowsETH.toString() / Math.pow(10, ETHDecimal)
        setAvailableBorrowETH(availableBorrowsETH)
      }
    )
  }
  getAvailableInETH()

  const priceOracleContract = new ethers.Contract(PRICE_ORACLE_ADDRESS, PriceOracleAbi, web3Provider)
  const getBorrowableTokenAmountInETH = tokens.map(async(token) => {
    const borrowableTokenAmount = await priceOracleContract.getAssetPrice(token.address)
    return borrowableTokenAmount
  })

  useEffect(() => {
    const borrowableAmountOfTokens = []
    const ETHDecimal = 18
    const merge = []
    Promise.all(getBorrowableTokenAmountInETH).then(res => {
      for (let i = 0; i < res.length; i++) {
        const borrowableAmountInETH = res[i]._hex.toString() / Math.pow(10, ETHDecimal)
        const borrowableAmount = availableBorrowETH / borrowableAmountInETH
        borrowableAmountOfTokens.push({ borrowableAmount: borrowableAmount })
      }
      for (let i = 0; i < dataForBorrow.length; i++) {
        merge.push({
          ...dataForBorrow[i],
          ...borrowableAmountOfTokens[i]
        })
      }
      setDataForBorrow2(merge)
    })
  }, [availableBorrowETH, dataForBorrow])

  const renderAmount = (record) => {
    let total
    const amountTotal = record?.borrowableAmount
    if ((amountTotal > 0) && (amountTotal < 1)) {
      total = amountTotal.toFixed(8).replace(/\d(?=(\d{3})+\.)/g, '$&,')
    }
    if (amountTotal === 0) {
      total = 0
    }
    if (amountTotal >= 1) {
      total = amountTotal.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')
    }
    return total
  }

  const columnDeposit = [{
    title: 'Name',
    dataIndex: 'name',
    key: 'name'
  }, {
    title: 'Balance (Token)',
    dataIndex: 'balance',
    key: 'balance'
  }, {
    title: 'APY (%)',
    dataIndex: 'APY',
    key: 'apy'
  }, {
    title: '',
    dataIndex: 'address',
    key: 'address',
    render: (text, record, index) =>
      tokens.length >= 1 ? (
        <Button
          id={record.address}
          onClick={() => { runDeposit(record) }}>Deposit</Button>
      ) : null
  }]

  const columnBorrow = [{
    title: 'Name',
    dataIndex: 'name',
    key: 'name'
  }, {
    title: 'Available',
    dataIndex: 'borrowableAmount',
    key: 'available',
    render: (_, record) => (<span className='amount'>
      {record.borrowableAmount ? renderAmount(record) : 0}</span>)
  }, {
    title: 'APY-Stable (%)',
    dataIndex: 'sAPY',
    key: 'apy-stable'
  }, {
    title: 'APY-Variable (%)',
    dataIndex: 'vAPY',
    key: 'apy-variable'
  }, {
    title: '',
    dataIndex: 'address',
    key: 'address',
    render: (text, record, index) =>
      tokens.length >= 1 ? (
        <Button
          id={record.address}
          onClick={() => { runBorrow(record) }}>Borrow</Button>
      ) : null
  }]

  const runDeposit = (value) => {
    setDataForDepositModal(value)
    setIsDepositModal(true)
  }

  const runBorrow = (value) => {
    setDataForBorrowModal(value)
    setIsBorrowModal(true)
  }

  return (
    <div className='AAVE-protocol__container'>
      <div className='transaction-table'>
        <div className='deposit-token'>
          <div className='deposit-token__token--part-title'>Deposit Tokens</div>
          <Table dataSource={dataForDepositTable2} columns={columnDeposit} pagination={false}/>
        </div>
        <div className='borrow-token'>
          <div className='borrow-token__token--part-title'>Borrow Tokens</div>
          <Table dataSource={dataForBorrow2} columns={columnBorrow} pagination={false}/>
        </div>
      </div>
      <Modal
        visible={isDepositModal}
        footer={null}
        onOk={() => setIsDepositModal(false)}
        onCancel={() => setIsDepositModal(false)}
        className='deposit-modal'
      >
        <DepositModal
          setIsDepositModal={setIsDepositModal}
          dataForDepositModal={dataForDepositModal}
          lendingPoolProviderAddress={lendingPoolProviderAddress && lendingPoolProviderAddress}
          onBehalfOf={onBehalfOf}
        />
      </Modal>
      <Modal
        visible={isBorrowModal}
        footer={null}
        onOk={() => setIsBorrowModal(false)}
        onCancel={() => setIsBorrowModal(false)}
        className='borrow-modal'
      >
        <BorrowModal
          setIsBorrowModal={setIsBorrowModal}
          dataForBorrowModal={dataForBorrowModal}
          lendingPoolProviderAddress={lendingPoolProviderAddress && lendingPoolProviderAddress}
          onBehalfOf={onBehalfOf}
        />
      </Modal>
    </div>
  )
}

export default AAVEProtocol
