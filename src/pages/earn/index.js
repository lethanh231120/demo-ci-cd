import React, { useState, useContext, useEffect } from 'react'
import { Layout, Typography, Button, Modal, notification, Select, Form, Image } from 'antd'
import { SelectNetwork } from '../../components/swap/SelectNetwork'
import AAVEProtocol from './AAVEProtocol'
import './style.scss'
import { ethers } from 'ethers'
import { AccountMetamask } from '../../layouts'
import { get } from '../../api/supportEarnService'

const { Content } = Layout
const { Title } = Typography
const { ethereum } = window
const provider = ((window.ethereum != null) ? new ethers.providers.Web3Provider(window.ethereum) : ethers.getDefaultProvider())

const Earn = () => {
  const [openModalWallet, setOpenModalWallet] = useState(false)
  const [symbolNetwork, setSymbolNetwork] = useState(undefined)
  const [isSelectNetworkModal, setIsSelectNetworkModal] = useState(false)
  const [chainId, setChainId] = useState(undefined)
  const [addressSigner, setAddressSigner] = useState()
  const accounts = useContext(AccountMetamask)
  const [protocols, setProtocols] = useState([])
  const [protocol, setProtocol] = useState(undefined)
  const { Option } = Select
  const [form] = Form.useForm()
  useEffect(() => {
    setAddressSigner(accounts?.accounts && accounts?.accounts[0])
  }, [accounts])

  const getProtocols = async(id) => {
    const idInDec = parseInt(id, 16)
    await get(`earn/list-protocol/chainId=${idInDec}`).then(res => {
      setProtocols(res?.data?.protocols)
    })
  }

  const getAllContractsByProtocolId = async(id) => {
    await get(`earn/list-contract/protocolId=${id}`).then(res => {
      console.log(res)
    })
  }

  useEffect(() => {
    getProtocols(chainId)
  }, [chainId])

  useEffect(() => {
    if (protocol !== undefined) {
      getAllContractsByProtocolId(protocol)
    }
  }, [protocol])

  const handleConnectMetamask = async() => {
    if (ethereum) {
      try {
        const accounst = await provider.send('eth_requestAccounts', [])
        accounts.handleSetAccounts({ newAccounts: accounst })
        setOpenModalWallet(false)
      } catch (error) {
        return error('Error', error.message)
      }
    } else {
      notification.error({
        message: 'Cannot find meta mask',
        description: 'Cannot find metamask extensions. Please check again and install if not have meta mask.'
      })
    }
  }

  const handleChooseProtocol = (value) => {
    setProtocol(value)
  }

  const openSelectNetworkModal = () => {
    setIsSelectNetworkModal(true)
  }

  return (
    <div className='earn__protocol--container'>
      <Button onClick={openSelectNetworkModal}>
        {
          symbolNetwork === undefined ? 'Network' : symbolNetwork
        }
      </Button>
      <Content style={{ padding: '100px 0' }}>
        <div style={{ margin: '0px auto', background: '#fff', minHeight: '500px', borderRadius: '5px', padding: '70px' }}>
          {!addressSigner ? (<>
            <Title level={3}>Please, connect your wallet</Title>
            <Title level={5}>Please connect your wallet to see your supplies, borrowings, and open positions.</Title>
            <Button onClick={() => setOpenModalWallet(true)}>Connect wallet</Button>
          </>)
            : (
              <div>
                <Form autoComplete='off' layout='vertical' form={form}>
                  {/* <Form.Item label='Categories' name='category' >
                    <Select
                      showSearch
                      placeholder='Select category --'
                      optionFilterProp='children'
                      filterOption={(input, option) => option.children.includes(input)}
                      filterSort={(optionA, optionB) =>
                        optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
                      }
                    >
                      <Option value={1}>Add Liquidity</Option>
                      <Option value={2}>Lending</Option>
                    </Select>
                  </Form.Item> */}
                  <Form.Item label='Protocols' name='protocol'>
                    <Select
                      showSearch
                      placeholder='Select protocol --'
                      optionFilterProp='children'
                      filterOption={(input, option) => option.value.includes(input)}
                      onChange={handleChooseProtocol}
                    >
                      {protocols && protocols.length > 0
                        ? protocols?.map((item) => (
                          <Option key={item.id} value={item.id} className='earn-protocol'>
                            <Image width={30} src={item.image} alt='Earn protocol image'/>
                            <div>{item.name}</div>
                          </Option>
                        ))
                        : (
                          <Option value=''>We still not support earn in this network</Option>
                        )
                      }
                    </Select>
                  </Form.Item>
                </Form>
                { protocol === undefined
                  ? ''
                  : protocol === 1
                    ? ''
                    : (
                      <>
                        <AAVEProtocol addressSigner={addressSigner}/>
                      </>
                    )
                }
              </div>
            )}
        </div>
      </Content>
      <Modal
        className='reset-password-modal'
        visible={openModalWallet}
        onOk={() => setOpenModalWallet(false)}
        onCancel={() => setOpenModalWallet(false)}
        footer={null}
      >
        <Title level={3}>Connect a wallet</Title>
        <button className='earn-item-protocol' onClick={handleConnectMetamask}>
          <span>Browser wallet</span>
          <div className='earn-item-protocol-img'>
            <img src='/coins/metamask.png'/>
          </div>
        </button>
        <button className='earn-item-protocol' onClick={handleConnectMetamask}>
          <span>WalletConnect</span>
          <div className='earn-item-protocol-img'>
            <img src='/coins/walletconnect.png'/>
          </div>
        </button>
        <button className='earn-item-protocol' onClick={handleConnectMetamask}>
          <span>Coin base</span>
          <div className='earn-item-protocol-img'>
            <img src='/coins/coinbase.png'/>
          </div>
        </button>
      </Modal>
      <Modal
        className='select-network-modal'
        visible={isSelectNetworkModal}
        onOk={() => setIsSelectNetworkModal(false)}
        onCancel={() => setIsSelectNetworkModal(false)}
        footer={null}
        closable={false}
      >
        <SelectNetwork setIsSelectNetworkModal={setIsSelectNetworkModal} setSymbolNetwork={setSymbolNetwork} setChainId={setChainId}/>
      </Modal>
    </div>
  )
}

export default Earn
