import React, { useEffect, useContext, useState } from 'react'
import { ethers } from 'ethers'
import UniSwapV3NFTAbi from '../../abi/UniSwapV3NFTAbi.json'
// import ERC20_ABI from '../../abi/ERC20.json'
import { notification, Modal } from 'antd'
import { AccountMetamask } from '../../layouts'

const NFT_MANAGER = process.env.REACT_APP_NFT_MANAGER_ADD
// const UNI_ADD = '0x1f9840a85d5aF5bf1D1762F925BDADdC4201F984'
// const WETH_ADD = '0xB4FBF271143F4FBf7B91A5ded31805e42b2208d6'
const provider = ((window.ethereum != null) ? new ethers.providers.Web3Provider(window.ethereum) : ethers.getDefaultProvider())

const UniSwapProtocol = () => {
  const [accounts, setAccounts] = useState()
  const signer = provider.getSigner()
  const nftManager = new ethers.Contract(NFT_MANAGER, UniSwapV3NFTAbi, signer)
  //   const uniToken = new ethers.Contract(UNI_ADD, ERC20_ABI, signer)
  //   const wethToken = new ethers.Contract(WETH_ADD, ERC20_ABI, signer)
  const accountsMetamask = useContext(AccountMetamask)
  const chainId = accountsMetamask?.chainId && accountsMetamask?.chainId
  console.log(nftManager)

  const changeNetwork = async(id) => {
    try {
      await window.ethereum.request({
        method: 'wallet_switchEthereumChain',
        params: [{ chainId: id }]
      })
    } catch (error) {
      notification.error({
        message: 'Error',
        description: error.message
      })
    }
  }

  const errorMesage = () => {
    Modal.error({
      title: 'Oops, Something Went Wrong',
      content: (
        <div className='modal'>
          <p>MetaMask Extension Not Found</p>
        </div>
      ),
      onOk() {}
    })
  }

  useEffect(() => {
    setAccounts(accountsMetamask?.accounts && accountsMetamask?.accounts[0])
  }, [accountsMetamask])

  useEffect(() => {
    const getData = async() => {
      if (accounts) {
        if (parseInt(chainId, 16) === 5) {
          try {
            // const amount = parseFloat(0.001) * Math.pow(10, 18)
            // yêu câu quyền sd uni
            // const approve = await uniToken.approve(NFT_MANAGER, `${amount}`)
            // const receiptApprove = await approve.wait()
            // if (receiptApprove) {
            //   // yeu cầu quyền sd weth
            //   const approveWeth = await wethToken.approve(NFT_MANAGER, `${amount}`)
            //   const receiptApproveWeth = await approveWeth.wait()
            //   if (receiptApproveWeth) {
            //     // Tao position
            //     // them thanh khoan
            //     // nftManager.mint([
            //     //   UNI_ADD, // The first of the two tokens of the pool, sorted by address
            //     //   WETH_ADD, // The second of the two tokens of the pool, sorted by address
            //     //   3000, // fees
            //     //   -887220,
            //     //   887220,
            //     //   `${amount}`, // amount0 you want to transfer
            //     //   `${amount}`, // amount1 you want to transfer
            //     //   0, // Minimum0 amount you want to transfer
            //     //   0, // Minimum1 amount you want to transfer
            //     //   accounts, // Dai only wallet will receive tokens
            //     //   Math.floor(Date.now() / 1000 + 400) // deadline
            //     // ])
            //   }
            // }
            // lay so luong positon ma dia chi dang co
            const numPosition = await nftManager.balanceOf(accounts)
            console.log(Number.parseFloat(numPosition.toBigInt().toString()))
            // lay tokenId cua position theo index
            const tokenId = await nftManager.tokenOfOwnerByIndex(accounts, 0)
            console.log(Number.parseFloat(tokenId.toBigInt().toString()))
            // lay thong tin cua position
            const infoPosition = await nftManager.positions(Number.parseFloat(tokenId.toBigInt().toString()))
            console.log(infoPosition)
            console.log(Number.parseFloat(infoPosition.liquidity.toBigInt().toString()))
            // giam thanh khoan
            // nftManager.decreaseLiquidity([
            //   Number.parseFloat(tokenId.toBigInt().toString()), // tokenId
            //   Number.parseFloat(infoPosition.liquidity.toBigInt().toString()) / 2, // so luong muon giam (50%)
            //   0, // amount0min
            //   0, // amount1min
            //   Math.floor(Date.now() / 1000 + 400) // deadline
            // ])
            // rut thanh khoan ve vi
            // const amount = parseFloat(0.003) * Math.pow(10, 18)
            // nftManager.collect([
            //   Number.parseFloat(tokenId.toBigInt().toString()), // tokenId
            //   accounts, // address your wallet
            //   `${amount}`, // so luong dong max 0 co the nhan
            //   `${amount}` // so luong dong max 1 co the nhan
            // ])
            // them thanh khoan cho 1 position theo tokenId
            // const amount = parseFloat(0.003) * Math.pow(10, 18)
            // yêu câu quyền sd uni
            // const approve = await uniToken.approve(NFT_MANAGER, `${amount}`)
            // const receiptApprove = await approve.wait()
            // if (receiptApprove) {
            //   // yeu cầu quyền sd weth
            //   const approveWeth = await wethToken.approve(NFT_MANAGER, `${amount}`)
            //   const receiptApproveWeth = await approveWeth.wait()
            // }
            // nftManager.increaseLiquidity([
            //   Number.parseFloat(tokenId.toBigInt().toString()), // tokenId
            //   `${amount}`, // amount0 you want to transfer
            //   `${amount}`, // amount1 you want to transfer
            //   0, // Minimum0 amount you want to transfer
            //   0, // Minimum1 amount you want to transfer
            //   Math.floor(Date.now() / 1000 + 400) // deadline
            // ])
          } catch (error) {
            return errorMesage('Error', error.message)
          }
        } else {
          changeNetwork(5)
        }
      }
    }
    getData()
  }, [accounts])

  return (
    <div>
      hello uniswwap
    </div>
  )
}

export default UniSwapProtocol
