import React from 'react'
import cETHAbi from '../../abi/CETHCompoundAbi.json'
import ERC20Abi from '../../abi/ERC20.json'
import { ethers } from 'ethers'
import { Button } from 'antd'

const cEthAddress = '0x859e9d8a4edadfEDb5A2fF311243af80F85A91b8'
const onBehalfOf = window.sessionStorage.getItem('address')
const WETHAddress = '0xc778417e063141139fce010982780140aa0cd5ab'
// const priceFeedAddress =  '0x7BBF806F69ea21Ea9B8Af061AD0C1c41913063A1'
// const cDaiAddress = '0xbc689667C13FB2a04f09272753760E38a95B998C'
// const daiAddress = '0x31F42841c2db5173425b5223809CF3A38FEde360'
const INFURA_URL_ROPSTEN = process.env.REACT_APP_INFURA_URL_ROPSTEN
const web3Provider = new ethers.providers.JsonRpcProvider(INFURA_URL_ROPSTEN)
const cETHProvider = new ethers.Contract(WETHAddress, ERC20Abi, web3Provider)

const Compound = () => {
  const provider = new ethers.providers.Web3Provider(window.ethereum)
  const signer = provider.getSigner()
  const cETHContract = new ethers.Contract(cEthAddress, cETHAbi, signer)
  console.log('CONTRACT', cETHContract)
  const useMint = async() => {
    const amount = 0.2
    // const WETH = new ethers.Contract(WETHAddress, ERC20Abi, web3Provider)
    const WDecimals = 18
    const amountInUnit256 = (amount * Math.pow(10, WDecimals)).toString()
    cETHContract.mint({ value: amountInUnit256 }).then(res => console.log(res))
    await cETHProvider.balanceOfUnderlying(onBehalfOf).then(res => {
      console.log(res)
    })
  }

  return (
    <Button onClick={useMint}>Mint</Button>
  )
}

export default Compound
