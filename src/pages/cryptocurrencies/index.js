import React from 'react'
import ServiceHeader from './ServiceHeader'
import ServiceTable from './ServiceTable'
import { Layout } from 'antd'
import './style.scss'
import MarketCoin from '../../components/market/MarketCoin'
import { Row, Col } from 'antd'

const { Content } = Layout
const Cryptocurrencies = () => {
  return (
    <Layout>
      <Content className='content'>
        <ServiceHeader/>
        <div className='market'>
          <Row gutter={16}>
            <Col span={6}>
              <MarketCoin
                title='Top Gainer'
              />
            </Col>
            <Col span={6}>
              <MarketCoin
                title='The most trading Volumn'
              />
            </Col>
            <Col span={6}>
              <MarketCoin
                title='Recently Added'
              />
            </Col>
            <Col span={6}>
              <MarketCoin
                title='Comming Soon'
              />
            </Col>
          </Row>
        </div>
        <ServiceTable/>
      </Content>
    </Layout>
  )
}
export default Cryptocurrencies
