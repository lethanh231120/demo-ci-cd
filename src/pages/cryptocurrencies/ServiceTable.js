import React, { useState, useEffect } from 'react'
import { Tabs } from 'antd'
import { Table, Popover } from 'antd'
import { EllipsisOutlined, CaretUpOutlined, CaretDownOutlined } from '@ant-design/icons'
import Chart from '../../components/chart/LineChart'
import { SKIP, LIMIT, DEFAULT_PAGE } from '../../constants/params'
import { get } from '../../api/coinPriceService'
import { BILLION } from '../../constants/TypeConstants'
import { useNavigate } from 'react-router-dom'
import { LazyLoadImage } from 'react-lazy-load-image-component'
import { useQuery } from '@tanstack/react-query'
import { FIVE_MINUTE } from '../../constants/params'

const { TabPane } = Tabs

const ServiceTable = (props) => {
  const { paramsForHomePage, isAction } = props
  const [data, setData] = useState()
  const [dataForShortTable, setDataForShortTable] = useState()
  const [params, setParams] = useState({
    page: DEFAULT_PAGE,
    skip: SKIP,
    limit: LIMIT
  })

  const [selectedRowKeys, setSelectedRowKeys] = useState([
    'rank',
    'coinName',
    'price',
    'marketCap',
    'high24h',
    // 'volume_24h',
    'priceGraph',
    'priceChange24h',
    'low24h',
    'percentChange24h'
  ])

  const navigate = useNavigate()

  const { data: listCoin, isLoading } = useQuery(
    ['listCoin', params],
    async() => {
      const dataBtc = await get(`price/info?skip=${params.skip}&limit=${params.limit}`)
      return dataBtc.data
    },
    {
      refetchInterval: FIVE_MINUTE
    }
  )
  useEffect(() => {
    listCoin && setData(listCoin?.coins)
  }, [listCoin])

  const { data: listShortCoin } = useQuery(
    ['listShortCoin', params],
    async() => {
      const data = await get(`price/info?skip=${paramsForHomePage.skip}&limit=${paramsForHomePage.limit}`)
      return data.data
    }
  )
  useEffect(() => {
    listShortCoin && setDataForShortTable(listShortCoin?.coins)
  }, [listShortCoin])

  const columnsPopover = [
    {
      title: 'Title',
      dataIndex: 'title',
      width: '85%',
      render: (_, record) => (<span style={{ color: '#A8ADB3' }}>{record.title}</span>)
    }
  ]

  const items = [
    {
      key: 'rank',
      title: '#'
    },
    {
      key: 'coinName',
      title: 'Name'
    },
    {
      key: 'high24h',
      title: '5m Change'
    },
    {
      key: 'low24h',
      title: '1h Change'
    },
    {
      key: 'percentChange24h',
      title: '2h Change'
    },
    {
      key: 'priceChange24h',
      title: 'Change (24h)'
    },
    // {
    //   key: 'priceChange7d',
    //   title: '7d Change'
    // },
    {
      key: 'price',
      title: 'Price'
    },
    {
      key: 'marketCap',
      title: 'Market Cap'
    },
    {
      key: 'volume_24h',
      title: 'Volumn 24h'
    },
    {
      key: 'priceGraph',
      title: 'Price Graph (7d)'
    }
  ]

  const onSelectChange = (newselectedRowKeys) => {
    setSelectedRowKeys(newselectedRowKeys)
  }

  const rowSelection = {
    selectedRowKeys,
    onChange: onSelectChange
  }

  const renderPrice = (record) => {
    let price
    if ((record?.price >= 0) && (record?.price < 1)) {
      price = record?.price.toFixed(5).replace(/\d(?=(\d{3})+\.)/g, '$&,')
    }
    if (record?.price >= 1) {
      price = record?.price.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')
    }
    return price
  }

  const columns = [
    {
      title: '#',
      sorter: (a, b) => a.rank - b.rank,
      width: '20px',
      hidden: !selectedRowKeys.includes('rank'),
      render: (_, record, index) => (<span style={{ color: '#A8ADB3' }}>{(params.page - 1) * params.limit + index + 1}</span>)
    },
    {
      title: <span style={{ textAlign: 'left !important' }}>Name</span>,
      className: 'table-name',
      width: '250px',
      hidden: !selectedRowKeys.includes('coinName'),
      sorter: (a, b) => a.coinName.localeCompare(b.coinName),
      render: (_, record) => (<div>
        <div className='table-icon-coin'>
          {record?.coinImage
            ? (
              <LazyLoadImage src={record.coinImage}/>
            ) : (
              <span className='table-icon-coin-logo'>{(record.symbol && record.symbol.slice(0, 3)).toUpperCase()}</span>
            )}
        </div>
        <div className='table-name-content'>
          <div className='table-name-text'>
            <span>
              {record?.coinName ? record?.coinName : ''}
            </span>
          </div>
          <div className='table-name-symbol'>
            <span>
              {record?.symbol ? record?.symbol.toUpperCase() : ''}
            </span>
          </div>
        </div>
      </div>)
    },
    {
      title: 'Price',
      dataIndex: 'price',
      width: '150px',
      sorter: (a, b) => a.price - b.price,
      hidden: !selectedRowKeys.includes('price'),
      render: (_, record) => (<span className='amount'>
        $ {renderPrice(record)}
      </span>)
    },
    {
      title: 'High 24H',
      width: '150px',
      hidden: !selectedRowKeys.includes('high24h'),
      render: (_, record) => (
        <div className='price-change'>
          $ {record?.high24h ? record?.high24h.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,') : 0}
        </div>
      ),
      sorter: (a, b) => a.high24h - b.high24h
    },
    {
      title: 'Low 24H',
      width: '150px',
      hidden: !selectedRowKeys.includes('low24h'),
      render: (_, record) => (
        <div className='price-change'>
          $ {record?.low24h ? record?.low24h.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,') : 0}
        </div>
      ),
      sorter: (a, b) => a.low24h - b.low24h
    },
    {
      title: 'Price Change 24H',
      sorter: (a, b) => a.priceChange24h - b.priceChange24h,
      width: '200px',
      hidden: !selectedRowKeys.includes('priceChange24h'),
      render: (_, record) => (
        <div className='price-change'
          style={{
            color: record.priceChange24h ? (record.priceChange24h >= 0 ? '#34b349' : '#ff4d4d') : '#34b349'
          }}
        >
          $ {record.priceChange24h ? parseFloat(record.priceChange24h >= 0 ? record.priceChange24h : record.priceChange24h.toString().slice(1)).toFixed(4).replace(/\d(?=(\d{3})+\.)/g, '$&,') : '0.00'}
        </div>
      )
    },
    {
      title: 'Percent Change 24H',
      width: '200px',
      hidden: !selectedRowKeys.includes('percentChange24h'),
      render: (_, record) => (
        <div className='price-change'
          style={{
            backgroundColor: record.priceChangePercentage24h ? (record.priceChangePercentage24h >= 0 ? 'rgba(52, 199, 89, 0.1)' : 'rgba(255,53,53,0.1)') : 'rgba(52, 199, 89, 0.1)',
            color: record.priceChangePercentage24h ? (record.priceChangePercentage24h >= 0 ? '#34b349' : '#ff4d4d') : '#34b349'
          }}
        >
          {record.priceChangePercentage24h ? (record.priceChangePercentage24h >= 0 ? <CaretUpOutlined/> : <CaretDownOutlined/>) : <CaretUpOutlined/>}
          {record.priceChangePercentage24h ? parseFloat(record.priceChangePercentage24h >= 0 ? record.priceChangePercentage24h : record.priceChangePercentage24h.toString().slice(1)).toFixed(2) : '0.00'} %
        </div>
      ),
      sorter: (a, b) => a.priceChangePercentage24h - b.priceChangePercentage24h
    },
    {
      title: 'Market Cap',
      sorter: (a, b) => a.marketCap - b.marketCap,
      width: '200px',
      defaultSortOrder: 'descend',
      hidden: !selectedRowKeys.includes('marketCap'),
      render: (_, record) => (<span style={{ color: '#A8ADB3' }}>
        $ {(record.marketCap * BILLION).toFixed(5).replace(/\d(?=(\d{3})+\.)/g, '$&,')} B
      </span>)
    },
    {
      title: 'Volumn 24h',
      sorter: (a, b) => a.volume_24h - b.volume_24h,
      width: '120px',
      hidden: !selectedRowKeys.includes('volume_24h'),
      render: (_, record) => (<span style={{ color: '#A8ADB3' }}>
        $ {record?.volumn ? (record?.volumn * BILLION).toFixed(5).replace(/\d(?=(\d{3})+\.)/g, '$&,') : 0} B
      </span>)
    },
    {
      title: 'Price Graph (7d)',
      dataIndex: 'priceGraph',
      width: '120px',
      className: 'table-graph',
      hidden: !selectedRowKeys.includes('priceGraph'),
      render: (_, record) => (<Chart record={record?.priceChart7d} change7d={record.percentChange7d}/>)
    },
    {
      title: <Popover
        placement='bottomRight'
        content={(<Table
          showHeader={false}
          scroll={{
            y: 260
          }}
          style={{ maxWidth: '290px' }}
          className='tableabc'
          rowSelection={rowSelection}
          pagination={false}
          columns={columnsPopover}
          dataSource={items}
        ></Table>)}
        trigger='click'
      >
        +
      </Popover>,
      className: 'table-plus',
      width: '20px',
      dataIndex: 'key',
      render: (_, record) => (
        <>
          {isAction && (
            <Popover
              placement='bottomRight'
              content={(<Table
                showHeader={false}
                scroll={{
                  y: 260
                }}
                style={{ maxWidth: '290px' }}
                className='tableabc'
                rowSelection={rowSelection}
                pagination={false}
                columns={columnsPopover}
                dataSource={items}
              ></Table>)}
              trigger='click'
            >
              <EllipsisOutlined className='table-row-item-icon' style={{ color: '#fff', fontSize: '20px' }}/>
            </Popover>

          )}
        </>
      )
    }
  ].filter(item => !item.hidden)

  const handleChangePage = (page, pageSize) => {
    setParams({
      ...params,
      page: page,
      skip: ((page - 1) * pageSize)
    })
  }

  const handleClickItem = (record) => {
    navigate(`../coins/${record.coinId}`, { state: { data: record }})
  }

  return (
    paramsForHomePage === undefined
      ? (
        <Tabs defaultActiveKey='1' style={{ padding: '40px 0' }}>
          <TabPane tab='Cryptocurrencies' key='1'>
            <Table
              loading={isLoading}
              columns={columns}
              dataSource={data && data}
              scroll={{ x: 'max-content' }}
              showSorterTooltip={false}
              rowKey='coinId'
              pagination={{
                total: listCoin?.totalCoins,
                defaultCurrent: params.page,
                defaultPageSize: params.limit,
                showSizeChanger: false,
                onChange: handleChangePage
              }}
              onRow={(record) => ({
                onClick: () => { handleClickItem(record) }
              })}
            />
          </TabPane>
          <TabPane tab='Exchanges' key='2'>
            Content of Tab Pane 2
          </TabPane>
          <TabPane tab='Favorites' key='3'>
            Content of Tab Pane 3
          </TabPane>
          <TabPane tab='DeFi' key='4'>
            Content of Tab Pane 3
          </TabPane>
          <TabPane tab='Heatmap' key='5'>
            Content of Tab Pane 3
          </TabPane>
        </Tabs>
      ) : (
        <Table
          columns={columns}
          dataSource={dataForShortTable && dataForShortTable}
          scroll={{ x: 'max-content' }}
          showSorterTooltip={false}
          rowKey='coinId'
          pagination={false}
          onRow={(record) => ({
            onClick: () => { handleClickItem(record) }
          })}
        />
      )
  )
}

export default ServiceTable
