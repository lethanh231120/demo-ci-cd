import React from 'react'
import { Row, Col, Typography, Layout } from 'antd'
import { CaretDownOutlined } from '@ant-design/icons'

const { Content } = Layout
const { Title, Text } = Typography

const ServiceHeader = () => {
  return (
    <>
      <Row>
        <Col span={12} offset={6}>
          <Typography>
            <Title className='cryptocurrencies-header-title'>
              Best Coin Price Tracker in the Market
            </Title>
            <Text className='cryptocurrencies-header-text'>
              With CoinStats, you can manage all your crypto assets from one interface.
            </Text>
            <Text className='cryptocurrencies-header-text'>
              The global crypto market cap is $979.1B a 2.37 % decrease over the last day.
            </Text>
          </Typography>
        </Col>
      </Row>
      <Content className='cryptocurrencies-content'>
        <Row gutter={20}>
          <Col span={8}>
            <div
              className='cryptocurrencies-content-item'
              style={{ backgroundColor: 'rgba(255,53,53,0.1)' }}
            >
              <Text className='cryptocurrencies-content-item-text'> Market Cap</Text>
              <div className='cryptocurrencies-content-item-info'>
                <p className='cryptocurrencies-content-item-num'>$987,987,987</p>
                <div
                  className='cryptocurrencies-content-item-percent'
                  style={{ backgroundColor: 'rgba(52, 199, 89, 0.1)', color: '#34b349' }}
                >
                  <CaretDownOutlined/>-2.31%
                </div>
              </div>
            </div>
          </Col>
          <Col span={8}>
            <div
              className='cryptocurrencies-content-item'
              style={{ backgroundColor: 'rgba(255,53,53,0.1)' }}
            >
              <Text className='cryptocurrencies-content-item-text'>Volumn 24h</Text>
              <div className='cryptocurrencies-content-item-info'>
                <p className='cryptocurrencies-content-item-num'>$987,987,987</p>
                <div
                  className='cryptocurrencies-content-item-percent'
                  style={{ backgroundColor: 'rgba(52, 199, 89, 0.1)', color: '#34b349' }}
                >
                  <CaretDownOutlined />+3.5%
                </div>
              </div>
            </div>
          </Col>
          <Col span={8}>
            <div
              className='cryptocurrencies-content-item'
              style={{ backgroundColor: 'rgba(255,53,53,0.1)' }}
            >
              <Text className='cryptocurrencies-content-item-text'>BTC Dominance</Text>
              <div className='cryptocurrencies-content-item-info'>
                <p className='cryptocurrencies-content-item-num'>$987,987,987</p>
                <div
                  className='cryptocurrencies-content-item-percent'
                  style={{ backgroundColor: 'rgba(52, 199, 89, 0.1)', color: '#34b349' }}
                >
                  <CaretDownOutlined/>-0.025%
                </div>
              </div>
            </div>
          </Col>
        </Row>
      </Content>
    </>
  )
}

export default ServiceHeader
